function validate(obj, validValue) {
	// required
	if ($(obj).attr('required') != null && $(obj).attr('required') != undefined && ($(obj).attr('required') == 'true' || $(obj).attr('required') == true)) {
		var value = $(obj).val();
		if (value == null || value == "" || value == undefined || value == validValue) {
			return 'null';
		}
	}
	
	if ($(obj).attr('name') == 'login_email' || $(obj).attr('name') == 'input_email' ) {
		if (!isValidEmail($(obj).val())) {
			return 'form';
		}
	}
	
	if ($(obj).attr('name') == 'input_phone') {
		if ($(obj).val() != null && $(obj).val() != '' && $(obj).val() != '--') {
			if (!isValidCellPhone($(obj).val())) {
				return 'form';
			}
		}
	}
	
	if ($(obj).attr('name') == 'buyStore') {
		if ($(obj).val() == "구입처를 입력하세요.") {
			return 'null';
		}
	}
	
	return 'success';
}

function isValidEmail(val) {
	var pattern = /^[_a-zA-Z0-9-\.]+@[\.a-zA-Z0-9-]+\.[a-zA-Z]+$/;
	return (pattern.test(val)) ? true : false;
}

function isValidCellPhone(val) {
	var pattern = /[01](0|1|6|7|8|9)[-](\d{4}|\d{3})[-]\d{4}$/g;
	return (pattern.test(val)) ? true : false;
}

// message
function validMessage(data) {
	var message = '';
	for (var i=0; i<data.length; i++) {
		var el = data[i];
		if (el.result == 'null') {
			message = el.name + '을(를) 입력하세요.';
			break;
		}
		else if (el.result == 'form') {
			message = el.name + ' 형식이 잘못되었습니다.\n다시 입력하세요.';
			break;
		}
	}
	return message;
}
