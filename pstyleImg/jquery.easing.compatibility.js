/*
 *
 * TERMS OF USE - EASING EQUATIONS
 * 
 * Open source under the BSD License. 
 * 
 * Copyright © 2001 Robert Penner
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 * 
 * Redistributions of source code must retain the above copyright notice, this list of 
 * conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice, this list 
 * of conditions and the following disclaimer in the documentation and/or other materials 
 * provided with the distribution.
 * 
 * Neither the name of the author nor the names of contributors may be used to endorse 
 * or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 *  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED 
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
 * OF THE POSSIBILITY OF SUCH DAMAGE. 
 *
 */
 
 
 /*
 * jQuery Easing Compatibility v1 - http://gsgd.co.uk/sandbox/jquery.easing.php
 *
 * Adds compatibility for applications that use the pre 1.2 easing names
 *
 * Copyright (c) 2007 George Smith
 * Licensed under the MIT License:
 *   http://www.opensource.org/licenses/mit-license.php
 */

jQuery.extend(jQuery.easing,{easeIn:function(x,t,b,c,d){return jQuery.easing.easeInQuad(x,t,b,c,d);},easeOut:function(x,t,b,c,d){return jQuery.easing.easeOutQuad(x,t,b,c,d);},easeInOut:function(x,t,b,c,d){return jQuery.easing.easeInOutQuad(x,t,b,c,d);},expoin:function(x,t,b,c,d){return jQuery.easing.easeInExpo(x,t,b,c,d);},expoout:function(x,t,b,c,d){return jQuery.easing.easeOutExpo(x,t,b,c,d);},expoinout:function(x,t,b,c,d){return jQuery.easing.easeInOutExpo(x,t,b,c,d);},bouncein:function(x,t,b,c,d){return jQuery.easing.easeInBounce(x,t,b,c,d);},bounceout:function(x,t,b,c,d){return jQuery.easing.easeOutBounce(x,t,b,c,d);},bounceinout:function(x,t,b,c,d){return jQuery.easing.easeInOutBounce(x,t,b,c,d);},elasin:function(x,t,b,c,d){return jQuery.easing.easeInElastic(x,t,b,c,d);},elasout:function(x,t,b,c,d){return jQuery.easing.easeOutElastic(x,t,b,c,d);},elasinout:function(x,t,b,c,d){return jQuery.easing.easeInOutElastic(x,t,b,c,d);},backin:function(x,t,b,c,d){return jQuery.easing.easeInBack(x,t,b,c,d);},backout:function(x,t,b,c,d){return jQuery.easing.easeOutBack(x,t,b,c,d);},backinout:function(x,t,b,c,d){return jQuery.easing.easeInOutBack(x,t,b,c,d);}});