$(document).ready(function() {
	$('#billBoard .hero').carouFredSel({
		width: 1080,
		height: 710,
		align: false,
		circular: true,
		infinite: false,
		items: 1,
		auto: {
			play: true,
			pauseDuration : 3000,
			easing: "easeInOutExpo",
			duration: 1000
		},
		prev	: {	
			button	: "#billBoardNav a.prev",
			key		: "left"
		},
		next	: { 
			button	: "#billBoardNav a.next",
			key		: "right",
			pauseOnHover : true
		},
		pagination : {
			items : 1,
			container : "#billBoardNav div.index",
			anchorBuilder : function(nr, item) {
				if(nr == 1) {
					return "<span class=indexCase><a href=#item0 class=thumb01>1st</a></span>";
				}
				else if(nr == 2) {
					return "<span class=indexCase><a href=#item1 class=thumb02>2nd</a></span>";
				}
				else if(nr == 3) {
					return "<span class=indexCase><a href=#item2 class=thumb03>3rd</a></span>";
				}
				else if(nr == 4) {
					return "<span class=indexCase><a href=#item3 class=thumb04>4th</a></span>";
				}
				else if(nr == 5) {
					return "<span class=indexCase><a href=#item4 class=thumb05>5th</a></span>";
				}
				else if(nr == 6) {
					return "<span class=indexCase><a href=#item5 class=thumb06>6th</a></span>";
				}
				else if(nr == 7) {
					return "<span class=indexCase><a href=#item6 class=thumb07>7th</a></span>";
				}
				else if(nr == 8) {
					return "<span class=indexCase><a href=#item7 class=thumb08>8th</a></span>";
				}
				else if(nr == 9) {
					return "<span class=indexCase><a href=#item8 class=thumb09>9th</a></span>";
				}
				else if(nr == 10) {
					return "<span class=indexCase><a href=#item9 class=thumb10>10th</a></span>";
				}
			}
		},
		scroll : {
			easing: "easeInOutExpo",
			duration: 1000,
			onBefore: function() {
				$("#billBoard .hero").trigger("currentPosition", function( pos ) {
					if (pos == "0") {
						$('#content_sdk #billBoardNav div.index').css('backgroundPosition','0 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(1) a').css('backgroundPosition','-14px 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(2) a').css('backgroundPosition','0 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(3) a').css('backgroundPosition','0 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(4) a').css('backgroundPosition','0 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(5) a').css('backgroundPosition','0 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(6) a').css('backgroundPosition','0 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(7) a').css('backgroundPosition','0 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(8) a').css('backgroundPosition','0 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(9) a').css('backgroundPosition','0 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(10) a').css('backgroundPosition','0 0');
						$('#content_sdk #billBoardNav div.bar span.gauge').animate({width:'0px'});
					}else if (pos == "1") {
						$('#content_sdk #billBoardNav div.index span:nth-child(1) a').css('backgroundPosition','-14px 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(2) a').css('backgroundPosition','-14px 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(3) a').css('backgroundPosition','0 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(4) a').css('backgroundPosition','0 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(5) a').css('backgroundPosition','0 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(6) a').css('backgroundPosition','0 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(7) a').css('backgroundPosition','0 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(8) a').css('backgroundPosition','0 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(9) a').css('backgroundPosition','0 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(10) a').css('backgroundPosition','0 0');
						$('#content_sdk #billBoardNav div.index').css('backgroundPosition','0 -26px');
						$('#content_sdk #billBoardNav div.bar span.gauge').animate({width:'60px'});
					}else if (pos == "2") {
						$('#content_sdk #billBoardNav div.index span:nth-child(1) a').css('backgroundPosition','-14px 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(2) a').css('backgroundPosition','-14px 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(3) a').css('backgroundPosition','-14px 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(4) a').css('backgroundPosition','0 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(5) a').css('backgroundPosition','0 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(6) a').css('backgroundPosition','0 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(7) a').css('backgroundPosition','0 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(8) a').css('backgroundPosition','0 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(9) a').css('backgroundPosition','0 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(10) a').css('backgroundPosition','0 0');
						$('#content_sdk #billBoardNav div.index').css('backgroundPosition','0 -52px');
						$('#content_sdk #billBoardNav div.bar span.gauge').animate({width:'120px'});
					}else if (pos == "3") {
						$('#content_sdk #billBoardNav div.index span:nth-child(1) a').css('backgroundPosition','-14px 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(2) a').css('backgroundPosition','-14px 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(3) a').css('backgroundPosition','-14px 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(4) a').css('backgroundPosition','-14px 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(5) a').css('backgroundPosition','0 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(6) a').css('backgroundPosition','0 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(7) a').css('backgroundPosition','0 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(8) a').css('backgroundPosition','0 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(9) a').css('backgroundPosition','0 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(10) a').css('backgroundPosition','0 0');
						$('#content_sdk #billBoardNav div.index').css('backgroundPosition','0 -78px');
						$('#content_sdk #billBoardNav div.bar span.gauge').animate({width:'180px'});
					}else if (pos == "4") {
						$('#content_sdk #billBoardNav div.index span:nth-child(1) a').css('backgroundPosition','-14px 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(2) a').css('backgroundPosition','-14px 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(3) a').css('backgroundPosition','-14px 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(4) a').css('backgroundPosition','-14px 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(5) a').css('backgroundPosition','-14px 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(6) a').css('backgroundPosition','0 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(7) a').css('backgroundPosition','0 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(8) a').css('backgroundPosition','0 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(9) a').css('backgroundPosition','0 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(10) a').css('backgroundPosition','0 0');
						$('#content_sdk #billBoardNav div.index').css('backgroundPosition','0 -104px');
						$('#content_sdk #billBoardNav div.bar span.gauge').animate({width:'240px'});
					}else if (pos == "5") {
						$('#content_sdk #billBoardNav div.index span:nth-child(1) a').css('backgroundPosition','-14px 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(2) a').css('backgroundPosition','-14px 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(3) a').css('backgroundPosition','-14px 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(4) a').css('backgroundPosition','-14px 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(5) a').css('backgroundPosition','-14px 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(6) a').css('backgroundPosition','-14px 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(7) a').css('backgroundPosition','0 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(8) a').css('backgroundPosition','0 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(9) a').css('backgroundPosition','0 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(10) a').css('backgroundPosition','0 0');
						$('#content_sdk #billBoardNav div.index').css('backgroundPosition','0 -130px');
						$('#content_sdk #billBoardNav div.bar span.gauge').animate({width:'300px'});
					}else if (pos == "6") {
						$('#content_sdk #billBoardNav div.index span:nth-child(1) a').css('backgroundPosition','-14px 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(2) a').css('backgroundPosition','-14px 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(3) a').css('backgroundPosition','-14px 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(4) a').css('backgroundPosition','-14px 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(5) a').css('backgroundPosition','-14px 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(6) a').css('backgroundPosition','-14px 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(7) a').css('backgroundPosition','-14px 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(8) a').css('backgroundPosition','0 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(9) a').css('backgroundPosition','0 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(10) a').css('backgroundPosition','0 0');
						$('#content_sdk #billBoardNav div.index').css('backgroundPosition','0 -156px');
						$('#content_sdk #billBoardNav div.bar span.gauge').animate({width:'360px'});
					}else if (pos == "7") {
						$('#content_sdk #billBoardNav div.index span:nth-child(1) a').css('backgroundPosition','-14px 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(2) a').css('backgroundPosition','-14px 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(3) a').css('backgroundPosition','-14px 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(4) a').css('backgroundPosition','-14px 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(5) a').css('backgroundPosition','-14px 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(6) a').css('backgroundPosition','-14px 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(7) a').css('backgroundPosition','-14px 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(8) a').css('backgroundPosition','-14px 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(9) a').css('backgroundPosition','0 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(10) a').css('backgroundPosition','0 0');
						$('#content_sdk #billBoardNav div.index').css('backgroundPosition','0 -182px');
						$('#content_sdk #billBoardNav div.bar span.gauge').animate({width:'420px'});
					}else if (pos == "8") {
						$('#content_sdk #billBoardNav div.index span:nth-child(1) a').css('backgroundPosition','-14px 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(2) a').css('backgroundPosition','-14px 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(3) a').css('backgroundPosition','-14px 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(4) a').css('backgroundPosition','-14px 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(5) a').css('backgroundPosition','-14px 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(6) a').css('backgroundPosition','-14px 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(7) a').css('backgroundPosition','-14px 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(8) a').css('backgroundPosition','-14px 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(9) a').css('backgroundPosition','-14px 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(10) a').css('backgroundPosition','0 0');
						$('#content_sdk #billBoardNav div.index').css('backgroundPosition','0 -208px');
						$('#content_sdk #billBoardNav div.bar span.gauge').animate({width:'480px'});
					}else if (pos == "9") {
						$('#content_sdk #billBoardNav div.index span:nth-child(1) a').css('backgroundPosition','-14px 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(2) a').css('backgroundPosition','-14px 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(3) a').css('backgroundPosition','-14px 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(4) a').css('backgroundPosition','-14px 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(5) a').css('backgroundPosition','-14px 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(6) a').css('backgroundPosition','-14px 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(7) a').css('backgroundPosition','-14px 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(8) a').css('backgroundPosition','-14px 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(9) a').css('backgroundPosition','-14px 0');
						$('#content_sdk #billBoardNav div.index span:nth-child(10) a').css('backgroundPosition','-14px 0');
						$('#content_sdk #billBoardNav div.index').css('backgroundPosition','0 -234px');
						$('#content_sdk #billBoardNav div.bar span.gauge').animate({width:'540px'});
					}
				});
			}
		}

	});
	$('#content_sdk .section.product ul.list li').hover(
		function(){
			$(this).children('h5').stop(true,false).css({display:'block'});
		},
		function(){
			$(this).children('h5').stop(true,false).css({display:'none'});
	});
	$('#content_sdk .section.team .list').carouFredSel({
		width: 990,
		height: 300,
		align: false,
		circular: true,
		infinite: false,
		items: 10,
		auto: {
			play: true,
			pauseDuration : 3000,
			easing: "easeInOutExpo",
			duration: 1000
		},
		prev	: {	
			button	: "#content_sdk .section.team a.prev",
			key		: "left"
		},
		next	: { 
			button	: "#content_sdk .section.team a.next",
			key		: "right",
			pauseOnHover : true
		},
		pagination : {
			items : 6,
			container : "#content_sdk .section.team span.index"
		},
		scroll : {
			easing: "easeInOutExpo",
			duration: 1000
		}

	});
	$('#billBoardNav div.index span:last-child').css({marginRight:'0'});
	var gWidth = $('#billBoardNav div.indexWrap').width();
	$('#billBoardNav div.bar').css({width:(gWidth-4)});
	
	$('#billBoardNav div.index a.thumb01').hover(function(){
		$('#content_sdk #billBoardNav div.indexWrap div.thumbIndex .thumbsImg1').css({display:'block'});
	},function(){
		$('#content_sdk #billBoardNav div.indexWrap div.thumbIndex .thumbsImg1').css({display:'none'});
	});
	$('#billBoardNav div.index a.thumb02').hover(function(){
		$('#content_sdk #billBoardNav div.indexWrap div.thumbIndex .thumbsImg2').css({display:'block'});
	},function(){
		$('#content_sdk #billBoardNav div.indexWrap div.thumbIndex .thumbsImg2').css({display:'none'});
	});
	$('#billBoardNav div.index a.thumb03').hover(function(){
		$('#content_sdk #billBoardNav div.indexWrap div.thumbIndex .thumbsImg3').css({display:'block'});
	},function(){
		$('#content_sdk #billBoardNav div.indexWrap div.thumbIndex .thumbsImg3').css({display:'none'});
	});
	$('#billBoardNav div.index a.thumb04').hover(function(){
		$('#content_sdk #billBoardNav div.indexWrap div.thumbIndex .thumbsImg4').css({display:'block'});
	},function(){
		$('#content_sdk #billBoardNav div.indexWrap div.thumbIndex .thumbsImg4').css({display:'none'});
	});
	$('#billBoardNav div.index a.thumb05').hover(function(){
		$('#content_sdk #billBoardNav div.indexWrap div.thumbIndex .thumbsImg5').css({display:'block'});
	},function(){
		$('#content_sdk #billBoardNav div.indexWrap div.thumbIndex .thumbsImg5').css({display:'none'});
	});
	$('#billBoardNav div.index a.thumb06').hover(function(){
		$('#content_sdk #billBoardNav div.indexWrap div.thumbIndex .thumbsImg6').css({display:'block'});
	},function(){
		$('#content_sdk #billBoardNav div.indexWrap div.thumbIndex .thumbsImg6').css({display:'none'});
	});
	$('#billBoardNav div.index a.thumb07').hover(function(){
		$('#content_sdk #billBoardNav div.indexWrap div.thumbIndex .thumbsImg7').css({display:'block'});
	},function(){
		$('#content_sdk #billBoardNav div.indexWrap div.thumbIndex .thumbsImg7').css({display:'none'});
	});
	$('#billBoardNav div.index a.thumb08').hover(function(){
		$('#content_sdk #billBoardNav div.indexWrap div.thumbIndex .thumbsImg8').css({display:'block'});
	},function(){
		$('#content_sdk #billBoardNav div.indexWrap div.thumbIndex .thumbsImg8').css({display:'none'});
	});
	$('#billBoardNav div.index a.thumb09').hover(function(){
		$('#content_sdk #billBoardNav div.indexWrap div.thumbIndex .thumbsImg9').css({display:'block'});
	},function(){
		$('#content_sdk #billBoardNav div.indexWrap div.thumbIndex .thumbsImg9').css({display:'none'});
	});
	$('#billBoardNav div.index a.thumb10').hover(function(){
		$('#content_sdk #billBoardNav div.indexWrap div.thumbIndex .thumbsImg10').css({display:'block'});
	},function(){
		$('#content_sdk #billBoardNav div.indexWrap div.thumbIndex .thumbsImg10').css({display:'none'});
	});
	
			

	/*fix ie*/
	if ($.browser.msie) {
		$('a').attr('onFocus','this.blur();');
	};
	if ($.browser.msie  && parseInt($.browser.version, 10) <= 8) {
		$('#newsList #content .section ul.list li:last-child').css("border","0 none");
		$('#media .section.player, #newsList .section ul.list, #newsView .section div.article, a.btnFunc').append('<span class=rndLT></span>','<span class=rndRT></span>','<span class=rndLB></span>','<span class=rndRB></span>');
		$('#aside ul li:nth-child(2n)').css("margin-right","4px");
		$('#aside ul li:last-child').css("margin-right","0");
		$('#team .section ul.list li').append('<span class=shadow-right></span>');
		$('#team .section ul.list li').append('<span class=shadow-bottom></span>');
		$('#media .section.sort ul.list li').append('<span class=shadow-right></span>');
		$('#media .section.sort ul.list li').append('<span class=shadow-bottom></span>');
		$('#content_sdk .section.product ul.list li:nth-child(7)').css("border","0 none");
		$('#content_sdk .section.product ul.list li:nth-child(8)').css("border","0 none");
		$('#newsList #content .section ul.list li h3 a').css("color","#90711e");
		$('#content_sdk .section.news ul.list a').css("color","#FFF");
		$('#content_sdk .section.news ul.list li p a').css("color","#4e3710");	
		$('#newsView .section .article div.pageMover').css("width","570px");
		$('#newsView .section .article div.functions div.pageMover').css("width","470px");
		$('#newsView .section .article div.pageMover a').css("cursor","pointer");
	};
	
		
});