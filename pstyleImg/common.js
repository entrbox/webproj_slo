function searchRegion(obj, selectRegion){
	var gugun_arr = new Array();
	gugun_arr['서울'] = new Array();
	gugun_arr['인천'] = new Array();
	gugun_arr['대전'] = new Array();
	gugun_arr['대구'] = new Array();
	gugun_arr['광주'] = new Array();
	gugun_arr['울산'] = new Array();
	gugun_arr['부산'] = new Array();
	gugun_arr['경기'] = new Array();
	gugun_arr['강원'] = new Array();
	gugun_arr['충남'] = new Array();
	gugun_arr['충북'] = new Array();
	gugun_arr['전북'] = new Array();
	gugun_arr['전남'] = new Array();
	gugun_arr['경남'] = new Array();
	gugun_arr['경북'] = new Array();
	gugun_arr['제주'] = new Array();

	gugun_arr['서울'][0] = "강남구";
	gugun_arr['서울'][1] = "강동구";
	gugun_arr['서울'][2] = "강북구";
	gugun_arr['서울'][3] = "강서구";
	gugun_arr['서울'][4] = "관악구";
	gugun_arr['서울'][5] = "광진구";
	gugun_arr['서울'][6] = "구로구";
	gugun_arr['서울'][7] = "금천구";
	gugun_arr['서울'][8] = "노원구";
	gugun_arr['서울'][9] = "도봉구";
	gugun_arr['서울'][10] = "동대문구";
	gugun_arr['서울'][11] = "동작구";
	gugun_arr['서울'][12] = "마포구";
	gugun_arr['서울'][13] = "서대문구";
	gugun_arr['서울'][14] = "서초구";
	gugun_arr['서울'][15] = "성동구";
	gugun_arr['서울'][16] = "성북구";
	gugun_arr['서울'][17] = "송파구";
	gugun_arr['서울'][18] = "양천구";
	gugun_arr['서울'][19] = "영등포구";
	gugun_arr['서울'][20] = "용산구";
	gugun_arr['서울'][21] = "은평구";
	gugun_arr['서울'][22] = "종로구";
	gugun_arr['서울'][23] = "중구";
	gugun_arr['서울'][24] = "중랑구";

	gugun_arr['인천'][0] = "강화군";
	gugun_arr['인천'][1] = "계양구";
	gugun_arr['인천'][2] = "남구";
	gugun_arr['인천'][3] = "남동구";
	gugun_arr['인천'][4] = "동구";
	gugun_arr['인천'][5] = "부평구";
	gugun_arr['인천'][6] = "서구";
	gugun_arr['인천'][7] = "연수구";
	gugun_arr['인천'][8] = "옹진군";
	gugun_arr['인천'][9] = "중구";

	gugun_arr['대전'][0] = "대덕구";
	gugun_arr['대전'][1] = "동구";
	gugun_arr['대전'][2] = "서구";
	gugun_arr['대전'][3] = "유성구";
	gugun_arr['대전'][4] = "중구";

	gugun_arr['대구'][0] = "남구";
	gugun_arr['대구'][1] = "달서구";
	gugun_arr['대구'][2] = "달성군";
	gugun_arr['대구'][3] = "동구";
	gugun_arr['대구'][4] = "북구";
	gugun_arr['대구'][5] = "서구";
	gugun_arr['대구'][6] = "수성구";
	gugun_arr['대구'][7] = "중구";

	gugun_arr['광주'][0] = "광산구";
	gugun_arr['광주'][1] = "남구";
	gugun_arr['광주'][2] = "동구";
	gugun_arr['광주'][3] = "북구";
	gugun_arr['광주'][4] = "서구";

	gugun_arr['울산'][0] = "남구";
	gugun_arr['울산'][1] = "동구";
	gugun_arr['울산'][2] = "북구";
	gugun_arr['울산'][3] = "울주군";
	gugun_arr['울산'][4] = "중구";

	gugun_arr['부산'][0] = "강서구";
	gugun_arr['부산'][1] = "금정구";
	gugun_arr['부산'][2] = "기장군";
	gugun_arr['부산'][3] = "남구";
	gugun_arr['부산'][4] = "동구";
	gugun_arr['부산'][5] = "동래구";
	gugun_arr['부산'][6] = "부산진구";
	gugun_arr['부산'][7] = "북구";
	gugun_arr['부산'][8] = "사상구";
	gugun_arr['부산'][9] = "사하구";
	gugun_arr['부산'][10] = "서구";
	gugun_arr['부산'][11] = "수영구";
	gugun_arr['부산'][12] = "연제구";
	gugun_arr['부산'][13] = "영도구";
	gugun_arr['부산'][14] = "중구";
	gugun_arr['부산'][15] = "해운대구";

	gugun_arr['경기'][0] = "가평군";
	gugun_arr['경기'][1] = "고양시 덕양구";
	gugun_arr['경기'][2] = "고양시 일산동구";
	gugun_arr['경기'][3] = "고양시 일산서구";
	gugun_arr['경기'][4] = "과천시";
	gugun_arr['경기'][5] = "광명시";
	gugun_arr['경기'][6] = "광주시";
	gugun_arr['경기'][7] = "구리시";
	gugun_arr['경기'][8] = "군포시";
	gugun_arr['경기'][9] = "김포시";
	gugun_arr['경기'][10] = "남양주시";
	gugun_arr['경기'][11] = "동두천시";
	gugun_arr['경기'][12] = "부천시 소사구";
	gugun_arr['경기'][13] = "부천시 오정구";
	gugun_arr['경기'][14] = "부천시 원미구";
	gugun_arr['경기'][15] = "성남시 분당구";
	gugun_arr['경기'][16] = "성남시 수정구";
	gugun_arr['경기'][17] = "성남시 중원구";
	gugun_arr['경기'][18] = "수원시 권선구";
	gugun_arr['경기'][19] = "수원시 영통구";
	gugun_arr['경기'][20] = "수원시 장안구";
	gugun_arr['경기'][21] = "수원시 팔달구";
	gugun_arr['경기'][22] = "시흥시";
	gugun_arr['경기'][23] = "안산시 단원구";
	gugun_arr['경기'][24] = "안산시 상록구";
	gugun_arr['경기'][25] = "안성시";
	gugun_arr['경기'][26] = "안양시 동안구";
	gugun_arr['경기'][27] = "안양시 만안구";
	gugun_arr['경기'][28] = "양주시";
	gugun_arr['경기'][29] = "양평군";
	gugun_arr['경기'][30] = "여주군";
	gugun_arr['경기'][31] = "연천군";
	gugun_arr['경기'][32] = "오산시";
	gugun_arr['경기'][33] = "용인시 기흥구";
	gugun_arr['경기'][34] = "용인시 수지구";
	gugun_arr['경기'][35] = "용인시 처인구";
	gugun_arr['경기'][36] = "의왕시";
	gugun_arr['경기'][37] = "의정부시";
	gugun_arr['경기'][38] = "이천시";
	gugun_arr['경기'][39] = "파주시";
	gugun_arr['경기'][40] = "평택시";
	gugun_arr['경기'][41] = "포천시";
	gugun_arr['경기'][42] = "하남시";
	gugun_arr['경기'][43] = "화성시";

	gugun_arr['강원'][0] = "강릉시";
	gugun_arr['강원'][1] = "고성군";
	gugun_arr['강원'][2] = "동해시";
	gugun_arr['강원'][3] = "삼척시";
	gugun_arr['강원'][4] = "속초시";
	gugun_arr['강원'][5] = "양구군";
	gugun_arr['강원'][6] = "양양군";
	gugun_arr['강원'][7] = "영월군";
	gugun_arr['강원'][8] = "원주시";
	gugun_arr['강원'][9] = "인제군";
	gugun_arr['강원'][10] = "정선군";
	gugun_arr['강원'][11] = "철원군";
	gugun_arr['강원'][12] = "춘천시";
	gugun_arr['강원'][13] = "태백시";
	gugun_arr['강원'][14] = "평창군";
	gugun_arr['강원'][15] = "홍천군";
	gugun_arr['강원'][16] = "화천군";
	gugun_arr['강원'][17] = "횡성군";

	gugun_arr['충남'][0] = "계룡시";
	gugun_arr['충남'][1] = "공주시";
	gugun_arr['충남'][2] = "금산군";
	gugun_arr['충남'][3] = "논산시";
	gugun_arr['충남'][4] = "당진군";
	gugun_arr['충남'][5] = "보령시";
	gugun_arr['충남'][6] = "부여군";
	gugun_arr['충남'][7] = "서산시";
	gugun_arr['충남'][8] = "서천군";
	gugun_arr['충남'][9] = "아산시";
	gugun_arr['충남'][10] = "연기군";
	gugun_arr['충남'][11] = "예산군";
	gugun_arr['충남'][12] = "천안시 동남구";
	gugun_arr['충남'][13] = "천안시 서북구";
	gugun_arr['충남'][14] = "청양군";
	gugun_arr['충남'][15] = "태안군";
	gugun_arr['충남'][16] = "홍성군";

	gugun_arr['충북'][0] = "괴산군";
	gugun_arr['충북'][1] = "단양군";
	gugun_arr['충북'][2] = "보은군";
	gugun_arr['충북'][3] = "영동군";
	gugun_arr['충북'][4] = "옥천군";
	gugun_arr['충북'][5] = "음성군";
	gugun_arr['충북'][6] = "제천시";
	gugun_arr['충북'][7] = "증평군";
	gugun_arr['충북'][8] = "진천군";
	gugun_arr['충북'][9] = "청원군";
	gugun_arr['충북'][10] = "청주시 상당구";
	gugun_arr['충북'][11] = "청주시 흥덕구";
	gugun_arr['충북'][12] = "충주시";

	gugun_arr['전북'][0] = "고창군";
	gugun_arr['전북'][1] = "군산시";
	gugun_arr['전북'][2] = "김제시";
	gugun_arr['전북'][3] = "남원시";
	gugun_arr['전북'][4] = "무주군";
	gugun_arr['전북'][5] = "부안군";
	gugun_arr['전북'][6] = "순창군";
	gugun_arr['전북'][7] = "완주군";
	gugun_arr['전북'][8] = "익산시";
	gugun_arr['전북'][9] = "임실군";
	gugun_arr['전북'][10] = "장수군";
	gugun_arr['전북'][11] = "전주시 덕진구";
	gugun_arr['전북'][12] = "전주시 완산구";
	gugun_arr['전북'][13] = "정읍시";
	gugun_arr['전북'][14] = "진안군";

	gugun_arr['전남'][0] = "강진군";
	gugun_arr['전남'][1] = "고흥군";
	gugun_arr['전남'][2] = "곡성군";
	gugun_arr['전남'][3] = "광양시";
	gugun_arr['전남'][4] = "구례군";
	gugun_arr['전남'][5] = "나주시";
	gugun_arr['전남'][6] = "담양군";
	gugun_arr['전남'][7] = "목포시";
	gugun_arr['전남'][8] = "무안군";
	gugun_arr['전남'][9] = "보성군";
	gugun_arr['전남'][10] = "순천시";
	gugun_arr['전남'][11] = "신안군";
	gugun_arr['전남'][12] = "여수시";
	gugun_arr['전남'][13] = "영광군";
	gugun_arr['전남'][14] = "영암군";
	gugun_arr['전남'][15] = "완도군";
	gugun_arr['전남'][16] = "장성군";
	gugun_arr['전남'][17] = "장흥군";
	gugun_arr['전남'][18] = "진도군";
	gugun_arr['전남'][19] = "함평군";
	gugun_arr['전남'][20] = "해남군";
	gugun_arr['전남'][21] = "화순군";

	gugun_arr['경남'][0] = "거제시";
	gugun_arr['경남'][1] = "거창군";
	gugun_arr['경남'][2] = "고성군";
	gugun_arr['경남'][3] = "김해시";
	gugun_arr['경남'][4] = "남해군";
	gugun_arr['경남'][5] = "마산시";
	gugun_arr['경남'][6] = "밀양시";
	gugun_arr['경남'][7] = "사천시";
	gugun_arr['경남'][8] = "산청군";
	gugun_arr['경남'][9] = "양산시";
	gugun_arr['경남'][10] = "의령군";
	gugun_arr['경남'][11] = "진주시";
	gugun_arr['경남'][12] = "진해시";
	gugun_arr['경남'][13] = "창녕군";
	gugun_arr['경남'][14] = "창원시";
	gugun_arr['경남'][15] = "통영시";
	gugun_arr['경남'][16] = "하동군";
	gugun_arr['경남'][17] = "함안군";
	gugun_arr['경남'][18] = "함양군";
	gugun_arr['경남'][19] = "합천군";

	gugun_arr['경남'][0] = "경산시";
	gugun_arr['경남'][1] = "경주시";
	gugun_arr['경남'][2] = "고령군";
	gugun_arr['경남'][3] = "구미시";
	gugun_arr['경남'][4] = "군위군";
	gugun_arr['경남'][5] = "김천시";
	gugun_arr['경남'][6] = "문경시";
	gugun_arr['경남'][7] = "봉화군";
	gugun_arr['경남'][8] = "상주시";
	gugun_arr['경남'][9] = "성주군";
	gugun_arr['경남'][10] = "안동시";
	gugun_arr['경남'][11] = "영덕군";
	gugun_arr['경남'][12] = "영양군";
	gugun_arr['경남'][13] = "영주시";
	gugun_arr['경남'][14] = "영천시";
	gugun_arr['경남'][15] = "예천군";
	gugun_arr['경남'][16] = "울릉군";
	gugun_arr['경남'][17] = "울진군";
	gugun_arr['경남'][18] = "의성군";
	gugun_arr['경남'][19] = "청도군";
	gugun_arr['경남'][20] = "청송군";
	gugun_arr['경남'][21] = "칠곡군";
	gugun_arr['경남'][22] = "포항시 남구";
	gugun_arr['경남'][23] = "포항시 북구";

	gugun_arr['제주'][0] = "서귀포시";
	gugun_arr['제주'][1] = "제주시";

	var searchRegion2 = document.getElementById("searchRegion2");
	searchRegion2.options.length = 0;
	thisOption = document.createElement("OPTION");
	thisOption.text = '지역선택';
	thisOption.value = '';
	searchRegion2.options.add(thisOption);
	thisOption = document.createElement("OPTION");
	thisOption.text = '-------------------';
	thisOption.value = '';
	searchRegion2.options.add(thisOption);

	if(obj.value){
		for(i=0;i<gugun_arr[obj.value].length;i++){
			thisOption = document.createElement("OPTION");
			thisOption.text = gugun_arr[obj.value][i];
			thisOption.value = gugun_arr[obj.value][i];
			if (thisOption.value == selectRegion) thisOption.selected=true;
			searchRegion2.options.add(thisOption);
		}
	}

}

function login_click(id){
	if(id)
		$('input[name=login_email]').val(id);

	$("#loginmenu-open-menu").click();
}

document.write(
    "<div onclick='javascript:pdt_view_link()' onmouseout=\"javascript:alt('','','off','','','','','')\" id=alt_div style=\"" +
	"cursor:pointer;" +
	"padding:20px 15px;" +
    "border:0px solid #FF7200;" +
    "background: url(./images/product/pdt_onbg.gif) left top no-repeat;" +
    "position:absolute;" +
    "left:0px;" +
    "top:0px;" +
    "display:none;" +
    "z-index:1;" +
    "width:164px;" +
    "height:212px;" +
    "\"></div>"

	

);

function findPosX(obj)
  {
    var curleft = 0;
    if(obj.offsetParent)
        while(1) 
        {
          curleft += obj.offsetLeft;
          if(!obj.offsetParent)
            break;
          obj = obj.offsetParent;
        }
    else if(obj.x)
        curleft += obj.x;
    return curleft;
  }

  function findPosY(obj)
  {
    var curtop = 0;
    if(obj.offsetParent)
        while(1)
        {
          curtop += obj.offsetTop;
          if(!obj.offsetParent)
            break;
          obj = obj.offsetParent;
        }
    else if(obj.y)
        curtop += obj.y;
    return curtop;
  }

function change_opacity(){
	setInterval("fade_sdk()",20);

}
function fade_sdk(){
	var o_val = document.getElementById("alt_div").filters.alpha.opacity;
	if(o_val < 100)
		document.getElementById("alt_div").filters.alpha.opacity  += 10;
}

var imageSize;
var flag_golf;
var link_val;
var ajaxSeq;
var ajaxCate;

function alt(msg,obj,onoff,link,tag,golf,aSeq,aCate){
	flag_golf = golf;
	link_val = link;
	ajaxSeq = aSeq;
	ajaxCate = aCate;

	var html;
	checkIE();
	var noBg = "./images/product/product_nobg_puma.png";

	if(onoff == "on"){
		html = "<img style='position:absolute;left:-2px;top:-3px;width: 100%;height: 100%;z-index: 10;' src='./images/common/empty_btn.png'/><table>";
		html = html + '<tr>';
		html = html + '<td>';
		if(golf=="on")
			html = html + "<img width=170 height=170 src='" + msg + "' onerror='this.src=\""+noBg+"\"' >";
		else
			html = html + "<img width=170 height=170 src='" + msg + "' onerror='this.src=\""+noBg+"\"' >";

		if(tag == 1)
			html = html + "<img src='./images/product/tag_new_on.png'  style='position:absolute;top:8px;left:20px;' />";
		else if(tag == 2)
			html = html + "<img src='./images/product/tag_best_on.png'  style='position:absolute;top:8px;left:20px;' />";
		else if(tag == 3)
			html = html + "<img src='./images/product/tag_sale_on.png'  style='position:absolute;top:8px;left:20px;' />";

		html = html + '</td>';
		html = html + '</tr>';
		html = html + '</table>';

		_div = document.getElementById("alt_div");
		_div.innerHTML = html;
		imageSize = getImageSize(msg);

		//document.getElementById("alt_div").style.pixelLeft = findPosX(obj)-15;
		//document.getElementById("alt_div").style.pixelTop = findPosY(obj)-20;
		$("#alt_div").css({left:findPosX(obj)-15, top:findPosY(obj)-20});

		
	//	document.getElementById("alt_div").style.visibility = "visible";
		//change_opacity();
		jQuery("#alt_div").fadeIn(500);

	}else{
		_div = document.getElementById("alt_div");
		_div.innerHTML = html;
		document.getElementById("alt_div").style.display = "none";

	}
}

function pdt_view_link(){
	//alert(link_val);
	link_val2 = link_val;
	if(flag_golf == "on"){
		SBoxOpenUrl(780, 550, link_val2 , true, false);
	}else{
		updateHitCnt(ajaxSeq, "c", ajaxCate, "");
		window.open(link_val2);
	}
}

function checkIE(){
	if(navigator.appName == "Microsoft Internet Explorer"){
		isIE = true;
	} else {
		isIE = false;
	}
}

function getImageSize(src) {
	testImg = new Image();
	testImg.src = src;
	return {width:testImg.width, height:testImg.height};
}


SBoxOpenUrl = function(w, h, url, close, modal){
	Shadowbox.open({
		  player		: 'iframe'
		, content	: url
		, width		: w
		, height		: h
		, options		:{
							displayNav	: close
							,modal			: modal
						}
	});
	return false;
};

function imgCbox(N, tabstop, alink)
{
       var objs, cboxes, Img, Span, A;

       objs = document.getElementsByTagName("INPUT");
       if (N == undefined) return false;
       if (tabstop == undefined) tabstop = true;

       for (var i=0; i < objs.length; i++) {
              if (objs[i].type != "checkbox" || objs[i].id != N) continue;
              
              if (imgCbox.Objs[N] == undefined) {
                     imgCbox.Objs[N] = [];
                     imgCbox.Imgs[N] = [];
                     imgCbox.ImgObjs[N] = [];
              }
              
              var len = imgCbox.Objs[N].length;
              imgCbox.Objs[N][len] = objs[i];
              imgCbox.Imgs[N][len] = {};

              // for image cache
              (Img = new Image()).src = objs[i].getAttribute("onsrc");
              imgCbox.Imgs[N][len]["on"] = Img;

              (Img = new Image()).src = objs[i].getAttribute("offsrc");
              imgCbox.Imgs[N][len]["off"] = Img;

              // image element
              Img = document.createElement("IMG");
              Img.src = objs[i].checked?objs[i].getAttribute("onsrc"):objs[i].getAttribute("offsrc");
              Img.style.borderWidth = "0px";
			  Img.style.marginBottom = "-3px";
			  Img.style.marginRight = "12px";
              Img.onclick = new Function("imgCbox.onclick('"+N+"','"+len+"')");
              imgCbox.ImgObjs[N][len] = Img;

              // anchor element for tab stop
              A = document.createElement("A");
              if (tabstop) {
                     A.href = "javascript:"+alink+";";
                     A.onkeypress = new Function("evt", "if(evt==undefined)evt=event;if(evt.keyCode==13){ imgCbox.onclick('"+N+"','"+len+"'); }");
              }
              A.style.borderWidth = "0px";
              A.appendChild(Img);

              // insert object
              Span = objs[i].parentNode;
              Span.style.display = "none";
              Span.parentNode.insertBefore(A, Span);
       }
}
imgCbox.onclick = function(N, idx) {
       var C = imgCbox.Objs[N][idx];
       var I = imgCbox.ImgObjs[N][idx];

       C.checked = !C.checked;
       I.src = imgCbox.Imgs[N][idx][C.checked?"on":"off"].src;
       
       // fire event
       if (C.onclick != undefined || C.onclick != null) C.onclick();

}
imgCbox.Objs = {};
imgCbox.Imgs = {};
imgCbox.ImgObjs = {};


function imgRbox(N, tabstop, alink)
{
       var objs, cboxes, Img, Span, A, totcnt=0;

       objs = document.getElementsByTagName("INPUT");
       if (N == undefined) return false;
       if (tabstop == undefined) tabstop = true;

	   for (var i=0; i < objs.length; i++) {
              if (objs[i].type != "radio" || objs[i].id != N) continue;
			  totcnt++;
	   }

       for (var i=0; i < objs.length; i++) {
              if (objs[i].type != "radio" || objs[i].id != N) continue;
              
              if (imgRbox.Objs[N] == undefined) {
                     imgRbox.Objs[N] = [];
                     imgRbox.Imgs[N] = [];
                     imgRbox.ImgObjs[N] = [];
              }
              
              var len = imgRbox.Objs[N].length;
              imgRbox.Objs[N][len] = objs[i];
              imgRbox.Imgs[N][len] = {};

              // for image cache
              (Img = new Image()).src = objs[i].getAttribute("onsrc");
              imgRbox.Imgs[N][len]["on"] = Img;

              (Img = new Image()).src = objs[i].getAttribute("offsrc");
              imgRbox.Imgs[N][len]["off"] = Img;

              // image element
              Img = document.createElement("IMG");
              Img.src = objs[i].checked?objs[i].getAttribute("onsrc"):objs[i].getAttribute("offsrc");
              Img.style.borderWidth = "0px";
			  Img.style.marginBottom = "-3px";
			  Img.style.marginRight = "12px";
              Img.onclick = new Function("imgRbox.onclick('"+N+"','"+len+"','"+totcnt+"')");
              imgRbox.ImgObjs[N][len] = Img;

              // anchor element for tab stop
              A = document.createElement("A");
              if (tabstop) {
                     A.href = "javascript:"+alink+";";
                     A.onkeypress = new Function("evt", "if(evt==undefined)evt=event;if(evt.keyCode==13){ imgRbox.onclick('"+N+"','"+len+"'); }");
              }
              A.style.borderWidth = "0px";
              A.appendChild(Img);

              // insert object
              Span = objs[i].parentNode;
              Span.style.display = "none";
              Span.parentNode.insertBefore(A, Span);
       }
}
imgRbox.onclick = function(N, idx, olen) {
       var C = imgRbox.Objs[N][idx];
       var I = imgRbox.ImgObjs[N][idx];

	   for(i=0;i<olen;i++){
		   var I2 = imgRbox.ImgObjs[N][i];
		   I2.src = imgRbox.Imgs[N][i]["off"].src;
	   }

       C.checked = !C.checked;
       I.src = imgRbox.Imgs[N][idx][C.checked?"on":"on"].src;
       
       // fire event
       if (C.onclick != undefined || C.onclick != null) C.onclick();

}
imgRbox.Objs = {};
imgRbox.Imgs = {};
imgRbox.ImgObjs = {};