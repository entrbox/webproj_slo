<!-- #include file="../include/sub_menu.asp" -->
      <!--contents-->
      <tr>
        <td align="center"><table width="1024" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="180" valign="top"><!--#include file="../include/lmenu_12.asp" --> </td>
            <td width="10"></td>
            <td width="834" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td height="40"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                  <tr>
		<td width="120"><span class="new_tit_st">쿠폰<span>적립내역</span></span></td>
                    <td>&nbsp;</td>
                    <td align="right"><span class="location_01">홈 > 쿠폰구매 > </span><span class="location_02"> 쿠폰접립내역 </span></td>

                </table></td>
              </tr>
                 <tr>
                <td height="1" bgcolor="#292a2d"></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td align="center">
                <!-- -->
               <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td align="center">
                    <table width="820" border="0" cellspacing="0" cellpadding="0" class="p_table">
                      <tr>
                        <td colspan="2">
                          <span class="ti">쿠폰포인트 지급- 회원활동내역에대한 포인트로서 이벤트쿠폰구매에만 이용가능한 포인트입니다.</span><br />
                        - 누적된 회원님의 쿠폰 포인트로 다양양한 아이템의 쿠폰을 구매 하실 수 있습니다.<br />
                        - 배당률 증가, 미 적중 시 일정 % 만큼의 환불, 일정 시간 동안의 배팅 취소 등 다양한 쿠폰을 구매 하실 수 있습니다.<br />
                        - 쿠폰 구매와 동시에 해당 구매 포인트 만큼 쿠포 포인트가 자동 차감 됩니다.<br />
                        - 쿠폰 1개는 한번의 배팅에만 적용되며, 적중여부와 상관없이 결과 적용 시 자동 소멸 됩니다 .<br />
                        - 선택하신 경기 1경기라도 유효한 경기가 있으면,쿠폰 적용이 되며, 모든 경기가 취소 된 경우 쿠폰은 자동 소멸 됩니다.
                        </td>
                        </tr>
                      <tr>
                        <th width="15%">보유 포인트</th>
                        <td class="po"><%=formatnumber(ConSet_usercpoint,0)%> Point</td>
                      </tr>
                      <!--tr>
                        <th>배팅머니로 전환</th>
                        <td>
                        <input name="" type="text">
                        <img src="../images/point_btn.jpg" width="122" height="28"></td>
                      </tr-->
                    </table>

                    </td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td align="center">
                    <!-- 리스트-->
                    <table width="820" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="center" colspan=2>
                        <table width="820" border="0" cellpadding="0" cellspacing="5" bgcolor="#1c1d1f">
                            <tr>
                                <td align="center" bgcolor="#292a2d">
                                <table width="99%" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td height="5"></td>
                                    </tr>
                                    <tr>
                                        <td height="32" background="../images/charge/charge_bg_01.jpg">
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td width="10%" align="center"><strong>번호</strong></td>
                                                <td width="20%" align="center"><strong>내용</strong></td>
                                                <td width="25%" align="center"><strong>보유포인트</strong></td>
                                                <td width="25%" align="center"><strong>변동포인트</strong></td>
                                                <td width="20%" align="center"><strong>일시</strong></td>
                                            </tr>
                                        </table>
                                        </td>
                                    </tr>

<%
    gotopage=rq("gotopage")
    stxt10  =rq("stxt10")
    '//
    if gotopage="" then
        gotopage=1
        stxt10 = cdbl(ConSet_usercpoint)
    end if
    stxt10 = cdbl(stxt10)
    '//
	set rs = server.CreateObject("ADODB.Recordset")
	SQL = " SELECT * FROM TB_MEMBER_UCPOINTLOG WHERE USERID='"& SESSION("USERID") &"' ORDER BY IDX DESC"
	rs.PageSize=15
	rs.Open sql, db, 1
	if not rs.EOF Then rs.AbsolutePage=int(gotopage)
	schCnt = rs.recordcount
	'//
	j=schCnt-((gotopage-1)*10)
	i=1
	do until rs.EOF or i>rs.pagesize
        idx    = rs("idx")
        userid = rs("userid")
        uflag  = rs("uflag")
        upoint = formatnumber(rs("upoint"),0)
        nowPoint = formatnumber(rs("nowPoint"),0)
        utitle = rs("utitle")
        wdate  = rs("wdate")
        '//
		wdate  = getDateRe("8",wdate,"-")
        '//
        bgcolor=""
        if i mod 2 = 0 then bgcolor="#292a2d"
        if uflag="2" then upoint = -upoint
        '//
%>

                                    <tr>
                                        <td height="30" bgcolor="<%=bgcolor%>">
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td width="10%" align="center"><%=j%></td>
                                                <td width="20%" align="center" class="co_name"><%=utitle%></td>
                                                <td width="25%" align="center" class="co_point"><%=formatnumber(nowPoint,0)%> Point</td>
                                                <td width="25%" align="center"><b><%=formatnumber(upoint,0)%> Point</b></td>
                                                <td width="20%" align="center"><%=wdate%></td>
                                            </tr>
                                        </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="1" bgcolor="#292a2d"></td>
                                    </tr>

<%
    '//
rs.movenext
j=j-1
i=i+1
loop
%>

                                </table>
                                </td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td height="30" align="center" colspan=2><!--#include virtual="/include/paging.asp" --></td>
                    </tr>
                    <tr>
                        <td height="50">&nbsp;</td>
                    </tr>
                </table>
                    <!-- //리스트-->
                    </td>
                  </tr>
                </table>
                <!-- -->
     </td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
           <!--footer-->
     <!-- #include file="../include/sub_footer.asp" -->