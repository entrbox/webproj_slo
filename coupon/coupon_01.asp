<!-- #include file="../include/sub_menu.asp" -->
<%
    '//
    set rs = server.CreateObject("ADODB.Recordset")
    SQL = "select * from TB_SPO_CPREG "
    rs.Open sql, db, 1
    if not rs.eof then
        cp_addper_3	     = formatnumber(rs("cp_addper_3"),0)
        cp_addper_5	     = formatnumber(rs("cp_addper_5"),0)
        cp_addper_10	 = formatnumber(rs("cp_addper_10"),0)
        cp_retper_50	 = formatnumber(rs("cp_retper_50"),0)
        cp_retper_30	 = formatnumber(rs("cp_retper_30"),0)
        cp_notSOmin_40	 = formatnumber(rs("cp_notSOmin_40"),0)
        cp_notBAmin_40	 = formatnumber(rs("cp_notBAmin_40"),0)
        cp_notBSmin_40	 = formatnumber(rs("cp_notBSmin_40"),0)
        cp_notmin_30	 = formatnumber(rs("cp_notmin_30"),0)
        cp_upmax	     = formatnumber(rs("cp_upmax"),0)

        cp_addper_3_yn	  = rs("cp_addper_3_yn")
        cp_addper_5_yn	  = rs("cp_addper_5_yn")
        cp_addper_10_yn	  = rs("cp_addper_10_yn")
        cp_retper_50_yn	  = rs("cp_retper_50_yn")
        cp_retper_30_yn	  = rs("cp_retper_30_yn")
        cp_notSOmin_40_yn = rs("cp_notSOmin_40_yn")
        cp_notBAmin_40_yn = rs("cp_notBAmin_40_yn")
        cp_notBSmin_40_yn = rs("cp_notBSmin_40_yn")
        cp_notmin_30_yn	  = rs("cp_notmin_30_yn")
        cp_upmax_yn	      = rs("cp_upmax_yn")
    end if
    rs.close
    '//
%>
      <!--contents-->
      <tr>
        <td align="center"><table width="1024" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="180" valign="top"><!--#include file="../include/lmenu_12.asp" --> </td>
            <td width="10"></td>
            <td width="834" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td height="40"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                  <tr>
		<td width="120"><span class="new_tit_st">쿠폰<span>구매</span></span></td>
                    <td>&nbsp;</td>
                    <td align="right"><span class="location_01">홈 > 쿠폰구매 > </span><span class="location_02"> 쿠폰구매 </span></td>

                </table></td>
              </tr>
                  <tr>
                <td height="1" bgcolor="#292a2d"></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td align="center">
                <!-- -->
               <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td align="center">
                    <table width="820" border="0" cellspacing="0" cellpadding="0" class="p_table">
                      <tr>
                        <td colspan="2">
                          <span class="ti">쿠폰포인트 지급- 회원활동내역에대한 포인트로서 이벤트쿠폰구매에만 이용가능한 포인트입니다.</span><br />
                        - 누적된 회원님의 쿠폰 포인트로 다양양한 아이템의 쿠폰을 구매 하실 수 있습니다.<br />
                        - 배당률 증가, 미 적중 시 일정 % 만큼의 환불, 일정 시간 동안의 배팅 취소 등 다양한 쿠폰을 구매 하실 수 있습니다.<br />
                        - 쿠폰 구매와 동시에 해당 구매 포인트 만큼 쿠포 포인트가 자동 차감 됩니다.<br />
                        - 쿠폰 1개는 한번의 배팅에만 적용되며, 적중여부와 상관없이 결과 적용 시 자동 소멸 됩니다 .<br />
                        - 선택하신 경기 1경기라도 유효한 경기가 있으면,쿠폰 적용이 되며, 모든 경기가 취소 된 경우 쿠폰은 자동 소멸 됩니다.
                        </td>
                        </tr>
                      <tr>
                        <th width="15%">보유 쿠폰포인트</th>
                        <td class="po"><%=formatnumber(ConSet_usercpoint,0)%> Point</td>
                      </tr>
                      <!--tr>
                        <th>배팅머니로 전환</th>
                        <td>
                        <input name="" type="text">
                        <img src="../images/point_btn.jpg" width="122" height="28"></td>
                      </tr-->
                    </table>
<script language='JavaScript'>
<!--

function orderchb(val1){
	if (window.confirm("선택한 쿠폰을 구매 하시겠습니까?")){
		form.cpnm.value=val1;
		form.submit();
		return false;
	}
}
//-->
</script>

<form action="../sports/cpregproc.asp" name=form method=post>
<input type=hidden name=cpnm>
</form>


                    </td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td align="center">
                    <!-- 쿠폰내용-->
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <!-- 항목-->
                      <tr>
                        <td class="p_td01">배당률증가 쿠폰</td>
                      </tr>
                      <tr>
                        <td class="p_td02">선택하신 경기의 배당률을 일정 %만큼 증가시켜 줍니다.</td>
                      </tr>
                      <tr>
                        <td align="center">
                        <!-- 리스트-->
                        <table width="820" border="0" cellpadding="0" cellspacing="5" bgcolor="#1c1d1f">
                            <tr>
                                <td align="center" bgcolor="#292a2d">
                                <table width="99%" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td height="5"></td>
                                    </tr>
                                    <tr>
                                        <td height="32" background="../images/charge/charge_bg_01.jpg">
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td width="20%" align="center"><strong>쿠폰명</strong></td>
                                                <td width="45%" align="center"><strong>쿠폰세부내용</strong></td>
                                                <td width="15%" align="center"><strong>구매포인트</strong></td>
                                                <td width="20%" align="center"><strong>구매하기</strong></td>
                                                </tr>
                                        </table>
                                        </td>
                                    </tr>
                                    <!-- 1-->
                                    <tr>
                                        <td height="30" bgcolor="">
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td width="20%" align="center" class="co_name">배당률증가 3%</td>
                                                <td width="45%" align="left">배당률이 3% 증가합니다.</td>
                                                <td width="15%" align="center" class="co_point"><%=cp_addper_3%> Point</td>
                                                <td width="20%" align="center"><%if cp_addper_3_yn="Y" then%><img src="../images/menu/btn_coupon.png" width="62" height="24" style="cursor:pointer" onclick="orderchb('cp_addper_3')"><%end if%></td>
                                                </tr>
                                        </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="1" bgcolor="#1c1d1f"></td>
                                    </tr>
                					<!-- //1-->
                                    <!-- 2-->
                                    <tr>
                                        <td height="30" bgcolor="#292a2d">
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td width="20%" align="center" class="co_name">배당률증가 5%</td>
                                                <td width="45%" align="left">배당률이 5% 증가합니다.</td>
                                                <td width="15%" align="center" class="co_point"><%=cp_addper_5%> Point</td>
                                                <td width="20%" align="center"><%if cp_addper_5_yn="Y" then%><img src="../images/menu/btn_coupon.png" width="62" height="24" style="cursor:pointer" onclick="orderchb('cp_addper_5')"><%end if%></td>
                                                </tr>
                                        </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="1" bgcolor="#1c1d1f"></td>
                                    </tr>
                					<!-- //2-->

                                    <!-- 1-->
                                    <tr>
                                        <td height="30" bgcolor="">
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td width="20%" align="center" class="co_name">배당률증가 10%</td>
                                                <td width="45%" align="left">배당률이 10% 증가합니다.</td>
                                                <td width="15%" align="center" class="co_point"><%=cp_addper_10%> Point</td>
                                                <td width="20%" align="center"><%if cp_addper_10_yn="Y" then%><img src="../images/menu/btn_coupon.png" width="62" height="24" style="cursor:pointer" onclick="orderchb('cp_addper_10')"><%end if%></td>
                                                </tr>
                                        </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="1" bgcolor="#1c1d1f"></td>
                                    </tr>
                					<!-- //1-->
                                    <!-- 2
                                    <tr>
                                        <td height="30" bgcolor="#ffffff">
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td width="20%" align="center" class="co_name">배당률증가 3%</td>
                                                <td width="45%" align="left">배당률이 3% 증가합니다.</td>
                                                <td width="15%" align="center" class="co_point">3,805 CPoint</td>
                                                <td width="20%" align="center"><img src="../images/menu/btn_coupon.png" width="63" height="24" style="cursor:pointer"></td>
                                                </tr>
                                        </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="1" bgcolor="#CCCCCC"></td>
                                    </tr>
                					//2-->
                                </table>
                                </td>
                            </tr>
                        </table>
                        <!-- //리스트-->
                        </td>
                      </tr>
                      <tr>
                        <td height="20"></td>
                      </tr>
                    <!-- //항목-->


                    <!-- 항목-->
                      <tr>
                        <td class="p_td01">미적중환급 쿠폰</td>
                      </tr>
                      <tr>
                        <td class="p_td02">선택하신 경기가 미적중시 일정 %만큼 환급을 해드립니다.</td>
                      </tr>
                      <tr>
                        <td align="center">
                        <!-- 리스트-->
                        <table width="820" border="0" cellpadding="0" cellspacing="5" bgcolor="#1c1d1f">
                            <tr>
                                <td height="103" align="center" bgcolor="#292a2d">
                                <table width="99%" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td height="5"></td>
                                    </tr>
                                    <tr>
                                        <td height="32" background="../images/charge/charge_bg_01.jpg">
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td width="20%" align="center"><strong>쿠폰명</strong></td>
                                                <td width="45%" align="center"><strong>쿠폰세부내용</strong></td>
                                                <td width="15%" align="center"><strong>구매포인트</strong></td>
                                                <td width="20%" align="center"><strong>구매하기</strong></td>
                                                </tr>
                                        </table>
                                        </td>
                                    </tr>
                                    <!-- 1-->
                                    <tr>
                                        <td height="30" bgcolor="">
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td width="20%" align="center" class="co_name">미적중환급 50%</td>
                                                <td width="45%" align="left">미적중(미당첨)시 50%환불</td>
                                                <td width="15%" align="center" class="co_point"><%=cp_retper_50%> Point</td>
                                                <td width="20%" align="center"><%if cp_retper_50_yn="Y" then%><img src="../images/menu/btn_coupon.png" width="62" height="24" style="cursor:pointer" onclick="orderchb('cp_retper_50')"><%end if%></td>
                                                </tr>
                                        </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="1" bgcolor="#1c1d1f"></td>
                                    </tr>
                					<!-- //1-->
                                    <!-- 2-->
                                    <tr>
                                        <td height="30" bgcolor="#292a2d">
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td width="20%" align="center" class="co_name">미적중환급 30%</td>
                                                <td width="45%" align="left">미적중(미당첨)시 30%환불</td>
                                                <td width="15%" align="center" class="co_point"><%=cp_retper_30%> Point</td>
                                                <td width="20%" align="center"><%if cp_retper_30_yn="Y" then%><img src="../images/menu/btn_coupon.png" width="62" height="24" style="cursor:pointer" onclick="orderchb('cp_retper_30')"><%end if%></td>
                                                </tr>
                                        </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="1" bgcolor="#1c1d1f"></td>
                                    </tr>
                					<!-- //2-->

                                </table>
                                </td>
                            </tr>
                        </table>
                        <!-- //리스트-->
                        </td>
                      </tr>
                      <tr>
                        <td height="20"></td>
                      </tr>
                    <!-- //항목-->



                    <!-- 항목-->
                      <tr>
                        <td class="p_td01">배팅취소 쿠폰</td>
                      </tr>
                      <tr>
                        <td class="p_td02">배팅을 하신 후 배팅을 취소할 수 있습니다. 같은 종목을 조합한경우에 취소가 가능하며 가장 먼저 시작하는 경기를 기준으로 적용 됩니다.</td>
                      </tr>
                      <tr>
                        <td align="center">
                        <!-- 리스트-->
                        <table width="820" border="0" cellpadding="0" cellspacing="5" bgcolor="#1c1d1f">
                            <tr>
                                <td align="center" bgcolor="#292a2d">
                                <table width="99%" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td height="5"></td>
                                    </tr>
                                    <tr>
                                        <td height="32" background="../images/charge/charge_bg_01.jpg">
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td width="20%" align="center"><strong>쿠폰명</strong></td>
                                                <td width="45%" align="center"><strong>쿠폰세부내용</strong></td>
                                                <td width="15%" align="center"><strong>구매포인트</strong></td>
                                                <td width="20%" align="center"><strong>구매하기</strong></td>
                                                </tr>
                                        </table>
                                        </td>
                                    </tr>
                                    <!-- 1-->
                                    <tr>
                                        <td height="30" bgcolor="">
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td width="20%" align="center" class="co_name">축구배팅취소 40분</td>
                                                <td width="45%" align="left">축구 경기시작후 40분까지 환불가능</td>
                                                <td width="15%" align="center" class="co_point"><%=cp_notSOmin_40%> Point</td>
                                                <td width="20%" align="center"><%if cp_notSOmin_40_yn="Y" then%><img src="../images/menu/btn_coupon.png" width="62" height="24" style="cursor:pointer" onclick="orderchb('cp_notSOmin_40')"><%end if%></td>
                                                </tr>
                                        </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="1" bgcolor="#1c1d1f"></td>
                                    </tr>
                					<!-- //1-->
                                    <!-- 2-->
                                    <tr>
                                        <td height="30" bgcolor="#292a2d">
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td width="20%" align="center" class="co_name">야구배팅취소 40분</td>
                                                <td width="45%" align="left">야구 경기시작후 40분까지 환불가능</td>
                                                <td width="15%" align="center" class="co_point"><%=cp_notBAmin_40%> Point</td>
                                                <td width="20%" align="center"><%if cp_notBAmin_40_yn="Y" then%><img src="../images/menu/btn_coupon.png" width="62" height="24" style="cursor:pointer" onclick="orderchb('cp_notBAmin_40')"><%end if%></td>
                                                </tr>
                                        </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="1" bgcolor="#1c1d1f"></td>
                                    </tr>
                					<!-- //2-->

                                    <!-- 1-->
                                    <tr>
                                        <td height="30" bgcolor="">
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td width="20%" align="center" class="co_name">농구배팅취소 40분</td>
                                                <td width="45%" align="left">농구 경기시작후 40분까지 환불가능</td>
                                                <td width="15%" align="center" class="co_point"><%=cp_notBSmin_40%> Point</td>
                                                <td width="20%" align="center"><%if cp_notBSmin_40_yn="Y" then%><img src="../images/menu/btn_coupon.png" width="62" height="24" style="cursor:pointer" onclick="orderchb('cp_notBSmin_40')"><%end if%></td>
                                                </tr>
                                        </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="1" bgcolor="#1c1d1f"></td>
                                    </tr>
                					<!-- //1-->
                                    <!-- 2
                                    <tr>
                                        <td height="30" bgcolor="#ffffff">
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td width="20%" align="center" class="co_name">배당률증가 3%</td>
                                                <td width="45%" align="left">배당률이 3% 증가합니다.</td>
                                                <td width="15%" align="center" class="co_point">3,805 CPoint</td>
                                                <td width="20%" align="center"><img src="../images/menu/btn_coupon.png" width="63" height="24" style="cursor:pointer"></td>
                                                </tr>
                                        </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="1" bgcolor="#CCCCCC"></td>
                                    </tr>
                					//2-->
                                </table>
                                </td>
                            </tr>
                        </table>
                        <!-- //리스트-->
                        </td>
                      </tr>
                      <tr>
                        <td height="20"></td>
                      </tr>
                    <!-- //항목-->



                    <!-- 항목-->
                      <tr>
                        <td class="p_td01">경기마감전 배팅취소 쿠폰</td>
                      </tr>
                      <tr>
                        <td class="p_td02">배팅을 하신 후 배팅을 취소할수 있습니다. 정해진 시간 전까지 취소가 가능하며 가장 먼저 시작하는 경기를 기준으로 적용됩니다.</td>
                      </tr>
                      <tr>
                        <td align="center">
                        <!-- 리스트-->
                        <table width="820" border="0" cellpadding="0" cellspacing="5" bgcolor="#1c1d1f">
                            <tr>
                                <td align="center" bgcolor="#292a2d">
                                <table width="99%" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td height="5"></td>
                                    </tr>
                                    <tr>
                                        <td height="32" background="../images/charge/charge_bg_01.jpg">
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td width="20%" align="center"><strong>쿠폰명</strong></td>
                                                <td width="45%" align="center"><strong>쿠폰세부내용</strong></td>
                                                <td width="15%" align="center"><strong>구매포인트</strong></td>
                                                <td width="20%" align="center"><strong>구매하기</strong></td>
                                                </tr>
                                        </table>
                                        </td>
                                    </tr>
                                    <!-- 1-->
                                    <tr>
                                        <td height="30" bgcolor="">
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td width="20%" align="center" class="co_name">배팅취소쿠폰</td>
                                                <td width="45%" align="left">배팅 후 1시간 이내 경기시작 30분전 배팅취소 쿠폰<br>
                                                  (마감 후 기준점이 변경된 경기는 배팅취소 불가)</td>
                                                <td width="15%" align="center" class="co_point"><%=cp_notmin_30%> Point</td>
                                                <td width="20%" align="center"><%if cp_notmin_30_yn="Y" then%><img src="../images/menu/btn_coupon.png" width="63" height="24" style="cursor:pointer" onclick="orderchb('cp_notmin_30')"><%end if%></td>
                                                </tr>
                                        </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="1" bgcolor="#292a2d"></td>
                                    </tr>
                					<!-- //1-->
                                    <!-- 2
                                    <tr>
                                        <td height="30" bgcolor="#ffffff">
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td width="20%" align="center" class="co_name">배당률증가 5%</td>
                                                <td width="45%" align="left">배당률이 5% 증가합니다.</td>
                                                <td width="15%" align="center" class="co_point">10,000 CPoint</td>
                                                <td width="20%" align="center"><img src="../images/menu/btn_coupon.png" width="63" height="24" style="cursor:pointer"></td>
                                                </tr>
                                        </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="1" bgcolor="#CCCCCC"></td>
                                    </tr>
                					//2-->
                                </table>
                                </td>
                            </tr>
                        </table>
                        <!-- //리스트-->
                        </td>
                      </tr>
                      <tr>
                        <td height="20"></td>
                      </tr>
                    <!-- //항목-->



                    <!-- 항목-->
                      <tr>
                        <td class="p_td01">최대 상한가 업 쿠폰</td>
                      </tr>
                      <tr>
                        <td class="p_td02">일정 %만큼 최대적중금 상한가를 증가시켜줍니다.</td>
                      </tr>
                      <tr>
                        <td align="center">
                        <!-- 리스트-->
                        <table width="820" border="0" cellpadding="0" cellspacing="5" bgcolor="#1c1d1f">
                            <tr>
                                <td align="center" bgcolor="#292a2d">
                                <table width="99%" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td height="5"></td>
                                    </tr>
                                    <tr>
                                        <td height="32" background="../images/charge/charge_bg_01.jpg">
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td width="20%" align="center"><strong>쿠폰명</strong></td>
                                                <td width="45%" align="center"><strong>쿠폰세부내용</strong></td>
                                                <td width="15%" align="center"><strong>구매포인트</strong></td>
                                                <td width="20%" align="center"><strong>구매하기</strong></td>
                                                </tr>
                                        </table>
                                        </td>
                                    </tr>
                                    <!-- 1-->
                                    <tr>
                                        <td height="30" bgcolor="">
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td width="20%" align="center" class="co_name">상한가업 50%</td>
                                                <td width="45%" align="left">최대상한가가 업 됩니다. 300만 -&gt;450만</td>
                                                <td width="15%" align="center" class="co_point"><%=cp_upmax%> Point</td>
                                                <td width="20%" align="center"><%if cp_upmax_yn="Y" then%><img src="../images/menu/btn_coupon.png" width="63" height="24" style="cursor:pointer" onclick="orderchb('cp_upmax')"><%end if%></td>
                                                </tr>
                                        </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="1" bgcolor="#292a2d"></td>
                                    </tr>
                					<!-- //1-->
                                    <!-- 2
                                    <tr>
                                        <td height="30" bgcolor="#ffffff">
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td width="20%" align="center" class="co_name">배당률증가 5%</td>
                                                <td width="45%" align="left">배당률이 5% 증가합니다.</td>
                                                <td width="15%" align="center" class="co_point">10,000 CPoint</td>
                                                <td width="20%" align="center"><img src="../images/menu/btn_coupon.png" width="63" height="24" style="cursor:pointer"></td>
                                                </tr>
                                        </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="1" bgcolor="#CCCCCC"></td>
                                    </tr>
                					 //2-->

                                </table>
                                </td>
                            </tr>
                        </table>
                        <!-- //리스트-->
                        </td>
                      </tr>
                      <tr>
                        <td height="20"></td>
                      </tr>
                    <!-- //항목-->


                    </table>

                    <!-- //쿠폰내용-->
                    </td>
                  </tr>
                </table>
                <!-- -->
     </td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
           <!--footer-->
     <!-- #include file="../include/sub_footer.asp" -->
