<%
	SUB getPopup(val1,val2,val3)	'팝업창관리- val1:구분 val2:메세지 val3:이동페이지
		'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		If val1="1" Then
			response.write "<script language=javascript>history.back(-1);</script>"
		elseif val1="2" Then
			response.write "<script language=javascript>alert('"& val2 &"');history.back(-1);</script>"
		elseif val1="3" Then
			response.write "<script language=javascript>location.href='"& val2 &"';</script>"
		elseif val1="4" Then
			response.write "<script language=javascript>alert('"& val2 &"');location.href='"& val3 &"';</script>"
		elseif val1="44" Then
			response.write "<script language=javascript>alert('"& val2 &"');location.href='"& val3 &"';parent.location.reload();</script>"
		elseif val1="444" Then
			response.write "<script language=javascript>alert('"& val2 &"');window.location.reload();</script>"
		'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		elseif val1="11" Then
			response.write "<script language=javascript>window.close();</script>"						'팝업닫기
		elseif val1="12" or val1="25" Then
			response.write "<script language=javascript>alert('"& val2 &"');window.close();</script>"	'메세지후 팝업닫기
		elseif val1="13" or val1="31" Then
			response.write "<script language=javascript>alert('"& val2 &"');</script>"	'메세지만
		'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		elseif val1="21" Then
			response.write "<script language=javascript>opener.location.reload();</script>"						'팝업에서 부모창리로드
		elseif val1="22" Then
			response.write "<script language=javascript>opener.location.reload();window.close();</script>"		'팝업에서 부모창리로드후 닫기
		elseif val1="23" Then
			response.write "<script language=javascript>alert('"& val2 &"');opener.location.reload()</script>"	'팝업에서 메세지후 부모창리로드
		elseif val1="24" Then
			response.write "<script language=javascript>alert('"& val2 &"');opener.location.reload();window.close();</script>"	'팝업에서 메세지후 부모창리로드후 닫기
		'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		elseif val1="32" Then
			response.write "<script language=javascript>alert('"& val2 &"');parent.location.reload();</script>"	'아이프레임에서 메세지후 부모창리로드
		elseif val1="33" Then
			response.write "<script language=javascript>parent.location.reload();</script>"	'아이프레임에서 부모창리로드
		elseif val1="34" Then
			response.write "<script language=javascript>parent.location.href='"& val3 &"';</script>"	'아이프레임에서 부모창이동
		elseif val1="35" Then
			response.write "<script language=javascript>alert('"& val2 &"');parent.location.href='"& val3 &"';</script>"	'아이프레임에서 부모창이동
		'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		elseif val1="41" Then
			response.write "<script language=javascript>window.open('"& val1 &"','"& val2 &"');</script>"	
		elseif val1="42" Then
			response.write "<script language=javascript>window.open('"& val1 &"','"& val2 &"','"& val3 &"');</script>"	
		elseif val1="51" Then
			response.write "<script language=javascript>alert('"& val2 &"');window.open('/', '_top')</script>"			'로그아웃시
		End If
		Response.end
	End Sub

	Function getDateTime(val1)				'14자리 년월일시분초 구함
		fun_ymd = replace(left(val1,10),"-","")
		fun_ho  = Right("0"&cstr(hour(time)),2)
		fun_mi  = Right("0"&cstr(minute(time)),2)
		fun_se  = Right("0"&cstr(second(time)),2)
		getDateTime = fun_ymd&fun_ho&fun_mi&fun_se
	End Function

	Function getDateTime2(val1)				'14자리 년월일시분초 구함
		val1 = CDate(val1)
		fun_ymd = replace(left(val1,10),"-","")
		fun_ho  = Right("0"&cstr(hour(val1)),2)
		fun_mi  = Right("0"&cstr(minute(val1)),2)
		fun_se  = Right("0"&cstr(second(val1)),2)
		getDateTime2 = fun_ymd&fun_ho&fun_mi&fun_se
	End Function

	Function getDateRe(val1,val2,val3)	'날자 :	20091109(2009-11-09) / 1:2009.11.09 / 2:09.11.09 / 3:11.09 / 4:2009 / 5:11 / 6:09
		if val1="1" then
			getDateRe = mid(val2,1,4) & val3 & mid(val2,5,2) & val3 & mid(val2,7,2)	'2012-05-01
		elseif val1="2" then
			getDateRe = mid(val2,3,2) & val3 & mid(val2,5,2) & val3 & mid(val2,7,2)	'12-05-01
		elseif val1="3" then
			getDateRe = mid(val2,1,4) & val3 & mid(val2,5,2)	'2012-05
		elseif val1="4" then
			getDateRe = mid(val2,5,2) & val3 & mid(val2,7,2)	'05-01
		elseif val1="5" then
			getDateRe = mid(val2,1,4)	'2012
		elseif val1="6" then
			getDateRe = mid(val2,5,2)	'05
		elseif val1="7" then
			getDateRe = mid(val2,7,2)	'01
		elseif val1="8" then
			getDateRe = mid(val2,1,4) & val3 & mid(val2,5,2) & val3 & mid(val2,7,2) &" "& mid(val2,9,2) & ":" & mid(val2,11,2) & ":" & mid(val2,13,2)	'2012-05-01 12:25:38
		elseif val1="9" then
			getDateRe = mid(val2,1,2) & val3 & mid(val2,3,2) & val3 & mid(val2,5,2)	'12:25:38
		elseif val1="10" then
			getDateRe = mid(val2,9,2) & val3 & mid(val2,11,2)							'12:25
		end if
	End Function

	sub getSelectDate(yearDa,monthDa,dayDa,yvalD,mvalD,dvalD,val1)
		imsiymd = left(getDateTime(now()),8)
		imsiye  = left(imsiymd,4)
		imsimo  = mid(imsiymd,5,2)
		imsida  = right(imsiymd,2)
		response.write "<select name="&yearDa&" style='width:70px' "& val1 &">"
					response.write "<option value=>년선택"
					for i=2010 to (imsiye+1)
						imsiselected = ""
						if yvalD<>"" then
							if cstr(yvalD)=cstr(i) then
								imsiselected = "selected"
							end if
						end if
						response.write "<option value="& i &" "& imsiselected &">"&i&"년"
					next
		response.write "</select>"
		if monthDa<>"" then
		response.write "<select name="&monthDa&" style='width:70px' "& val1 &">"
					response.write "<option value=>월선택"
					for i=1 to 12
						if i<10 then
							ii=cstr("0"&i)
						else
							ii=cstr(i)
						end if
						imsiselected = ""
						if mvalD<>"" then
							if mvalD=ii then
								imsiselected = "selected"
							end if
						end if
						response.write "<option value="& ii &" "& imsiselected &">"&ii&"월"
					next
		response.write "</select>"
		end if
		if dayDa<>"" then
		response.write "<select name="&dayDa&" style='width:70px'>"
					response.write "<option value=>일선택"
					for i=1 to 31
						if i<10 then
							ii=cstr("0"&i)
						else
							ii=cstr(i)
						end if
						imsiselected = ""
						if dvalD<>"" then
							if dvalD=ii then
								imsiselected = "selected"
							end if
						end if
						response.write "<option value="&ii&" "& imsiselected &">"&ii&"일"
					next
		response.write "</select>"
		end if
	end Sub

	Function getDateChb(val1,val2)
		If val1="" Or val2="" Then getDateChb = "날자형식 오류입니다."
		val1 = Replace(val1,"-","")
		val2 = Replace(val2,"-","")
		If CDbl(val1)>CDbl(val2) then getDateChb = "날자형식 오류입니다."
	End Function

	Function getRandNum()
		Randomize
		MyValue = Int((999999 * Rnd) + 1)	'무작위 값을 발생합니다.
		getRandNum = MyValue
	End Function

	Function getIDENT_CURRENT(val1)		'idx값
		fun_SQL = " SELECT IDENT_CURRENT('"& val1 &"') "
		set fun_rs = Db.Execute(fun_sql)
		fun_idval = fun_rs(0)
		fun_rs.close
		getIDENT_CURRENT = fun_idval
	End Function

	function getRows(val)		'2차배열
		set rowrs = server.CreateObject("ADODB.Recordset")
		SQL = val
		rowrs.Open sql, db, 1
		if not rowrs.eof then
			getRowsarr    = rowrs.GetRows
		else
			getRowsarr    = null
		end if
		rowrs.close
		set rowrs=nothing
		getRows = getRowsarr
	end function

	function getRowsInt(val)		'2차배열에 값이 있는지 확인
		if isarray(val) then
			getRowsInt = ubound(val,2)
		else
			getRowsInt = ""
		end if
	end Function

	function getNumZero(val)		'0
		If Len(val)=1 Then
			realVal = "00000" & val
		ElseIf Len(val)=2 Then
			realVal = "0000" & val
		ElseIf Len(val)=3 Then
			realVal = "000" & val
		ElseIf Len(val)=4 Then
			realVal = "00" & val
		ElseIf Len(val)=5 Then
			realVal = "0" & val
		Else
			realVal = val
		End if
		'//
		getNumZero = realVal
	End Function
	
	Function RQ(PstrField)
		RQ = Trim(request(PstrField))
		if len(RQ)>0 then
		'	RQ = Replace(RQ,"%","")
		'	RQ = Replace(RQ,"""","")
			RQ = Replace(RQ,"'","")
			RQ = Replace(RQ,"--","―­―")
			RQ = Replace(RQ,"―-","―­―")
			RQ = Replace(RQ,"-―","―­―")
		'	RQ = Replace(RQ,";","")
			RQ = Replace(RQ,"@","ⓐ")
	
			If InStr(LCase(RQ), "script") > 0 then
				ReplaceCode = Mid(RQ, InStr(LCase(RQ), "script"), 6)
				RQ = Replace(RQ,ReplaceCode, "")
				ReplaceCode = ""
			End If
			If InStr(LCase(RQ), "object") > 0 then
				ReplaceCode = Mid(RQ, InStr(LCase(RQ), "object"), 6)
				RQ = Replace(RQ,ReplaceCode, "")
				ReplaceCode = ""
			End If
			If InStr(LCase(RQ), "iframe") > 0 then
				ReplaceCode = Mid(RQ, InStr(LCase(RQ), "iframe"), 6)
				RQ = Replace(RQ,ReplaceCode, "")
				ReplaceCode = ""
			End If
			
			If InStr(UCase(RQ), "DROP") > 0 Then
				ReplaceCode = Mid(RQ, InStr(UCase(RQ), "DROP"), 4)
				RQ = Replace(RQ, ReplaceCode, "")
				ReplaceCode = ""
			End If	
			If InStr(UCase(RQ), "EXEC") > 0 Then
				ReplaceCode = Mid(RQ, InStr(UCase(RQ), "EXEC"), 4)
				RQ = Replace(RQ, ReplaceCode, "")
				ReplaceCode = ""
			End If
			If InStr(UCase(RQ), "DECLARE") > 0 Then
				ReplaceCode = Mid(RQ, InStr(UCase(RQ), "DECLARE"), 7)
				RQ = Replace(RQ, ReplaceCode, "")
				ReplaceCode = ""
			End If
			If InStr(UCase(RQ), "INSERT") > 0 Then
				ReplaceCode = Mid(RQ, InStr(UCase(RQ), "INSERT"), 6)
				RQ = Replace(RQ, ReplaceCode, "")
				ReplaceCode = ""
			End If		
			If InStr(UCase(RQ), "DELETE") > 0 Then
				ReplaceCode = Mid(RQ, InStr(UCase(RQ), "DELETE"), 6)
				RQ = Replace(RQ, ReplaceCode, "")
				ReplaceCode = ""
			End If	
			If InStr(UCase(RQ), "SELECT") > 0 Then
				ReplaceCode = Mid(RQ, InStr(UCase(RQ), "SELECT"), 6)
				RQ = Replace(RQ, ReplaceCode, "")
				ReplaceCode = ""
			End If		
				If InStr(UCase(RQ), "UPDATE") > 0 Then
				ReplaceCode = Mid(RQ, InStr(UCase(RQ), "UPDATE"), 6)
				RQ = Replace(RQ, ReplaceCode, "")
				ReplaceCode = ""
			End If	
			'RQ = Replace(RQ,"<","&lt;")
			'RQ = Replace(RQ,">","&gt;")				
		end if	
	End Function
	
	Function rqVal(VAL)
		RQVAL = Trim(VAL)
		if len(RQVAL)>0 then
		'	RQVAL = Replace(RQVAL,"%","")
		'	RQVAL = Replace(RQVAL,"""","")
			RQVAL = Replace(RQVAL,"'","")
			RQVAL = Replace(RQVAL,"--","­―­―")
			RQVAL = Replace(RQVAL,"―-","―­―")
			RQVAL = Replace(RQVAL,"-―","―­―")	
		'	RQVAL = Replace(RQVAL,";","")
			RQVAL = Replace(RQVAL,"@","ⓐ")
			
			If InStr(LCase(RQVAL), "script") > 0 then
				ReplaceCode = Mid(RQVAL, InStr(LCase(RQVAL), "script"), 6)
				RQVAL = Replace(RQVAL,ReplaceCode, "")
				ReplaceCode = ""
			End If
			If InStr(LCase(RQVAL), "object") > 0 then
				ReplaceCode = Mid(RQVAL, InStr(LCase(RQVAL), "object"), 6)
				RQVAL = Replace(RQVAL,ReplaceCode, "")
				ReplaceCode = ""
			End If
			If InStr(LCase(RQVAL), "iframe") > 0 then
				ReplaceCode = Mid(RQVAL, InStr(LCase(RQVAL), "iframe"), 6)
				RQVAL = Replace(RQVAL,ReplaceCode, "")
				ReplaceCode = ""
			End If
					
			If InStr(UCase(RQVAL), "DROP") > 0 Then
				ReplaceCode = Mid(RQVAL, InStr(UCase(RQVAL), "DROP"), 4)
				RQVAL = Replace(RQVAL, ReplaceCode, "")
				ReplaceCode = ""
			End If					
			If InStr(UCase(RQVAL), "EXEC") > 0 Then
				ReplaceCode = Mid(RQVAL, InStr(UCase(RQVAL), "EXEC"), 4)
				RQVAL = Replace(RQVAL, ReplaceCode, "")
				ReplaceCode = ""
			End If	
			If InStr(UCase(RQVAL), "DECLARE") > 0 Then
				ReplaceCode = Mid(RQVAL, InStr(UCase(RQVAL), "DECLARE"), 7)
				RQVAL = Replace(RQVAL, ReplaceCode, "")
				ReplaceCode = ""
			End If
			If InStr(UCase(RQVAL), "INSERT") > 0 Then
				ReplaceCode = Mid(RQVAL, InStr(UCase(RQVAL), "INSERT"), 6)
				RQVAL = Replace(RQVAL, ReplaceCode, "")
				ReplaceCode = ""
			End If		
			If InStr(UCase(RQVAL), "DELETE") > 0 Then
				ReplaceCode = Mid(RQVAL, InStr(UCase(RQVAL), "DELETE"), 6)
				RQVAL = Replace(RQVAL, ReplaceCode, "")
				ReplaceCode = ""
			End If	
			If InStr(UCase(RQVAL), "SELECT") > 0 Then
				ReplaceCode = Mid(RQVAL, InStr(UCase(RQVAL), "SELECT"), 6)
				RQVAL = Replace(RQVAL, ReplaceCode, "")
				ReplaceCode = ""
			End If		
				If InStr(UCase(RQVAL), "UPDATE") > 0 Then
				ReplaceCode = Mid(RQVAL, InStr(UCase(RQVAL), "UPDATE"), 6)
				RQVAL = Replace(RQVAL, ReplaceCode, "")
				ReplaceCode = ""
			End If	
			'RQVAL = Replace(RQVAL,"<","&lt;")
			'RQVAL = Replace(RQVAL,">","&gt;")
		end if	
	End Function
	
	SUB selMake(val1,val2,val3)
		If val1="1" Then
			Response.write "<select name='"& val2 &"'>"
			Response.write "<option value=''>------</option>"
			For i=0 To 23
				ii = Right("0"&i,2)
				imsiselected = ""
				If val3 = ii Then imsiselected = " selected"
				'//
				Response.write "<option value='"& ii &"' "& imsiselected &">"& ii &" "
				'//
			next
			Response.write "</select>"
		elseIf val1="2" Then
			Response.write "<select name='"& val2 &"'>"
			Response.write "<option value=''>------</option>"
			For i=0 To 59
				ii = Right("0"&i,2)
				imsiselected = ""
				If val3 = ii Then imsiselected = " selected"
				'//
				Response.write "<option value='"& ii &"' "& imsiselected &">"& ii &" "
				'//
			next
			Response.write "</select>"
		elseIf val1="3" Then
			Response.write "<select name='"& val2 &"'>"
			Response.write "<option value=''>------</option>"
			For i=5 To 30
				ii = Right("0"&i,2)
				imsiselected = ""
				If val3 = ii Then imsiselected = " selected"
				'//
				Response.write "<option value='"& ii &"' "& imsiselected &">"& ii &" "
				'//
				i=i+4
			next
			Response.write "</select>"
		elseIf val1="4" Then
			Response.write "<select name='"& val2 &"'>"
			Response.write "<option value=''>------</option>"
			For i=1 To 5
				ii = Right("0"&i,2)
				imsiselected = ""
				If val3 = ii Then imsiselected = " selected"
				'//
				Response.write "<option value='"& ii &"' "& imsiselected &">"& ii &" "
				'//
			next
			Response.write "</select>"
		elseIf val1="5" Then
			Response.write "<select name='"& val2 &"'>"
			Response.write "<option value=''>==전체==</option>"
			For i=1 To 78
				ii = CStr(i)
				imsiselected = ""
				If val3 = ii Then imsiselected = " selected"
				'//
				Response.write "<option value='"& ii &"' "& imsiselected &">제 "& ii &" 회차"
				'//
			next
			Response.write "</select>"
		elseIf val1="6" Then
			Response.write "<select name='"& val2 &"' onchange='actPage()'>"
			For i=1 To 78
				ii = CStr(i)
				imsiselected = ""
				If val3 = ii Then imsiselected = " selected"
				'//
				Response.write "<option value='"& ii &"' "& imsiselected &">제 "& ii &" 회차"
				'//
			next
			Response.write "</select>"
		elseIf val1="7" Then
			Response.write "<select name='"& val2 &"'>"
			Response.write "<option value=''>------</option>"
			For i=0 To 55 step 5
				ii = Right("0"&i,2)
				imsiselected = ""
				If val3 = ii Then imsiselected = " selected"
				'//
				Response.write "<option value='"& ii &"' "& imsiselected &">"& ii &" "
				'//
			next
			Response.write "</select>"
		elseIf val1="8" Then
			If val3 = "Y" Then imsiselected1 = " selected"
			If val3 = "N" Then imsiselected2 = " selected"
			Response.write "<select name='"& val2 &"'>"
			Response.write "<option value='Y' "& imsiselected1 &">사용"
			Response.write "<option value='N' "& imsiselected2 &">사용안함"
			Response.write "</select>"
		elseIf val1="9" Then
			Response.write "<select name='"& val2 &"'>"
			Response.write "<option value=''>구매취소 사용안함</option>"
			For i=1 To 59
				'ii = Right("0"&i,2)
				imsiselected = ""
				If val3 = i Then imsiselected = " selected"
				'//
				Response.write "<option value='"& i &"' "& imsiselected &">"& i &"분까지 취소가능 "
				'//
			next
			Response.write "</select>"
		End If

	End SUB


	Function getRevalue(val1,val2)	'NULL일때 값 대치
		if val1 = "" or isnull(val1) then
			getRevalue = val2
		else
			getRevalue = val1
		end if
	End Function

	Function getAmemSch(val1)
		If val1="2" Then
			val1Value = "where mb_outFlag = 'n' "
		elseIf val1="3" Then
			val1Value = "where mb_outFlag = 'y' "
		elseIf val1="4" Then
			val1Value = "where substring(mb_wdate,1,8) = '"& Replace(Left(now(),10),"-","") &"' "
		elseIf val1="5" Then
			val1Value = "where mb_money > 0 "
		Else
			val1Value = "where idx > 0 "
		End if
		getAmemSch = val1Value
	End Function


	Function getAmemSch2(val1)
		If val1="2" Then
			val1Value = "and c.mb_outFlag = 'n' "
		elseIf val1="3" Then
			val1Value = "and c.mb_outFlag = 'y' "
		elseIf val1="4" Then
			val1Value = "and substring(c.mb_wdate,1,8) = '"& Replace(Left(now(),10),"-","") &"' "
		elseIf val1="5" Then
			val1Value = "and c.mb_money > 0 "
		Else
			val1Value = "and c.idx > 0 "
		End if
		getAmemSch2 = val1Value
	End Function


	SUB getFbnkInfo(val1,val2)
		fbnkVale = "-----은행선택-----,경남은행,광주은행,국민은행,기업은행,농협,대구은행,도이치은행,부산은행,산업은행,상호저축은행,새마을금고,수출입은행,수협,신용협동조합,신한은행,외환은행,우체국,우리은행,전북은행,제주은행,하나은행,시티은행,ABN-AMRO,HSBC은행,SC제일은행"
		fbnkCode = ",경남은행,광주은행,국민은행,기업은행,농협,대구은행,도이치은행,부산은행,산업은행,상호저축은행,새마을금고,수출입은행,수협,신용협동조합,신한은행,외환은행,우체국,우리은행,전북은행,제주은행,하나은행,시티은행,ABN-AMRO,HSBC은행,SC제일은행"
		fbnkValeArr = Split(fbnkVale,",")
		fbnkCodeArr = Split(fbnkCode,",")
		Response.write "<select name='"& val2 &"'>"
		Response.write "<option value=''>------</option>"
		For i=0 To ubound(fbnkValeArr)
			imsiselected = ""
			If val1 = Trim(fbnkCodeArr(i)) Then imsiselected = " selected"
			Response.write "<option value='"& Trim(fbnkCodeArr(i)) &"' "& imsiselected &">"& Trim(fbnkValeArr(i)) &" "
		next
		Response.write "</select>"
	End Sub

	Function getwinMoney(val1)
		If val1<>"" Then
			val1Value = 1000*val1
		else
			val1Value = 0
		End if
		getwinMoney = FormatNumber(val1Value,0)
	End Function

	Function getTimerTime(val1,val2)
		S_remain = DateDiff("s",val1,val2)
		DD = Fix(S_remain / 86400)                   '몇일 남았는지.. 하루가 86400초이므로...
		HH = Fix(S_remain / 3600)   Mod 24           '몇시간 남았는지.. 남은 일 수를 제외하고 시간만 구한다.
		MM = Fix(S_remain / 60) Mod 60               '몇분 남았는지..남은 시간은 제외하고 분만 구함
		SS = S_remain  Mod  60                       '초

		HH = Right("0" & HH,2)
		MM = Right("0" & MM,2)
		SS = Right("0" & SS,2)

		If HH="" Or HH="0" Or HH="00" then
			getTimerTime = MM&":"&SS
		Else
			imsiHH = CDbl(HH)*60
			imsiMM = CDbl(MM)+imsiHH
			MM = imsiMM
			getTimerTime = MM&":"&SS
			'getTimerTime = HH&":"&MM&":"&SS
		End if
	End Function


	Function subMemUmoneyLog_site(val1,val2,val3,val4,val5,gubun,site)	'아이디 / 사용금액 / 가감구분(1+,2-) / 사용내역 / 주문번호orSeq
		uWdate = getDateTime(now())
		imsigubun = gubun
		
		dbname = ""
		if site = "spo" then
			dbname = "dbo."
		elseif site = "ground" then
			dbname = "OPENDATASOURCE('SQLOLEDB','Data Source=127.0.0.1,15333;User ID=groundwebuser;Password=lte#$%789').GROUNDDB2.DBO."	
		elseif site = "txco" then
			dbname = "OPENDATASOURCE('SQLOLEDB','Data Source=127.0.0.1,15333;User ID=txcodbuser;Password=txdb)(8765').TXCODB.DBO."	
		elseif site = "slamdunk" then
			dbname = "OPENDATASOURCE('SQLOLEDB','Data Source=127.0.0.1,15333;User ID=slamdbuser;Password=slam#$%789').SLAMDUNK.DBO."							
		else
			dbname = ""		
		end if
				
		If val5="" Then val5=0
		sqlC = " userid,uFlag,uMoney,utitle,wdate,orSeq,gubun,nowmoney "
		sqlV = " '"& val1 &"','"& val3 &"','"& val2 &"','"& val4 &"','"& uWdate &"', "& val5 &", '"& imsigubun &"', (select mb_money from " & dbname & "tb_member where mb_id='"& val1 &"') "
		sql = "insert into " & dbname & "tb_member_uMoneyLog ("& sqlC &") values ( "& sqlV &" ) "
		db.execute sql

		If val3="1" then	'충전
			sql = "update " & dbname & "tb_member set mb_money = mb_money+"& val2 &" where mb_id = '"& val1 &"' "
			db.execute sql
		Else				'사용
			sql = "update " & dbname & "tb_member set mb_money = mb_money-"& val2 &" where mb_id = '"& val1 &"' "
			db.execute sql
		End If
	End Function
	
	Function subMemUmoneyLog(val1,val2,val3,val4,val5)	'아이디 / 사용금액 / 가감구분(1+,2-) / 사용내역 / 주문번호orSeq
		subMemUmoneyLog_site(val1,val2,val3,val4,val5,"","")
	End Function

	Function subMemUmoneyLog_spo(val1,val2,val3,val4,val5)	'아이디 / 사용금액 / 가감구분(1+,2-) / 사용내역 / 주문번호orSeq
		subMemUmoneyLog_site(val1,val2,val3,val4,val5,"5","spo")
	End Function

	Function subMemUmoneyLog_spo_ground(val1,val2,val3,val4,val5)	'아이디 / 사용금액 / 가감구분(1+,2-) / 사용내역 / 주문번호orSeq
		subMemUmoneyLog_site(val1,val2,val3,val4,val5,"5","ground")
	End Function

	Function subMemUmoneyLog_spo_txco(val1,val2,val3,val4,val5)	'아이디 / 사용금액 / 가감구분(1+,2-) / 사용내역 / 주문번호orSeq
		subMemUmoneyLog_site(val1,val2,val3,val4,val5,"5","txco")
	End Function

	Function subMemUmoneyLog_spo_slamdunk(val1,val2,val3,val4,val5)	'아이디 / 사용금액 / 가감구분(1+,2-) / 사용내역 / 주문번호orSeq
		subMemUmoneyLog_site(val1,val2,val3,val4,val5,"5","slamdunk")
	End Function

	Function subMemUpointLogs_site(val1,val2,val3,val4,site)	'아이디 / 사용포인트 / 가감구분(1+,2-) / 사용내역
		dbname = ""
		if site = "spo" then
			dbname = "dbo."
		elseif site = "ground" then
			dbname = "OPENDATASOURCE('SQLOLEDB','Data Source=127.0.0.1,15333;User ID=groundwebuser;Password=lte#$%789').GROUNDDB2.DBO."	
		elseif site = "txco" then
			dbname = "OPENDATASOURCE('SQLOLEDB','Data Source=127.0.0.1,15333;User ID=txcodbuser;Password=txdb)(8765').TXCODB.DBO."	
		elseif site = "slamdunk" then
			dbname = "OPENDATASOURCE('SQLOLEDB','Data Source=127.0.0.1,15333;User ID=slamdbuser;Password=slam#$%789').SLAMDUNK.DBO."							
		else
			dbname = ""		
		end if
		
        set Frs = server.CreateObject("ADODB.Recordset")
        SQL = " SELECT MB_POINT FROM " & dbname & "TB_MEMBER WHERE MB_ID='"& VAL1 &"' "
        frs.open sql, db, 1
        conset_userpoint = frs(0)
        frs.close

		If val1<>"" And val2<>"" And CDbl(val2)>0 then
            senduser_title = val4
            if instr(senduser_title,"추천인낙첨")>0 then
                senduser_title = left(senduser_title,instr(senduser_title,"추천인낙첨")-1)
                senduser_title = replace(right(senduser_title,instr(senduser_title,"]["))," ","")
                senduser_title = replace(senduser_title,"[","")
                senduser_title = replace(senduser_title,"]","")
            else
                senduser_title= ""
            end if

			uWdate = getDateTime(now())
			sqlC   = " userid,uFlag,uPoint,utitle,wdate,nowPoint,suserid "
			sqlV   = " '"& val1 &"','"& val3 &"','"& val2 &"','"& val4 &"','"& uWdate &"', "& ConSet_userpoint &", '"& senduser_title &"' "
			sql = "insert into " & dbname & "tb_member_uPointLog ("& sqlC &") values ( "& sqlV &" ) "
			db.execute sql

			If val3="1" then	'충전
				sql = "update " & dbname & "tb_member set mb_point = mb_point+"& val2 &" where mb_id = '"& val1 &"' "
				db.execute sql
			Else				'사용
				sql = "update " & dbname & "tb_member set mb_point = mb_point-"& val2 &" where mb_id = '"& val1 &"' "
				db.execute sql
			End If
		End if
	End Function

	
	Function subMemUpointLogs(val1,val2,val3,val4)	'아이디 / 사용포인트 / 가감구분(1+,2-) / 사용내역
        subMemUpointLogs_site(val1,val2,val3,val4,"")
	End Function

	Function subMemUpointLogs_ground(val1,val2,val3,val4)	'아이디 / 사용포인트 / 가감구분(1+,2-) / 사용내역
        subMemUpointLogs_site(val1,val2,val3,val4,"ground")
	End Function

	Function subMemUpointLogs_txco(val1,val2,val3,val4)	'아이디 / 사용포인트 / 가감구분(1+,2-) / 사용내역
        subMemUpointLogs_site(val1,val2,val3,val4,"txco")
	End Function
	
	Function subMemUpointLogs_slamdunk(val1,val2,val3,val4)	'아이디 / 사용포인트 / 가감구분(1+,2-) / 사용내역
        subMemUpointLogs_site(val1,val2,val3,val4,"slamdunk")
	End Function
	

	Function getZeroValue(val1)	'0붙임
		if val1 = "" or isnull(val1) then
			getZeroValue = val1
		else
			getZeroValue = Right("0"&val1,2)
		end if
	End Function

	Function getZeroValue2(val1)	'0뺌
		if val1 = "" or isnull(val1) then
			getZeroValue2 = val1
		Else
			If Left(val1,1)="0" Then
				getZeroValue2 = Right(val1,1)
			Else
				getZeroValue2 = val1
			End if
		end if
	End Function

	Function getWinNumSqlStep(val1)	'당첨시sql구분
		gnum = val1

		set rs = server.CreateObject("ADODB.Recordset")
		SQL = "select * from tb_game_dividendSet "
		rs.Open sql, db, 1
		dvS1	 = rs("dvS1")
		dvS2	 = rs("dvS2")
		dvS3	 = rs("dvS3")
		dvS2R	 = rs("dvS2R")
		dvS3R	 = rs("dvS3R")
		dvR2	 = rs("dvR2")
		dvR3	 = rs("dvR3")
		dvR4	 = rs("dvR4")
		dvR5	 = rs("dvR5")
		dvR6	 = rs("dvR6")
		dvR7	 = rs("dvR7")
		dvR8	 = rs("dvR8")
		rs.close
		'//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		sql1 = getWinNumSql("1",gnum,dvS1) & " union all "
		sql2 = getWinNumSql("2",gnum,dvS2) & " union all "
		sql3 = getWinNumSql("3",gnum,dvS3) & " union all "
		sql4 = getWinNumSql("4",gnum,dvS2R) & " union all "
		sql5 = getWinNumSql("5",gnum,dvS3R) & " union all "
		sql6 = getWinNumSql("6",gnum,dvR2) & " union all "
		sql7 = getWinNumSql("7",gnum,dvR3) & " union all "
		sql8 = getWinNumSql("8",gnum,dvR4) & " union all "
		sql9 = getWinNumSql("9",gnum,dvR5) & " union all "
		sql10 = getWinNumSql("10",gnum,dvR6) & " union all "
		sql11 = getWinNumSql("11",gnum,dvR7) & " union all "
		sql12 = getWinNumSql("12",gnum,dvR8) & " union all "
		sql13 = getWinNumSql("13",gnum,dvR8) & " union all "
		sql14 = getWinNumSql("14",gnum,dvR8) & " union all "
		sql15 = getWinNumSql("15",gnum,dvR8) & " union all "
		sql16 = getWinNumSql("16",gnum,dvR8) & " union all "
		sql17 = getWinNumSql("17",gnum,dvR8) & " union all "
		sql18 = getWinNumSql("18",gnum,dvR8) & " union all "
		sql19 = getWinNumSql("19",gnum,dvR8) & " union all "
		sql20 = getWinNumSql("20",gnum,dvR8) & " union all "
		sql21 = getWinNumSql("21",gnum,dvR8) & " union all "
		sql22 = getWinNumSql("22",gnum,dvR8) & " order by a.userid asc, a.orseq asc, b.gseq asc "
		'//
		getWinNumSqlStep = sql1 & sql2 & sql3 & sql4 & sql5 & sql6 & sql7 & sql8 & sql9 & sql10 & sql11 & sql12 & sql13 & sql14 & sql15 & sql16 & sql17 & sql18 & sql19 & sql20 & sql21 & sql22
		'//
	End Function

	Function getWinNumSql(val1,val2,val3)	'당첨시sql구분 val1:구분1~12, val2:회차키값, val3:배당배수
		gnum = val2

		sql = " select "
		sql = sql & " a.gmCode,a.orSeq,b.gSeq,b.gmKind,b.chCnt,(b.chCnt*1000) as orMoney,((b.chCnt*1000)*"& val3 &") as winMoney, "& val3 &" as winPer, a.userid,chnumber, "
		sql = sql & " b.chNumber1, b.chNumber2, b.chNumber3, b.chNumber4, b.chNumber5, b.chNumber6, b.chNumber7, b.chNumber8, b.sp_gmper "
		sql = sql & " from tb_order_info a "
		sql = sql & " left outer join (select * from tb_order_game where gmKind = '"& val1 &"') b on a.orSeq = b.orSeq  "
		sql = sql & " inner join (select * from tb_game_result) c on a.gmCode=c.gnum "

		If val1="1" Then	'직렬1
			sql = sql & " and chnumber1 = c.wnum1 "
		elseIf val1="2" Then	'직렬2
			sql = sql & " and chnumber1 = c.wnum1 and chnumber2 = c.wnum2 "
		elseIf val1="3" Then	'직렬3
			sql = sql & " and chnumber1 = c.wnum1 and chnumber2 = c.wnum2 and chnumber3 = c.wnum3 "
		elseIf val1="4" Then	'직렬임의2
			sql = sql & " and ( (chnumber1 = c.wnum1 or chnumber1 = c.wnum2) "
			sql = sql & "	and (chnumber2 = c.wnum1 or chnumber2 = c.wnum2) ) "
		elseIf val1="5" Then	'직렬임의3
			sql = sql & " and ( (chnumber1 = c.wnum1 or chnumber1 = c.wnum2 or chnumber1 = c.wnum3) "
			sql = sql & "	and (chnumber2 = c.wnum1 or chnumber2 = c.wnum2 or chnumber2 = c.wnum3) "
			sql = sql & "	and (chnumber3 = c.wnum1 or chnumber3 = c.wnum2 or chnumber3 = c.wnum3) ) "
		elseIf val1="6" Then	'임의2
			sql = sql & " and ( (chnumber1 = c.wnum1 or chnumber1 = c.wnum2 or chnumber1 = c.wnum3 or chnumber1 = c.wnum4 or chnumber1 = c.wnum5) "
			sql = sql & "	and (chnumber2 = c.wnum1 or chnumber2 = c.wnum2 or chnumber2 = c.wnum3 or chnumber2 = c.wnum4 or chnumber2 = c.wnum5) ) "
		elseIf val1="7" Then	'임의3
			sql = sql & " and ( (chnumber1 = c.wnum1 or chnumber1 = c.wnum2 or chnumber1 = c.wnum3 or chnumber1 = c.wnum4 or chnumber1 = c.wnum5) "
			sql = sql & "	and (chnumber2 = c.wnum1 or chnumber2 = c.wnum2 or chnumber2 = c.wnum3 or chnumber2 = c.wnum4 or chnumber2 = c.wnum5) "
			sql = sql & "	and (chnumber3 = c.wnum1 or chnumber3 = c.wnum2 or chnumber3 = c.wnum3 or chnumber3 = c.wnum4 or chnumber3 = c.wnum5) ) "
		elseIf val1="8" Then	'임의4
			sql = sql & " and ( (chnumber1 = c.wnum1 or chnumber1 = c.wnum2 or chnumber1 = c.wnum3 or chnumber1 = c.wnum4 or chnumber1 = c.wnum5) "
			sql = sql & "	and (chnumber2 = c.wnum1 or chnumber2 = c.wnum2 or chnumber2 = c.wnum3 or chnumber2 = c.wnum4 or chnumber2 = c.wnum5) "
			sql = sql & "	and (chnumber3 = c.wnum1 or chnumber3 = c.wnum2 or chnumber3 = c.wnum3 or chnumber3 = c.wnum4 or chnumber3 = c.wnum5) "
			sql = sql & "	and (chnumber4 = c.wnum1 or chnumber4 = c.wnum2 or chnumber4 = c.wnum3 or chnumber4 = c.wnum4 or chnumber4 = c.wnum5) ) "
		elseIf val1="9" Then	'임의5
			sql = sql & " and ( (chnumber1 = c.wnum1 or chnumber1 = c.wnum2 or chnumber1 = c.wnum3 or chnumber1 = c.wnum4 or chnumber1 = c.wnum5) "
			sql = sql & "	and (chnumber2 = c.wnum1 or chnumber2 = c.wnum2 or chnumber2 = c.wnum3 or chnumber2 = c.wnum4 or chnumber2 = c.wnum5) "
			sql = sql & "	and (chnumber3 = c.wnum1 or chnumber3 = c.wnum2 or chnumber3 = c.wnum3 or chnumber3 = c.wnum4 or chnumber3 = c.wnum5) "
			sql = sql & "	and (chnumber4 = c.wnum1 or chnumber4 = c.wnum2 or chnumber4 = c.wnum3 or chnumber4 = c.wnum4 or chnumber4 = c.wnum5) "
			sql = sql & "	and (chnumber5 = c.wnum1 or chnumber5 = c.wnum2 or chnumber5 = c.wnum3 or chnumber5 = c.wnum4 or chnumber5 = c.wnum5) ) "
		elseIf val1="10" Then	'임의6
			sql = sql & " and ( (chnumber1 = c.wnum1 or chnumber2 = c.wnum1 or chnumber3 = c.wnum1 or chnumber4 = c.wnum1 or chnumber5 = c.wnum1 or chnumber6 = c.wnum1) "
			sql = sql & "	and (chnumber1 = c.wnum2 or chnumber2 = c.wnum2 or chnumber3 = c.wnum2 or chnumber4 = c.wnum2 or chnumber5 = c.wnum2 or chnumber6 = c.wnum2) "
			sql = sql & "	and (chnumber1 = c.wnum3 or chnumber2 = c.wnum3 or chnumber3 = c.wnum3 or chnumber4 = c.wnum3 or chnumber5 = c.wnum3 or chnumber6 = c.wnum3) "
			sql = sql & "	and (chnumber1 = c.wnum4 or chnumber2 = c.wnum4 or chnumber3 = c.wnum4 or chnumber4 = c.wnum4 or chnumber5 = c.wnum4 or chnumber6 = c.wnum4) "
			sql = sql & "	and (chnumber1 = c.wnum5 or chnumber2 = c.wnum5 or chnumber3 = c.wnum5 or chnumber4 = c.wnum5 or chnumber5 = c.wnum5 or chnumber6 = c.wnum5) ) "
		elseIf val1="11" Then	'임의7
			sql = sql & " and ( (chnumber1 = c.wnum1 or chnumber2 = c.wnum1 or chnumber3 = c.wnum1 or chnumber4 = c.wnum1 or chnumber5 = c.wnum1 or chnumber6 = c.wnum1 or chnumber7 = c.wnum1) "
			sql = sql & "	and (chnumber1 = c.wnum2 or chnumber2 = c.wnum2 or chnumber3 = c.wnum2 or chnumber4 = c.wnum2 or chnumber5 = c.wnum2 or chnumber6 = c.wnum2 or chnumber7 = c.wnum2) "
			sql = sql & "	and (chnumber1 = c.wnum3 or chnumber2 = c.wnum3 or chnumber3 = c.wnum3 or chnumber4 = c.wnum3 or chnumber5 = c.wnum3 or chnumber6 = c.wnum3 or chnumber7 = c.wnum3) "
			sql = sql & "	and (chnumber1 = c.wnum4 or chnumber2 = c.wnum4 or chnumber3 = c.wnum4 or chnumber4 = c.wnum4 or chnumber5 = c.wnum4 or chnumber6 = c.wnum4 or chnumber7 = c.wnum4) "
			sql = sql & "	and (chnumber1 = c.wnum5 or chnumber2 = c.wnum5 or chnumber3 = c.wnum5 or chnumber4 = c.wnum5 or chnumber5 = c.wnum5 or chnumber6 = c.wnum5 or chnumber7 = c.wnum5) ) "
		elseIf val1="12" Then	'임의8
			sql = sql & " and ( (chnumber1 = c.wnum1 or chnumber2 = c.wnum1 or chnumber3 = c.wnum1 or chnumber4 = c.wnum1 or chnumber5 = c.wnum1 or chnumber6 = c.wnum1 or chnumber7 = c.wnum1 or chnumber8 = c.wnum1) "
			sql = sql & "	and (chnumber1 = c.wnum2 or chnumber2 = c.wnum2 or chnumber3 = c.wnum2 or chnumber4 = c.wnum2 or chnumber5 = c.wnum2 or chnumber6 = c.wnum2 or chnumber7 = c.wnum2 or chnumber8 = c.wnum2) "
			sql = sql & "	and (chnumber1 = c.wnum3 or chnumber2 = c.wnum3 or chnumber3 = c.wnum3 or chnumber4 = c.wnum3 or chnumber5 = c.wnum3 or chnumber6 = c.wnum3 or chnumber7 = c.wnum3 or chnumber8 = c.wnum3) "
			sql = sql & "	and (chnumber1 = c.wnum4 or chnumber2 = c.wnum4 or chnumber3 = c.wnum4 or chnumber4 = c.wnum4 or chnumber5 = c.wnum4 or chnumber6 = c.wnum4 or chnumber7 = c.wnum4 or chnumber8 = c.wnum4) "
			sql = sql & "	and (chnumber1 = c.wnum5 or chnumber2 = c.wnum5 or chnumber3 = c.wnum5 or chnumber4 = c.wnum5 or chnumber5 = c.wnum5 or chnumber6 = c.wnum5 or chnumber7 = c.wnum5 or chnumber8 = c.wnum5) ) "

		elseIf val1="13" Then	'직렬1 스페셜 홀짝
			sql = sql & " and chnumber1 = c.s1_wnum1"
		elseIf val1="14" Then	'직렬1 스페셜 대소
			sql = sql & " and chnumber1 = c.s2_wnum1"

		elseIf val1="15" Then	'직렬2 스페셜 홀짝
			sql = sql & " and chnumber1 = c.s1_wnum1 and chnumber2 = c.s1_wnum2 "
		elseIf val1="16" Then	'직렬2 스페셜 대소
			sql = sql & " and chnumber1 = c.s2_wnum1 and chnumber2 = c.s2_wnum2"

		elseIf val1="17" Then	'직렬3 스페셜 홀짝
			sql = sql & " and chnumber1 = c.s1_wnum1 and chnumber2 = c.s1_wnum2 and chnumber3 = c.s1_wnum3 "
		elseIf val1="18" Then	'직렬3 스페셜 대소
			sql = sql & " and chnumber1 = c.s2_wnum1 and chnumber2 = c.s2_wnum2 and chnumber3 = c.s2_wnum3 "

		elseIf val1="19" Then	'직렬4 스페셜 홀짝
			sql = sql & " and chnumber1 = c.s1_wnum1 and chnumber2 = c.s1_wnum2 and chnumber3 = c.s1_wnum3 and chnumber4 = c.s1_wnum4 "
		elseIf val1="20" Then	'직렬4 스페셜 대소
			sql = sql & " and chnumber1 = c.s2_wnum1 and chnumber2 = c.s2_wnum2 and chnumber3 = c.s2_wnum3 and chnumber4 = c.s2_wnum4 "

		elseIf val1="21" Then	'직렬5 스페셜 홀짝
			sql = sql & " and chnumber1 = c.s1_wnum1 and chnumber2 = c.s1_wnum2 and chnumber3 = c.s1_wnum3 and chnumber4 = c.s1_wnum4 and chnumber5 = c.s1_wnum5 "
		elseIf val1="22" Then	'직렬5 스페셜 대소
			sql = sql & " and chnumber1 = c.s2_wnum1 and chnumber2 = c.s2_wnum2 and chnumber3 = c.s2_wnum3 and chnumber4 = c.s2_wnum4 and chnumber5 = c.s2_wnum5 "
		End If

		sql = sql & " where a.gmCode = "& gNum &" "

		getWinNumSql = sql
	End Function

	Function getLoseNumSql(val1)	'낙첨시sql구분
		sql = " select "
		sql = sql & " a.gmCode,a.orSeq,b.gSeq,b.gmKind,b.chCnt,(b.chCnt*1000) as orMoney,a.userid, b.winFlag, c.mb_bbsicon, d.mb_cwinflag, "
		sql = sql & " (CASE c.mb_bbsicon WHEN 1 THEN ((b.chCnt*1000)*0.01) WHEN 2 THEN ((b.chCnt*1000)*0.02) WHEN 3 THEN ((b.chCnt*1000)*0.03) WHEN 4 THEN ((b.chCnt*1000)*0.04) WHEN 5 THEN ((b.chCnt*1000)*0.05) else 0 END) as memAddPoint, "
		sql = sql & " (CASE d.mb_cwinflag WHEN 1 THEN  (1000*0.01)*b.chCnt WHEN 2 THEN  (1000*0.02)*b.chCnt WHEN 3 THEN  (1000*0.03)*b.chCnt WHEN 4 THEN  (1000*0.04)*b.chCnt WHEN 5 THEN  (1000*0.05)*b.chCnt WHEN 6 THEN  (1000*0.06)*b.chCnt WHEN 7 THEN  (1000*0.07)*b.chCnt WHEN 8 THEN  (1000*0.08)*b.chCnt WHEN 9 THEN  (1000*0.09)*b.chCnt WHEN 10 THEN (1000*0.1)*b.chCnt else 0 END) as chuAddPoint, c.mb_cid "
        SQL = SQL & "        ,(ISNULL(E.MEM_LOSE_PER,0)/CONVERT(FLOAT,100)) MEM_LOSE_PER "
        SQL = SQL & "        ,(ISNULL(F.CMEM_LOSE_PER,0)/CONVERT(FLOAT,100)) CMEM_LOSE_PER "
		sql = sql & " from tb_order_info a "
		sql = sql & " left outer join (select * from tb_order_game where winFlag = '0') b on a.orSeq = b.orSeq  "
		sql = sql & " left outer join tb_member c on a.userid = c.mb_id  "
		sql = sql & " left outer join tb_member d on c.mb_cid = d.mb_id  "
        SQL = SQL & "   LEFT OUTER JOIN TB_MEMBER_LEV_REG E ON c.MB_BBSICON = e.LEV_IDX "
        SQL = SQL & "   LEFT OUTER JOIN TB_MEMBER_LEV_REG F ON d.MB_BBSICON = f.LEV_IDX "
		sql = sql & " where a.gmCode = "& val1 &" "
		sql = sql & " and b.winFlag='0' "
		sql = sql & " order by a.userid asc, a.orseq asc, b.gseq asc "
		
		getLoseNumSql = sql
	End Function


	Function getGameTitle(val1,val2)	'val1 구분 1: 숫자==>한글 | 2: 한글==>숫자
		aaVal    = "직렬1,직렬2,직렬3,직렬2임의,직렬3임의,임의2,임의3,임의4,임의5,임의6,임의7,임의8,직렬1 스페셜 홀짝,직렬1 스페셜 대소,직렬2 스페셜 홀짝,직렬2 스페셜 대소,직렬3 스페셜 홀짝,직렬3 스페셜 대소,직렬4 스페셜 홀짝,직렬4 스페셜 대소,직렬5 스페셜 홀짝,직렬5 스페셜 대소,직렬6 스페셜 홀짝,직렬6 스페셜 대소"

		bbVal    = "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22"
		aaValArr = Split(aaVal,",")
		bbValArr = Split(bbVal,",")
		'//
		val2 = CDbl(val2)-1
		If val1="1" Then
			getGameTitle = Trim(aaValArr(val2))
		ElseIf val1="2" Then
			getGameTitle = Trim(bbValArr(val2))
		End if
	End Function

	Function getWinTime(val1,val2)
		SQL = "select gstime from tb_game_ymdt Where gday = '"& val1 &"' And gno = "& val2 &" "
		set rs = Db.Execute(sql)
		If Not rs.eof Then
			imsival = rs(0)
		Else
			imsival = ""
		end If
		rs.close
		'//
		getWinTime = imsival
	End Function

	Function getImageChange(val1,val2)
		If val2<>"" then
			imgArr = Split(val2,",")

			If val1="1" Then
				imgGubun = "W"
			ElseIf val1="2" Then
				imgGubun = "O"
			End If

			For k=0 To ubound(imgArr)
				imsival = Trim(imgArr(k))
				If Len(imsival)=1 Then imsival = "0" & imsival
				imsiImgval = imsiImgval & "<img src='/images/gongcolar/icon_ball_"& imgGubun &"_"& imsival &".png' alt='"& imsival &"'/>"
			next
		Else
			imsiImgval = "<img src='/images/gongcolar/icon_ball_O_no.png' /><img src='/images/gongcolar/icon_ball_O_no.png' /><img src='/images/gongcolar/icon_ball_O_no.png' /><img src='/images/gongcolar/icon_ball_O_no.png' /><img src='/images/gongcolar/icon_ball_O_no.png' />"
		End If

		getImageChange = imsiImgval
	End Function

	Function getImageChange_sub(val1,val2)	'val2 소,대,소,소,대
		val1      = CDbl(val1)
		imsicolor = "red,yellow,green,blue,purple"
		imsiimgnm = ""
		If val2<>"" then
			funVal2Arr  = Split(val2,",")
			funcolorArr = Split(imsicolor,",")
			If val1=13 Or val1=15 Or val1=17 Or val1=19 Or val1=21 then	'홀작
				For fi = 0 To ubound(funVal2Arr)
					orval        = Trim(funVal2Arr(fi))
					orcolor      = Trim(funcolorArr(fi))
					odd_even_val = "odd"
					If orval="짝" Then odd_even_val = "even"
					imsiimgnm = imsiimgnm & "<img src='/images/special/colorball_28_"& orcolor &"_"& odd_even_val &".png' width='28' height='28'>"
				next
			ElseIf val1=14 Or val1=16 Or val1=18 Or val1=20 Or val1=22 Then	'대소
				For fi = 0 To ubound(funVal2Arr)
					orval        = Trim(funVal2Arr(fi))
					orcolor      = Trim(funcolorArr(fi))
					odd_even_val = "big"
					If orval="소" Then odd_even_val = "small"
					imsiimgnm = imsiimgnm & "<img src='/images/special/colorball_28_"& orcolor &"_"& odd_even_val &".png' width='28' height='28'>"
				Next
			'else
			End if
		End if
		
		getImageChange_sub = imsiimgnm
	End Function

	Function getWinFlagChange(val1,val2,val3)	'당첨여부 0:발표전 5:당첨 9:낙첨, 당첨시간, 회차
		wintime = Replace(val2,":","")
		windate = "20" & Left(val3,6) & wintime
		nowdate = getDateTime(now())
		
		If val1="0" Then
			If nowdate < windate Then
				imgGubun = "추첨중"
			Else
				imgGubun = "결과입력중"
			End if
		ElseIf val1="5" Then
			imgGubun = "당첨"
		ElseIf val1="9" Then
			imgGubun = "미당첨"
		End If

		getWinFlagChange = imgGubun
	End Function

	Function getBoardLevel(val1,val2)	'게시판 회원레벨별 아이콘 0~5,관리자 val2:가상아이디이기때문
		If val2="" then
			If val1="" Or val1="0" Or isnull(val1) then
				imsival = "<img src='/images/board/level_s.gif' width='20' height='20' align='absmiddle' />"
			Else
				imsival = "<img src='/images/board/level_"& val1 &".gif' width='20' height='20' align='absmiddle' />"
			End If
		Else
			If val2="0" then
				imsival = "<img src='/images/board/level_s.gif' width='20' height='20' align='absmiddle' />"
			elseIf val2="9" then
				imsival = "<img src='/images/board/level_a.gif' width='20' height='20' align='absmiddle' />"
			Else
				imsival = "<img src='/images/board/level_"& val2 &".gif' width='20' height='20' align='absmiddle' />"
			End If
		End If

		getBoardLevel = imsival
	End Function


	function lwRtrimByte(str, n)
		for fi=1 to len(str)
			charat=mid(str, fi, 1)
            if asc(charat)>0 and asc(charat)<255 then
                  wLen=wLen+1
            else
                  wLen=wLen+2
            end if
            rLen = rLen + 1
            if wLen >= n then
                lwRtrimByte = left(str, rLen - 2) &".."
                  exit for
            end if
		next
        if lwRtrimByte = empty then
              lwRtrimByte = str
        end if
	end Function

	function getMoneyIngflag(val1, val2)
		'0:처리중, 1:대기중, 5:충전(환전)완료, 7:머니추가(삭감),8:포인트전환, 9:취소
		If val1="1" Then
			txtflag1 = "충전"
			txtflag2 = "추가"
		Else
			txtflag1 = "환전"
			txtflag2 = "삭감"
		End If

		If val2="0" Then
			imsival = "처리중"
		ElseIf val2="1" Then
			imsival = "대기중"
		ElseIf val2="5" Then
			imsival = txtflag1 & "완료"
		ElseIf val2="7" Then
			imsival = "머니" & txtflag2
		ElseIf val2="8" Then
			imsival = "포인트전환"
		ElseIf val2="9" Then
			imsival = "취소"
		End if

		getMoneyIngflag = imsival
	end Function

	function getAgentPayReg(val1, val2, val3)	'val1 - c:충전 / h:환전 / j:정산, val2 - 금액 - c:충전 / h:환전 / j:정산, val3 - id
		'//
		set rs = server.CreateObject("ADODB.Recordset")
		SQL = "select mb_cid from tb_member where mb_id = '"& val3 &"' "
		rs.Open sql, db, 1
		mb_cid = rs(0)	'총판아이디
		rs.close
		'//
		set rs = server.CreateObject("ADODB.Recordset")
		SQL = "select sper from tb_agent where userid = (select mb_cid from tb_member where mb_id = '"& val3 &"') and aingflag = 'y' and sper >0 "
		rs.Open sql, db, 1
		If Not rs.eof then
			sper = rs(0)
		Else
			sper = 0
		End if
		rs.close
		'//
		If mb_cid <> "" And sper > 0 Then
			'//
			wdate  = getDateTime(now())
			smoney = CDbl((sper/100)*CDbl(val2))
			sqlc   = "userid,suserid,flag,pmoney,wdate,pper,smoney"
			sqlv   = " '"& mb_cid &"','"& val3 &"','"& val1 &"',"& val2 &",'"& wdate &"',"& sper &","& smoney &" "
			'//
			sql = " insert into tb_agent_pay ("& sqlc &") values ("& sqlv &") "
'Response.write sql
			db.execute sql
		End If
		'//
	end Function

	function getWritePoint(val1, val2, val3 ,val4, val5)	'구분/yn/회수/포인트/아이디
		If val2="y" then
			imsinow = Left(getDateTime(now()),8)
			'//
			If val1="1" Then
				imsitbname = "tb_board"
				imsiidx    = "idx"
				imsititle  = "게시판글작성!!"
                imsiflag   = "B"
			Else
				imsitbname = "tb_board_ment"
				imsiidx    = "idx"
				imsititle  = "댓글작성!!"
                imsiflag   = "M"
			End If
			'//
'			set rs = server.CreateObject("ADODB.Recordset")
'			SQL = "select isnull(count("&imsiidx&"),0) as writeCnt from "&imsitbname&" where userid = '"& val5 &"' and substring(wdate,1,8) = '"& imsinow &"' "
'			rs.Open sql, db, 1
'			writeCnt = rs(0)
'			rs.close
    		imsitbname = "tb_board_write_cnt"
			set rs = server.CreateObject("ADODB.Recordset")
			SQL = "select isnull(count("&imsiidx&"),0) as writeCnt from "&imsitbname&" where flag = '"& imsiflag &"' and userid = '"& val5 &"' and substring(wdate,1,8) = '"& imsinow &"' "
'response.Write sql
'response.end

			rs.Open sql, db, 1
			writeCnt = rs(0)
			rs.close
			'//
			If writeCnt<CDbl(val3) then
				Call subMemUpointLog(val5,val4,"1",imsititle)	'아이디 / 사용포인트 / 가감구분(1+,2-) / 사용내역
				Call subMemUpointLog_c(val5,val4,"1",imsititle)	'아이디 / 사용포인트 / 가감구분(1+,2-) / 사용내역
			End if
			'//

			'//
		End if
	end Function

	function getOddEvenChb(val1, val2)
		'//
		valArr = Split(val2,",")
		'//
		If val1="1" Then
			For fi=0 To ubound(valArr)
				valArrval = Trim(valArr(fi))
				imsiaa = "홀"
				If CDbl(valArrval) Mod 2 = 0 Then imsiaa = "짝"
				'//
				If totimsiaa="" then
					totimsiaa = imsiaa
				Else
					totimsiaa = totimsiaa &","& imsiaa
				End if
			next
		Else
			For fi=0 To ubound(valArr)
				valArrval = CDbl(Trim(valArr(fi)))
				imsiaa = "대"
				If valArrval<=5 Then imsiaa = "소"
				'//
				If totimsiaa="" then
					totimsiaa = imsiaa
				Else
					totimsiaa = totimsiaa &","& imsiaa
				End if
			next
		End If
		'//
		getOddEvenChb = totimsiaa
	end Function

   	function getSpoGameKind(val1)
		select case val1
            case "J"
                getSpoGameKind = "종목 등록 및 수정"
            case "T"
                getSpoGameKind = "TEAM 등록 및 수정"
            case "L"
                getSpoGameKind = "리그 등록 및 수정"
            case "N"
                getSpoGameKind = "국가 등록 및 수정"
        end select
	end Function

   	SUB leagueRadioBtn(val1,val2)
        If val1<>"" Then
            '//
            if val2="" then checked=" checked"
            response.Write "<input type='radio' name='"& val1 &"' value='' "& checked &" >전체 &nbsp; "
            '//
            set frs = server.CreateObject("ADODB.Recordset")
            SQL = "SELECT SPO_CODE,SPO_NAME FROM TB_SPO_KIND WHERE SPO_FLAG = 'J' ORDER BY SPO_NAME ASC "
            frs.Open sql, db, 1
            Do until frs.eof
                 checked=""
                if val2=frs(0) then checked=" checked"
                response.Write "<input type='radio' name='"& val1 &"' value='"& frs(0) &"' "& checked &" >"& frs(1) &" &nbsp; "
            frs.movenext
            Loop
            frs.close
            '//
        End if
	END SUB

   	SUB jmkSelectBox(val1,val2,val3)
        If val1<>"" Then
            '//
            response.Write "<select name='"& val1 &"' "& val3 &" >"
            response.Write "<option value=''>-종목전체-"
            '//
            set frs = server.CreateObject("ADODB.Recordset")
            SQL = "SELECT SPO_CODE,SPO_NAME FROM TB_SPO_KIND WHERE SPO_FLAG = 'J' ORDER BY SPO_NAME ASC "
            frs.Open sql, db, 1
            Do until frs.eof
                 selected=""
                if val2=frs(0) then selected=" selected"
                response.Write "<option name='"& val1 &"' value='"& frs(0) &"' "& selected &" >"& frs(1)
            frs.movenext
            Loop
            frs.close
            '//
            response.Write "</select>"
            '//
        End if
	END SUB

   	SUB lgeSelectBox(val1,val2,val3)
        If val1<>"" Then
            '//
            response.Write "<select name='"& val1 &"' "& val3 &" >"
            response.Write "<option value=''>-리그전체-"
            '//
            set frs = server.CreateObject("ADODB.Recordset")
            SQL = "SELECT SPO_CODE,SPO_NAME FROM TB_SPO_KIND WHERE SPO_FLAG = 'L' ORDER BY SPO_NAME ASC "
            frs.Open sql, db, 1
            Do until frs.eof
                 selected=""
                if val2=frs(0) then selected=" selected"
                response.Write "<option name='"& val1 &"' value='"& frs(0) &"' "& selected &" >"& frs(1)
            frs.movenext
            Loop
            frs.close
            '//
            response.Write "</select>"
            '//
        End if
	END SUB

   	SUB selIngWrite(val1,val2,val3)
        If val1<>"" Then
            '//
            if val2="10" then selected1 = " selected"
            if val2="20" then selected2 = " selected"
            if val2="50" then selected3 = " selected"
            if val2="60" then selected4 = " selected"
            if val2="99" then selected5 = " selected"
            '//
            response.Write "<select name='"& val1 &"' "& val3 &" >"
            response.Write "<option value=''>-진행상태전체-"
            '//
            response.Write "<option value='10' "& selected1 &" >베팅대기"
            response.Write "<option value='20' "& selected2 &" >베팅중"
            response.Write "<option value='50' "& selected3 &" >베팅마감"
            response.Write "<option value='60' "& selected4 &" >결과입력"
            response.Write "<option value='99' "& selected5 &" >경기취소"
            '//
            response.Write "</select>"
            '//
        End if
	END SUB

   	SUB lgeSelectBoxAjax(val1,val2,val3,val4,val5,val6)
        If val1<>"" Then
            '//
            response.Write "<select name='"& val1 &"' "& val3 &" >"
            response.Write "<option value=''>-리그선택-"
            '//
            SET FRS = SERVER.CREATEOBJECT("ADODB.RECORDSET")
            SQL = "  EXEC    Get_LeagueSelBox "
            SQL = SQL & " @val4	 = '"&rqVal(val4)&"'"
            SQL = SQL & " ,@val5	 = '"&rqVal(val5)&"'"
            FRS.OPEN SQL, DB, 1
            Do until frs.eof
                spo_bonus = frs("spo_bonus")
                selected=""
                if val2=frs(0) then selected=" selected"
                response.Write "<option name='"& val1 &"' value='"& frs(0) &"' "& selected &" >"& frs(1)
            frs.movenext
            Loop
            frs.close
            '//
            response.Write "</select>"
            '//
            if spo_bonus="Y" then
                if val6="" then val6=0
                response.Write "<select name='gm_bonuscnt' >"
                response.Write "<option value=''>-조합수 선택-"
                '//
                for f=1 to 10
                    selected = ""
                    if cdbl(val6)=cdbl(f) then selected = " selected"
                    response.Write "<option value='"& f &"' "& selected &" >"& f &"개 조합"
                next
                '//
                response.Write "</select> * 보너스배당 제외"
            end if
            '//
        End if
	END SUB

   	SUB natSelectBox(val1,val2,val3)
        If val1<>"" Then
            '//
            response.Write "<select name='"& val1 &"' "& val3 &" >"
            response.Write "<option value=''>-국가선택-"
            '//
            set frs = server.CreateObject("ADODB.Recordset")
            SQL = "SELECT SPO_CODE,SPO_ENAME FROM TB_SPO_KIND WHERE SPO_FLAG = 'N' ORDER BY SPO_NAME ASC "
            frs.Open sql, db, 1
            Do until frs.eof
                 selected=""
                if val2=frs(0) then selected=" selected"
                response.Write "<option name='"& val1 &"' value='"& frs(0) &"' "& selected &" >"& frs(1)
            frs.movenext
            Loop
            frs.close
            '//
            response.Write "</select>"
            '//
        End if
	END SUB

   	SUB teamSelectBox(val1,val2,val3,val4,val5,val6)
        'response.Write val2&"-"&val5&"-"&val6
        If val1<>"" Then
            '//
            response.Write "<select name='"& val1 &"' id='"& val1 &"' "& val3 &" >"
            response.Write "<option value=''>" & val4
            response.Write "<option value='newnew'>신규등록"
            '//
            SET FRS = SERVER.CREATEOBJECT("ADODB.RECORDSET")
            SQL = "  EXEC    Get_teamSelectBox "
            SQL = SQL & " @val5	 = '"&rqVal(val5)&"'"
            SQL = SQL & " ,@val6	 = '"&rqVal(val6)&"'"
            FRS.OPEN SQL, DB, 1
            Do until frs.eof
                selected=""
                if val2=frs(0) then selected=" selected"
                response.Write "<option name='"& val1 &"' value='"& frs(0) &"' "& selected &" >"& frs(1)
            frs.movenext
            Loop
            frs.close
            '//
            response.Write "</select>"
            '//
        End if
	END SUB

   	FUNCTION getGmStateRe(val1)
		select case val1
            case "10"
                getGmStateRe = "<font color=black>베팅대기</font>"
            case "20"
                getGmStateRe = "<font color=red>베팅중</font>"
            case "50"
                getGmStateRe = "<font color=blue>베팅마감</font>"
            case "60"
                getGmStateRe = "<font color=blue>경기종료</font>"
            case "99"
                getGmStateRe = "<font color=green>경기취소</font>"
        end select
	END FUNCTION

   	FUNCTION getListSql_main(val1)
		sql = "SELECT TOP 10 A.*, ISNULL(H.GM_IDX,0) CART_GM_IDX, ISNULL(H.GCT_BTFLAG,'') CART_BTTFLAG FROM vw_SpoGames A "
        sql = sql & " LEFT OUTER JOIN TB_SPO_CART H ON A.GM_IDX = H.GM_IDX AND H.USERID = '"& rqVal(SESSION("USERID")) &"' AND SESSIONID = '"& rqVal(SESSION.SESSIONID) &"' "
        sql = sql & "      WHERE A.GM_idx > 0 and A.GM_JCODE not in ('KK', 'sc','JJ','dari','ddos','power','vs', 'ald','bg') "

	    if val1="60" then
			sql = sql & "    AND A.GM_STATE in ('60','99')  "
	    else 
	    	sql = sql & "    AND A.GM_STATE='"& rqVal(val1) &"' "
	    end if
	        
	    if val1="60" then
	    	sql = sql & "    AND SUBSTRING(A.GM_DATETIME,1,8) >= "& replace(left(now()-7,10),"-","") &" "
	    else
	    	if val1<>"50" then
	            sql = sql & "    AND SUBSTRING(A.GM_DATETIME,1,8) >= '"& Mid(rqVal(ConSet_boardNowDate),1,8) &"' "
	        end if
	    end if

        sql = sql & "   ORDER BY A.GM_DATETIME ASC"
        getListSql_main = sql
	END FUNCTION
	
	
	
   	FUNCTION getListSql_crs(val1,val2, itop)
	   	if itop = "" then itop = 500
		sql = "SELECT TOP " & itop & " A.*, ISNULL(H.GM_IDX,0) CART_GM_IDX, ISNULL(H.GCT_BTFLAG,'') CART_BTTFLAG FROM vw_SpoGames A "
        sql = sql & " LEFT OUTER JOIN TB_SPO_CART H ON A.GM_IDX = H.GM_IDX AND H.USERID = '"& rqVal(SESSION("USERID")) &"' AND SESSIONID = '"& rqVal(SESSION.SESSIONID) &"' "

        if GM_FLAG="10" or GM_FLAG="11" then
            sql = sql & "      WHERE A.GM_FLAG in ('10','11') "
        else
            sql = sql & "      WHERE A.GM_FLAG='"& rqVal(GM_FLAG) &"' "
        end if

        if val1="60" then
            sql = sql & "    AND (A.GM_STATE in ('60','99') ) "
        else
            sql = sql & "    AND (A.GM_STATE='"& rqVal(val1) &"') "
        end if

        if val1="60" then
            sql = sql & "    AND SUBSTRING(A.GM_DATETIME,1,8) >= '"& replace(left(now()-7,10),"-","") &"' "
        elseif val1<>"50" then
            sql = sql & "    AND SUBSTRING(A.GM_DATETIME,1,8) >= '"& rqVal(GMDATE) &"' "
        end if

        if val2<>"" then
            sql = sql & "    AND gm_jcode='" & val2 & "' "
        else
            sql = sql & "    AND gm_jcode not in ('kk','JJ','dari','vs','power','ald','bg') "
        end if

        sql = sql & "   ORDER BY A.GM_DATETIME ASC, A.LEAGUE_NM ASC, A.SPOKIND_NM ASC, A.HTEAM_NM ASC, A.ATEAM_NM ASC, A.GM_HNDFLAG ASC "

        getListSql_crs = sql
	END FUNCTION
	
   	FUNCTION getListSql(val1)
        getListSql = getListSql_crs(val1,"", 500)
	END FUNCTION	
	
   	FUNCTION getListSql_sdi(val1,val2)
        getListSql_sdi = getListSql_crs(val1,"KK", 90)
	END FUNCTION

   	FUNCTION getListSql_dari(val1,val2)
        getListSql_dari = getListSql_crs(val1,"dari", 90)
	END FUNCTION

   	FUNCTION getListSql_dal(val1,val2)
        getListSql_dal = getListSql_crs(val1,"JJ", 90)
	END FUNCTION

   	FUNCTION getListSql_bg(val1,val2)
        getListSql_bg = getListSql_crs(val1,"bg", 90)
	END FUNCTION

   	FUNCTION getListSql_vc(val1,val2)
	   	getListSql_vc = getListSql_crs(val1,val2, 90)
	END Function

   	FUNCTION getListSql2(val1,val2)
		sql = "SELECT TOP 500 A.* FROM vw_SpoGames A WHERE A.GM_FLAG='"& rqVal(GM_FLAG) &"' AND A.GM_JCODE not in ('ald') "
        if val1="60" then
            sql = sql & "    AND (A.GM_STATE in ('60','99') ) "
        else
            sql = sql & "    AND (A.GM_STATE='"& rqVal(val1) &"') "
        end if

        sql = sql & "    AND SUBSTRING(A.GM_DATETIME,1,8) = '"& rqVal(val2) &"' "
        sql = sql & "   ORDER BY A.GM_DATETIME DESC, A.LEAGUE_NM ASC, A.SPOKIND_NM ASC, A.HTEAM_NM ASC, A.ATEAM_NM ASC, A.GM_HNDFLAG ASC "

        getListSql2 = sql
	END FUNCTION


    FUNCTION FloatCalcul(tmp_x, tmp_y, tmp_cal)

        tmp_x = trim(tmp_x)
        tmp_y = trim(tmp_y)


        tmp_x_count = 1
        if inStr(tmp_x,".") > 0 then
            tmpArr = Trim(Mid(tmp_x,inStr(tmp_x,".") + 1))

            for tmp_Loop_cnt = 1 to Len(tmpArr)
                tmp_x_count = tmp_x_count * 10
            Next
        End if

        tmp_y_count = 1
        if inStr(tmp_y,".") > 0 then
            tmpArr = Trim(Mid(tmp_y,inStr(tmp_y,".") + 1))

            for tmp_Loop_cnt = 1 to Len(tmpArr)
                tmp_y_count = tmp_y_count * 10
            Next
        End if

        if tmp_cal = "+" or tmp_cal = "-" then
            if tmp_x_count > tmp_y_count then
                tmp_y_count = tmp_x_count
            elseif tmp_x_count < tmp_y_count then
                tmp_x_count = tmp_y_count
            end if
        end if

        tmp_x = round(CCur(tmp_x) * tmp_x_count,0)
        tmp_y = round(CCur(tmp_y) * tmp_y_count,0)


        execute("tmp_rtn_value = tmp_x " + tmp_cal + " tmp_y")
        tmp_rtn_value = tmp_rtn_value / tmp_x_count
        tmp_rtn_value = tmp_rtn_value / tmp_y_count

        FloatCalcul = tmp_rtn_value

    END FUNCTION

   	function getGmflagRe(val1)
		select case val1
            case "10"
                getGmflagRe = "핸디캡"
            case "11"
                getGmflagRe = "승무패"
            case "12"
                getGmflagRe = "스페셜1"
            case "13"
                getGmflagRe = "스페셜2"
        end select
	end Function

   	function getbttflagRe(val1)
		select case val1
            case "H"
                getbttflagRe = "홈팀"
            case "T"
                getbttflagRe = "무승부"
            case "A"
                getbttflagRe = "원정팀"
        end select
	END FUNCTION

    FUNCTION getBttlist(val1,val2)
        SQL = "     SELECT A.WDATE, "
        SQL = SQL & " 	   C.MB_ID, "
        SQL = SQL & " 	   C.MB_NICK, "
        SQL = SQL & " 	   B.BTS_DIVIDEND, "
        SQL = SQL & " 	   B.BTS_CHFLAG, "
        SQL = SQL & " 	   B.BTS_CHMONEY "
        SQL = SQL & "   FROM TB_SPO_BETTING_MASTER A, "
        SQL = SQL & "        TB_SPO_BETTING_INFO B, "
        SQL = SQL & "        TB_MEMBER C "
        SQL = SQL & "  WHERE A.BTT_IDX=B.BTT_IDX "
        SQL = SQL & "    AND A.USERID = C.MB_ID "
        SQL = SQL & "    AND B.GM_IDX = "& val1 &" "
        SQL = SQL & "    AND B.BTS_CHFLAG = '"& val2 &"' "
        SQL = SQL & "  ORDER BY A.WDATE DESC "
        '//
        getBttlist = sql
        '//
    END FUNCTION

    FUNCTION getBttTrHtml(val1,val2,val3,val4,val5,val6)
        '//
        if val6="당첨" then val6="<font color=red>당첨"
        '//
        haptrhtm = "<tr height=35 bgcolor=#4B4B4B> "
        haptrhtm = haptrhtm & "    <td colspan=20> "
        haptrhtm = haptrhtm & "    <table border=0 cellpadding=0 cellspacing=0 width='100%'> "
        haptrhtm = haptrhtm & "        <tr bgcolor=#4B4B4B> "
        haptrhtm = haptrhtm & "            <td width='21%'>&nbsp;<font color=white><B>배팅날짜   : <font color=#B4FF00>"& val1 &"</td> "
        haptrhtm = haptrhtm & "            <td width='16%'><font color=white><B>배팅금액   : <font color=#B4FF00>"& formatnumber(val2,0) &"</font></td> "
        haptrhtm = haptrhtm & "            <td width='16%'><font color=white><B>배당률     : <font color=#B4FF00>"& FORMATNUMBER(val3,2) &"</font></td> "
        haptrhtm = haptrhtm & "            <td width='21%'><font color=white><B>예상배당금 : <font color=#B4FF00>"& formatnumber(val4,0) &"</font></td> "
        haptrhtm = haptrhtm & "            <td width='21%'><font color=white><B>당첨금 : <font color=#B4FF00><span id='jsmoney2_"& btt_idx &"'>0</span></font></td> "
        'haptrhtm = haptrhtm & "            <td width='16%'><font color=white><B>당첨금     : <font color=#B4FF00>"& val5 &"</font></td> "
        haptrhtm = haptrhtm & "            <td width='10%' align=right><font color=white><B><font color=#B4FF00>"& val6 &"</font>&nbsp;</td> "
        haptrhtm = haptrhtm & "        </tr> "
        haptrhtm = haptrhtm & "    </table> "
        haptrhtm = haptrhtm & "    </td> "
        haptrhtm = haptrhtm & "</tr> "
        haptrhtm = haptrhtm & "<tr height=10 bgcolor=white> "
        haptrhtm = haptrhtm & "    <td colspan=20></td> "
        haptrhtm = haptrhtm & "</tr> "
        '//
        getBttTrHtml = haptrhtm
        '//
    END FUNCTION

    FUNCTION getBttTrHtml_mypage(val1,val2,val3,val4,val5,val6)
        '//
        if val6="당첨" then val6="<font color=red>당첨"
        '//
        haptrhtm = "<tr height=35 bgcolor=#4B4B4B> "
        haptrhtm = haptrhtm & "    <td colspan=20> "
        haptrhtm = haptrhtm & "    <table border=0 cellpadding=0 cellspacing=0 width='100%' bgcolor=red> "
        haptrhtm = haptrhtm & "        <tr bgcolor=#4B4B4B align=left> "
        haptrhtm = haptrhtm & "            <td width='20%'>&nbsp;<font color=white><B>배팅날짜   : <font color=#B4FF00>"& val1 &"</td> "
        haptrhtm = haptrhtm & "            <td width='16%'><font color=white><B>배팅금액   : <font color=#B4FF00>"& formatnumber(val2,0) &"</font></td> "
        haptrhtm = haptrhtm & "            <td width='15%'><font color=white><B>배당률     : <font color=#B4FF00>"& FORMATNUMBER(val3,2) &"</font></td> "
        haptrhtm = haptrhtm & "            <td width='20%'><font color=white><B>예상배당금 : <font color=#B4FF00>"& formatnumber(val4,0) &"</font></td> "
        haptrhtm = haptrhtm & "            <td width='20%'><font color=white><B>당첨금 : <font color=#B4FF00><span id='jsmoney2_"& btt_idx &"'>0</span></font></td> "
        haptrhtm = haptrhtm & "            <td width='14%' align=right><font color=white><B><font color=#B4FF00>"& val6 &"</font>&nbsp;</td> "
        haptrhtm = haptrhtm & "        </tr> "
        haptrhtm = haptrhtm & "    </table> "
        haptrhtm = haptrhtm & "    </td> "
        haptrhtm = haptrhtm & "</tr> "
'        haptrhtm = haptrhtm & "<tr height=10 bgcolor=white> "
'        haptrhtm = haptrhtm & "    <td colspan=20></td> "
'        haptrhtm = haptrhtm & "</tr> "
        '//
        getBttTrHtml_mypage = haptrhtm
        '//
    END FUNCTION

    FUNCTION getBttTrHtml_Board(val1,val2,val3,val4,val5,val6,val7)
        '//
        if val6="당첨" then val6="<font color=red>당첨"
        '//
        haptrhtm = "<tr height=35 bgcolor=#4B4B4B> "
        haptrhtm = haptrhtm & "    <td colspan=20> "
        haptrhtm = haptrhtm & "    <table border=0 cellpadding=0 cellspacing=0 width='100%' bgcolor=red> "
        haptrhtm = haptrhtm & "        <tr bgcolor=#4B4B4B align=left> "
        haptrhtm = haptrhtm & "            <td width='20%' height=50>&nbsp;<font color=white><B>배팅날짜   : <font color=#B4FF00>"& val1 &" "

        haptrhtm = haptrhtm & "<BR> &nbsp;<font color=white><input type=checkbox name=snum2 id=snum2 value="""&val7&""">금액미표시</font>"

        haptrhtm = haptrhtm & "            </td> "
        haptrhtm = haptrhtm & "            <td width='16%'><font color=white><B>배팅금액   : <font color=#B4FF00>"& formatnumber(val2,0) &"</font></td> "
        haptrhtm = haptrhtm & "            <td width='15%'><font color=white><B>배당률     : <font color=#B4FF00>"& FORMATNUMBER(val3,2) &"</font></td> "
        haptrhtm = haptrhtm & "            <td width='20%'><font color=white><B>예상배당금 : <font color=#B4FF00>"& formatnumber(val4,0) &"</font></td> "
        haptrhtm = haptrhtm & "            <td width='20%'><font color=white><B>당첨금 : <font color=#B4FF00><span id='jsmoney2_"& btt_idx &"'>0</span></font></td> "
        haptrhtm = haptrhtm & "            <td width='14%' align=right><font color=white><B><font color=#B4FF00>"& val6 &"</font>&nbsp;</td> "
        haptrhtm = haptrhtm & "        </tr> "
        haptrhtm = haptrhtm & "    </table> "
        haptrhtm = haptrhtm & "    </td> "
        haptrhtm = haptrhtm & "</tr> "
        haptrhtm = haptrhtm & "<tr height=10 bgcolor=white> "
        haptrhtm = haptrhtm & "    <td colspan=20></td> "
        haptrhtm = haptrhtm & "</tr> "
        '//
        getBttTrHtml_Board = haptrhtm
        '//
    END FUNCTION

    FUNCTION getBttTrHtml_Board2(val1,val2,val3,val4,val5,val6,val7,val8)
        '//
        if val6="당첨" then val6="<font color=red>당첨"
        if val8="1" then
            val2 = "*****"
            val4 = "*****"
            imsimoney = "*****"
        else
            val2 = formatnumber(val2,0)
            val4 = formatnumber(val4,0)
            imsimoney = 0
        end if
        '//
        haptrhtm = "<tr height=35 bgcolor=#4B4B4B> "
        haptrhtm = haptrhtm & "    <td colspan=20> "
        haptrhtm = haptrhtm & "    <table border=0 cellpadding=0 cellspacing=0 width='100%' bgcolor=red> "
        haptrhtm = haptrhtm & "        <tr bgcolor=#4B4B4B align=left> "
        haptrhtm = haptrhtm & "            <td width='20%'>&nbsp;<font color=white><B>배팅날짜   : <font color=#B4FF00>"& val1 &" </td> "
        haptrhtm = haptrhtm & "            <td width='16%'><font color=white><B>배팅금액   : <font color=#B4FF00>"& val2 &"</font></td> "
        haptrhtm = haptrhtm & "            <td width='15%'><font color=white><B>배당률     : <font color=#B4FF00>"& FORMATNUMBER(val3,2) &"</font></td> "
        haptrhtm = haptrhtm & "            <td width='20%'><font color=white><B>예상배당금 : <font color=#B4FF00>"& val4 &"</font></td> "
        haptrhtm = haptrhtm & "            <td width='20%'><font color=white><B>당첨금 : <font color=#B4FF00><span id='jsmoney2_"& btt_idx &"'>"& imsimoney &"</span></font></td> "
        haptrhtm = haptrhtm & "            <td width='14%' align=right><font color=white><B><font color=#B4FF00>"& val6 &"</font>&nbsp;</td> "
        haptrhtm = haptrhtm & "        </tr> "
        haptrhtm = haptrhtm & "    </table> "
        haptrhtm = haptrhtm & "    </td> "
        haptrhtm = haptrhtm & "</tr> "
        haptrhtm = haptrhtm & "<tr height=10 bgcolor=white> "
        haptrhtm = haptrhtm & "    <td colspan=20></td> "
        haptrhtm = haptrhtm & "</tr> "
        '//
        getBttTrHtml_Board2 = haptrhtm
        '//
    END FUNCTION

    FUNCTION getWinText(val1)
        '//
        if val1="W" then
            getWinText = "적중"
        elseif val1="L" then
            getWinText = "미적중"
        elseif val1="C" then
            getWinText = "취소"
        elseif val1="T" then
            getWinText = "적특"
        else
            getWinText = ""
        end if
        '//
    END FUNCTION

    FUNCTION getWinText2(val1)
        '//
        if val1="H" then
            getWinText2 = "승"
        elseif val1="A" then
            getWinText2 = "패"
        elseif val1="T" then
            getWinText2 = "적특"
        elseif val1="C" then
            getWinText2 = "취소"
        else
            getWinText2 = ""
        end if
        '//
    END FUNCTION

    FUNCTION getWinText3(val1)
        '//
        if val1="W" then
            getWinText3 = "<font color=red>적중"
        elseif val1="L" then
            getWinText3 = "<font color=white>미적중"
        elseif val1="C" then
            getWinText3 = "<font color=green>취소"
        elseif val1="T" then
            getWinText3 = "<font color=blue>적특"
        else
            getWinText3 = ""
        end if
        '//
    END FUNCTION

    FUNCTION getWinText4(val1)
        '//
        if val1="W" then
            getWinText4 = "<font color=red>적중"
        elseif val1="L" then
            getWinText4 = "<font color=white>미적중"
        elseif val1="C" then
            getWinText4 = "<font color=green>취소"
        elseif val1="T" then
            getWinText4 = "<font color=blue>적특"
        else
            getWinText4 = ""
        end if
        '//
    END Function
    
    FUNCTION getwinflagval(gm_state,rst_htm_score,rst_atm_score,gm_idx)
        rst_htm_score = cdbl(rst_htm_score)
        rst_atm_score = cdbl(rst_atm_score)
        scorehap      = rst_htm_score+rst_atm_score
        '//
        set frs = server.CreateObject("ADODB.Recordset")
        SQL = "       SELECT * "
        SQL = SQL & "   FROM TB_SPO_GAME "
        SQL = SQL & "  WHERE GM_IDX = "& GM_IDX &" "
        frs.Open sql, db, 1
        if not frs.eof then
            '//
            gm_flag    = frs("gm_flag")
            gm_tdvde   = cdbl(frs("gm_tdvde"))
            gm_hndflag = frs("gm_hndflag")
            gm_vschb   = frs("gm_vschb")
            '//
            select case gm_flag
                case "11"
                    if rst_htm_score > rst_atm_score then
                        rst_winflag = "H"
                    elseif rst_htm_score = rst_atm_score then
                        rst_winflag = "T"
                    elseif rst_htm_score < rst_atm_score then
                        rst_winflag = "A"
                    end if
                case "10"
                    if gm_hndflag="UO" then '언더오버
                        if scorehap>gm_tdvde then
                            rst_winflag = "H"
                        elseif scorehap=gm_tdvde then
                            rst_winflag = "T"   '적특 100%환불
                        elseif scorehap<gm_tdvde then
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="CH" then  'CH:핸디캡 홈팀(-)
                        rst_htm_score_f = rst_htm_score-gm_tdvde
                        if rst_htm_score_f > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score_f = rst_atm_score then
                            rst_winflag = "T"   '적특 100%환불
                        elseif rst_htm_score_f < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="CA" then  'CA:핸디캡 원정팀(+)
                        rst_htm_score_f = rst_htm_score+gm_tdvde
                        if rst_htm_score_f > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score_f = rst_atm_score then
                            rst_winflag = "T"   '적특 100%환불
                        elseif rst_htm_score_f < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="VS" then  '무승부
                        if rst_htm_score > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score = rst_atm_score then
                            rst_winflag = "T"
                        elseif rst_htm_score < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    end if
                case "12"
                    if gm_hndflag="UO" then '언더오버

                        if scorehap>gm_tdvde then
                            rst_winflag = "H"
                        elseif scorehap=gm_tdvde then
                            rst_winflag = "T"   '적특 100%환불
                        else
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="CH" then  'CH:핸디캡 홈팀(-)
                        rst_htm_score_f = rst_htm_score-gm_tdvde
                        if rst_htm_score_f > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score_f = rst_atm_score then
                            rst_winflag = "T"
                        elseif rst_htm_score_f < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="CA" then  'CA:핸디캡 원정팀(+)
                        rst_htm_score_f = rst_htm_score+gm_tdvde
                        if rst_htm_score_f > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score_f = rst_atm_score then
                            rst_winflag = "T"
                        elseif rst_htm_score_f < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="VS" then  '무승부
                        if rst_htm_score > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score = rst_atm_score then
                            rst_winflag = "T"
                        elseif rst_htm_score < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    end if
                case "13"
                    if gm_hndflag="UO" then '언더오버
                        if scorehap>gm_tdvde then
                            rst_winflag = "H"
                        elseif scorehap=gm_tdvde then
                            rst_winflag = "T"   '적특 100%환불
                        else
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="CH" then  'CH:핸디캡 홈팀(-)
                        rst_htm_score_f = rst_htm_score-gm_tdvde
                        if rst_htm_score_f > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score_f = rst_atm_score then
                            rst_winflag = "T"
                        elseif rst_htm_score_f < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="CA" then  'CA:핸디캡 원정팀(+)
                        rst_htm_score_f = rst_htm_score+gm_tdvde
                        if rst_htm_score_f > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score_f = rst_atm_score then
                            rst_winflag = "T"
                        elseif rst_htm_score_f < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="VS" then  '무승부
                        if rst_htm_score > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score = rst_atm_score then
                            rst_winflag = "T"
                        elseif rst_htm_score < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    end if
            end select
            '//
        end if
        frs.close
        '//
        getwinflagval = rst_winflag
        '//
    END FUNCTION

    FUNCTION getwinflagval_txco(gm_state,rst_htm_score,rst_atm_score,se_gm_idx)
        rst_htm_score = cdbl(rst_htm_score)
        rst_atm_score = cdbl(rst_atm_score)
        scorehap      = rst_htm_score+rst_atm_score
        '//
        set frs = server.CreateObject("ADODB.Recordset")
        SQL = "       SELECT * "
        SQL = SQL & "   FROM OPENDATASOURCE('SQLOLEDB','Data Source=127.0.0.1,15333;User ID=txcodbuser;Password=txdb)(8765').TXCODB.DBO.TB_SPO_GAME "
        SQL = SQL & "  WHERE GM_IDX = "& se_gm_idx &" "
        frs.Open sql, db, 1
        if not frs.eof then
            '//
            gm_flag    = frs("gm_flag")
            gm_tdvde   = cdbl(frs("gm_tdvde"))
            gm_hndflag = frs("gm_hndflag")
            gm_vschb   = frs("gm_vschb")
            '//
            select case gm_flag
                case "11"
                    if rst_htm_score > rst_atm_score then
                        rst_winflag = "H"
                    elseif rst_htm_score = rst_atm_score then
                        rst_winflag = "T"
                    elseif rst_htm_score < rst_atm_score then
                        rst_winflag = "A"
                    end if
                case "10"
                    if gm_hndflag="UO" then '언더오버
                        if scorehap>gm_tdvde then
                            rst_winflag = "H"
                        elseif scorehap=gm_tdvde then
                            rst_winflag = "T"   '적특 100%환불
                        elseif scorehap<gm_tdvde then
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="CH" then  'CH:핸디캡 홈팀(-)
                        rst_htm_score_f = rst_htm_score-gm_tdvde
                        if rst_htm_score_f > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score_f = rst_atm_score then
                            rst_winflag = "T"   '적특 100%환불
                        elseif rst_htm_score_f < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="CA" then  'CA:핸디캡 원정팀(+)
                        rst_htm_score_f = rst_htm_score+gm_tdvde
                        if rst_htm_score_f > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score_f = rst_atm_score then
                            rst_winflag = "T"   '적특 100%환불
                        elseif rst_htm_score_f < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="VS" then  '무승부
                        if rst_htm_score > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score = rst_atm_score then
                            rst_winflag = "T"
                        elseif rst_htm_score < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    end if
                case "12"
                    if gm_hndflag="UO" then '언더오버

                        if scorehap>gm_tdvde then
                            rst_winflag = "H"
                        elseif scorehap=gm_tdvde then
                            rst_winflag = "T"   '적특 100%환불
                        else
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="CH" then  'CH:핸디캡 홈팀(-)
                        rst_htm_score_f = rst_htm_score-gm_tdvde
                        if rst_htm_score_f > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score_f = rst_atm_score then
                            rst_winflag = "T"
                        elseif rst_htm_score_f < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="CA" then  'CA:핸디캡 원정팀(+)
                        rst_htm_score_f = rst_htm_score+gm_tdvde
                        if rst_htm_score_f > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score_f = rst_atm_score then
                            rst_winflag = "T"
                        elseif rst_htm_score_f < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="VS" then  '무승부
                        if rst_htm_score > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score = rst_atm_score then
                            rst_winflag = "T"
                        elseif rst_htm_score < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    end if
                case "13"
                    if gm_hndflag="UO" then '언더오버
                        if scorehap>gm_tdvde then
                            rst_winflag = "H"
                        elseif scorehap=gm_tdvde then
                            rst_winflag = "T"   '적특 100%환불
                        else
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="CH" then  'CH:핸디캡 홈팀(-)
                        rst_htm_score_f = rst_htm_score-gm_tdvde
                        if rst_htm_score_f > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score_f = rst_atm_score then
                            rst_winflag = "T"
                        elseif rst_htm_score_f < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="CA" then  'CA:핸디캡 원정팀(+)
                        rst_htm_score_f = rst_htm_score+gm_tdvde
                        if rst_htm_score_f > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score_f = rst_atm_score then
                            rst_winflag = "T"
                        elseif rst_htm_score_f < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="VS" then  '무승부
                        if rst_htm_score > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score = rst_atm_score then
                            rst_winflag = "T"
                        elseif rst_htm_score < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    end if
            end select
            '//
        end if
        frs.close
        '//
        getwinflagval_txco = rst_winflag
        '//
    END FUNCTION
    
    FUNCTION getwinflagval_ground(gm_state,rst_htm_score,rst_atm_score,se_gm_idx)
        rst_htm_score = cdbl(rst_htm_score)
        rst_atm_score = cdbl(rst_atm_score)
        scorehap      = rst_htm_score+rst_atm_score
        '//
        set frs = server.CreateObject("ADODB.Recordset")
        SQL = "       SELECT * "
        SQL = SQL & "   FROM OPENDATASOURCE('SQLOLEDB','Data Source=127.0.0.1,15333;User ID=groundwebuser;Password=lte#$%789').GROUNDDB2.DBO.TB_SPO_GAME "
        SQL = SQL & "  WHERE GM_IDX = "& se_gm_idx &" "
        frs.Open sql, db, 1
        if not frs.eof then
            '//
            gm_flag    = frs("gm_flag")
            gm_tdvde   = cdbl(frs("gm_tdvde"))
            gm_hndflag = frs("gm_hndflag")
            gm_vschb   = frs("gm_vschb")
            '//
            select case gm_flag
                case "11"
                    if rst_htm_score > rst_atm_score then
                        rst_winflag = "H"
                    elseif rst_htm_score = rst_atm_score then
                        rst_winflag = "T"
                    elseif rst_htm_score < rst_atm_score then
                        rst_winflag = "A"
                    end if
                case "10"
                    if gm_hndflag="UO" then '언더오버
                        if scorehap>gm_tdvde then
                            rst_winflag = "H"
                        elseif scorehap=gm_tdvde then
                            rst_winflag = "T"   '적특 100%환불
                        elseif scorehap<gm_tdvde then
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="CH" then  'CH:핸디캡 홈팀(-)
                        rst_htm_score_f = rst_htm_score-gm_tdvde
                        if rst_htm_score_f > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score_f = rst_atm_score then
                            rst_winflag = "T"   '적특 100%환불
                        elseif rst_htm_score_f < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="CA" then  'CA:핸디캡 원정팀(+)
                        rst_htm_score_f = rst_htm_score+gm_tdvde
                        if rst_htm_score_f > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score_f = rst_atm_score then
                            rst_winflag = "T"   '적특 100%환불
                        elseif rst_htm_score_f < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="VS" then  '무승부
                        if rst_htm_score > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score = rst_atm_score then
                            rst_winflag = "T"
                        elseif rst_htm_score < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    end if
                case "12"
                    if gm_hndflag="UO" then '언더오버

                        if scorehap>gm_tdvde then
                            rst_winflag = "H"
                        elseif scorehap=gm_tdvde then
                            rst_winflag = "T"   '적특 100%환불
                        else
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="CH" then  'CH:핸디캡 홈팀(-)
                        rst_htm_score_f = rst_htm_score-gm_tdvde
                        if rst_htm_score_f > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score_f = rst_atm_score then
                            rst_winflag = "T"
                        elseif rst_htm_score_f < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="CA" then  'CA:핸디캡 원정팀(+)
                        rst_htm_score_f = rst_htm_score+gm_tdvde
                        if rst_htm_score_f > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score_f = rst_atm_score then
                            rst_winflag = "T"
                        elseif rst_htm_score_f < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="VS" then  '무승부
                        if rst_htm_score > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score = rst_atm_score then
                            rst_winflag = "T"
                        elseif rst_htm_score < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    end if
                case "13"
                    if gm_hndflag="UO" then '언더오버
                        if scorehap>gm_tdvde then
                            rst_winflag = "H"
                        elseif scorehap=gm_tdvde then
                            rst_winflag = "T"   '적특 100%환불
                        else
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="CH" then  'CH:핸디캡 홈팀(-)
                        rst_htm_score_f = rst_htm_score-gm_tdvde
                        if rst_htm_score_f > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score_f = rst_atm_score then
                            rst_winflag = "T"
                        elseif rst_htm_score_f < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="CA" then  'CA:핸디캡 원정팀(+)
                        rst_htm_score_f = rst_htm_score+gm_tdvde
                        if rst_htm_score_f > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score_f = rst_atm_score then
                            rst_winflag = "T"
                        elseif rst_htm_score_f < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="VS" then  '무승부
                        if rst_htm_score > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score = rst_atm_score then
                            rst_winflag = "T"
                        elseif rst_htm_score < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    end if
            end select
            '//
        end if
        frs.close
        '//
        getwinflagval_ground = rst_winflag
        '//
    END FUNCTION     
    
    
    FUNCTION getwinflagval_slamdunk(gm_state,rst_htm_score,rst_atm_score,se_gm_idx)
        rst_htm_score = cdbl(rst_htm_score)
        rst_atm_score = cdbl(rst_atm_score)
        scorehap      = rst_htm_score+rst_atm_score
        '//
        set frs = server.CreateObject("ADODB.Recordset")
        SQL = "       SELECT * "
        SQL = SQL & "   FROM OPENDATASOURCE('SQLOLEDB','Data Source=127.0.0.1,15333;User ID=slamdbuser;Password=slam#$%789').SLAMDUNK.DBO.TB_SPO_GAME "
        SQL = SQL & "  WHERE GM_IDX = "& se_gm_idx &" "
        frs.Open sql, db, 1
        if not frs.eof then
            '//
            gm_flag    = frs("gm_flag")
            gm_tdvde   = cdbl(frs("gm_tdvde"))
            gm_hndflag = frs("gm_hndflag")
            gm_vschb   = frs("gm_vschb")
            '//
            select case gm_flag
                case "11"
                    if rst_htm_score > rst_atm_score then
                        rst_winflag = "H"
                    elseif rst_htm_score = rst_atm_score then
                        rst_winflag = "T"
                    elseif rst_htm_score < rst_atm_score then
                        rst_winflag = "A"
                    end if
                case "10"
                    if gm_hndflag="UO" then '언더오버
                        if scorehap>gm_tdvde then
                            rst_winflag = "H"
                        elseif scorehap=gm_tdvde then
                            rst_winflag = "T"   '적특 100%환불
                        elseif scorehap<gm_tdvde then
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="CH" then  'CH:핸디캡 홈팀(-)
                        rst_htm_score_f = rst_htm_score-gm_tdvde
                        if rst_htm_score_f > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score_f = rst_atm_score then
                            rst_winflag = "T"   '적특 100%환불
                        elseif rst_htm_score_f < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="CA" then  'CA:핸디캡 원정팀(+)
                        rst_htm_score_f = rst_htm_score+gm_tdvde
                        if rst_htm_score_f > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score_f = rst_atm_score then
                            rst_winflag = "T"   '적특 100%환불
                        elseif rst_htm_score_f < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="VS" then  '무승부
                        if rst_htm_score > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score = rst_atm_score then
                            rst_winflag = "T"
                        elseif rst_htm_score < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    end if
                case "12"
                    if gm_hndflag="UO" then '언더오버

                        if scorehap>gm_tdvde then
                            rst_winflag = "H"
                        elseif scorehap=gm_tdvde then
                            rst_winflag = "T"   '적특 100%환불
                        else
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="CH" then  'CH:핸디캡 홈팀(-)
                        rst_htm_score_f = rst_htm_score-gm_tdvde
                        if rst_htm_score_f > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score_f = rst_atm_score then
                            rst_winflag = "T"
                        elseif rst_htm_score_f < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="CA" then  'CA:핸디캡 원정팀(+)
                        rst_htm_score_f = rst_htm_score+gm_tdvde
                        if rst_htm_score_f > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score_f = rst_atm_score then
                            rst_winflag = "T"
                        elseif rst_htm_score_f < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="VS" then  '무승부
                        if rst_htm_score > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score = rst_atm_score then
                            rst_winflag = "T"
                        elseif rst_htm_score < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    end if
                case "13"
                    if gm_hndflag="UO" then '언더오버
                        if scorehap>gm_tdvde then
                            rst_winflag = "H"
                        elseif scorehap=gm_tdvde then
                            rst_winflag = "T"   '적특 100%환불
                        else
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="CH" then  'CH:핸디캡 홈팀(-)
                        rst_htm_score_f = rst_htm_score-gm_tdvde
                        if rst_htm_score_f > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score_f = rst_atm_score then
                            rst_winflag = "T"
                        elseif rst_htm_score_f < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="CA" then  'CA:핸디캡 원정팀(+)
                        rst_htm_score_f = rst_htm_score+gm_tdvde
                        if rst_htm_score_f > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score_f = rst_atm_score then
                            rst_winflag = "T"
                        elseif rst_htm_score_f < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="VS" then  '무승부
                        if rst_htm_score > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score = rst_atm_score then
                            rst_winflag = "T"
                        elseif rst_htm_score < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    end if
            end select
            '//
        end if
        frs.close
        '//
        getwinflagval_slamdunk = rst_winflag
        '//
    END FUNCTION    
    

    FUNCTION getwinflagval_new(gm_state,rst_htm_score,rst_atm_score,bts_idx)
        rst_htm_score = cdbl(rst_htm_score)
        rst_atm_score = cdbl(rst_atm_score)
        scorehap      = rst_htm_score+rst_atm_score
        '//
        set frs = server.CreateObject("ADODB.Recordset")
        SQL = "       SELECT A.*,B.* "
        SQL = SQL & "   FROM TB_SPO_BETTING_INFO A, TB_SPO_GAME B "
        SQL = SQL & "  WHERE A.GM_IDX=B.GM_IDX AND A.BTS_IDX = "& BTS_IDX &" "
        frs.Open sql, db, 1
        if not frs.eof then
            '//
            gm_flag    = frs("gm_flag")
            gm_tdvde   = cdbl(frs("bts_tdvde"))
            gm_hndflag = frs("gm_hndflag")
            gm_vschb   = frs("gm_vschb")
            '//
            select case gm_flag
                case "11"
                    if rst_htm_score > rst_atm_score then
                        rst_winflag = "H"
                    elseif rst_htm_score = rst_atm_score then
                        rst_winflag = "T"
                    elseif rst_htm_score < rst_atm_score then
                        rst_winflag = "A"
                    end if
                case "10"
                    if gm_hndflag="UO" then '언더오버
                        if scorehap>gm_tdvde then
                            rst_winflag = "H"
                        elseif scorehap=gm_tdvde then
                            rst_winflag = "T"   '적특 100%환불
                        elseif scorehap<gm_tdvde then
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="CH" then  'CH:핸디캡 홈팀(-)
                        rst_htm_score_f = rst_htm_score-gm_tdvde
                        if rst_htm_score_f > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score_f = rst_atm_score then
                            rst_winflag = "T"   '적특 100%환불
                        elseif rst_htm_score_f < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="CA" then  'CA:핸디캡 원정팀(+)
                        rst_htm_score_f = rst_htm_score+gm_tdvde
                        if rst_htm_score_f > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score_f = rst_atm_score then
                            rst_winflag = "T"   '적특 100%환불
                        elseif rst_htm_score_f < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="VS" then  '무승부
                        if rst_htm_score > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score = rst_atm_score then
                            rst_winflag = "T"
                        elseif rst_htm_score < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    end if
                case "12"
                    if gm_hndflag="UO" then '언더오버
                        if scorehap>gm_tdvde then
                            rst_winflag = "H"
                        elseif scorehap=gm_tdvde then
                            rst_winflag = "T"   '적특 100%환불
                        else
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="CH" then  'CH:핸디캡 홈팀(-)
                        rst_htm_score_f = rst_htm_score-gm_tdvde
                        if rst_htm_score_f > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score_f = rst_atm_score then
                            rst_winflag = "T"
                        elseif rst_htm_score_f < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="CA" then  'CA:핸디캡 원정팀(+)
                        rst_htm_score_f = rst_htm_score+gm_tdvde
                        if rst_htm_score_f > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score_f = rst_atm_score then
                            rst_winflag = "T"
                        elseif rst_htm_score_f < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="VS" then  '무승부
                        if rst_htm_score > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score = rst_atm_score then
                            rst_winflag = "T"
                        elseif rst_htm_score < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    end if
                case "13"
                    if gm_hndflag="UO" then '언더오버
                        if scorehap>gm_tdvde then
                            rst_winflag = "H"
                        elseif scorehap=gm_tdvde then
                            rst_winflag = "T"   '적특 100%환불
                        else
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="CH" then  'CH:핸디캡 홈팀(-)
                        rst_htm_score_f = rst_htm_score-gm_tdvde
                        if rst_htm_score_f > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score_f = rst_atm_score then
                            rst_winflag = "T"
                        elseif rst_htm_score_f < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="CA" then  'CA:핸디캡 원정팀(+)
                        rst_htm_score_f = rst_htm_score+gm_tdvde
                        if rst_htm_score_f > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score_f = rst_atm_score then
                            rst_winflag = "T"
                        elseif rst_htm_score_f < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="VS" then  '무승부
                        if rst_htm_score > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score = rst_atm_score then
                            rst_winflag = "T"
                        elseif rst_htm_score < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    end if
            end select
            '//
        end if
        frs.close
        '//
        getwinflagval_new = rst_winflag
        '//
    END FUNCTION


    
    FUNCTION getwinflagval_new_txco(gm_state,rst_htm_score,rst_atm_score,bts_idx)
        rst_htm_score = cdbl(rst_htm_score)
        rst_atm_score = cdbl(rst_atm_score)
        scorehap      = rst_htm_score+rst_atm_score
        '//
        set frs = server.CreateObject("ADODB.Recordset")
        SQL = "       SELECT A.*,B.* "
        SQL = SQL & "   FROM OPENDATASOURCE('SQLOLEDB','Data Source=127.0.0.1,15333;User ID=txcodbuser;Password=txdb)(8765').TXCODB.DBO.TB_SPO_BETTING_INFO A, OPENDATASOURCE('SQLOLEDB','Data Source=127.0.0.1,15333;User ID=txcodbuser;Password=txdb)(8765').TXCODB.DBO.TB_SPO_GAME B "
        SQL = SQL & "  WHERE A.GM_IDX=B.GM_IDX AND A.BTS_IDX = "& BTS_IDX &" "
        frs.Open sql, db, 1
        if not frs.eof then
            '//
            gm_flag    = frs("gm_flag")
            gm_tdvde   = cdbl(frs("bts_tdvde"))
            gm_hndflag = frs("gm_hndflag")
            gm_vschb   = frs("gm_vschb")
            '//
            select case gm_flag
                case "11"
                    if rst_htm_score > rst_atm_score then
                        rst_winflag = "H"
                    elseif rst_htm_score = rst_atm_score then
                        rst_winflag = "T"
                    elseif rst_htm_score < rst_atm_score then
                        rst_winflag = "A"
                    end if
                case "10"
                    if gm_hndflag="UO" then '언더오버
                        if scorehap>gm_tdvde then
                            rst_winflag = "H"
                        elseif scorehap=gm_tdvde then
                            rst_winflag = "T"   '적특 100%환불
                        elseif scorehap<gm_tdvde then
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="CH" then  'CH:핸디캡 홈팀(-)
                        rst_htm_score_f = rst_htm_score-gm_tdvde
                        if rst_htm_score_f > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score_f = rst_atm_score then
                            rst_winflag = "T"   '적특 100%환불
                        elseif rst_htm_score_f < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="CA" then  'CA:핸디캡 원정팀(+)
                        rst_htm_score_f = rst_htm_score+gm_tdvde
                        if rst_htm_score_f > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score_f = rst_atm_score then
                            rst_winflag = "T"   '적특 100%환불
                        elseif rst_htm_score_f < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="VS" then  '무승부
                        if rst_htm_score > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score = rst_atm_score then
                            rst_winflag = "T"
                        elseif rst_htm_score < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    end if
                case "12"
                    if gm_hndflag="UO" then '언더오버
                        if scorehap>gm_tdvde then
                            rst_winflag = "H"
                        elseif scorehap=gm_tdvde then
                            rst_winflag = "T"   '적특 100%환불
                        else
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="CH" then  'CH:핸디캡 홈팀(-)
                        rst_htm_score_f = rst_htm_score-gm_tdvde
                        if rst_htm_score_f > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score_f = rst_atm_score then
                            rst_winflag = "T"
                        elseif rst_htm_score_f < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="CA" then  'CA:핸디캡 원정팀(+)
                        rst_htm_score_f = rst_htm_score+gm_tdvde
                        if rst_htm_score_f > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score_f = rst_atm_score then
                            rst_winflag = "T"
                        elseif rst_htm_score_f < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="VS" then  '무승부
                        if rst_htm_score > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score = rst_atm_score then
                            rst_winflag = "T"
                        elseif rst_htm_score < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    end if
                case "13"
                    if gm_hndflag="UO" then '언더오버
                        if scorehap>gm_tdvde then
                            rst_winflag = "H"
                        elseif scorehap=gm_tdvde then
                            rst_winflag = "T"   '적특 100%환불
                        else
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="CH" then  'CH:핸디캡 홈팀(-)
                        rst_htm_score_f = rst_htm_score-gm_tdvde
                        if rst_htm_score_f > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score_f = rst_atm_score then
                            rst_winflag = "T"
                        elseif rst_htm_score_f < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="CA" then  'CA:핸디캡 원정팀(+)
                        rst_htm_score_f = rst_htm_score+gm_tdvde
                        if rst_htm_score_f > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score_f = rst_atm_score then
                            rst_winflag = "T"
                        elseif rst_htm_score_f < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="VS" then  '무승부
                        if rst_htm_score > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score = rst_atm_score then
                            rst_winflag = "T"
                        elseif rst_htm_score < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    end if
            end select
            '//
        end if
        frs.close
        '//
        getwinflagval_new_txco = rst_winflag
        '//
    END FUNCTION

    FUNCTION getwinflagval_new_ground(gm_state,rst_htm_score,rst_atm_score,bts_idx)
        rst_htm_score = cdbl(rst_htm_score)
        rst_atm_score = cdbl(rst_atm_score)
        scorehap      = rst_htm_score+rst_atm_score
        '//
        set frs = server.CreateObject("ADODB.Recordset")
        SQL = "       SELECT A.*,B.* "
        SQL = SQL & "   FROM OPENDATASOURCE('SQLOLEDB','Data Source=127.0.0.1,15333;User ID=groundwebuser;Password=lte#$%789').GROUNDDB2.DBO.TB_SPO_BETTING_INFO A, OPENDATASOURCE('SQLOLEDB','Data Source=127.0.0.1,15333;User ID=groundwebuser;Password=lte#$%789').GROUNDDB2.DBO.TB_SPO_GAME B "
        SQL = SQL & "  WHERE A.GM_IDX=B.GM_IDX AND A.BTS_IDX = "& BTS_IDX &" "
        frs.Open sql, db, 1
        if not frs.eof then
            '//
            gm_flag    = frs("gm_flag")
            gm_tdvde   = cdbl(frs("bts_tdvde"))
            gm_hndflag = frs("gm_hndflag")
            gm_vschb   = frs("gm_vschb")
            '//
            select case gm_flag
                case "11"
                    if rst_htm_score > rst_atm_score then
                        rst_winflag = "H"
                    elseif rst_htm_score = rst_atm_score then
                        rst_winflag = "T"
                    elseif rst_htm_score < rst_atm_score then
                        rst_winflag = "A"
                    end if
                case "10"
                    if gm_hndflag="UO" then '언더오버
                        if scorehap>gm_tdvde then
                            rst_winflag = "H"
                        elseif scorehap=gm_tdvde then
                            rst_winflag = "T"   '적특 100%환불
                        elseif scorehap<gm_tdvde then
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="CH" then  'CH:핸디캡 홈팀(-)
                        rst_htm_score_f = rst_htm_score-gm_tdvde
                        if rst_htm_score_f > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score_f = rst_atm_score then
                            rst_winflag = "T"   '적특 100%환불
                        elseif rst_htm_score_f < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="CA" then  'CA:핸디캡 원정팀(+)
                        rst_htm_score_f = rst_htm_score+gm_tdvde
                        if rst_htm_score_f > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score_f = rst_atm_score then
                            rst_winflag = "T"   '적특 100%환불
                        elseif rst_htm_score_f < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="VS" then  '무승부
                        if rst_htm_score > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score = rst_atm_score then
                            rst_winflag = "T"
                        elseif rst_htm_score < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    end if
                case "12"
                    if gm_hndflag="UO" then '언더오버
                        if scorehap>gm_tdvde then
                            rst_winflag = "H"
                        elseif scorehap=gm_tdvde then
                            rst_winflag = "T"   '적특 100%환불
                        else
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="CH" then  'CH:핸디캡 홈팀(-)
                        rst_htm_score_f = rst_htm_score-gm_tdvde
                        if rst_htm_score_f > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score_f = rst_atm_score then
                            rst_winflag = "T"
                        elseif rst_htm_score_f < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="CA" then  'CA:핸디캡 원정팀(+)
                        rst_htm_score_f = rst_htm_score+gm_tdvde
                        if rst_htm_score_f > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score_f = rst_atm_score then
                            rst_winflag = "T"
                        elseif rst_htm_score_f < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="VS" then  '무승부
                        if rst_htm_score > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score = rst_atm_score then
                            rst_winflag = "T"
                        elseif rst_htm_score < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    end if
                case "13"
                    if gm_hndflag="UO" then '언더오버
                        if scorehap>gm_tdvde then
                            rst_winflag = "H"
                        elseif scorehap=gm_tdvde then
                            rst_winflag = "T"   '적특 100%환불
                        else
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="CH" then  'CH:핸디캡 홈팀(-)
                        rst_htm_score_f = rst_htm_score-gm_tdvde
                        if rst_htm_score_f > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score_f = rst_atm_score then
                            rst_winflag = "T"
                        elseif rst_htm_score_f < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="CA" then  'CA:핸디캡 원정팀(+)
                        rst_htm_score_f = rst_htm_score+gm_tdvde
                        if rst_htm_score_f > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score_f = rst_atm_score then
                            rst_winflag = "T"
                        elseif rst_htm_score_f < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="VS" then  '무승부
                        if rst_htm_score > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score = rst_atm_score then
                            rst_winflag = "T"
                        elseif rst_htm_score < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    end if
            end select
            '//
        end if
        frs.close
        '//
        getwinflagval_new_ground = rst_winflag
        '//
    END FUNCTION    


    FUNCTION getwinflagval_new_slamdunk(gm_state,rst_htm_score,rst_atm_score,bts_idx)
        rst_htm_score = cdbl(rst_htm_score)
        rst_atm_score = cdbl(rst_atm_score)
        scorehap      = rst_htm_score+rst_atm_score
        '//
        set frs = server.CreateObject("ADODB.Recordset")
        SQL = "       SELECT A.*,B.* "
        SQL = SQL & "   FROM OPENDATASOURCE('SQLOLEDB','Data Source=127.0.0.1,15333;User ID=slamdbuser;Password=slam#$%789').SLAMDUNK.DBO.TB_SPO_BETTING_INFO A, OPENDATASOURCE('SQLOLEDB','Data Source=127.0.0.1,15333;User ID=slamdbuser;Password=slam#$%789').SLAMDUNK.DBO.TB_SPO_GAME B "
        SQL = SQL & "  WHERE A.GM_IDX=B.GM_IDX AND A.BTS_IDX = "& BTS_IDX &" "
        frs.Open sql, db, 1
        if not frs.eof then
            '//
            gm_flag    = frs("gm_flag")
            gm_tdvde   = cdbl(frs("bts_tdvde"))
            gm_hndflag = frs("gm_hndflag")
            gm_vschb   = frs("gm_vschb")
            '//
            select case gm_flag
                case "11"
                    if rst_htm_score > rst_atm_score then
                        rst_winflag = "H"
                    elseif rst_htm_score = rst_atm_score then
                        rst_winflag = "T"
                    elseif rst_htm_score < rst_atm_score then
                        rst_winflag = "A"
                    end if
                case "10"
                    if gm_hndflag="UO" then '언더오버
                        if scorehap>gm_tdvde then
                            rst_winflag = "H"
                        elseif scorehap=gm_tdvde then
                            rst_winflag = "T"   '적특 100%환불
                        elseif scorehap<gm_tdvde then
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="CH" then  'CH:핸디캡 홈팀(-)
                        rst_htm_score_f = rst_htm_score-gm_tdvde
                        if rst_htm_score_f > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score_f = rst_atm_score then
                            rst_winflag = "T"   '적특 100%환불
                        elseif rst_htm_score_f < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="CA" then  'CA:핸디캡 원정팀(+)
                        rst_htm_score_f = rst_htm_score+gm_tdvde
                        if rst_htm_score_f > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score_f = rst_atm_score then
                            rst_winflag = "T"   '적특 100%환불
                        elseif rst_htm_score_f < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="VS" then  '무승부
                        if rst_htm_score > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score = rst_atm_score then
                            rst_winflag = "T"
                        elseif rst_htm_score < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    end if
                case "12"
                    if gm_hndflag="UO" then '언더오버
                        if scorehap>gm_tdvde then
                            rst_winflag = "H"
                        elseif scorehap=gm_tdvde then
                            rst_winflag = "T"   '적특 100%환불
                        else
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="CH" then  'CH:핸디캡 홈팀(-)
                        rst_htm_score_f = rst_htm_score-gm_tdvde
                        if rst_htm_score_f > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score_f = rst_atm_score then
                            rst_winflag = "T"
                        elseif rst_htm_score_f < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="CA" then  'CA:핸디캡 원정팀(+)
                        rst_htm_score_f = rst_htm_score+gm_tdvde
                        if rst_htm_score_f > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score_f = rst_atm_score then
                            rst_winflag = "T"
                        elseif rst_htm_score_f < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="VS" then  '무승부
                        if rst_htm_score > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score = rst_atm_score then
                            rst_winflag = "T"
                        elseif rst_htm_score < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    end if
                case "13"
                    if gm_hndflag="UO" then '언더오버
                        if scorehap>gm_tdvde then
                            rst_winflag = "H"
                        elseif scorehap=gm_tdvde then
                            rst_winflag = "T"   '적특 100%환불
                        else
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="CH" then  'CH:핸디캡 홈팀(-)
                        rst_htm_score_f = rst_htm_score-gm_tdvde
                        if rst_htm_score_f > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score_f = rst_atm_score then
                            rst_winflag = "T"
                        elseif rst_htm_score_f < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="CA" then  'CA:핸디캡 원정팀(+)
                        rst_htm_score_f = rst_htm_score+gm_tdvde
                        if rst_htm_score_f > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score_f = rst_atm_score then
                            rst_winflag = "T"
                        elseif rst_htm_score_f < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    elseif gm_hndflag="VS" then  '무승부
                        if rst_htm_score > rst_atm_score then
                            rst_winflag = "H"
                        elseif rst_htm_score = rst_atm_score then
                            rst_winflag = "T"
                        elseif rst_htm_score < rst_atm_score then
                            rst_winflag = "A"
                        end if
                    end if
            end select
            '//
        end if
        frs.close
        '//
        getwinflagval_new_slamdunk = rst_winflag
        '//
    END FUNCTION  
    
    FUNCTION getBtsState(flag, rst_winflag, bts_chflag, gm_hndflag, spo_tie)
        if flag="11" then
            if spo_tie="Y" And gm_hndflag<>"UO" then
                if rst_winflag = bts_chflag then
                    bts_state = "W"
                else
                    bts_state = "L"
                end if
            else
                if rst_winflag = "T" then
                    bts_state = "T"
                elseif rst_winflag = bts_chflag then
                    bts_state = "W"
                else
                    bts_state = "L"
                end if
            end if
        elseif flag="10" or flag="12" then
            if rst_winflag="T" then '적특
                bts_state = "T" '적특
            else
                if rst_winflag = bts_chflag then
                    bts_state = "W"
                else
                    bts_state = "L"
                end if
            end if
        elseif flag="13" then
            if gm_hndflag="VS" then
                if rst_winflag="T" then '적특
                    bts_state = "T" '적특
                else
                    if rst_winflag = bts_chflag then
                        bts_state = "W"

                    else
                        bts_state = "L"
                    end if
                end if
            else
                if rst_winflag="T" then '적특
                    bts_state = "T" '적특
                else
                    if rst_winflag = bts_chflag then
                        bts_state = "W"
                    else
                        bts_state = "L"
                    end if
                end if
            end if
        end if

        getBtsState = bts_state
    END Function

    FUNCTION getBtsState_new(flag, rst_winflag, bts_chflag, gm_hndflag, spo_tie, imsigmkind,imsi_gm_vschb,imsi_htnm)
        if flag="11" then
            if spo_tie="Y" And gm_hndflag<>"UO" then
                if rst_winflag = bts_chflag then
                    bts_state = "W"
                else
                    bts_state = "L"
                end if
            else
                if rst_winflag = "T" then
                    bts_state = "T"
                elseif rst_winflag = bts_chflag then
                    bts_state = "W"
                else
                    bts_state = "L"
                end if
            end if
        elseif flag="10" or flag="12" then
            if rst_winflag="T" then '적특
				if flag="12" And imsigmkind="FOOT" And InStr(imsi_htnm,"전반전")>0 Then
					if rst_winflag = bts_chflag then
						bts_state = "W"
					else
						bts_state = "L"
					end if
				else
	                bts_state = "T" '적특
				End if
            else
                if rst_winflag = bts_chflag then
                    bts_state = "W"
                else
                    bts_state = "L"
                end if
            end if
        elseif flag="13" Then
            if gm_hndflag="VS" then
                if rst_winflag="T" then '적특
					If imsigmkind="JJ" Or imsigmkind="power" Or imsigmkind="vs" Then
						If imsi_gm_vschb="Y" Then
		                    bts_state = "T" '적특
						Else
							if rst_winflag = bts_chflag then
								bts_state = "W"
							else
								bts_state = "L"
							end if
						End if
					else
	                    bts_state = "T" '적특
					End if
                else
                    if rst_winflag = bts_chflag then
                        bts_state = "W"

                    else
                        bts_state = "L"
                    end if
                end if
            else
                if rst_winflag="T" then '적특
                    bts_state = "T" '적특
                else
                    if rst_winflag = bts_chflag then
                        bts_state = "W"
                    else
                        bts_state = "L"
                    end if
                end if
            end if
        end if

        getBtsState_new = bts_state
    END Function	


    FUNCTION getTeamCode(val1,val2,val3,val4)
        set rs = server.CreateObject("ADODB.Recordset")
        SQL = " SELECT SPO_CODE "
        SQL = SQL & " FROM TB_SPO_KIND "
        SQL = SQL & " WHERE SPO_FLAG='T' AND CH_JCODE='"& val1 &"' AND CH_LCODE='"& val2 &"' "
        SQL = SQL & " AND SPO_NAME = '"& val3 &"' "
        rs.Open sql, db, 1
        if not rs.eof then
            ret_code  = rs(0)
        else
            set rs2 = server.CreateObject("ADODB.Recordset")
            SQL = "SELECT ISNULL(MAX(convert(int,SPO_CODE)),10000)+1 FROM TB_SPO_KIND WHERE SPO_FLAG = 'T' "
            rs2.Open sql, db, 1
            ret_code = rs2(0)
            rs2.close

            SQL = " INSERT INTO TB_SPO_KIND (SPO_FLAG,SPO_NAME,SPO_ENAME,SPO_CODE,SPO_IMGNAME,CH_JCODE,CH_LCODE,CH_NCODE,SPO_TIE,SPO_SPW,SPO_SPT,SPO_BONUS,SPO_WTL,WDATE) VALUES "
            SQL = SQL & " ('T', '"& VAL3 &"', '"& VAL3 &"', '"& RET_CODE &"', '', '"& VAL1 &"', "
            SQL = SQL & " '"& VAL2 &"', '', '', '', '', 'N', '', '"& VAL4 &"' ) "
            DB.EXECUTE SQL
        end if
        rs.close

        getTeamCode = ret_code
    END FUNCTION

    FUNCTION getAgentPaySas(val1,val2,val3)
        SQL = "     SELECT A.IDX, A.AID, A.IN_MONEY, A.OUT_MONEY, A.END_MONEY, A.ORMONEY, A.USER_MONEY, A.WDATE, A.ORHAPMONEY FROM "
        SQL = SQL & " (	SELECT IDX, AID, IN_MONEY, OUT_MONEY, END_MONEY, ORMONEY, USER_MONEY, SUBSTRING(WDATE,1,8) WDATE, ORHAPMONEY FROM TB_AGENT_DAYLIST "
        SQL = SQL & " 	UNION ALL  "
        SQL = SQL & " 	SELECT 0, T1.userid AID, ISNULL(T2.IN_MONEY,0), ISNULL(T2.OUT_MONEY,0), ISNULL(T2.END_MONEY,0), ISNULL(T2.ORMONEY,0), ISNULL(T3.USER_MONEY,0), "& REPLACE(LEFT(NOW(),10),"-","") &" WDATE, ISNULL(T2.ORHAPMONEY,0) FROM tb_agent T1 "
        SQL = SQL & "   LEFT OUTER JOIN ( "
        SQL = SQL & " 		SELECT 0 IDX, A.MB_CODE AID, SUM(A.IN_MONEY)  IN_MONEY, SUM(A.OUT_MONEY) OUT_MONEY, (SUM(A.IN_MONEY)-SUM(A.OUT_MONEY)-SUM(A.ORMONEY)) END_MONEY, SUM(A.ORMONEY) ORMONEY, A.WDATE, SUM(A.ORHAPMONEY) ORHAPMONEY FROM "
        SQL = SQL & "         	(	SELECT SUM(A.CMONEY) IN_MONEY, 0 OUT_MONEY, 0 ORMONEY, B.MB_CODE, SUBSTRING(A.SWDATE,1,8) WDATE, 0 ORHAPMONEY FROM TB_CHARGE A, TB_MEMBER B "
        SQL = SQL & " 		 			WHERE A.USERID=B.MB_ID AND A.CFLAG='1' AND A.INGFLAG='5' "
        SQL = SQL & " 		 			GROUP BY B.MB_CODE, SUBSTRING(A.SWDATE,1,8) "
        SQL = SQL & " 		 		UNION ALL "
        SQL = SQL & " 				SELECT 0, SUM(A.CMONEY) OUT_MONEY, 0, B.MB_CODE, SUBSTRING(A.SWDATE,1,8) WDATE, 0 ORHAPMONEY FROM TB_CHARGE A, TB_MEMBER B "
        SQL = SQL & " 		 			WHERE A.USERID=B.MB_ID AND A.CFLAG='2' AND A.INGFLAG='5'  "
        SQL = SQL & " 		 			GROUP BY B.MB_CODE, SUBSTRING(A.SWDATE,1,8) "
        SQL = SQL & " 		 		UNION ALL "
        SQL = SQL & " 					SELECT 0,0,SUM(A.ORMONEY) ORMONEY, A.MB_CODE, A.WDATE, 0 ORHAPMONEY "
        SQL = SQL & " 		  			FROM (	SELECT ISNULL(SUM(ORMONEY),0) AS ORMONEY , B.MB_CODE, SUBSTRING(WDATE,1,8) WDATE FROM TB_ORDER_INFO A, TB_MEMBER B "
        SQL = SQL & " 				 				WHERE A.USERID=B.MB_ID AND INGFLAG = '0' "
        SQL = SQL & " 				 				GROUP BY SUBSTRING(WDATE,1,8),B.MB_CODE "
        SQL = SQL & " 							UNION ALL "
        SQL = SQL & " 							SELECT ISNULL(SUM(BTT_MONEY),0) AS ORMONEY , B.MB_CODE, SUBSTRING(WDATE,1,8) WDATE FROM TB_SPO_BETTING_MASTER A, TB_MEMBER B "
        SQL = SQL & " 				 				WHERE A.USERID=B.MB_ID AND BTT_ACCFLAG = '1' "
        SQL = SQL & " 				 				GROUP BY SUBSTRING(WDATE,1,8),B.MB_CODE "
        SQL = SQL & " 			  			) A "
        SQL = SQL & " 		 			GROUP BY A.WDATE, A.MB_CODE "
        SQL = SQL & " 		 		UNION ALL "
        SQL = SQL & " 		 		SELECT 0,0,0, A.MB_CODE, A.WDATE, SUM(A.ORMONEY) ORHAPMONEY "
        SQL = SQL & " 		 			FROM (	SELECT ISNULL(SUM(BTT_MONEY),0) AS ORMONEY, B.MB_CODE, SUBSTRING(A.WDATE,1,8) WDATE FROM tb_spo_betting_MASTER A "
        SQL = SQL & " 		 						INNER JOIN TB_MEMBER B ON A.USERID=B.MB_ID "
        SQL = SQL & " 		 						GROUP BY B.MB_CODE, SUBSTRING(A.WDATE,1,8) "
        SQL = SQL & " 		 					UNION ALL "
        SQL = SQL & " 		 					SELECT ISNULL(SUM(ORMONEY),0) AS ORMONEY , B.MB_CODE, SUBSTRING(WDATE,1,8) WDATE FROM TB_ORDER_INFO A, TB_MEMBER B  "
        SQL = SQL & " 		 						WHERE A.USERID=B.MB_ID  "
        SQL = SQL & " 		 						GROUP BY SUBSTRING(WDATE,1,8),B.MB_CODE "
        SQL = SQL & " 		 				) A "
        SQL = SQL & " 		  			GROUP BY A.WDATE, A.MB_CODE "
        SQL = SQL & "          	) A "
        SQL = SQL & "			WHERE A.WDATE = '"& REPLACE(LEFT(NOW(),10),"-","") &"' "
        SQL = SQL & " 			GROUP BY A.MB_CODE, A.WDATE "
        SQL = SQL & " 	) T2 ON T1.userid=T2.AID "
        SQL = SQL & " 	LEFT OUTER JOIN ( "
        SQL = SQL & " 		SELECT SUM(MB_MONEY) USER_MONEY, MB_CODE FROM TB_MEMBER  "
        SQL = SQL & " 			GROUP BY MB_CODE "
        SQL = SQL & " 	) T3 ON T1.USERID=T3.MB_CODE "
        SQL = SQL & " ) A "
        SQL = SQL & " WHERE A.WDATE >= '"& VAL1 &"' AND A.WDATE <= '"& VAL2 &"' "

        if val3<>"" Then SQL = SQL & " AND A.AID = '"& VAL3 &"' "

        SQL = SQL & " GROUP BY A.IDX, A.AID, A.IN_MONEY, A.OUT_MONEY, A.END_MONEY, A.ORMONEY, A.USER_MONEY, A.WDATE, A.ORHAPMONEY  "
        SQL = SQL & " ORDER BY A.WDATE DESC, A.AID ASC "

        getAgentPaySas = SQL
    END FUNCTION

    FUNCTION getRepCp(val1,val2)
        cp_nm_val = "배당률증가 3%,배당률증가 5%,배당률증가 10%,미적중환급 50%,미적중환급 30%,축구배팅취소,야구배팅취소,농구배팅취소,배팅취소쿠폰,상한가업 50%"
        cp_fd_val = "cp_addper_3,cp_addper_5,cp_addper_10,cp_retper_50,cp_retper_30,cp_notSOmin_40,cp_notBAmin_40,cp_notBSmin_40,cp_notmin_30,cp_upmax"
        cp_nm_val_arr = split(cp_nm_val,",")
        cp_fd_val_arr = split(cp_fd_val,",")

        if val1="1" then
            For f=0 to ubound(cp_nm_val_arr)
                imsival_nm = trim(cp_nm_val_arr(f))
                imsival_fd = trim(cp_fd_val_arr(f))
                if imsival_nm=val2 then
                    realval_val = imsival_fd
                    exit for
                end if
            Next
        else
            For f=0 to ubound(cp_nm_val_arr)
                imsival_nm = trim(cp_nm_val_arr(f))
                imsival_fd = trim(cp_fd_val_arr(f))
                if imsival_fd=val2 then
                    realval_val = imsival_nm
                    exit for
                end if
            Next
        end if

        getRepCp = realval_val
    END FUNCTION

    FUNCTION getRepCpSel(val1,val2,val3,val4)
        if val4="1" then
            Response.write "<select name='"& val1 &"' "& val3 &" style='font-size:11px;height:18px'>"
        else
            Response.write "<select name='"& val1 &"' "& val3 &" style='font-size:11px;height:18px' disabled>"
        end if
        Response.write "<option value=''>쿠폰선택</option>"

        set rs2 = server.CreateObject("ADODB.Recordset")
        SQL = "SELECT CPNM FROM TB_SPO_CP_USE WHERE USERID = '"& SESSION("USERID") &"' AND UFLAG = 'Y' AND CPNM NOT IN ('cp_notSOmin_40','cp_notBAmin_40','cp_notBSmin_40','cp_notmin_30') ORDER BY WDATE DESC"
        rs2.Open sql, db, 1
        Do until rs2.eof
            cpnm_fd = rs2(0)
            cpnm_nm = getRepCp("2",cpnm_fd)
            selected = ""
            if val2=cpnm_fd then selected = " selected"
            '//
            response.Write "<option value='"& cpnm_fd &"' "& selected &">" & cpnm_nm
            '//
        rs2.movenext
        Loop
        '//
		Response.write "</select>"
        '//
    END FUNCTION

    FUNCTION getRepCpSel_in(val1,val2,val3,val4,val5,val6)
        if val4<>"" then
            if val6="" then
                set rs2 = server.CreateObject("ADODB.Recordset")
                SQL = "SELECT CPNM,CP_IDX FROM TB_SPO_CP_USE WHERE USERID = '"& SESSION("USERID") &"' AND UFLAG = 'Y' AND CPNM IN ("& val4 &") ORDER BY WDATE DESC"
                rs2.Open sql, db, 1
                imsicnt = rs2.recordcount
                if imsicnt>0 then
                    Response.write "<select name='"& val1 &"' "& val3 &" style='font-size:11px;height:18px;width:68px' onchange='cporderchb("""& val5 &""",this.value)'>"
                    Response.write "<option value=''>쿠폰선택</option>"
                    Do until rs2.eof
                        cpnm_fd = rs2(0)
                        cpnm_nm = getRepCp("2",cpnm_fd)
                        selected = ""
                        if val2=cpnm_fd then selected = " selected"
                        response.Write "<option value='"& cpnm_fd &"' "& selected &">" & cpnm_nm
						rs2.movenext
                    Loop
                    rs2.close
                    Response.write "</select>"
                end if
            end if
        end if
    END FUNCTION

    FUNCTION getGmJcode(val1)
        set frs = server.CreateObject("ADODB.Recordset")
        SQL = " SELECT SPO_CODE FROM TB_SPO_KIND WHERE SPO_FLAG='J' AND SPO_NAME='"& val1 &"' "
        frs.Open sql, db, 1
        if not frs.eof then
            FF_SPO_NAME = frs(0)
        end if
        frs.close

        getGmJcode = FF_SPO_NAME
    END FUNCTION

    FUNCTION getGmDatetimeMin(val1)
        set frs = server.CreateObject("ADODB.Recordset")
        SQL = " SELECT MIN(GM_DATETIME) FROM TB_SPO_BETTING_INFO WHERE BTT_IDX = "& VAL1
        frs.Open sql, db, 1
        if not frs.eof then
            min_gm_datetime = frs(0)
        end if
        frs.close

        getGmDatetimeMin = min_gm_datetime
    END FUNCTION

    FUNCTION getCpKindChb(val1,val2)
        set frs = server.CreateObject("ADODB.Recordset")
        SQL = "      SELECT CASE WHEN A.GM_JCODE_CNT=A.BTT_CNT THEN 'Y' ELSE 'N' END "
        SQL = SQL & "  FROM ( "
        SQL = SQL & " 		SELECT COUNT(A.GM_JCODE) GM_JCODE_CNT, B.BTT_CNT "
        SQL = SQL & " 		  FROM VW_SPO_BETTING_INFO A, TB_SPO_BETTING_MASTER B "
        SQL = SQL & " 		 WHERE A.BTT_IDX=B.BTT_IDX "
        SQL = SQL & " 		   AND A.BTT_IDX  =  "& VAL1 &"  "
        SQL = SQL & " 	   	   AND A.GM_JCODE = '"& VAL2 &"' "
        SQL = SQL & " 		 GROUP BY BTT_CNT "
        SQL = SQL & " 	 ) A "
        frs.Open sql, db, 1
        if not frs.eof then
            if frs(0)="Y" then imsifubval = "Y"
        end if
        frs.close
        getCpKindChb = imsifubval
    END FUNCTION

    FUNCTION getCpKindChb_sql(val1,val2)
        sqlff      = val2
        gmjcode_sk = getGmJcode("축구")
        gmjcode_bb = getGmJcode("야구")
        gmjcode_bs = getGmJcode("농구")
        gmjcode_sk_chb = getCpKindChb(val1,gmjcode_sk)
        gmjcode_bb_chb = getCpKindChb(val1,gmjcode_bb)
        gmjcode_bs_chb = getCpKindChb(val1,gmjcode_bs)

        if gmjcode_sk_chb="Y" then
            if sqlff = "" then
                sqlff = "'cp_notSOmin_40'"
            else
                sqlff = sqlff & ",'cp_notSOmin_40'"
            end if
        end if

        if gmjcode_bb_chb="Y" then
            if sqlff = "" then
                sqlff = "'cp_notBAmin_40'"
            else
                sqlff = sqlff & ",'cp_notBAmin_40'"
            end if
        end if

        if gmjcode_bs_chb="Y" then
            if sqlff = "" then
                sqlff = "'cp_notBSmin_40'"
            else
                sqlff = sqlff & ",'cp_notBSmin_40'"
            end if
        end if

        getCpKindChb_sql = sqlff
    END FUNCTION


    FUNCTION cancelOrderSave(val1)
        sql = " insert into tb_spo_betting_master_back select * from tb_spo_betting_master where btt_idx = "& val1
        db.execute sql
        sql = " insert into tb_spo_betting_info_back select * from tb_spo_betting_info where btt_idx = "& val1
        db.execute sql
    END FUNCTION
%>
