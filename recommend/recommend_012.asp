<!-- #include file="../include/sub_menu.asp" -->
      <!--contents-->
      <tr>
        <td align="center"><table width="1024" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="180" valign="top"><!--#include file="../include/lmenu_08.asp" --> </td>
            <td width="10"></td>
            <td width="834" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td height="40"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                  <tr>
		<td width="120"><span class="new_tit_st">포인트<span>전환</span></span></td>
                    <td>&nbsp;</td>
                    <td align="right"><span class="location_01">홈 > 추천내역 > </span><span class="location_02"> 포인트전환 </span></td>

                </table></td>
              </tr>
                 <tr>
                <td height="1" bgcolor="#292a2d"></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td align="center">
                <!-- -->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td align="center">
                    <table width="820" border="0" cellspacing="0" cellpadding="0" class="p_table">
                      <tr>
                        <th width="15%">적립포인트 안내</th>
                        <td>
                        <span class="ti">배팅머니 적립금(미당첨 시 지급) 이벤트 진행 시 적용</span><br />
                        1. 회원님께서 배팅하신 배팅 머니의 일정 % 만큼 미당첨 되었을 경우 자동으로 적립 됩니다.<br />
                            적립 %는 회원 레벨에 따라 차등 적용됩니다.<br />
                        2. 회원 등급은 게시판 참여율에 따라 관리자에 의해 레밸업 됩니다.<br />
                        3. 선택하신 모든 폴더의 경기 결과가 적용되어야 적립금이 지급되며, 선택하신 경기가 취소되면 적립금은 지급되지 않습니다.<br /><br />

                        <span class="ti">추천인 적립금 이벤트 진행시 적용</span><br />
                        1. 추천인 적립금은 회원님께서 지인이나 기타 소개로 새로운 가입자의 추천인으로 등록이 되면, 해당 회원의 배팅머니의 일정 % 만큼 적립금을
                            지급받으실 수 있습니다.<br />
                        2. 추천인 적립금을 받기 위해서는 본인의 아이디가 추천인 아이디로 등록이 되어야 함으로 운영자에게 신청 하셔야 합니다.<br />
                        3. 월별 추천인 인원제한은 없으며, 보너스머니 또한 별도 제한이 없습니다.<br />
                        4. 동일한 IP 대역을 사용중인 아이디는 추천인 적립금이 지급되지 않습니다.<br />

                        </td>
                      </tr>
                      <tr>
                        <th>나의 보유적립금</th>
                        <td class="po"><%=FORMATNUMBER(ConSet_userpoint,0)%> Point</td>
                      </tr>

<script language='JavaScript'>
<!--
function addpp(){
	var gpoint = Number(<%=ConSet_userpoint%>);
	if (gpoint==0){
        alert('적립할 포인트가 없습니다.');
        return false;
    }
	if (form.addpoint.value==""){
        alert('사용할 포인트를 입력해 주세요.');
        return false;
    }
	form.submit();
	return false;
}

function onlyNumber(){
   if((event.keyCode<48)||(event.keyCode>57))
      event.returnValue=false;
}

//-->
</script>
<form action="recommend_012_PROC.asp" name=form method=post onsubmit="return addpp();">

                      <tr>
                        <th>적립금 포인트사용</th>
                        <td>
                        <table width=100% border="0" cellpadding="0" cellspacing=0>
                            <tr>
                                <td width=50><input name="addpoint" type="text" ONKEYPRESS="onlyNumber()"></td>
                                <td><input type=image src="../images/point_btn.jpg" width="135" height="33"></td>
                            </tr>
                        </table>
                        </td>
                      </tr>
</form>
                      <tr>
                        <th>추천아이디</th>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="rec_id">

<%
    gotopage=rq("gotopage")
    stxt7  = rq("stxt7")
    '//
    if gotopage="" then
        gotopage=1
    end if
    '//
	set rs = server.CreateObject("ADODB.Recordset")
	SQL = "       SELECT A.IDX, A.MEMTYPE, A.USERID "
	SQL = SQL & "   FROM (SELECT TOP 1 0 IDX , '전체보기' MEMTYPE, '' USERID FROM TB_MEMBER "
	SQL = SQL & "          UNION ALL "
	SQL = SQL & "         SELECT 1 IDX , MB_ID +'('+MB_NICK+')' MEMTYPE, MB_ID USERID FROM TB_MEMBER WHERE MB_CID = '"& SESSION("USERID") &"' "
	SQL = SQL & "        ) A "
	SQL = SQL & "  ORDER BY A.IDX ASC, A.MEMTYPE ASC "
	rs.Open sql, db, 1
    '//
%>

                          <tr>

<%
	i=1
	do until rs.EOF or i>rs.pagesize
        imsinum = i mod 5
        if i>1 and imsinum=1 then
            response.Write "</tr><tr>"
        end if
        memtype = rs("memtype")
        userid  = rs("userid")
%>

                            <td><a href="?stxt7=<%=userid%>"><%=memtype%></a></td>

<%
    '//
rs.movenext
j=j-1
i=i+1
loop
rs.close
%>

                          </tr>
                        </table>
                        </td>
                      </tr>
                    </table>

                    </td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td align="center">
                    <!-- 리스트-->
                    <table width="820" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="center" colspan=2>
                        <table width="820" border="0" cellpadding="0" cellspacing="5" bgcolor="#292a2d">
                            <tr>
                                <td align="center" bgcolor="#1c1d1f">
                                <table width="99%" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td height="5"></td>
                                    </tr>
                                    <tr>
                                        <td height="32" background="../images/charge/charge_bg_01.jpg">
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td width="5%" align="center"><strong>번호</strong></td>
                                                <td width="47%" align="center"><strong>내용</strong></td>
                                                <td width="16%" align="center"><strong>보유포인트</strong></td>
                                                <td width="16%" align="center"><strong>변동포인트</strong></td>
                                                <td width="16%" align="center"><strong>일시</strong></td>
                                            </tr>
                                        </table>
                                        </td>
                                    </tr>



<%
    '//
	set rs = server.CreateObject("ADODB.Recordset")
	SQL = " SELECT * FROM tb_member_uPointLog WHERE USERID='"& SESSION("USERID") &"' "
	if stxt7<>"" then SQL = SQL & " AND suserid = '"& stxt7 &"' "
	SQL = SQL & " ORDER BY IDX DESC"
	rs.PageSize=15
	rs.Open sql, db, 1
	if not rs.EOF Then rs.AbsolutePage=int(gotopage)
	schCnt = rs.recordcount
	'//
	j=schCnt-((gotopage-1)*10)
	i=1
	do until rs.EOF or i>rs.pagesize
        idx    = rs("idx")
        userid = rs("userid")
        uflag  = rs("uflag")
        upoint = formatnumber(rs("upoint"),0)
        nowPoint = formatnumber(rs("nowPoint"),0)
        utitle = rs("utitle")
        wdate  = rs("wdate")
        '//
		wdate  = getDateRe("8",wdate,"-")
        '//
        bgcolor=""
        if i mod 2 = 0 then bgcolor="#292a2d"
        if uflag="2" then upoint = -upoint
        '//
%>

                                    <tr>
                                        <td height="30" bgcolor="<%=bgcolor%>">
                                        <table width="100%" border=0 cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td width="5%" align="center"><%=j%></td>
                                                <td width="47%" align="LEFT"><%=utitle%></td>
                                                <td width="16%" align="center" class="co_point"><%=formatnumber(nowPoint,0)%> P</td>
                                                <td width="16%" align="center"><b><%=formatnumber(upoint,0)%> P</b></td>
                                                <td width="16%" align="center"><%=wdate%></td>
                                            </tr>
                                        </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="1" bgcolor="#292a2d"></td>
                                    </tr>

<%
    '//
rs.movenext
j=j-1
i=i+1
loop
%>








                                </table>
                                </td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td height="30" align="center" colspan=2><!--#include virtual="/include/paging.asp" --></td>
                    </tr>
                    <tr>
                        <td height="50">&nbsp;</td>
                    </tr>
                </table>
                    <!-- //리스트-->
                    </td>
                  </tr>
                </table>

                <!-- -->
     </td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
           <!--footer-->
     <!-- #include file="../include/sub_footer.asp" -->