<!--#include virtual="/admin/include/top.asp" -->
<%
	'//
	nowdate = Left(getDateTime(now()),8)
	yesdate = Left(getDateTime(now()-1),8)
	now_yy  = mid(nowdate,1,4)
	now_mm  = mid(nowdate,5,2)
	now_dd  = mid(nowdate,7,2)
	yes_yy  = mid(yesdate,1,4)
	yes_mm  = mid(yesdate,5,2)
	yes_dd  = mid(yesdate,7,2)
	If Left(now_mm,1)="0" Then now_mm=Right(now_mm,1)
	If Left(now_dd,1)="0" Then now_dd=Right(now_dd,1)
	If Left(yes_mm,1)="0" Then yes_mm=Right(yes_mm,1)
	If Left(yes_dd,1)="0" Then yes_dd=Right(yes_dd,1)
	'//
	
	sql = "EXEC Get_BriefInfo @nowdate='" & rqVal(nowdate) & "', @yesdate='" & rqVal(yesdate) & "' "
	rs.Open sql, db, 1
	If Not rs.eof then
		TotMemCnt = rs(0)
		inMemCnt  = rs(1)
		outMemCnt = rs(2)
		nowMemCnt = rs(3)
		NowConCnt = rs(4)
		vTotCnt   = rs(5)
		vtCnt     = rs(6)
		vyCnt     = rs(7)
	End if
	rs.close
	'//
	
'	sql = " exec sp_helpfile "
'	set rs = db.execute(sql)
'	maxsize = rs("maxsize")
'	used    = FormatNumber(Replace(rs("size"),"KB","")/1024,2)	'기본값은 KB 단위
'	rs.close
'	set rs = nothing
	'//
'	Set FSO=server.createobject("scripting.filesystemobject")
'	folderpath = "d:\web\slotto"
'	Set gf=fso.getfolder(folderpath)
'	fsize = gf.size
'	fsize = FormatNumber((fsize/1024)/1024,2)
	'//
	myip = request.ServerVariables("REMOTE_HOST")
	'//
%>

<table width="1200px" border="0" cellpadding="10" cellspacing="10" align="center">
  <tr>
    <td style="width:530px;padding:10px" align="center" valign="top">
	    <h1 class="w3-xxlarge w3-text-grey" style="font-family:'Oswald'">H-SPORTS management</h1>
	    <div  style="white-space:nowrap;margin: 30px"><span class="w3-center w3-padding-large w3-orange w3-large w3-wide w3-animate-opacity" style="font-family:'Oswald'">MASTER PAGE</span></div>
	    <div class="w3-padding w3-opacity-max"><img src="/admin/images/mba.jpg" width="500" height="270"></div>
	    <!--table  border="0" cellpadding="0" cellspacing="0">
		    <tr>
			    <td align="right"><img src="/admin/images/main_img_01.gif" width="284" height="274"></td>
			    <td><img src="/admin/images/main_img_02.gif" width="284" height="274"></td>
		    </tr>
			<tr>
				<td align="right"><img src="/admin/images/main_img_03.gif" width="284" height="277"></td>
				<td><img src="/admin/images/main_img_04.jpg" width="284" height="277"></td>
	  		</tr>
	    </table-->	    
	</td>
    <td valign="top">
<!---------- 회원현황 시작 ---------->
<div class="w3-card-4">
	<div class="w3-green w3-padding">
	  <h5><i class="fa fa-user"></i> 회원현황</h5>
	</div>
	<div class="w3-light-grey w3-padding-large">
		<table class="w3-table">
			<tr>
	             <td class="service2 w3-medium"><i class="fa fa fa-bars"></i> 전체:
                  <b><%=TotMemCnt%></b>명</td>
                <td class="service2 w3-medium">정상: <b><%=inMemCnt%></b>명</td>
                <td class="service2 w3-medium">탈퇴: <b><%=outMemCnt%></b>명</td>
                <td class="service2 w3-medium"><i class="fa fa fa-refresh"></i> 오늘가입:
                  <b><%=nowMemCnt%></b>명</td>
            </tr>
         </table>
	</div>
	<div class="w3-green w3-grayscale-min w3-padding  w3-medium w3-center">
	  <i class="fa fa-plus"></i>  현재 접속회원수 : <%=NowConCnt%>명
	</div>
</div>
	  <!---------- 회원현황 끝 ---------->

	  <!---------- 기타정보 시작 ---------->
<div class="w3-card-4" style="margin-top: 20px;margin-bottom: 20px">
	<div class="w3-blue w3-padding">
	  <h5><i class="fa fa-cloud"></i> 기타정보</h5>
	</div>
	<div class="w3-light-grey w3-padding-large">
		<table class="w3-table">
              <!--tr>
                <td height="20" class="service3"><img src="/admin/images/icon_db.gif" width="18" height="11" align="absmiddle">DB사용량:
                  <b><%=used%></b>MB</td>
                <td class="service1">HDD사용량: <b><%=fsize%></b> MB</td>
              </tr-->			
              <tr>
                <td class="service1 w3-medium"><i class="fa fa fa-bars"></i>  방문객 카운터: </td>
                <td class="service1 w3-medium"><b>Total <%=FormatNumber(vTotCnt,0)%> </b></td>
                <td class="service1 w3-medium"><b>Yesterday: <%=FormatNumber(vyCnt,0)%> </b></td>
                <td class="service1 w3-medium"><b>Today: <%=FormatNumber(vtCnt,0)%> </b></td>
              </tr>
         </table>
	</div>
	<div class="w3-blue w3-grayscale-min w3-padding  w3-medium w3-center">
	  <i class="fa fa-plus"></i>  My IP Address : <%=myip%>
	</div>
</div>	  
	  
      <!---------- 기타정보 끝 ---------->



      <!---------- 고객센터  시작 ---------->
<div style="margin-top: 20px;margin-bottom: 20px">
	<div class="w3-brown w3-card-2 w3-padding-tiny"  style="margin-bottom: 5px">
	  <h6 style="padding-left: 10px"><i class="fa fa-comment"></i> 고객센터</h6>
	</div>      
		<table  class="w3-table w3-hoverable w3-bordered">

<%
	set rs = server.CreateObject("ADODB.Recordset")
	SQL = " select top 7 * from tb_cus_qna order by idx desc "
	rs.Open sql, db, 1
	'//
	Do Until rs.eof
		wdate  = getDateRe("1",rs("qwdate"),"-")
%>
			<tr >
				<td>
					<!--i class="fa fa-arrow-right w3-tiny w3-text-grey"></i--> 				
					<a href='/admin/board/list.asp' class="w3-small"><%=rs("qmemo")%></a>
					<%If Left(now(),10)=wdate then%><span class="w3-tag w3-red w3-tiny">new</span><%end if%>
				</td>
				<td width="100" align="right" nowrap><span class="w3-text-grey w3-tiny"><%=wdate%></span></td>
			</tr>

<%
	rs.movenext
	Loop
	rs.close
%>
            </table>	
</div>	

      <!---------- 고객센터  끝 ---------->

      <!---------- 자유게시판 시작 ---------->
      
<div style="margin-top: 20px;margin-bottom: 20px">
	<div class="w3-dark-grey w3-card-2 w3-padding-tiny"  style="margin-bottom: 5px">
	  <h6 style="padding-left: 10px"><i class="fa fa-comment"></i> 자유게시판</h6>
	</div>
			<table  class="w3-table w3-hoverable w3-bordered">
<%
	set rs = server.CreateObject("ADODB.Recordset")
	SQL = " select top 7 * from tb_board order by idx desc "
	rs.Open sql, db, 1
	'//
	Do Until rs.eof
		wdate  = getDateRe("1",rs("wdate"),"-")
%>

				<tr >
					<td>
						<!--i class="fa fa-arrow-right w3-tiny w3-text-grey"></i--> 
						<a href='/admin/board/board/board_view.asp?idx=<%=rs("idx")%>' class="w3-small"><%=rs("title")%></a>
						<%If Left(now(),10)=wdate then%><span class="w3-tag w3-red w3-tiny">new</span><%end if%>
					</td>
					<td width="100" align="right" nowrap><span class="w3-text-grey w3-tiny"><%=wdate%></span></td>
				</tr>

<%
	rs.movenext
	Loop
	rs.close
%>

            </table>	
	
</div>	      

      <!---------- 자유게시판 끝 ---------->
      
    </td>
  </tr>
  <tr>
    <td height="5"> </td>
  </tr>
</table>

<!--#include virtual="/admin/include/bottom.asp" -->