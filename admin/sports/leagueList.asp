<!--#include virtual="/admin/include/top.asp" -->
<%
   	SUB leagueRadioBtn2(val1,val2)
        If val1<>"" Then
            if val2="" then checked=" checked"
            response.Write "<input type='radio' name='"& val1 &"' value='' "& checked &" > <label>전체</label> "
            set frs = server.CreateObject("ADODB.Recordset")
            SQL = "SELECT SPO_CODE,SPO_NAME FROM TB_SPO_KIND WHERE SPO_FLAG = 'J' ORDER BY SPO_NAME ASC "
            frs.Open sql, db, 1
            Do until frs.eof
                 checked=""
                if val2=frs(0) then checked=" checked"
                response.Write "<input type='radio' name='"& val1 &"' value='"& frs(0) &"' "& checked &" > <label>"& frs(1) &"</label> "
            frs.movenext
            Loop
            frs.close
        End if
	END SUB
%>
<table style="width:100%;min-width:1200px!important;height:100%;min-height:600px;border:0px;padding:0px;margin:0px;">
	<tr>
		<td style="width:160px!important;min-width:160px!important;background-color:#F7F7F7" valign="top">
			<!--#include virtual="/admin/include/leftSports.asp" -->
		</td>
		<td valign="top" style="min-width:1040px!important;">

    	<table width=100% cellpadding=10 cellspacing=0 border=0>
			<tr>
				<td valign=top>
					<div class="w3-container w3-card-2 w3-blue w3-text-16 w3-padding-large w3-bolder" style="margin-bottom:10px;">
					  리그관리
					</div>

<%
	flag  = "L"
	stxt1 = rq("stxt1")
	stxt2 = rq("stxt2")
	stxt3 = rq("stxt3")
	stxt4 = rq("stxt4")
	stxt5 = rq("stxt5")
	stxt6 = rq("stxt6")
	stxt7 = rq("stxt7")
	stxt8 = rq("stxt8")
	stxt9 = rq("stxt9")
	stxt10 = rq("stxt10")
	GotoPage = rq("GotoPage")
	'//
    if gotopage="" then gotopage=1
    '//
	set rs = server.CreateObject("ADODB.Recordset")
	SQL = "       SELECT A.* , B.SPO_NAME JNAME, D.SPO_ENAME NNAME, D.SPO_IMGNAME NIMGNAME, ISNULL(E.TEAM_CNT,0) TEAM_CNT "
	SQL = SQL & "   FROM TB_SPO_KIND A "
	SQL = SQL & "  INNER JOIN (SELECT SPO_CODE, SPO_NAME FROM TB_SPO_KIND WHERE SPO_FLAG='J') B "
	SQL = SQL & "     ON A.CH_JCODE = B.SPO_CODE "
	SQL = SQL & "  INNER JOIN (SELECT SPO_CODE, SPO_ENAME, SPO_IMGNAME FROM TB_SPO_KIND WHERE SPO_FLAG='N') D "
	SQL = SQL & "     ON A.CH_NCODE = D.SPO_CODE "
	SQL = SQL & "   LEFT OUTER JOIN (SELECT ISNULL(COUNT(SPO_ENAME),0) TEAM_CNT, CH_LCODE FROM TB_SPO_KIND WHERE SPO_FLAG='T' GROUP BY CH_LCODE) E "
	SQL = SQL & "     ON A.SPO_CODE = E.CH_LCODE "
	SQL = SQL & "  WHERE SPO_FLAG='L' "
    '//
    IF STXT1<>"" THEN SQL = SQL & " AND CH_JCODE = '"& STXT1 &"' "
    IF STXT4<>"" AND STXT5<>"" THEN SQL = SQL & " AND "& STXT4 &" LIKE '%"& STXT5 &"%' "
    '//
	SQL = SQL & " ORDER BY WDATE DESC "
	rs.PageSize=50
	rs.Open sql, db, 1
	if not rs.EOF Then rs.AbsolutePage=int(gotopage)
	schCnt = rs.recordcount
	'//
    param = "flag="&flag&"&gotopage="&gotopage&"&stxt1="&stxt1&"stxt2="&stxt2&"&stxt3="&stxt3&"&stxt4="&stxt4&"&stxt5="&stxt5&"&stxt6="&stxt6&"&stxt7="&stxt7&"&stxt8="&stxt8&"&stxt9="&stxt9&"&stxt10="&stxt10
    '//
%>

<script language='JavaScript'>
<!--
function searChb(){
	if ((form.stxt4.value != "")&&(form.stxt5.value == "")){alert('검색구분을 선택해 주세요.');form.stxt5.focus();	return false;}
	if ((form.stxt4.value == "")&&(form.stxt5.value != "")){alert('검색어를 입력해 주세요.');form.stxt4.focus();	return false;}
	form.submit();
}
//-->
</script>
					<div class="w3-container w3-leftbar w3-light-grey w3-padding w3-border" style="margin-bottom:10px;">		
						<form name="form" id="form" method="post" action="?" onsubmit="return searChb()" class="w3-right">
						<div class="w3-left w3-small w3-padding">
							<% call leagueRadioBtn2("stxt1",stxt1)%>
                        </div>  	
						<p>
						<div class="w3-right" style="margin-left: 3px;padding-top: 5px;">
							<div class="w3-left" style="margin-left: 3px;">
								<select class="w3-select w3-border w3-small w3-padding" name="stxt4" style="min-width:125px;max-width:250px">
									<option value="">검색구분</option>
									<option value="A.CH_JCODE" <%If stxt4="A.CH_JCODE" Then Response.write " selected"%>>종목코드
									<option value="B.SPO_NAME" <%If stxt4="B.SPO_NAME" Then Response.write " selected"%>>종목명
									<option value="A.SPO_CODE" <%If stxt4="A.SPO_CODE" Then Response.write " selected"%>>리그코드
									<option value="A.SPO_NAME" <%If stxt4="A.SPO_NAME" Then Response.write " selected"%>>리그명		
								</select>						
							</div>							
							<div class="w3-left" style="margin-left: 3px;"><input class="w3-input w3-padding w3-border w3-left" type="text" placeholder="검색어" name="stxt5" style="width:378px"></div>
							<div class="w3-left" style="margin-left: 3px;"><button class="w3-btn w3-padding w3-black w3-small w3-left w3-bold" type="submit" style="width:125px">검색하기</button></div>
						</div>
						</p>
						<!--/form-->						
					</div>	
					
				<!--table border="0" cellpadding="3" cellspacing="1" style="width:100%; border:1px solid #CBE1EE;">

<form name="form" method="post" action="?">

					<tr>
						<td bgcolor=#F2F2F2 width=15% align=center>조건검색</td>
						<td height=30>
                            <% call leagueRadioBtn("stxt1",stxt1)%>
                            <BR>
							<select name=stxt4>
								<option value="">--검색구분--
								<option value="A.CH_JCODE" <%If stxt4="A.CH_JCODE" Then Response.write " selected"%>>종목코드
								<option value="B.SPO_NAME" <%If stxt4="B.SPO_NAME" Then Response.write " selected"%>>종목명
								<option value="A.SPO_CODE" <%If stxt4="A.SPO_CODE" Then Response.write " selected"%>>리그코드
								<option value="A.SPO_NAME" <%If stxt4="A.SPO_NAME" Then Response.write " selected"%>>리그명
							</select>
							<input type=text name=stxt5 value="<%=stxt5%>" size=20>
							<input type="image" src="/admin/images/btn_search1.gif" align=bottom onclick="return searChb()">
						</td>
					</tr>

</form>

				</table-->

			  <div class="w3-panel w3-row w3-padding-small w3-sand w3-leftbar w3-border-blue">
			    <div class="w3-col s6 w3-padding-small">
				    <span class="w3-text-dark-grey w3-text-14">검색결과 :</span> <span class="w3-text-blue w3-text-14 w3-bold"><%=FormatNumber(schCnt,0)%></span> 
				</div>
			    <div class="w3-col s6">
				    <div class="w3-right">
					    <button type=button class="w3-btn w3-green w3-bold" onclick="location.href='regFrm.asp?flag=<%=flag%>'">리그 등록</button>
				    </div>			
			    </div>	    
			  </div>		

				<!--BR>
				<table width=100% cellpadding=3 cellspacing=0 border=0>
					<tr>
						<td>
							<img src='/admin/images/icon_arrow3.gif' align=absmiddle>
                            &nbsp;Total <font color=blue><%=FormatNumber(totCnt,0)%></font>
							/ 검색결과 <font color=blue><%=FormatNumber(schCnt,0)%></font>
						</td>
						<td width=30% align=right>
							<input type=button value="등록" onclick="location.href='regFrm.asp?flag=<%=flag%>'">
						</td>
				</table-->

<!--form name="frm" method="post" action="?">
<input type="hidden" name="stxt1" value="<%=stxt1%>">
<input type="hidden" name="stxt2" value="<%=stxt2%>">
<input type="hidden" name="stxt3" value="<%=stxt3%>">
<input type="hidden" name="stxt4" value="<%=stxt4%>">
<input type="hidden" name="stxt5" value="<%=stxt5%>">
<input type="hidden" name="stxt6" value="<%=stxt6%>">
<input type="hidden" name="stxt7" value="<%=stxt7%>">
<input type="hidden" name="stxt8" value="<%=stxt8%>">
<input type="hidden" name="stxt9" value="<%=stxt9%>">
<input type="hidden" name="stxt10" value="<%=stxt10%>">
<input type="hidden" name="gotopage" value="<%=gotopage%>">
<input type="hidden" name="idx">
<input type="hidden" name="dflag">
</form-->


				<table class="w3-table-all w3-centered w3-card-2" style="min-width: 1000px !important">
					<tr class="w3-blue w3-opacity-min">
						<th class="w3-dark-grey w3-small w3-padding-large" style="width:10%;white-space:nowrap">No.</th>
						<th class="w3-dark-grey w3-small w3-padding-large" style="width:10%;white-space:nowrap">종목코드</th>
						<th class="w3-dark-grey w3-small w3-padding-large" style="white-space:nowrap">종목명</th>
						<th class="w3-dark-grey w3-small w3-padding-large" style="white-space:nowrap">국가명</th>
						<th class="w3-dark-grey w3-small w3-padding-large" style="width:10%;white-space:nowrap">리그코드</th>
						<th class="w3-dark-grey w3-small w3-padding-large" style="white-space:nowrap">리그명</th>
						<th class="w3-dark-grey w3-small w3-padding-large" style="width:5%;white-space:nowrap">등록팀수</th>
						<th class="w3-dark-grey w3-small w3-padding-large" style="width:10%;white-space:nowrap">등록일자</th>
					</tr>

<%
j=schCnt-((gotopage-1)*20)
i=1
do until rs.EOF or i>rs.pagesize
    '//
	spo_idx   = rs("spo_idx")
	spo_code  = rs("spo_code")
	spo_name  = rs("spo_name")
	spo_ename = rs("spo_ename")
	ch_jcode  = rs("ch_jcode")
	jname     = rs("jname")
	nname     = rs("nname")
    nimgname  = rs("nimgname")
    team_cnt  = rs("team_cnt")
	wdate     = getDateRe("1",rs("wdate"),"-")
	'//
%>

					<tr>
						<td><%=j%></td>
						<td><%=ch_jcode%></td>
						<td><%=jname%></td>
						<td><div class="w3-left"><%if nimgname<>"" then%><img src="/fileupdown/icon/<%=nimgname%>" width=16 height=11><%end if%><%=nname%></div></td>
						<td><a href="regFrm.asp?spo_idx=<%=spo_idx%>&<%=param%>"><%=spo_code%></a></td>
						<td><div class="w3-left"><a href="regFrm.asp?spo_idx=<%=spo_idx%>&<%=param%>"><%=spo_name%></a></div></td>
						<td><a href="teamList.asp?stxt1=<%=ch_jcode%>&stxt4=C.SPO_NAME&stxt5=<%=spo_name%>"><font color=red><B><%=team_cnt%></a></td>
						<td><%=wdate%></td>
					</tr>

<%
rs.movenext
j=j-1
i=i+1
loop
%>

				</table>

				<table border="0" cellpadding="0" cellspacing="0" width=100% bgcolor=white>
					<tr height=45>
						<td align=center>
							<!--#include virtual="/admin/include/paging.asp" -->
						</td>
					</tr>
				</table>

		 		</td>
			</tr>
		</table>
 		</td>
	</tr>
</table>

<!--#include virtual="/admin/include/bottom.asp" -->