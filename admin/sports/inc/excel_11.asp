<%
	'//
    flagmsg = getGmflagRe(flag)
	If stxt1="" then stxt1 = Left(now(),10)
	If stxt2="" then stxt2 = Left(now()+1,10)
	if GotoPage = "" Then GotoPage = 1
	stxt11 = Replace(stxt1,"-","")
	stxt22 = Replace(stxt2,"-","")
    '//
	set rs = server.CreateObject("ADODB.Recordset")
	SQL = "           SELECT ISNULL(COUNT(A.IDX),0) "
	SQL = SQL & "       FROM TB_SPO_UP_EXCEL A "
	SQL = SQL & " INNER JOIN (SELECT SPO_IDX, SPO_CODE, SPO_NAME, CH_NCODE, CH_JCODE FROM TB_SPO_KIND WHERE SPO_FLAG='L') B ON CONVERT(decimal(18, 0), A.EXL_VAL1) = B.SPO_IDX "
	SQL = SQL & " INNER JOIN (SELECT SPO_CODE, SPO_NAME, SPO_TIE FROM TB_SPO_KIND WHERE SPO_FLAG='J') C ON B.CH_JCODE = C.SPO_CODE "
	SQL = SQL & "      WHERE GM_FLAG = '"& FLAG &"' AND Filename='"& Filename &"'"
	rs.Open sql, db, 1
    totcnt = rs(0)
    rs.close
    '//
	set rs = server.CreateObject("ADODB.Recordset")
	SQL = "           SELECT C.SPO_NAME JNAME, "
	SQL = SQL & "            B.SPO_NAME LNAME, "
	SQL = SQL & "            A.EXL_VAL4 HTNAME, "
	SQL = SQL & "            A.EXL_VAL5 ATNAME, "
	SQL = SQL & "            A.EXL_VAL6 HTDPER, "
	SQL = SQL & "            A.EXL_VAL7 TTDPER, "
	SQL = SQL & "            A.EXL_VAL8 ATDPER, "
	SQL = SQL & "            A.FILENAME, A.EXL_VAL1 CH_LCODE, A.EXL_VAL2 GM_YMD, A.EXL_VAL3 GM_HMS, A.IDX, C.SPO_TIE "
	SQL = SQL & "       FROM TB_SPO_UP_EXCEL A "
	SQL = SQL & " INNER JOIN (SELECT SPO_IDX, SPO_CODE, SPO_NAME, CH_NCODE, CH_JCODE FROM TB_SPO_KIND WHERE SPO_FLAG='L') B ON CONVERT(decimal(18, 0), A.EXL_VAL1) = B.SPO_IDX "
	SQL = SQL & " INNER JOIN (SELECT SPO_CODE, SPO_NAME, SPO_TIE FROM TB_SPO_KIND WHERE SPO_FLAG='J') C ON B.CH_JCODE = C.SPO_CODE "
	SQL = SQL & "      WHERE GM_FLAG = '"& FLAG &"' AND Filename='"& Filename &"'"
	SQL = SQL & "   ORDER BY A.IDX DESC "
	rs.Open sql, db, 1
	schCnt = rs.recordcount
	'//
    param = "flag="&flag&"&gotopage="&gotopage&"&stxt1="&stxt1&"&stxt2="&stxt2&"&stxt3="&stxt3&"&stxt4="&stxt4&"&stxt5="&stxt5&"&stxt6="&stxt6&"&stxt7="&stxt7&"&stxt8="&stxt8&"&stxt9="&stxt9&"&stxt10="&stxt10
    '//
%>

<script language='JavaScript'>
<!--
function searChb(){
	if ((form.stxt4.value != "")&&(form.stxt5.value == "")){alert('검색구분을 선택해 주세요.');form.stxt5.focus();	return false;}
	if ((form.stxt4.value == "")&&(form.stxt5.value != "")){alert('검색어를 입력해 주세요.');form.stxt4.focus();	return false;}
	form.submit();
}

function CheckAll(objChkBox){
    if (objChkBox.checked==true){
        AllCheck(true);
    }else{
        AllCheck(false);
    }
}

function AllCheck(flag){
    theForm = document.form;
    <%if schCnt=1 then%>
        theForm.snum.checked = flag;
    <%else%>
    for(var i = 0; i < document.getElementsByName("snum").length; i++){
        theForm.snum[i].checked = flag;
    }
    <%end if%>

    return false;
}

function bettingChb(val1) {
	var imsicnt = 0;
	for(var i=0; i<document.form.snum.length;i++){  	//체크박스 갯수
		if(document.form.snum[i].checked == true){
			imsicnt = 1
		}
	}
	if(document.form.snum.checked == true){ imsicnt = 1 }
	if (imsicnt<1) { alert("데이타를 선택해 주세요.") ; return false ; }

	if (val1=="1") {
        jsmsg="등록";
    }else if(val1=="2") {
        jsmsg="삭제";
    }

	if (window.confirm("선택한 데이타를 "+jsmsg+" 하시겠습니까?")){
		form.action = 'uploadExcelProc.asp';
		form.method='post';
		form.ingflag.value=val1;
		form.submit();
		return false;
	}
    return false;
}

function onlyNumKey2() {
    var key = event.keyCode;
    if(!(key==8||key==9||key==144||key==46||(key>=48&&key<=57)||(key<96&&key>105))) {
        event.returnValue = false;
    }
 }

function onlyNumber(){
   if((event.keyCode<48)||(event.keyCode>57))
      event.returnValue=false;
}
//-->
</script>

<form name="form" method="post" action="?">
<input type="hidden" name="flag" value="<%=flag%>">
<input type="hidden" name="ingflag">
<input type="hidden" name="Filename" value="<%=Filename%>">

				<table width=100% cellpadding=3 cellspacing=0 border=0>
					<tr>
						<td>
							<img src='/admin/images/icon_arrow3.gif' align=absmiddle>&nbsp;<%=flagmsg%> - 업로드 엑셀목록
                            &nbsp;Total <font color=blue><%=FormatNumber(totCnt,0)%></font>
							/ 검색결과 <font color=blue><%=FormatNumber(schCnt,0)%></font>
						</td>
						<td width=50% align=right>
							<input type=button value="경기등록"  <%if schCnt>0 then%>onclick="return bettingChb('1')"<%end if%> name=abtn33 style='color:red;'>
							<input type=button value="리스트삭제" <%if schCnt>0 then%>onclick="return bettingChb('2')"<%end if%> name=btn22 style='color:black;'>
						</td>
                    </tr>
				</table>

				<table border="0" cellpadding="1" cellspacing="1" width=100% bgcolor="#CBE1EE">
					<tr height=35 bgcolor="#F2F2F2" align=center>
						<td width=3%><input type=checkbox name="canta" onclick="CheckAll(this)" class="input02"></td>
						<td width=5%>No.</td>
						<td width=10%>종목</td>
						<td width=10%>리그</td>
						<td width=8%>경기일자</td>
						<td width=8%>경기시간</td>
						<td width=19%>홈팀</td>
						<td width=19%>원정팀</td>
						<td width=6%>홈팀<br>(배당)</td>
						<td width=6%>무승부<br>(배당)</td>
						<td width=6%>원정팀<br>(배당)</td>
					</tr>

<%
j=schCnt-((gotopage-1)*20)
i=1
do until rs.EOF
    '//
    jname    = rs(0)
	lname    = rs(1)
	htname   = rs(2)
	atname   = rs(3)
	htdper   = rs(4)
	ttdper   = rs(5)
	atdper   = rs(6)
	filename = rs(7)
    ch_lcode = rs(8)
    gm_ymd   = rs(9)
    gm_hms   = rs(10)
    idx      = rs(11)
    spo_tie  = rs("spo_tie")
	'//
'    gm_datetime  = gm_ymd & gm_hms
'	gm_datetime1 = mid(getDateRe("1",gm_datetime,"-"),3)
'	gm_datetime2 = getDateRe("10",gm_datetime,":")
'	gm_datetime  = gm_datetime1 &" "& gm_datetime2
    '//
    if spo_tie="Y" then
        IF ttdper="" then ttdper=0
        ttdper = formatnumber(ttdper,2)
    else
        ttdper = "VS"
    end if
    'IF ttdper="0" then ttdper = "VS"
%>

<input type="hidden" name="idx" value="<%=idx%>">

					<tr height=25 bgcolor=white align=left>
						<td align=center><%if gm_state<>"50" then%><input type=checkbox name=snum value="<%=idx%>" class="input02" id="snum" ><%end if%></td>
						<td align=center><%=j%></td>
						<td>&nbsp;<%=jname%></td>
						<td>&nbsp;<%=lname%></td>
						<td><input type=text name=gm_ymd value="<%=gm_ymd%>" style="width:100%;text-align:center;" onkeypress="onlyNumKey()" maxlength=8></td>
						<td><input type=text name=gm_hms value="<%=gm_hms%>" style="width:100%;text-align:center;" onkeypress="onlyNumKey()" maxlength=6></td>
						<td><input type=text name=htname value="<%=htname%>" style="width:100%;"></td>
						<td><input type=text name=atname value="<%=atname%>" style="width:100%;"></td>
						<td><input type=text name=htdper value="<%=htdper%>" style="width:100%;text-align:center;" onkeypress="onlyNumKey2()" maxlength=6></td>
						<td <%if spo_tie<>"Y" then%>align=center<%end if%>>
                            <%if spo_tie="Y" then%>
                                <input type=text name=ttdper value="<%=ttdper%>" style="width:100%;text-align:center;" maxlength=6 onkeypress="onlyNumKey2()">
                            <%else%>
                                <input type=hidden name=ttdper value="0">VS
                            <%end if%>
                        </td>
						<td><input type=text name=atdper value="<%=atdper%>" style="width:100%;text-align:center;" onkeypress="onlyNumKey2()" maxlength=6></td>
					</tr>

<%
rs.movenext
j=j-1
i=i+1
loop
%>

</form>

				</table>