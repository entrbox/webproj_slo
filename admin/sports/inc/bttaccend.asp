<%
	    FUNCTION getBttTrHtml2(val1,val2,val3,val4,val5,val6)
        if val6="당첨" then val6="<span class='w3-text-deep-orange w3-bolder w3-text-14'>"& val6 &"</span>"
        haptrhtm = "<tr class='w3-dark-grey'> "
        haptrhtm = haptrhtm & "    <td colspan=20> "
        haptrhtm = haptrhtm & "            <div class='w3-left w3-padding w3-xlarge' style='width:20%;text-align:center;text-shadow:1px 1px 0 #444'><b>배팅날짜 : <span class='w3-text-light-grey w3-bolder'>"& val1 &"</span></b></div> "
        haptrhtm = haptrhtm & "            <div class='w3-left w3-padding w3-xlarge' style='width:17%;text-align:center;text-shadow:1px 1px 0 #444'><b>배당률 : <span class='w3-text-aqua w3-bolder'>"& FORMATNUMBER(val3,2) &"</span></b></div> "
        haptrhtm = haptrhtm & "            <div class='w3-left w3-padding w3-xlarge' style='width:17%;text-align:center;text-shadow:1px 1px 0 #444'><b>예상배당금 : <span class='w3-text-aqua w3-bolder'>"& formatnumber(val4,0) &"</span></b></div> "
        haptrhtm = haptrhtm & "            <div class='w3-left w3-padding w3-xlarge' style='width:17%;text-align:center;text-shadow:1px 1px 0 #444'><b>배팅금액 : <span class='w3-text-amber w3-bolder'>"& formatnumber(val2,0) &"</span></b></div> "
        
        haptrhtm = haptrhtm & "            <div class='w3-left w3-padding w3-xlarge' style='width:17%;text-align:center;text-shadow:1px 1px 0 #444'><b>당첨금 : <span class='w3-text-aqua w3-bolder' id='jsmoney2_"& btt_idx &"'>0</span></span></b></div> "
        haptrhtm = haptrhtm & "            <div class='w3-left w3-padding w3-xlarge' style='width:10%;text-align:center;text-shadow:1px 1px 0 #444'><b><span class='w3-text-aqua w3-bolder'>"& val6 &"</span></b></div> "
        haptrhtm = haptrhtm & "    </td> "
        haptrhtm = haptrhtm & "</tr> "
        getBttTrHtml2 = haptrhtm
    END FUNCTION
    
    accdate = rq("accdate")
	flag  = rq("flag")    '승무패
	stxt1 = rq("stxt1")
	stxt2 = rq("stxt2")
	stxt3 = rq("stxt3")
	stxt4 = rq("stxt4")
	stxt5 = rq("stxt5")
	stxt6 = rq("stxt6")
	stxt7 = rq("stxt7")
	stxt8 = rq("stxt8")
	stxt9 = rq("stxt9")
	stxt10 = rq("stxt10")
	GotoPage = rq("GotoPage")
	'//
    set rs = server.CreateObject("ADODB.Recordset")
    SQL = "       SELECT A.BTT_IDX, "
    SQL = SQL & "        B.BTS_IDX, "
    SQL = SQL & "  	     A.WDATE, "
    SQL = SQL & "        C.MB_ID, "
    SQL = SQL & "        C.MB_NICK, "
    SQL = SQL & "        D.GM_DATETIME, "
    SQL = SQL & "        E.SPO_NAME HTEAMNM, "
    SQL = SQL & "        F.SPO_NAME ATEAMNM, "
    SQL = SQL & "        CONVERT(VARCHAR, D.RST_HTM_SCORE) + ':' + CONVERT(VARCHAR, D.RST_ATM_SCORE) GMSCORE, "
    SQL = SQL & "        B.BTS_STATE, "
    SQL = SQL & "        A.BTT_TOT_DIVIDEND, "
    SQL = SQL & "        A.BTT_MONEY, "
    SQL = SQL & "        A.BTT_DIVIDEND_MONEY, "
    SQL = SQL & "        A.BTT_GEND_MONEY, "
    SQL = SQL & "        A.BTT_ACCFLAG, "
    SQL = SQL & "        A.BTT_CNT,B.BTS_CHFLAG, D.GM_HNDFLAG, D.GM_FLAG, G.SPO_TIE, "
    SQL = SQL & "        D.GM_HDVDE,D.GM_TDVDE,D.GM_ADVDE, "
    SQL = SQL & "        B.BTS_HDVDE, B.BTS_TDVDE, B.BTS_ADVDE, D.GM_STATE, D.GM_VSCHB, H.SPO_NAME LEAGE_NM "
    SQL = SQL & "   FROM TB_SPO_BETTING_MASTER A  "
    SQL = SQL & "  INNER JOIN TB_SPO_BETTING_INFO B ON A.BTT_IDX=B.BTT_IDX "
    SQL = SQL & "  INNER JOIN TB_MEMBER   C ON A.USERID    = C.MB_ID "
    SQL = SQL & "  INNER JOIN tb_spo_game D ON B.GM_IDX    = D.GM_IDX "
    SQL = SQL & "  INNER JOIN tb_spo_kind E ON D.GM_HTCODE = E.SPO_CODE "
    SQL = SQL & "  INNER JOIN tb_spo_kind F ON D.GM_ATCODE = F.SPO_CODE "
	SQL = SQL & "  INNER JOIN TB_SPO_KIND G ON D.GM_JCODE  = G.SPO_CODE AND G.SPO_FLAG='J' "
	SQL = SQL & "  INNER JOIN TB_SPO_KIND H ON D.GM_LCODE  = H.SPO_CODE AND H.SPO_FLAG='L' "
    SQL = SQL & "  WHERE A.BTT_IDX=B.BTT_IDX "
    'SQL = SQL & "    AND A.GM_FLAG     = '"& FLAG &"' "
    SQL = SQL & "    AND A.BTT_ACCFLAG = '9' "
    '//
    'SQL = SQL & "    AND SUBSTRING(A.WDATE,1,8) = '"& accdate &"' "
    SQL = SQL & "    AND SUBSTRING(A.BTT_ACCDATE,1,8) = '"& accdate &"' "
    '//
    SQL = SQL & "  ORDER BY A.WDATE DESC "
    rs.Open sql, db, 1
    '//
%>
<!--#include virtual="/admin/sports/inc/js.asp" -->

<form name="form" id="frm" method="post" action="?">
<input type="hidden" name="flag" value="<%=flag%>">
<input type="hidden" name="accFlag">
<input type="hidden" name="pagenm" value="<%=pagenm%>">

			  <div class="w3-panel w3-row w3-padding-small w3-sand w3-leftbar w3-border-khaki">
			    <div class="w3-col s6 w3-padding">
				    <span class="w3-text-dark-grey w3-text-14">정산일자 :</span> <span class="w3-text-blue w3-text-14 w3-bold"><%=mid(accdate,1,4)%>-<%=mid(accdate,5,2)%>-<%=mid(accdate,7,2)%></span> 
				</div>
			    <div class="w3-col s6 w3-padding-small">
				    <div class="w3-right">
					    <button type=button class="w3-btn"  onclick="history.back()">돌아가기</button>
				    </div>			
			    </div>	    
			  </div>	
			  
				<!--table border="0" cellpadding="3" cellspacing="1" style="width:100%; border:1px solid #CBE1EE;">
					<tr>
						<td bgcolor=#F2F2F2 width=15% align=center>정산일자</td>
						<td height=30>&nbsp;<%=mid(accdate,1,4)%>-<%=mid(accdate,5,2)%>-<%=mid(accdate,7,2)%></td>
					</tr>
				</table>

				<BR>
				<table width=100% cellpadding=3 cellspacing=0 border=0>
					<tr>
						<td>

						</td>
						<td width=50% align=right>
							<input type=button value="돌아가기"   onclick="history.back()" name=btn22 style='color:black;'>
						</td>
                    </tr>
				</table-->
				
				
				

				<table class="w3-table-all w3-bordered w3-centered" style="min-width: 1000px !important">
					<tr class="w3-blue w3-opacity-min">
						<th class="w3-small w3-padding-large" nowrap>No.</th>
						<!--th class="w3-small w3-padding-large" nowrap>베팅일시</th-->
						<th class="w3-small w3-padding-large" nowrap>아이디</th>
						<th class="w3-small w3-padding-large" nowrap>닉네임</th>

                        <th class="w3-small w3-padding-large w3-indigo" style="white-space: nowrap">경기일시</th>
                        <th class="w3-small w3-padding-large w3-indigo" style="min-width:120px;white-space: nowrap">리그</th>
                        
                        <th class="w3-small w3-padding-large w3-indigo" style="min-width:210px;white-space: nowrap" colspan="2">홈팀</th>
                        <th class="w3-small w3-padding-large w3-indigo" style="white-space: nowrap">VS</th>
                        <th class="w3-small w3-padding-large w3-indigo" style="min-width:210px;white-space: nowrap" colspan="2">원정팀</th>
                        <th class="w3-small w3-padding-large w3-indigo" style="white-space: nowrap">스코어</th>
                        <th class="w3-small w3-padding-large w3-indigo" style="white-space: nowrap">결과</th>

						<!--th class="w3-small w3-padding-large" nowrap>배당율</th>
						<th class="w3-small w3-padding-large" nowrap>베팅금액</th>
						<!--th class="w3-small w3-padding-large" nowrap>예상당첨금</th>
						<th class="w3-small w3-padding-large" nowrap>당첨금</td-->
                        <th class="w3-small w3-padding-large" nowrap>대기</td>
					</tr>

<%
    '//
    i=1
    p=1
    old_btt_idx=0
    listidx = 1
    totlist = rs.recordcount
    ing_icnt    = 0
    ing_ccnt    = 0
    ing_wcnt    = 0
    ing_lcnt    = 0
    ing_tcnt    = 0
    '//
    do until rs.eof
        '//
        btt_idx = rs("btt_idx")
        bts_idx = rs("bts_idx")
        wdate   = rs("wdate")
        mb_id   = rs("mb_id")
        mb_nick = rs("mb_nick")
        gm_datetime = rs("gm_datetime")
        hteamnm   = rs("hteamnm")
        ateamnm   = rs("ateamnm")
        gmscore   = rs("gmscore")
        bts_state = rs("bts_state")
        btt_tot_dividend   = rs("btt_tot_dividend")
        btt_money          = rs("btt_money")
        btt_dividend_money = rs("btt_dividend_money")
        btt_gend_money = rs("btt_gend_money")
        btt_accflag    = rs("btt_accflag")
        btt_cnt        = rs("btt_cnt")
        bts_chflag     = rs("bts_chflag")
        gm_hndflag     = rs("gm_hndflag")
        gm_flag        = rs("gm_flag")
        spo_tie        = rs("spo_tie")
    	gm_vschb = rs("gm_vschb")
        leage_nm = rs("leage_nm")

        gm_hdvde = formatnumber(rs("gm_hdvde"),2)
        gm_tdvde = rs("gm_tdvde")
        gm_advde = formatnumber(rs("gm_advde"),2)
        '//
        bts_hdvde = formatnumber(rs("bts_hdvde"),2)
        bts_tdvde = rs("bts_tdvde")
        bts_advde = formatnumber(rs("bts_advde"),2)
        gm_hdvde = bts_hdvde
        gm_tdvde = bts_tdvde
        gm_advde = bts_advde
        '//
        wdate2      = mid(getDateRe("8",wdate,"-"),6)
        wdate       = mid(getDateRe("8",wdate,"-"),6)
        gm_datetime = mid(getDateRe("8",gm_datetime,"-"),6)
        gm_datetime = left(gm_datetime,len(gm_datetime)-1)
        if not isnull(btt_gend_money) then
            btt_gend_money = formatnumber(btt_gend_money,0)
        else
            btt_gend_money="0"
        end if
        '//
        winflag    = getWinText(bts_state)
        '//
        if bts_state="I" then
            ing_icnt = ing_icnt+1
        elseif bts_state="C" then
            ing_ccnt = ing_ccnt+1
        elseif bts_state="W" then
            ing_wcnt = ing_wcnt+1
        elseif bts_state="L" then
            ing_lcnt = ing_lcnt+1
        elseif bts_state="T" then
            ing_tcnt = ing_tcnt+1
        end if
        '//
        h_ftcolor = "khaki"
        t_ftcolor = "khaki"
        a_ftcolor = "khaki"
        hicon     = ""
        aicon     = ""
        '//
        if bts_chflag="H" then h_ftcolor = "deep-orange"
        if bts_chflag="T" then t_ftcolor = "deep-orange"
        if bts_chflag="A" then a_ftcolor = "deep-orange"
        if gm_hndflag="UO" then
            hicon = "[오버]<font color=#FFFF00>▲</font>"
            aicon = "<font color=#00D3F9>▼</font>[언더]"
        end if
        '//
        gm_tdvde_val=""
        IF GM_FLAG="11" THEN
            IF SPO_TIE<>"Y" THEN
                GM_TDVDE_VAL = "VS"
            ELSE
                GM_TDVDE_VAL = GM_TDVDE
            END IF
        ELSEIF GM_FLAG="10" THEN
            IF GM_HNDFLAG="VS" THEN
            ELSEIF GM_HNDFLAG="UO" THEN
                GM_TDVDE_VAL = GM_TDVDE
            ELSEIF GM_HNDFLAG="CH" OR GM_HNDFLAG="CA" THEN
                IF GM_TDVDE<>"0" THEN
                    IF GM_HNDFLAG="CA" THEN
                        GM_TDVDE_VAL = "+" & GM_TDVDE
                    ELSE
                        GM_TDVDE_VAL = "-" & GM_TDVDE
                    END IF
                ELSE
                    GM_TDVDE_VAL = GM_TDVDE
                END IF
            END IF
        ELSEIF GM_FLAG="12" THEN
            IF GM_HNDFLAG="VS" THEN
                IF GM_VSCHB="Y" THEN
                    GM_TDVDE_VAL = "VS"
                ELSE
                    GM_TDVDE_VAL = GM_TDVDE
                END IF
            ELSEIF GM_HNDFLAG="UO" THEN
                GM_TDVDE_VAL = GM_TDVDE
            ELSEIF GM_HNDFLAG="CH" OR GM_HNDFLAG="CA" THEN
                IF GM_TDVDE<>"0" THEN
                    IF GM_HNDFLAG="CA" THEN
                        GM_TDVDE_VAL = "+" & GM_TDVDE
                    ELSE
                        GM_TDVDE_VAL = "-" & GM_TDVDE
                    END IF
                ELSE
                    GM_TDVDE_VAL = GM_TDVDE
                END IF
            END IF
        ELSEIF GM_FLAG="13" THEN
            IF GM_HNDFLAG="VS" THEN
                IF GM_VSCHB="Y" THEN
                    GM_TDVDE_VAL = "VS"
                ELSE
                    GM_TDVDE_VAL = FORMATNUMBER(GM_TDVDE,2)
                END IF
            ELSEIF GM_HNDFLAG="UO" THEN
                GM_TDVDE_VAL = GM_TDVDE
            ELSEIF GM_HNDFLAG="CH" OR GM_HNDFLAG="CA" THEN
                IF GM_TDVDE<>"0" THEN
                    IF GM_HNDFLAG="CA" THEN
                        GM_TDVDE_VAL = "+" & GM_TDVDE
                    ELSE
                        GM_TDVDE_VAL = "-" & GM_TDVDE
                    END IF
                ELSE
                    GM_TDVDE_VAL = GM_TDVDE
                END IF
            END IF
        END IF
        '//
        if winflag="적중" then
            winflag = "<span class='w3-text-red'>" & winflag & "</span>"
        elseif winflag="취소" then
            winflag = "<span class='w3-text-green'>" & winflag & "</span>"
        elseif winflag="적특" then
            winflag = "<span class='w3-text-blue'>" & winflag & "</span>"
        end if
        '//
        rowspanval = ""
        if btt_cnt>1 then rowspanval = " rowspan='"& btt_cnt &"'"
        '//
%>

               <%if p=1 then%>
					<tr><td colspan=20></td></tr>                
					<tr style="min-height: 30px">
                        <td <%=rowspanval%> style="white-space:nowrap" class="w3-text-dark-grey w3-padding-small w3-small">
	                        <%=listidx%>            
                        </td>
                        <td <%=rowspanval%>>
	                        <div style="white-space:nowrap" class="w3-text-blue w3-padding-small"><b><%=mb_id%></b></div>
	                    </td>
                        <td <%=rowspanval%>>
	                        <div style="white-space:nowrap" class="w3-text-black w3-padding-tiny"><%=mb_nick%></div>
	                    </td>	                    
	                    <td class="w3-tiny w3-light-grey"><%=gm_datetime%></td>
                        <td class="w3-light-grey"><%=leage_nm%></td>
                        
                        <td class="w3-<%=h_ftcolor%>" style="text-align:center;white-space:nowrap" colspan="2">
	                        <div class="w3-right w3-bold" style="width:60"><%=gm_hdvde%></div>
	                        <div class="w3-right"><%=hteamnm%><%=hicon%></div>	                        
		                </td>
                        <td class="w3-<%=t_ftcolor%>" style="text-align:center;white-space:nowrap;width:80">
	                        <div class="w3-bold" style="width:80"><%=gm_tdvde_val%></div>
	                    </td>
						<td class="w3-<%=a_ftcolor%>" style="text-align:center;white-space:nowrap" colspan="2">
	                        <div class="w3-left w3-bold" style="width:60"><%=gm_advde%></div>
	                        <span class="w3-left"><%=aicon%><%=ateamnm%></div>
		                </td>		                
                        <td class="w3-light-grey"><%=gmscore%></td>
                        <td class="w3-light-grey"><%=winflag%></td>

                        <!--td <%=rowspanval%>><%=formatnumber(btt_tot_dividend,2)%></td>
                        <td <%=rowspanval%> align=right><%=formatnumber(btt_money,0)%>&nbsp;</td>
                        <!--td <%=rowspanval%> align=right><%=formatnumber(btt_dividend_money,0)%>&nbsp;</td>
                        <td <%=rowspanval%> align=right><div id="jsmoney1_<%=btt_idx%>">0&nbsp;</div></td-->
                        <td <%=rowspanval%> style="text-align:center;white-space:nowrap">
	                        <div id=stysee_<%=btt_idx%> style="display:;"><%if btt_accflag="1" then%><div class="w3-text-blue w3-padding">정산대기</div><%else%><div class="w3-text-red w3-padding">정산완료</div><%end if%></div>
		                </td>
					</tr>
                <%else%>
					<tr style="min-height: 30px">
						<td class="w3-tiny w3-light-grey"><%=gm_datetime%></td>
                        <td class="w3-light-grey"><%=leage_nm%></td>
                        
                        <td class="w3-<%=h_ftcolor%>" style="text-align:center;white-space:nowrap" colspan="2">
	                        <div class="w3-right w3-bold" style="width:60"><%=gm_hdvde%></div>
	                        <div class="w3-right"><%=hteamnm%><%=hicon%></div>	                        
		                </td>
                        <td class="w3-<%=t_ftcolor%>" style="text-align:center;white-space:nowrap;width:80">
	                        <div class="w3-bold" style="width:80"><%=gm_tdvde_val%></div>
	                    </td>
						<td class="w3-<%=a_ftcolor%>" style="text-align:center;white-space:nowrap" colspan="2">
	                        <div class="w3-left w3-bold" style="width:60"><%=gm_advde%></div>
	                        <span class="w3-left"><%=aicon%><%=ateamnm%></div>
		                </td>	
                        <td class="w3-light-grey"><%=gmscore%></td>
                        <td class="w3-light-grey"><%=winflag%></td>
					</tr>
                <%end if%>

<%
        if p=btt_cnt then
            '//
            all_tot_cnt = btt_cnt - ing_ccnt - ing_tcnt     '//베팅수 - 취소수 - 적특수
            allwinflag=""
            if all_tot_cnt=ing_wcnt then
                allwinflag = "당첨"
                '//
                if hiddenVal_W  = "" then
                    hiddenVal_W = btt_idx
                    hiddenVal_M = formatnumber(btt_dividend_money,0)
                else
                    hiddenVal_W = hiddenVal_W &","& btt_idx
                    hiddenVal_M = hiddenVal_M &"&"& formatnumber(btt_dividend_money,0)
                end if
                '//
            elseif ing_lcnt>0 then
                allwinflag = "미당첨"
                '//
                if hiddenVal_L  = "" then
                    hiddenVal_L = btt_idx
                else
                    hiddenVal_L = hiddenVal_L &","& btt_idx
                end if
                '//
            elseif btt_cnt=ing_ccnt then
                allwinflag = "경기취소"
                '//
                if hiddenVal_C  = "" then
                    hiddenVal_C = btt_idx
                else
                    hiddenVal_C = hiddenVal_C &","& btt_idx
                end if
                '//
            elseif btt_cnt=ing_tcnt then
                allwinflag = "적특"
                if hiddenVal_T  = "" then
                    hiddenVal_T = btt_idx
                else
                    hiddenVal_T = hiddenVal_T &","& btt_idx
                end if
            else
                allwinflag = "게임중"
            end if
            '//
            haptrhtm = getBttTrHtml2(wdate2,btt_money,btt_tot_dividend,btt_dividend_money,btt_gend_money,allwinflag)
            response.write haptrhtm
            '//
            p=1
            listidx = listidx + 1
            ing_icnt    = 0
            ing_ccnt    = 0
            ing_wcnt    = 0
            ing_lcnt    = 0
            ing_tcnt    = 0
        else
            p=p+1
        end if
        '//
    i=i+1
    old_btt_idx = btt_idx
    rs.movenext
    loop
%>

<%
Rs.close
Set rs=nothing
Db.close
Set db=nothing
%>

					<tr height=10 bgcolor=white>
						<td colspan=20></td>
                    </tr>

<input type=hidden name=hiddenVal_W value="<%=hiddenVal_W%>">
<input type=hidden name=hiddenVal_L value="<%=hiddenVal_L%>">
<input type=hidden name=hiddenVal_C value="<%=hiddenVal_C%>">
<input type=hidden name=hiddenVal_T value="<%=hiddenVal_T%>">
</form>

				</table>


<script type="text/javascript" language="javascript">
<!--
$(document).ready(function(){
    var imsival  = "<%=hiddenVal_W%>";
    var imsival2 = "<%=hiddenVal_M%>";
	var imsiArr  = imsival.split(',');
	var imsiArr2 = imsival2.split('&');
	//
    for (i=0; i<imsiArr.length; i++){
        var realval  = imsiArr[i];
        var realval2 = imsiArr2[i];
        //$('#jsmoney1_'+realval).replaceWith("<span class='w3-text-orange'>"+realval2+"</span>");
        $('#jsmoney2_'+realval).replaceWith("<span class='w3-text-deep-orange w3-text-14 w3-bolder'>"+realval2+"</span>");


    }
});
-->
</script>
