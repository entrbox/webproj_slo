				<!--table width='100%' cellpadding=0 cellspacing=0 border=0>
					<tr>
						<td height=30 colspan=2 class=dotum_15><img src='/admin/images/icon_arrow0.gif' align=absmiddle>&nbsp;일일정산</td>
					</tr>
					<tr height=2>
						<td width=200 bgcolor='#AF0B0A'></td>
						<td bgcolor='#F2F8FB'></td>
					</tr>
				</table-->

				<!--table border="0" cellpadding="3" cellspacing="1" style="width:100%; border:1px solid #CBE1EE;"-->

<%
	flag  = rq("flag")    '승무패
	stxt1 = rq("stxt1")
	stxt2 = rq("stxt2")
	stxt3 = rq("stxt3")
	stxt4 = rq("stxt4")
	stxt5 = rq("stxt5")
	stxt6 = rq("stxt6")
	stxt7 = rq("stxt7")
	stxt8 = rq("stxt8")
	stxt9 = rq("stxt9")
	stxt10 = rq("stxt10")
	GotoPage = rq("GotoPage")
	'//
	If stxt1="" then stxt1 = Left(now(),8) & "01"
	If stxt2="" then stxt2 = Left(now(),10)
	if GotoPage = "" Then GotoPage = 1
	stxt11 = Replace(stxt1,"-","")
	stxt22 = Replace(stxt2,"-","")
    '//
    set rs = server.CreateObject("ADODB.Recordset")
    SQL = "      SELECT A.ISDATE, A.ISDAY, "
    SQL = SQL & "       ISNULL(B.BETTINGCNT,0) BETTINGCNT, ISNULL(B.BETTINGMONEY,0) BETTINGMONEY, "
    SQL = SQL & "       ISNULL(C.ACC_WCNT,0) ACC_WCNT, ISNULL(C.ACC_WMONEY,0) ACC_WMONEY "
    SQL = SQL & "  FROM "
    SQL = SQL & " 		( "
    SQL = SQL & " 		SELECT REPLACE(CONVERT(CHAR(10),DATEADD(D,NUMBER,'"& STXT1 &"'),120),'-','') AS ISDATE, "
    SQL = SQL & " 			   SUBSTRING(CONVERT(CHAR(10),DATEADD(D,NUMBER,'"& STXT1 &"'),120),9,2) AS ISDAY "
    SQL = SQL & " 		  FROM MASTER..SPT_VALUES WITH(NOLOCK)  "
    SQL = SQL & " 		 WHERE TYPE = 'P' "
    SQL = SQL & " 		   AND CONVERT(CHAR(10),DATEADD(D,NUMBER,'"& STXT1 &"'),120) <= '"& STXT2 &"' "
    SQL = SQL & " 		) A "

    SQL = SQL & " 		LEFT OUTER JOIN "
    SQL = SQL & " 		( "
    SQL = SQL & " 		SELECT A.CNTDATE, "
    SQL = SQL & " 		       COUNT(A.BTT_MONEY) BETTINGCNT, "
    SQL = SQL & " 		       SUM(A.BTT_MONEY) BETTINGMONEY"
    SQL = SQL & " 		  FROM"
    SQL = SQL & " 		  	   (SELECT SUBSTRING(A.WDATE,1,8) CNTDATE, BTT_MONEY"
    SQL = SQL & " 			      FROM TB_SPO_BETTING_MASTER A, TB_SPO_BETTING_INFO B, TB_SPO_GAME C "
    SQL = SQL & " 			      WHERE A.BTT_IDX = B.BTT_IDX "
    SQL = SQL & " 			        AND B.GM_IDX = C.GM_IDX "
    IF STXT3<>"" THEN SQL = SQL & " AND C.GM_JCODE = '"& STXT3 &"' "
    IF STXT7<>"" THEN SQL = SQL & " AND A.GM_FLAG = '"& STXT7 &"' "
    IF STXT8<>"" THEN SQL = SQL & " AND C.GM_LCODE = '"& STXT8 &"' "
    IF STXT9="" THEN
    	SQL = SQL & " AND C.GM_STATE IN ('60', '99') "
    ELSE
        SQL = SQL & " AND C.GM_STATE = '"& STXT9 &"' "
    END IF
    SQL = SQL & " 			      GROUP BY SUBSTRING(A.WDATE,1,8), BTT_MONEY"
    SQL = SQL & " 			   ) A"
    SQL = SQL & " 		 GROUP BY A.CNTDATE"
    SQL = SQL & " 		) B "
    SQL = SQL & " 		ON A.ISDATE = B.CNTDATE "

    SQL = SQL & " 		LEFT OUTER JOIN "
    SQL = SQL & " 		( "
    SQL = SQL & " 		SELECT A.CNTDATE, "
    SQL = SQL & " 		       COUNT(A.BTT_GEND_MONEY) ACC_WCNT, "
    SQL = SQL & " 		       SUM(A.BTT_GEND_MONEY) ACC_WMONEY"
    SQL = SQL & " 		  FROM "
    SQL = SQL & " 		  	   (SELECT SUBSTRING(A.BTT_ACCDATE,1,8) CNTDATE, BTT_GEND_MONEY"
    SQL = SQL & " 			      FROM TB_SPO_BETTING_MASTER A, TB_SPO_BETTING_INFO B, TB_SPO_GAME C "
    SQL = SQL & " 			      WHERE A.BTT_IDX = B.BTT_IDX "
    SQL = SQL & " 			        AND B.GM_IDX = C.GM_IDX "
    IF STXT3<>"" THEN SQL = SQL & " AND C.GM_JCODE = '"& STXT3 &"' "
    IF STXT7<>"" THEN SQL = SQL & " AND A.GM_FLAG = '"& STXT7 &"' "
    IF STXT8<>"" THEN SQL = SQL & " AND C.GM_LCODE = '"& STXT8 &"' "
    IF STXT9="" THEN
    	SQL = SQL & " AND C.GM_STATE IN ('60', '99') "
    ELSE
        SQL = SQL & " AND C.GM_STATE = '"& STXT9 &"' "
    END IF
    SQL = SQL & " 			        AND A.BTT_ACCFLAG = '9' "
    SQL = SQL & " 			        AND A.BTT_WINFLAG = 'W' "
    SQL = SQL & " 			      GROUP BY SUBSTRING(A.BTT_ACCDATE,1,8), BTT_GEND_MONEY"
    SQL = SQL & " 			    ) A"
    SQL = SQL & " 		  GROUP BY A.CNTDATE"
    SQL = SQL & " 		) C "
    SQL = SQL & " 		ON A.ISDATE = C.CNTDATE "
    SQL = SQL & " ORDER BY A.ISDATE DESC "
    rs.Open sql, db, 1
    '//
%>
<!--#include virtual="/admin/sports/inc/js.asp" -->

<script language='JavaScript'>
	function load_league2(){
		var ch_jcode = form.stxt3.value;
		var flag = form.stxt7.value;
		var lcode = "stxt8";
        var ch_lcode = form.stxt8.value;

		//alert(ch_lcode);

        $.ajax({
				type:"POST"
				, async:true
				, url:"../ajax/ajax_load_league2.asp"
				, dataType:"html"
				, timeout:30000
				, cache:false
				, data:"lcode="+lcode+"&ch_jcode="+ch_jcode+"&ch_lcode="+ch_lcode+"&jsflag=&flag="+flag+"&gm_bonuscnt=<%=gm_bonuscnt%>"
				, contentType:"application/x-www-form-urlencoded; charset=UTF-8"
				, error: function(request, status, error){
						//alert("code : " + request.status + "\r\nmessage : " + request.responseText);
						//$("body").append(request.responseText);
					}
				, success: function(response, status, request){
						result_html = response;
						//alert(result_html);
                        document.getElementById("lgeSelectBox2").innerHTML=result_html;
                        //alert('');
					}
			});
	}
	
	function init_league2(){
		var ch_jcode = form.stxt3.value;
		var flag = form.stxt7.value;
		var lcode = "stxt8";
        var ch_lcode = "<%=stxt8%>";
        
        //alert(ch_lcode);

        $.ajax({
				type:"POST"
				, async:true
				, url:"../ajax/ajax_load_league2.asp"
				, dataType:"html"
				, timeout:30000
				, cache:false
				, data:"lcode="+lcode+"&ch_jcode="+ch_jcode+"&ch_lcode="+ch_lcode+"&jsflag=&flag="+flag+"&gm_bonuscnt=<%=gm_bonuscnt%>"
				, contentType:"application/x-www-form-urlencoded; charset=UTF-8"
				, error: function(request, status, error){
						//alert("code : " + request.status + "\r\nmessage : " + request.responseText);
						//$("body").append(request.responseText);
					}
				, success: function(response, status, request){
						result_html = response;
						//alert(result_html);
                        document.getElementById("lgeSelectBox2").innerHTML=result_html;
                        //alert('');
					}
			});
	}	
</script>	
<form name="form" id="form" method="post" action="?" onsubmit="return searChb()">
					<div class="w3-container w3-leftbar w3-light-grey w3-padding w3-border" style="margin-bottom:10px;">		
						<div class="w3-right">
						<input type="hidden" name="flag" value="<%=flag%>">
						<input type="hidden" name="accFlag">
						<input type="hidden" name="pagenm" value="<%=pagenm%>">				

						<!--div class="w3-left w3-padding w3-small2">조건검색 :</div-->
						<div class="w3-left"><input class="w3-input w3-padding w3-border w3-small box-calendar" type="text" required name="stxt1" id="stxt1" value="<%=stxt1%>" style="cursor:pointer;width:125px" OnClick="calendar(event, 'stxt1')"  readonly /></div>
						<div class="w3-left w3-small2" style="padding: 9 3 3 3;">~ </div>
						<div class="w3-left"><input class="w3-input w3-padding w3-border w3-small box-calendar" type="text" required name="stxt2" id="stxt2" value="<%=stxt2%>" style="cursor:pointer;width:125px" OnClick="calendar(event, 'stxt2')" readonly /></div>
                        <div class="w3-left" style="margin-left: 6px;">    
	                        <select class="w3-select w3-border w3-small w3-padding" name="stxt7" onchange="chg_teamShow(this.value)" style="min-width:125px;max-width:250px">
                                <option value="" disabled selected>게임타입</option>
                                <option value="">게임타입전체</option>
                                <option value="11" <%if stxt7="11" then response.write " selected"%>>승무패</option>
                                <option value="10" <%if stxt7="10" then response.write " selected"%>>핸디캡</option>
                                <option value="12" <%if stxt7="12" then response.write " selected"%>>스폐셜</option>
                                <option value="13" <%if stxt7="13" then response.write " selected"%>>스폐셜2</option>
                            </select>
                         </div>  	
						<div class="w3-left" style="margin-left: 3px;"><% call jmkSelectBox2("stxt3",stxt3," onchange='load_league2()' ")%></div>
						<div class="w3-left" style="margin-left: 3px;" id="lgeSelectBox2"><!--% call lgeSelectBox2("stxt8",stxt8,"")%--></div>
						<div class="w3-left" style="margin-left: 3px;"><% call selIngWrite2("stxt9",stxt9,"")%></div>
						<p>
						<div class="w3-right" style="margin-left: 3px;padding-top: 5px;">
							<div class="w3-left" style="margin-left: 3px;">
								<select class="w3-select w3-border w3-small w3-padding" name="stxt4" style="min-width:125px;max-width:250px">
									<option value="">검색구분</option>
									<option value="B.SPO_NAME" <%If stxt4="B.SPO_NAME" Then Response.write " selected"%>>종목명
									<option value="C.SPO_NAME" <%If stxt4="C.SPO_NAME" Then Response.write " selected"%>>리그명
									<option value="D.SPO_NAME" <%If stxt4="D.SPO_NAME" Then Response.write " selected"%>>홈팀명
									<option value="F.SPO_NAME" <%If stxt4="F.SPO_NAME" Then Response.write " selected"%>>원정팀명								
								</select>						
							</div>
							
							<div class="w3-left" style="margin-left: 3px;"><input class="w3-input w3-padding w3-border w3-left" type="text" placeholder="검색어" name="stxt5" style="width:378px"></div>
							<div class="w3-left" style="margin-left: 3px;"><button class="w3-btn w3-padding w3-black w3-small w3-left w3-bold" type="submit" style="width:125px">검색하기</button></div>
						</div>
						</p>
						</div>				
					</div>	
			

				<table class="w3-table w3-bordered w3-large w3-centered">
					<tr class="w3-blue w3-opacity-min">
						<th class="w3-dark-grey w3-small w3-padding-large" nowrap width=10%>No.</th>
						<th class="w3-dark-grey w3-small w3-padding-large" nowrap width=15%>정산일자</th>
						<th class="w3-dark-grey w3-small w3-padding-large" nowrap width=15%>베팅수</th>
						<th class="w3-dark-grey w3-small w3-padding-large" style="text-align:right;white-space:nowrap" width=20%>베팅금액</th>
						<th class="w3-dark-grey w3-small w3-padding-large" style="text-align:right;white-space:nowrap" width=20%>정산금액</th>
                        <th class="w3-dark-grey w3-small w3-padding-large" style="text-align:right;white-space:nowrap" width=20%>수익금</th>
					</tr>

<%
    param = "flag="&flag&"&gotopage="&gotopage&"&stxt1="&stxt1&"&stxt2="&stxt2&"&stxt3="&stxt3&"&stxt4="&stxt4&"&stxt5="&stxt5&"&stxt6="&stxt6&"&stxt7="&stxt7&"&stxt8="&stxt8&"&stxt9="&stxt9&"&stxt10="&stxt10
    '//
    i=1
    tot_bettingcnt   = 0
    tot_bettingmoney = 0
    tot_acc_wmoney   = 0
    tot_profitmoney  = 0
    '//
    do until rs.eof
        '//
        accdate      = rs(0)
        isday        = rs(1)
        bettingcnt   = rs(2)
        bettingmoney = formatnumber(rs(3),0)
        acc_wcnt     = rs(4)
        acc_wmoney   = formatnumber(rs(5),0)
        profitmoney  = formatnumber(rs(3)-rs(5),0)
        accdate      = getDateRe("1",accdate,"-")
        '//
        tot_bettingcnt   = tot_bettingcnt   + rs(2)
        tot_bettingmoney = tot_bettingmoney + rs(3)
        tot_acc_wmoney   = tot_acc_wmoney   + rs(5)
        tot_profitmoney  = tot_profitmoney  + (rs(3)-rs(5))
        '//
%>

					<tr>
						<td style="white-space:nowrap"><%=i%></td>
                        <td style="white-space:nowrap"><a href="accList.asp?accdate=<%=replace(accdate,"-","")%>&<%=param%>" class="w3-bold w3-text-indigo w3-text-13"><%=accdate%></a></td>
                        <td style="white-space:nowrap"><%=formatnumber(bettingcnt,0)%></td>
                        <td class="w3-bold" style="text-align:right;white-space:nowrap"><font color=red><%=bettingmoney%></font> 원</td>
                        <td class="w3-bold" style="text-align:right;white-space:nowrap"><font color=blue><%=acc_wmoney%></font> 원</td>
                        <td class="w3-bold" style="text-align:right;white-space:nowrap"><font color=red><%=profitmoney%></font> 원</td>
					</tr>

<%
    i=i+1
    rs.movenext
    loop
%>

					<tr class="w3-light-grey">
						<th class="w3-padding-large" colspan=2><b>합 계</th>
                        <th class="w3-padding-large"><b><%=formatnumber(tot_bettingcnt,0)%></th>
                        <th class="w3-padding-large w3-bolder" style="text-align:right;white-space:nowrap"><font color=red><b><%=formatnumber(tot_bettingmoney,0)%></b></font> 원</th>
                        <th class="w3-padding-large w3-bolder" style="text-align:right;white-space:nowrap"><font color=blue><b><%=formatnumber(tot_acc_wmoney,0)%></b></font> 원</th>
                        <th class="w3-padding-large w3-bolder" style="text-align:right;white-space:nowrap"><font color=red><b><%=formatnumber(tot_profitmoney,0)%></b></font> 원</th>
					</tr>

				</table>
				
</form>				