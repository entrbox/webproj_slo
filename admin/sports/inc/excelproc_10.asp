<%
        idx_arr    = split(idx,",")
        ch_idx_arr = split(ch_idx_val,",")
        gm_ymd_arr = split(gm_ymd,",")
        gm_hms_arr = split(gm_hms,",")
        htname_arr = split(htname,",")
        atname_arr = split(atname,",")
        htdper_arr = split(htdper,",")
        hpoint_arr = split(hpoint,",")
        uopoint_arr= split(uopoint,",")
        atdper_arr = split(atdper,",")
        '//
        'On Error Resume Next
        'DB.BeginTrans
        '////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        For i=0 to ubound(ch_idx_arr)
            '//
            set rs = server.CreateObject("ADODB.Recordset")
            SQL = " SELECT DISTINCT GM_FLAG, EXL_VAL1, B.CH_JCODE JCODE, B.SPO_CODE LCODE "
            SQL = SQL & " FROM TB_SPO_UP_EXCEL A "
            SQL = SQL & " INNER JOIN (SELECT * FROM tb_spo_kind WHERE SPO_FLAG='L') B ON CONVERT(DECIMAL(18, 0), A.EXL_VAL1) = B.SPO_IDX "
            SQL = SQL & " WHERE IDX = "& TRIM(CH_IDX_ARR(I))
            rs.Open sql, db, 1
            if not rs.eof then
                gm_flag  = rs(0)
                gm_lcode = rs(1)
                jcode = rs(2)
                lcode = rs(3)
            end if
            rs.close
            '//
            p = ""
            For j=0 to ubound(idx_arr)
                if trim(ch_idx_arr(i))=trim(idx_arr(j)) then
                    p = j
                    exit for
                end if
            Next
            '//
            errcnt=0
            gm_ymd_val = trim(gm_ymd_arr(p))
            gm_hms_val = trim(gm_hms_arr(p))
            htname_val = trim(htname_arr(p))
            atname_val = trim(atname_arr(p))
            htdper_val = formatnumber(getRevalue(trim(htdper_arr(p)),0),2)
            hpoint_val = formatnumber(getRevalue(trim(hpoint_arr(p)),0),2)
            uopoint_val = formatnumber(getRevalue(trim(uopoint_arr(p)),0),2)
            atdper_val = formatnumber(getRevalue(trim(atdper_arr(p)),0),2)
            '//
            if cdbl(hpoint_val)<>0 then
                if cdbl(hpoint_val)>0 then
                    gm_hndflag = "CA"
                else
                    gm_hndflag = "CH"
                    hpoint_val = Abs(hpoint_val)
                end if
                ttdper_val = hpoint_val
            elseif cdbl(uopoint_val)<>0 then
                gm_hndflag = "UO"
                ttdper_val = uopoint_val
            else
                ttdper_val = 0
                gm_hndflag = ""
                errcnt=1
            end if
'response.Write hpoint_val &" - "& uopoint_val &" - "& errcnt
'response.end
            '//
            gm_datetime = left(gm_ymd_val & gm_hms_val,12)
            '//
            if errcnt=0 then
                htname_code = ""
                set rs = server.CreateObject("ADODB.Recordset")
                SQL = " SELECT SPO_CODE "
                SQL = SQL & " FROM TB_SPO_KIND "
                SQL = SQL & " WHERE SPO_FLAG='T' AND CH_JCODE='"& JCODE &"' AND CH_LCODE='"& LCODE &"' "
                SQL = SQL & " AND SPO_NAME = '"& HTNAME_VAL &"' "
                rs.Open sql, db, 1
                if not rs.eof then
                    htname_code  = rs(0)
                else
                    '//
                    set rs2 = server.CreateObject("ADODB.Recordset")
                    SQL = "SELECT ISNULL(MAX(convert(int,SPO_CODE)),10000)+1 FROM TB_SPO_KIND WHERE SPO_FLAG = 'T' "
                    rs2.Open sql, db, 1
                    htname_code = rs2(0)
                    rs2.close
                    '//
                    SQL = " INSERT INTO TB_SPO_KIND (SPO_FLAG,SPO_NAME,SPO_ENAME,SPO_CODE,SPO_IMGNAME,CH_JCODE,CH_LCODE,CH_NCODE,SPO_TIE,SPO_SPW,SPO_SPT,SPO_BONUS,SPO_WTL,WDATE) VALUES "
                    SQL = SQL & " ('T', '"& HTNAME_VAL &"', '"& HTNAME_VAL &"', '"& HTNAME_CODE &"', '', '"& JCODE &"', "
                    SQL = SQL & " '"& LCODE &"', '', '', '', '', 'N', '', '"& WDATE &"' ) "
                    DB.EXECUTE SQL
                    '//
                end if
                rs.close
                '//
                atname_code = ""
                set rs = server.CreateObject("ADODB.Recordset")
                SQL = " SELECT SPO_CODE "
                SQL = SQL & " FROM TB_SPO_KIND "
                SQL = SQL & " WHERE SPO_FLAG='T' AND CH_JCODE='"& JCODE &"' AND CH_LCODE='"& LCODE &"' "
                SQL = SQL & " AND SPO_NAME = '"& ATNAME_VAL &"' "
                rs.Open sql, db, 1
                if not rs.eof then
                    atname_code  = rs(0)
                else
                    '//
                    set rs2 = server.CreateObject("ADODB.Recordset")
                    SQL = "SELECT ISNULL(MAX(convert(int,SPO_CODE)),10000)+1 FROM TB_SPO_KIND WHERE SPO_FLAG = 'T' "
                    rs2.Open sql, db, 1
                    atname_code = rs2(0)
                    rs2.close
                    '//
                    SQL = " INSERT INTO TB_SPO_KIND (SPO_FLAG,SPO_NAME,SPO_ENAME,SPO_CODE,SPO_IMGNAME,CH_JCODE,CH_LCODE,CH_NCODE,SPO_TIE,SPO_SPW,SPO_SPT,SPO_BONUS,SPO_WTL,WDATE) VALUES "
                    SQL = SQL & " ('T', '"& ATNAME_VAL &"', '"& ATNAME_VAL &"', '"& ATNAME_CODE &"', '', '"& JCODE &"', "
                    SQL = SQL & " '"& LCODE &"', '', '', '', '', 'N', '', '"& WDATE &"' ) "
                    DB.EXECUTE SQL
                    '//
                end if
                rs.close
                '//
                SQL = " INSERT INTO TB_SPO_GAME (GM_FLAG,GM_JCODE,GM_LCODE,GM_HTCODE,GM_ATCODE,GM_DATETIME,GM_HDVDE,GM_TDVDE,GM_ADVDE,GM_HNDFLAG,GM_VSCHB,GM_BONUSCNT,WDATE) VALUES "
                SQL = SQL & " ('"& FLAG &"', '"& JCODE &"', '"& LCODE &"', '"& HTNAME_CODE &"', '"& ATNAME_CODE &"', '"& GM_DATETIME &"', "
                SQL = SQL & " '"& HTDPER_VAL &"', '"& TTDPER_VAL &"', '"& ATDPER_VAL &"', '"& GM_HNDFLAG &"', '', 0, '"& WDATE &"' ) "
                DB.EXECUTE SQL
                '//
                SQL = " DELETE TB_SPO_UP_EXCEL WHERE IDX = "& trim(ch_idx_arr(i)) &" "
                DB.EXECUTE SQL
            end if
            '//
        Next
        '//
        If Err.Number<>0 Then
            DB.RollBackTrans
            response.write "에러넘버:" & Err.Number & "<br>"
            response.write "에러설명:" & Err.Description & "<br>"
            response.write "에러소스:" & Err.Source & "<br>"
            response.write "에러파일3:" & Err.NativeError & "<br>"
            response.write "에러파일1:" & Err.HelpFile & "<br>"
            response.write "에러파일2:" & Err.HelpContext & "<br>"
    		Call getPopup("13","예기치 않은 오류 관리자 문의!!","")
        Else
            DB.CommitTrans
            Call getPopup("3","uplodExcel.asp?flag="&flag,"")
        End If
%>