<script language='JavaScript'>
<!--

    function formChb(val1){
        if (form.ch_jcode.value == ''){alert('종목을 선택해 주세요.');form.ch_jcode.focus();	return false;}
        if (form.ch_lcode.value == ''){alert('리그를 선택해 주세요.');form.ch_lcode.focus();	return false;}
        if (form.spo_name.value == ''){alert('팀(선수)명을 입력해 주세요.');form.spo_name.focus();	return false;}
        form.dflag.value=val1;
        form.action="regFrmProc.asp";
        form.submit();
        return false;
    }

	function load_league(){
		var ch_jcode = form.ch_jcode.value;
        var ch_lcode = "<%=ch_lcode%>";

        $.ajax({
				type:"POST"
				, async:true
				, url:"ajax/ajax_load_league.asp"
				, dataType:"html"
				, timeout:30000
				, cache:false
				, data:"ch_jcode="+ch_jcode+"&ch_lcode="+ch_lcode
				, contentType:"application/x-www-form-urlencoded; charset=UTF-8"
				, error: function(request, status, error){
						alert("code : " + request.status + "\r\nmessage : " + request.responseText);
						//$("body").append(request.responseText);
					}
				, success: function(response, status, request){
						result_html = response;
                        document.getElementById("lgeSelectBox").innerHTML=result_html;

					}
				, beforeSend: function(){

					}
				, complete: function(){

					}
			});
	}

//-->
</script>

<form name="form" method="post" action="?" ENCTYPE="MULTIPART/FORM-DATA">
<input type="hidden" name="stxt1" value="<%=stxt1%>">
<input type="hidden" name="stxt2" value="<%=stxt2%>">
<input type="hidden" name="stxt3" value="<%=stxt3%>">
<input type="hidden" name="stxt4" value="<%=stxt4%>">
<input type="hidden" name="stxt5" value="<%=stxt5%>">
<input type="hidden" name="stxt6" value="<%=stxt6%>">
<input type="hidden" name="stxt7" value="<%=stxt7%>">
<input type="hidden" name="stxt8" value="<%=stxt8%>">
<input type="hidden" name="stxt9" value="<%=stxt9%>">
<input type="hidden" name="stxt10" value="<%=stxt10%>">
<input type="hidden" name="gotopage" value="<%=gotopage%>">
<input type="hidden" name="spo_idx"  value="<%=spo_idx%>">
<input type="hidden" name="flag"     value="<%=flag%>">
<input type="hidden" name="dflag">

				<BR>
				<table border="0" cellpadding="2" cellspacing="1" width=100% bgcolor="#CBE1EE">
					<tr height=35 bgcolor="#F2F2F2">
						<td width=10% align=center>팀(선수)코드</td>
						<td style="background-color:#fff;width:90%;padding:10;text-align:left"><%=spo_code%></td>
					</tr>
					<tr height=35 bgcolor="#F2F2F2">
						<td align=center>종목선택</td>
						<td style="background-color:#fff;width:90%;padding:4;text-align:left"><% call jmkSelectBox2("ch_jcode",ch_jcode," onchange='load_league()'")%></td>
					</tr>
					<tr height=30 bgcolor="#F2F2F2">
						<td align=center>리그선택</td>
						<td style="background-color:#fff;width:90%;padding:4;text-align:left"><div id="lgeSelectBox" class="w3-left"></div> <div class="w3-left w3-padding">*해당리그가 없을 경우 <font color=red>기타리그</font>를 선택하거나, <font color=blue>리그등록</font>에서 리그를 등록해 주세요.</div> </td>
					</tr>
					<tr height=30 bgcolor="#F2F2F2">
						<td align=center>팀(선수)명</td>
						<td style="background-color:#fff;width:90%;padding:4;text-align:left"><input type=text name="spo_name" value="<%=spo_name%>" size=80></td>
					</tr>
				</table>
				<BR>
				<table border="0" cellpadding="0" cellspacing="0" width=100% bgcolor=white>
					<tr height=45>
						<td align=center>
							<%If spo_idx="" then%>
								<button type=button class="w3-btn w3-padding w3-blue" style="width:125" onclick="return formChb('')">저장하기</button>
							<%else%>
								<button type=button class="w3-btn w3-padding w3-blue" style="width:125" onclick="return formChb('')">수정하기</button>
								<button type=button class="w3-btn w3-padding w3-red" style="width:125" onclick="return formChb('1')">삭제하기</button>
								<button type=button class="w3-btn w3-padding w3-green" style="width:125" onclick="return formChb('2')">신규등록</button>
							<%End if%>
							<button type=button class="w3-btn w3-padding w3-dark-grey" style="width:125" onclick="history.back()">돌아가기</button>
						</td>
					</tr>
				</table>
</form>
<BR>

<script type="text/javascript">
	window.onload = function() {
        load_league();
	}
</script>
