<div class="w3-padding-small w3-bolder w3-text-14 w3-text-blue" style="margin:20 0 10 0">
	<i class="fa fa-bars"></i> <%=titlename%>
</div>
<!--BR>
<table width=100% cellpadding=3 cellspacing=0 border=0>
    <tr>
        <td>
            <img src='/admin/images/icon_arrow3.gif' align=absmiddle>
            &nbsp;<%=titlename%>
        </td>
    </tr>
</table-->

<table class="w3-htable">
    <tr>
        <th width=5%>No.</th>
        <th width=15%>베팅일시</th>
        <th width=15%>아이디</th>
        <th width=15%>닉네임</th>
        <th width=10%>배당율</th>
        <th width=10%>핸디</th>
        <th width=10%>베팅구분</th>
        <th width=20%>베팅금액</th>
    </tr>

<%
    i=1
    do until rs.EOF or i>rs.pagesize
        '//
        wdate        = rs("wdate")
        mb_id        = rs("mb_id")
        mb_nick      = rs("mb_nick")
        bts_dividend = rs("bts_dividend")
        bts_chflag   = rs("bts_chflag")
        bts_chmoney  = rs("bts_chmoney")
        '//
        wdate        = getDateRe("8",wdate,"-")
        bts_chflag   = getbttflagRe(bts_chflag)
        '//
%>

        <tr>
            <td><%=i%></td>
            <td><%=wdate%></td>
            <td><%=mb_id%></td>
            <td><%=mb_nick%></td>
            <td><%=bts_dividend%></td>
            <td></td>
            <td><%=bts_chflag%></td>
            <td style="text-align:right"><%=formatnumber(bts_chmoney,0)%></td>
        </tr>

<%
    rs.movenext
    i=i+1
    loop
    rs.close
%>

</table>