<%
	set rs = server.CreateObject("ADODB.Recordset")
	SQL = "       SELECT A.* , B.SPO_NAME GM_JNAME, C.SPO_NAME GM_LNAME, "
    SQL = SQL & "        D.SPO_NAME GM_HNAME, E.SPO_IMGNAME GM_HIMGNAME, "
    SQL = SQL & "        F.SPO_NAME GM_ANAME, G.SPO_IMGNAME GM_AIMGNAME, B.SPO_TIE SPO_TIE, "
    SQL = SQL & "        ISNULL(H.HAPBTTMONEY_H,0) HAPBTTMONEY_H, "
    SQL = SQL & "        ISNULL(H.HAPBTTMONEY_T,0) HAPBTTMONEY_T, "
    SQL = SQL & "        ISNULL(H.HAPBTTMONEY_A,0) HAPBTTMONEY_A, "
    SQL = SQL & "        ISNULL(H.BETTING_HCNT,0) BETTING_HCNT, "
    SQL = SQL & "        ISNULL(H.BETTING_TCNT,0) BETTING_TCNT, "
    SQL = SQL & "        ISNULL(H.BETTING_ACNT,0) BETTING_ACNT "
	SQL = SQL & "   FROM TB_SPO_GAME A "
	SQL = SQL & "  INNER JOIN (SELECT SPO_CODE, SPO_NAME, SPO_TIE FROM TB_SPO_KIND WHERE SPO_FLAG='J') B "
	SQL = SQL & "     ON A.GM_JCODE = B.SPO_CODE "
	SQL = SQL & "  INNER JOIN (SELECT SPO_CODE, SPO_NAME, CH_NCODE FROM TB_SPO_KIND WHERE SPO_FLAG='L') C "
	SQL = SQL & "     ON A.GM_LCODE = C.SPO_CODE "
    '//
	SQL = SQL & "  INNER JOIN (SELECT SPO_CODE, SPO_NAME, CH_NCODE FROM TB_SPO_KIND WHERE SPO_FLAG='T') D "
	SQL = SQL & "     ON A.GM_HTCODE = D.SPO_CODE "
	SQL = SQL & "  INNER JOIN (SELECT SPO_CODE, SPO_NAME,SPO_IMGNAME FROM TB_SPO_KIND WHERE SPO_FLAG='N') E "
	SQL = SQL & "     ON C.CH_NCODE = E.SPO_CODE "
	SQL = SQL & "  INNER JOIN (SELECT SPO_CODE, SPO_NAME, CH_NCODE FROM TB_SPO_KIND WHERE SPO_FLAG='T') F "
	SQL = SQL & "     ON A.GM_ATCODE = F.SPO_CODE "
	SQL = SQL & "  INNER JOIN (SELECT SPO_CODE, SPO_NAME,SPO_IMGNAME FROM TB_SPO_KIND WHERE SPO_FLAG='N') G "
	SQL = SQL & "     ON C.CH_NCODE = G.SPO_CODE "
    '//
	SQL = SQL & "  left outer JOIN ( "
	SQL = SQL & "                  SELECT A.GM_IDX, "
	SQL = SQL & "                         SUM(A.HAPBTTMONEY_H) HAPBTTMONEY_H, "
	SQL = SQL & "                         SUM(A.HAPBTTMONEY_T) HAPBTTMONEY_T, "
	SQL = SQL & "                         SUM(A.HAPBTTMONEY_A) HAPBTTMONEY_A, "
	SQL = SQL & "                         SUM(A.BETTING_HCNT) BETTING_HCNT, "
	SQL = SQL & "                         SUM(A.BETTING_TCNT) BETTING_TCNT, "
	SQL = SQL & "                         SUM(A.BETTING_ACNT) BETTING_ACNT "
	SQL = SQL & "                    FROM "
	SQL = SQL & "                         ( "
	SQL = SQL & "                         SELECT GM_IDX, "
	SQL = SQL & "                                ISNULL(CASE WHEN BTS_CHFLAG='H' THEN SUM(BTS_CHMONEY) END,0) HAPBTTMONEY_H, "
	SQL = SQL & " 	                             ISNULL(CASE WHEN BTS_CHFLAG='T' THEN SUM(BTS_CHMONEY) END,0) HAPBTTMONEY_T, "
	SQL = SQL & " 	                             ISNULL(CASE WHEN BTS_CHFLAG='A' THEN SUM(BTS_CHMONEY) END,0) HAPBTTMONEY_A, "
	SQL = SQL & " 	                             ISNULL(CASE WHEN BTS_CHFLAG='H' THEN COUNT(BTS_CHFLAG) END,0) BETTING_HCNT, "
	SQL = SQL & " 		                         ISNULL(CASE WHEN BTS_CHFLAG='T' THEN COUNT(BTS_CHFLAG) END,0) BETTING_TCNT, "
	SQL = SQL & " 	 	                         ISNULL(CASE WHEN BTS_CHFLAG='A' THEN COUNT(BTS_CHFLAG) END,0) BETTING_ACNT "
	SQL = SQL & "                           FROM TB_SPO_BETTING_INFO "
	SQL = SQL & "                          WHERE BTS_STATE='I' "
	SQL = SQL & "                          GROUP BY GM_IDX,BTS_CHFLAG "
	SQL = SQL & "                          ) A "
	SQL = SQL & "                    GROUP BY GM_IDX "
    SQL = SQL & "               ) H ON A.GM_IDX = H.GM_IDX "
    '//
	SQL = SQL & "  WHERE A.GM_IDX = "& GM_IDX &" "
	rs.Open sql, db, 1
	'//
	gm_datetime = rs("gm_datetime")
	gm_jname    = rs("gm_jname")
	gm_lname    = rs("gm_lname")
	gm_hname    = rs("gm_hname")
	gm_aname    = rs("gm_aname")
    gm_himgname = rs("gm_himgname")
    gm_aimgname = rs("gm_aimgname")
    gm_hdvde    = formatnumber(rs("gm_hdvde"),2)
    gm_tdvde    = formatnumber(rs("gm_tdvde"),2)
    gm_advde    = formatnumber(rs("gm_advde"),2)
    gm_state    = rs("gm_state")
    spo_tie     = rs("spo_tie")

    hapbttmoney_h = rs("hapbttmoney_h")
    hapbttmoney_t = rs("hapbttmoney_t")
    hapbttmoney_a = rs("hapbttmoney_a")
    betting_hcnt  = rs("betting_hcnt")
    betting_tcnt  = rs("betting_tcnt")
    betting_acnt  = rs("betting_acnt")
    rst_htm_score = rs("rst_htm_score")
    rst_atm_score = rs("rst_atm_score")
    rst_winflag   = rs("rst_winflag")

	gm_datetime1 = mid(getDateRe("1",gm_datetime,"-"),3)
	gm_datetime2 = getDateRe("10",gm_datetime,":")
	gm_datetime  = gm_datetime1 &" "& gm_datetime2

    gamescore = ""
    if rst_htm_score<>"" and rst_atm_score<>"" then
        gamescore = rst_htm_score &" <span class='w3-text-blue'>:</span> "& rst_atm_score
    end if
    rs.close
%>

<div class="w3-padding-small w3-bolder w3-text-16" style="margin:20 0 10 0">
	<i class="fa fa-bars"></i> 현재 경기내역 : No. <span class="w3-bolder w3-text-16 w3-text-blue"><%=pgindex%></span>
</div>


<table class="w3-htable">
    <tr>
        <th width=10%>경기시간</th>
        <th width=10%>종목</th>
        <th width=25%>홈팀 VS 원정팀</th>
        <th width=15%>승</th>
        <th width=15%>무</th>
        <th width=15%>패</th>
        <th width=10%>경기결과</th>
    </tr>
    <tr>
        <td><%=gm_datetime%></td>
        <td><%=gm_jname%></td>
        <td class="w3-bold"><%=gm_hname%> <span class="w3-text-blue">VS</span> <%=gm_aname%></td>
        <td><%=gm_hdvde%>(<%=betting_hcnt%>)</td>
        <td><%=gm_tdvde%>(<%=betting_tcnt%>)</td>
        <td><%=gm_advde%>(<%=betting_acnt%>)</td>
        <td><%=gamescore%></td>
    </tr>
</table>