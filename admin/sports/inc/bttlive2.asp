<%
    '//
	flag  = rq("flag")    '승무패
	stxt1 = rq("stxt1")
	stxt2 = rq("stxt2")
	stxt3 = rq("stxt3")
	stxt4 = rq("stxt4")
	stxt5 = rq("stxt5")
	stxt6 = rq("stxt6")
	stxt7 = rq("stxt7")
	stxt8 = rq("stxt8")
	stxt9 = rq("stxt9")
	stxt10 = rq("stxt10")
	GotoPage = rq("GotoPage")
    '//

    if flag="10" then
        iipgname = "../WinTieLose/"
    elseif flag="11" then
        iipgname = "../WinTieLose/"
    elseif flag="12" then
        iipgname = "../WinTieLose/"
    elseif flag="13" then
        iipgname = "../WinTieLose/"
    end if

	'//
	If stxt1="" then stxt1 = Left(now(),10)
	If stxt2="" then stxt2 = Left(now()+1,10)
	if GotoPage = "" Then GotoPage = 1
	stxt11 = Replace(stxt1,"-","")
	stxt22 = Replace(stxt2,"-","")
    '//
	set rs = server.CreateObject("ADODB.Recordset")
	SQL = "       SELECT ISNULL(COUNT(A.GM_IDX),0) , ISNULL(SUM(HAPBTTMONEY_H+HAPBTTMONEY_T+HAPBTTMONEY_A),0) "
	SQL = SQL & "   FROM TB_SPO_GAME A "
	SQL = SQL & "  INNER JOIN (SELECT SPO_CODE, SPO_NAME, SPO_TIE FROM TB_SPO_KIND WHERE SPO_FLAG='J') B "
	SQL = SQL & "     ON A.GM_JCODE = B.SPO_CODE "
	SQL = SQL & "  INNER JOIN (SELECT SPO_CODE, SPO_NAME, CH_NCODE FROM TB_SPO_KIND WHERE SPO_FLAG='L') C "
	SQL = SQL & "     ON A.GM_LCODE = C.SPO_CODE "
    '//
	SQL = SQL & "  INNER JOIN (SELECT SPO_CODE, SPO_NAME, CH_NCODE FROM TB_SPO_KIND WHERE SPO_FLAG='T') D "
	SQL = SQL & "     ON A.GM_HTCODE = D.SPO_CODE "
	SQL = SQL & "  INNER JOIN (SELECT SPO_CODE, SPO_NAME,SPO_IMGNAME FROM TB_SPO_KIND WHERE SPO_FLAG='N') E "
	SQL = SQL & "     ON C.CH_NCODE = E.SPO_CODE "
	SQL = SQL & "  INNER JOIN (SELECT SPO_CODE, SPO_NAME, CH_NCODE FROM TB_SPO_KIND WHERE SPO_FLAG='T') F "
	SQL = SQL & "     ON A.GM_ATCODE = F.SPO_CODE "
	SQL = SQL & "  INNER JOIN (SELECT SPO_CODE, SPO_NAME,SPO_IMGNAME FROM TB_SPO_KIND WHERE SPO_FLAG='N') G "
	SQL = SQL & "     ON C.CH_NCODE = G.SPO_CODE "
    '//
	SQL = SQL & "  left outer JOIN ( "

    SQL = SQL & " SELECT A.GM_IDX, "
    SQL = SQL & "        ISNULL(SUM(A.HAPBTTMONEY_H),0) HAPBTTMONEY_H,  "
    SQL = SQL & "        ISNULL(SUM(A.HAPBTTMONEY_T),0) HAPBTTMONEY_T,  "
    SQL = SQL & "        ISNULL(SUM(A.HAPBTTMONEY_A),0) HAPBTTMONEY_A, "
    SQL = SQL & "        ISNULL(SUM(A.BETTING_HCNT),0)  BETTING_HCNT,  "
    SQL = SQL & "        ISNULL(SUM(A.BETTING_TCNT),0)  BETTING_TCNT,  "
    SQL = SQL & "        ISNULL(SUM(A.BETTING_ACNT),0)  BETTING_ACNT "
    SQL = SQL & " FROM ( "
    SQL = SQL & " 		SELECT B.GM_IDX,  "
    SQL = SQL & " 			   SUM(A.BTT_MONEY) HAPBTTMONEY_H, 0 HAPBTTMONEY_T, 0 HAPBTTMONEY_A, "
    SQL = SQL & " 			   COUNT(B.GM_IDX) BETTING_HCNT, 0 BETTING_TCNT, 0 BETTING_ACNT "
    SQL = SQL & " 		FROM ( "
    SQL = SQL & " 				SELECT * "
    SQL = SQL & " 				FROM tb_spo_betting_master "
    SQL = SQL & " 				WHERE BTT_IDX IN (SELECT DISTINCT BTT_IDX FROM tb_spo_betting_info WHERE BTS_CHFLAG='H' AND BTS_STATE='I')  "
    SQL = SQL & " 			) A "
    SQL = SQL & " 		INNER JOIN tb_spo_betting_info B ON A.BTT_IDX=B.BTT_IDX AND B.BTS_CHFLAG='H' "
    SQL = SQL & " 		GROUP BY B.GM_IDX "
    SQL = SQL & " 		UNION ALL "
    SQL = SQL & " 		SELECT B.GM_IDX,  "
    SQL = SQL & " 			   0, SUM(A.BTT_MONEY) HAPBTTMONEY_T, 0, "
    SQL = SQL & " 			   0, COUNT(B.GM_IDX) BETTING_TCNT, 0 "
    SQL = SQL & " 		FROM ( "
    SQL = SQL & " 				SELECT * "
    SQL = SQL & " 				FROM tb_spo_betting_master "
    SQL = SQL & " 				WHERE BTT_IDX IN (SELECT DISTINCT BTT_IDX FROM tb_spo_betting_info WHERE BTS_CHFLAG='T' AND BTS_STATE='I')  "
    SQL = SQL & " 			) A "
    SQL = SQL & " 		INNER JOIN tb_spo_betting_info B ON A.BTT_IDX=B.BTT_IDX AND B.BTS_CHFLAG='T' "
    SQL = SQL & " 		GROUP BY B.GM_IDX "
    SQL = SQL & " 		UNION ALL "
    SQL = SQL & " 		SELECT B.GM_IDX,  "
    SQL = SQL & " 			   0, 0 , SUM(A.BTT_MONEY) HAPBTTMONEY_A, "
    SQL = SQL & " 			   0, 0 , COUNT(B.GM_IDX) BETTING_ACNT "
    SQL = SQL & " 		FROM ( "
    SQL = SQL & " 				SELECT * "
    SQL = SQL & " 				FROM tb_spo_betting_master "
    SQL = SQL & " 				WHERE BTT_IDX IN (SELECT DISTINCT BTT_IDX FROM tb_spo_betting_info WHERE BTS_CHFLAG='A' AND BTS_STATE='I')  "
    SQL = SQL & " 			) A "
    SQL = SQL & " 		INNER JOIN tb_spo_betting_info B ON A.BTT_IDX=B.BTT_IDX AND B.BTS_CHFLAG='A' "
    SQL = SQL & " 		GROUP BY B.GM_IDX "
    SQL = SQL & " 	) A "
    SQL = SQL & " GROUP BY A.GM_IDX	     "




'	SQL = SQL & "                  SELECT A.GM_IDX, "
'	SQL = SQL & "                         SUM(A.HAPBTTMONEY_H) HAPBTTMONEY_H, "
'	SQL = SQL & "                         SUM(A.HAPBTTMONEY_T) HAPBTTMONEY_T, "
'	SQL = SQL & "                         SUM(A.HAPBTTMONEY_A) HAPBTTMONEY_A, "
'	SQL = SQL & "                         SUM(A.BETTING_HCNT) BETTING_HCNT, "
'	SQL = SQL & "                         SUM(A.BETTING_TCNT) BETTING_TCNT, "
'	SQL = SQL & "                         SUM(A.BETTING_ACNT) BETTING_ACNT "
'	SQL = SQL & "                    FROM "
'	SQL = SQL & "                         ( "
'	SQL = SQL & "                         SELECT GM_IDX, "
'	SQL = SQL & "                                ISNULL(CASE WHEN BTS_CHFLAG='H' THEN SUM(BTS_CHMONEY) END,0) HAPBTTMONEY_H, "
'	SQL = SQL & " 	                             ISNULL(CASE WHEN BTS_CHFLAG='T' THEN SUM(BTS_CHMONEY) END,0) HAPBTTMONEY_T, "
'	SQL = SQL & " 	                             ISNULL(CASE WHEN BTS_CHFLAG='A' THEN SUM(BTS_CHMONEY) END,0) HAPBTTMONEY_A, "
'	SQL = SQL & " 	                             ISNULL(CASE WHEN BTS_CHFLAG='H' THEN COUNT(BTS_CHFLAG) END,0) BETTING_HCNT, "
'	SQL = SQL & " 		                         ISNULL(CASE WHEN BTS_CHFLAG='T' THEN COUNT(BTS_CHFLAG) END,0) BETTING_TCNT, "
'	SQL = SQL & " 	 	                         ISNULL(CASE WHEN BTS_CHFLAG='A' THEN COUNT(BTS_CHFLAG) END,0) BETTING_ACNT "
'	SQL = SQL & "                           FROM TB_SPO_BETTING_INFO "
'	SQL = SQL & "                          WHERE BTS_STATE='I' "
'	SQL = SQL & "                          GROUP BY GM_IDX,BTS_CHFLAG "
'	SQL = SQL & "                          ) A "
'	SQL = SQL & "                    GROUP BY GM_IDX "

    SQL = SQL & "               ) H ON A.GM_IDX = H.GM_IDX "
    '//
	SQL = SQL & "  WHERE A.GM_STATE IN ('20', '50') "
	'rs.Open sql, db, 1
    'totcnt      = formatnumber(rs(0),0)
    'tr_totmoney = formatnumber(rs(1),0)
    'rs.close
    '//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	set rs = server.CreateObject("ADODB.Recordset")
	SQL = "       SELECT A.* , B.SPO_NAME GM_JNAME, C.SPO_NAME GM_LNAME, "
    SQL = SQL & "        D.SPO_NAME GM_HNAME, E.SPO_IMGNAME GM_HIMGNAME, "
    SQL = SQL & "        F.SPO_NAME GM_ANAME, G.SPO_IMGNAME GM_AIMGNAME, B.SPO_TIE SPO_TIE, "
    SQL = SQL & "        ISNULL(H.HAPBTTMONEY_H,0) HAPBTTMONEY_H, "
    SQL = SQL & "        ISNULL(H.HAPBTTMONEY_T,0) HAPBTTMONEY_T, "
    SQL = SQL & "        ISNULL(H.HAPBTTMONEY_A,0) HAPBTTMONEY_A, "
    SQL = SQL & "        ISNULL(H.BETTING_HCNT,0) BETTING_HCNT, "
    SQL = SQL & "        ISNULL(H.BETTING_TCNT,0) BETTING_TCNT, "
    SQL = SQL & "        ISNULL(H.BETTING_ACNT,0) BETTING_ACNT , I.BTT_MONEY_HAP"
	SQL = SQL & "   FROM (select * from TB_SPO_GAME "
	SQL = SQL & "                  WHERE SUBSTRING(GM_DATETIME,1,8) >= '"& STXT11 &"' AND SUBSTRING(GM_DATETIME,1,8) <= '"& STXT22 &"' "
'                                   IF STXT3<>"" THEN SQL = SQL & " AND GM_JCODE = '"& STXT3 &"' "
'                                   IF STXT4<>"" AND STXT5<>"" THEN SQL = SQL & " AND "& STXT4 &" LIKE '%"& STXT5 &"%' "
'                                   IF STXT7<>"" THEN SQL = SQL & " AND GM_FLAG = '"& STXT7 &"' "
'                                   IF STXT8<>"" THEN SQL = SQL & " AND GM_LCODE = '"& STXT8 &"' "
'                                   IF STXT9="" THEN
'                                       SQL = SQL & " AND GM_STATE IN ('20', '50') "
'                                   ELSE
'                                       SQL = SQL & " AND GM_STATE = '"& STXT9 &"' "
'                                   END IF
	SQL = SQL & "         ) A "
	SQL = SQL & "  INNER JOIN (SELECT SPO_CODE, SPO_NAME, SPO_TIE FROM TB_SPO_KIND WHERE SPO_FLAG='J') B "
	SQL = SQL & "     ON A.GM_JCODE = B.SPO_CODE "
	SQL = SQL & "  INNER JOIN (SELECT SPO_CODE, SPO_NAME, CH_NCODE FROM TB_SPO_KIND WHERE SPO_FLAG='L') C "
	SQL = SQL & "     ON A.GM_LCODE = C.SPO_CODE "
    '//
	SQL = SQL & "  INNER JOIN (SELECT SPO_CODE, SPO_NAME, CH_NCODE FROM TB_SPO_KIND WHERE SPO_FLAG='T') D "
	SQL = SQL & "     ON A.GM_HTCODE = D.SPO_CODE "
	SQL = SQL & "  INNER JOIN (SELECT SPO_CODE, SPO_NAME,SPO_IMGNAME FROM TB_SPO_KIND WHERE SPO_FLAG='N') E "
	SQL = SQL & "     ON C.CH_NCODE = E.SPO_CODE "
	SQL = SQL & "  INNER JOIN (SELECT SPO_CODE, SPO_NAME, CH_NCODE FROM TB_SPO_KIND WHERE SPO_FLAG='T') F "
	SQL = SQL & "     ON A.GM_ATCODE = F.SPO_CODE "
	SQL = SQL & "  INNER JOIN (SELECT SPO_CODE, SPO_NAME,SPO_IMGNAME FROM TB_SPO_KIND WHERE SPO_FLAG='N') G "
	SQL = SQL & "     ON C.CH_NCODE = G.SPO_CODE "
    '//
	SQL = SQL & "  left outer JOIN ( "
    SQL = SQL & " SELECT A.GM_IDX, "
    SQL = SQL & "        ISNULL(SUM(A.HAPBTTMONEY_H),0) HAPBTTMONEY_H,  "
    SQL = SQL & "        ISNULL(SUM(A.HAPBTTMONEY_T),0) HAPBTTMONEY_T,  "
    SQL = SQL & "        ISNULL(SUM(A.HAPBTTMONEY_A),0) HAPBTTMONEY_A, "
    SQL = SQL & "        ISNULL(SUM(A.BETTING_HCNT),0)  BETTING_HCNT,  "
    SQL = SQL & "        ISNULL(SUM(A.BETTING_TCNT),0)  BETTING_TCNT,  "
    SQL = SQL & "        ISNULL(SUM(A.BETTING_ACNT),0)  BETTING_ACNT "
    SQL = SQL & " FROM ( "
    SQL = SQL & " 		SELECT B.GM_IDX,  "
    SQL = SQL & " 			   SUM(A.BTT_MONEY) HAPBTTMONEY_H, 0 HAPBTTMONEY_T, 0 HAPBTTMONEY_A, "
    SQL = SQL & " 			   COUNT(B.GM_IDX) BETTING_HCNT, 0 BETTING_TCNT, 0 BETTING_ACNT "
    SQL = SQL & " 		FROM ( "
    SQL = SQL & " 				SELECT * "
    SQL = SQL & " 				FROM tb_spo_betting_master "
    SQL = SQL & " 				WHERE BTT_IDX IN (SELECT DISTINCT BTT_IDX FROM tb_spo_betting_info WHERE BTS_CHFLAG='H' AND BTS_STATE='I')  "
    SQL = SQL & " 			) A "
    SQL = SQL & " 		INNER JOIN tb_spo_betting_info B ON A.BTT_IDX=B.BTT_IDX AND B.BTS_CHFLAG='H' "
    SQL = SQL & " 		GROUP BY B.GM_IDX "
    SQL = SQL & " 		UNION ALL "
    SQL = SQL & " 		SELECT B.GM_IDX,  "
    SQL = SQL & " 			   0, SUM(A.BTT_MONEY) HAPBTTMONEY_T, 0, "
    SQL = SQL & " 			   0, COUNT(B.GM_IDX) BETTING_TCNT, 0 "
    SQL = SQL & " 		FROM ( "
    SQL = SQL & " 				SELECT * "
    SQL = SQL & " 				FROM tb_spo_betting_master "
    SQL = SQL & " 				WHERE BTT_IDX IN (SELECT DISTINCT BTT_IDX FROM tb_spo_betting_info WHERE BTS_CHFLAG='T' AND BTS_STATE='I')  "
    SQL = SQL & " 			) A "
    SQL = SQL & " 		INNER JOIN tb_spo_betting_info B ON A.BTT_IDX=B.BTT_IDX AND B.BTS_CHFLAG='T' "
    SQL = SQL & " 		GROUP BY B.GM_IDX "
    SQL = SQL & " 		UNION ALL "
    SQL = SQL & " 		SELECT B.GM_IDX,  "
    SQL = SQL & " 			   0, 0 , SUM(A.BTT_MONEY) HAPBTTMONEY_A, "
    SQL = SQL & " 			   0, 0 , COUNT(B.GM_IDX) BETTING_ACNT "
    SQL = SQL & " 		FROM ( "
    SQL = SQL & " 				SELECT * "
    SQL = SQL & " 				FROM tb_spo_betting_master "
    SQL = SQL & " 				WHERE BTT_IDX IN (SELECT DISTINCT BTT_IDX FROM tb_spo_betting_info WHERE BTS_CHFLAG='A' AND BTS_STATE='I')  "
    SQL = SQL & " 			) A "
    SQL = SQL & " 		INNER JOIN tb_spo_betting_info B ON A.BTT_IDX=B.BTT_IDX AND B.BTS_CHFLAG='A' "
    SQL = SQL & " 		GROUP BY B.GM_IDX "
    SQL = SQL & " 	) A "
    SQL = SQL & " GROUP BY A.GM_IDX	     "







'	SQL = SQL & "                  SELECT A.GM_IDX, "
'	SQL = SQL & "                         SUM(A.HAPBTTMONEY_H) HAPBTTMONEY_H, "
'	SQL = SQL & "                         SUM(A.HAPBTTMONEY_T) HAPBTTMONEY_T, "
'	SQL = SQL & "                         SUM(A.HAPBTTMONEY_A) HAPBTTMONEY_A, "
'	SQL = SQL & "                         SUM(A.BETTING_HCNT) BETTING_HCNT, "
'	SQL = SQL & "                         SUM(A.BETTING_TCNT) BETTING_TCNT, "
'	SQL = SQL & "                         SUM(A.BETTING_ACNT) BETTING_ACNT "
'	SQL = SQL & "                    FROM "
'	SQL = SQL & "                         ( "
'	SQL = SQL & "                         SELECT GM_IDX, "
'	SQL = SQL & "                                ISNULL(CASE WHEN BTS_CHFLAG='H' THEN SUM(BTS_CHMONEY) END,0) HAPBTTMONEY_H, "
'	SQL = SQL & " 	                             ISNULL(CASE WHEN BTS_CHFLAG='T' THEN SUM(BTS_CHMONEY) END,0) HAPBTTMONEY_T, "
'	SQL = SQL & " 	                             ISNULL(CASE WHEN BTS_CHFLAG='A' THEN SUM(BTS_CHMONEY) END,0) HAPBTTMONEY_A, "
'	SQL = SQL & " 	                             ISNULL(CASE WHEN BTS_CHFLAG='H' THEN COUNT(BTS_CHFLAG) END,0) BETTING_HCNT, "
'	SQL = SQL & " 		                         ISNULL(CASE WHEN BTS_CHFLAG='T' THEN COUNT(BTS_CHFLAG) END,0) BETTING_TCNT, "
'	SQL = SQL & " 	 	                         ISNULL(CASE WHEN BTS_CHFLAG='A' THEN COUNT(BTS_CHFLAG) END,0) BETTING_ACNT "
'	SQL = SQL & "                           FROM TB_SPO_BETTING_INFO "
'	SQL = SQL & "                          WHERE BTS_STATE='I' "
'	SQL = SQL & "                          GROUP BY GM_IDX,BTS_CHFLAG "
'	SQL = SQL & "                          ) A "
'	SQL = SQL & "                    GROUP BY GM_IDX "





    SQL = SQL & "               ) H ON A.GM_IDX = H.GM_IDX "
    '//
	SQL = SQL & "  left outer JOIN ( "
	SQL = SQL & "                  SELECT SUM(A.BTT_MONEY) BTT_MONEY_HAP,B.GM_IDX FROM tb_spo_betting_master A, TB_SPO_BETTING_INFO B "
	SQL = SQL & "                   WHERE A.BTT_IDX = B.BTT_IDX "
	SQL = SQL & "                     AND B.BTS_STATE='I' GROUP BY B.GM_IDX "
    SQL = SQL & "               ) I ON A.GM_IDX = I.GM_IDX "
    '//

    '//

'    SQL = SQL & "   ORDER BY A.GM_DATETIME ASC,C.SPO_NAME ASC,G.SPO_NAME ASC, B.SPO_NAME, "
'    'SQL = SQL & "           CASE WHEN A.GM_HNDFLAG='VS' THEN 'CV' ELSE A.GM_HNDFLAG END ASC,"
'    SQL = SQL & "            D.SPO_NAME ASC, E.SPO_NAME ASC, A.GM_HNDFLAG ASC "

	'SQL = SQL & "  ORDER BY B.SPO_NAME ASC, C.SPO_NAME ASC, A.GM_DATETIME ASC, A.WDATE DESC "

	SQL = SQL & "  ORDER BY B.SPO_NAME ASC, C.SPO_NAME ASC, "
    'SQL = SQL & "  CASE WHEN A.GM_HNDFLAG='VS' THEN 'CV' ELSE A.GM_HNDFLAG END ASC, "
    SQL = SQL & "  A.GM_DATETIME ASC, D.SPO_NAME ASC, F.SPO_NAME ASC, A.GM_HNDFLAG ASC "

RESPONSE.Write SQL
'RESPONSE.END

	rs.PageSize=1000
	rs.Open sql, db, 1
	if not rs.EOF Then rs.AbsolutePage=int(gotopage)
	schCnt = rs.recordcount
	'//
    param = "gotopage="&gotopage&"&stxt1="&stxt1&"&stxt2="&stxt2&"&stxt3="&stxt3&"&stxt4="&stxt4&"&stxt5="&stxt5&"&stxt6="&stxt6&"&stxt7="&stxt7&"&stxt8="&stxt8&"&stxt9="&stxt9&"&stxt10="&stxt10
    '//
%>
<!--#include virtual="/admin/sports/inc/js.asp" -->

				<table width='100%' cellpadding=0 cellspacing=0 border=0>
					<tr>
						<td height=30 colspan=2 class=dotum_15><img src='/admin/images/icon_arrow0.gif' align=absmiddle>&nbsp; 배팅현황</td>
					</tr>
					<tr height=2>
						<td width=200 bgcolor='#AF0B0A'></td>
						<td bgcolor='#F2F8FB'></td>
					</tr>
				</table>

				<table border="0" cellpadding="3" cellspacing="1" style="width:100%; border:1px solid #CBE1EE;">

<iframe name=bettifrm width=0 height=0 frameborder=0 border=0></iframe>
<form name="form" id="frm" method="post" action="?">
<input type="hidden" name="flag" value="<%=flag%>">
<input type="hidden" name="btflag">

<input type="hidden" name="rst_htm_score">
<input type="hidden" name="rst_atm_score">
<input type="hidden" name="gm_state">
<input type="hidden" name="gm_idx">
<input type="hidden" name="pagenm" value="<%=pagenm%>">

					<tr>
						<td bgcolor=#F2F2F2 width=15% align=center>조건검색</td>
						<td height=30>
							<input type="text" name="stxt1" id="stxt1" value="<%=stxt1%>" size="10" readonly />
							&nbsp;<img src="/admin/images/calendar.gif" align="absmiddle" OnClick="calendar(event, 'stxt1')" style="cursor:pointer">
							~
							<input type="text" name="stxt2" id="stxt2" value="<%=stxt2%>" size="10" readonly />
							&nbsp;<img src="/admin/images/calendar.gif" align="absmiddle" OnClick="calendar(event, 'stxt2')" style="cursor:pointer">
                            <BR>
                            <select name="stxt7" onchange="chg_teamShow(this.value)">
                                <option value="">-게임타입 전체-
                                <option value="11" <%if stxt7="11" then response.write " selected"%>>승무패
                                <option value="10" <%if stxt7="10" then response.write " selected"%>>핸디캡
                                <option value="12" <%if stxt7="12" then response.write " selected"%>>스폐셜
                                <option value="13" <%if stxt7="13" then response.write " selected"%>>스폐셜2
                            </select>
                            <% call jmkSelectBox("stxt3",stxt3,"")%>
                            <% call lgeSelectBox("stxt8",stxt8,"")%>
                            <% call selIngWrite("stxt9",stxt9,"")%>
							<select name=stxt4>
								<option value="">--검색구분--
								<option value="B.SPO_NAME" <%If stxt4="B.SPO_NAME" Then Response.write " selected"%>>종목명
								<option value="C.SPO_NAME" <%If stxt4="C.SPO_NAME" Then Response.write " selected"%>>리그명
								<option value="D.SPO_NAME" <%If stxt4="D.SPO_NAME" Then Response.write " selected"%>>홈팀명
								<option value="F.SPO_NAME" <%If stxt4="F.SPO_NAME" Then Response.write " selected"%>>원정팀명
							</select>
							<input type=text name=stxt5 value="<%=stxt5%>" size=20>
							<input type="image" src="/admin/images/btn_search1.gif" align=bottom onclick="return searChb()">
						</td>
					</tr>
				</table>

				<BR>
				<table width=100% cellpadding=3 cellspacing=0 border=0>
					<tr>
						<td>
							<img src='/admin/images/icon_arrow3.gif' align=absmiddle>
                            &nbsp;Total <font color=blue><%=FormatNumber(totCnt,0)%></font>
							/ 검색결과 <font color=blue><%=FormatNumber(schCnt,0)%></font>
						</td>
						<td width=50% align=right>
                            * 총베팅금액 : <FONT color=red><%=tr_totmoney%></font> 원
						</td>
                    </tr>
				</table>

				<table border="0" cellpadding="1" cellspacing="1" width=100% bgcolor="#CBE1EE">
					<tr height=35 bgcolor="#F2F2F2" align=center>
						<td width=4%>No.</td>
						<td width=6%>경기일시</td>
						<td width=5%>종목</td>
						<td width=4%>구분</td>
						<td width=14%>홈팀</td>
						<td width=15%>원정팀</td>
						<td width=4%>홈팀<br>배당</td>
  						<td width=4%>핸디<br>(기준)</td>
						<td width=4%>원정팀<br>배당</td>
						<td width=5%>홈팀<br>배팅금</td>
						<td width=5%>무승부<br>배팅금</td>
						<td width=6%>원정팀<br>배팅금</td>
						<td width=4%>총<br>베팅수</td>
						<td width=4%>상태</td>
						<td width=6%>결과</td>
						<td width=10%>비고</td>
					</tr>

<%
j=schCnt-((gotopage-1)*20)
i=1
do until rs.EOF or i>rs.pagesize
    '//
	gm_idx      = rs("gm_idx")
	gm_datetime = rs("gm_datetime")
	gm_jname    = rs("gm_jname")
	gm_lname    = rs("gm_lname")
	gm_hname    = rs("gm_hname")
	gm_aname    = rs("gm_aname")
    gm_himgname = rs("gm_himgname")
    gm_aimgname = rs("gm_aimgname")
    gm_hdvde    = formatnumber(rs("gm_hdvde"),2)
    gm_tdvde    = rs("gm_tdvde")
    gm_advde    = formatnumber(rs("gm_advde"),2)
    gm_state    = rs("gm_state")
    spo_tie     = rs("spo_tie")

    gm_flag     = rs("gm_flag")
    gm_hndflag  = rs("gm_hndflag")
    gm_vschb    = rs("gm_vschb")

    hapbttmoney_h = rs("hapbttmoney_h")
    hapbttmoney_t = rs("hapbttmoney_t")
    hapbttmoney_a = rs("hapbttmoney_a")
    btt_money_hap = rs("btt_money_hap")

'if hapbttmoney_h>0 then hapbttmoney_h=btt_money_hap
'if hapbttmoney_t>0 then hapbttmoney_t=btt_money_hap
'if hapbttmoney_a>0 then hapbttmoney_a=btt_money_hap

    'hapbttmoney = hapbttmoney_h+hapbttmoney_t+hapbttmoney_a

    betting_hcnt  = rs("betting_hcnt")
    betting_tcnt  = rs("betting_tcnt")
    betting_acnt  = rs("betting_acnt")
    rst_htm_score = rs("rst_htm_score")
    rst_atm_score = rs("rst_atm_score")
    gm_flag     = rs("gm_flag")

    betCnt        = betting_hcnt + betting_tcnt + betting_acnt
	wdate         = getDateRe("1",rs("wdate"),"-")
	'//
	gm_datetime1 = mid(getDateRe("1",gm_datetime,"-"),3)
	gm_datetime2 = getDateRe("10",gm_datetime,":")
	gm_datetime  = mid(gm_datetime1 &" "& gm_datetime2,4)
    gm_state_val = getGmStateRe(gm_state)
    '//
    if old_gm_lname<>gm_lname then
        imgval = ""
        if gm_himgname<>"" then
            imgval = "<img src='/fileupdown/icon/"&gm_himgname&"' width=16 height=11>"
        end if
        response.write "<tr height=2><td colspan=25 height=26 bgcolor=#E7F1F8 valign=bottom><B>&nbsp;"& imgval & gm_lname &"</td></tr>"
    end if
    '//
    'IF spo_tie<>"Y" and then gm_tdvde = "VS"
    gmscore = rst_htm_score & " : " &rst_atm_score
    if gmscore=" : " then gmscore=""
    '//
    hicon = ""
    aicon = ""
    if gm_hndflag="UO" then
        hicon = "[오버]<font color=red>▲</font>"
        aicon = "[언더]<font color=blue>▼</font>"
    end if
    '//
    gm_tdvde_val=""
    IF GM_FLAG="11" THEN
        IF SPO_TIE<>"Y" THEN
            GM_TDVDE_VAL = "vs"
        ELSE
            GM_TDVDE_VAL = FORMATNUMBER(GM_TDVDE,2)
        END IF
    ELSEIF GM_FLAG="10" THEN
        IF GM_HNDFLAG="VS" THEN
        ELSEIF GM_HNDFLAG="UO" THEN
            GM_TDVDE_VAL = GM_TDVDE
        ELSEIF GM_HNDFLAG="CH" OR GM_HNDFLAG="CA" THEN
            IF GM_TDVDE<>"0" THEN
                IF GM_HNDFLAG="CA" THEN
                    GM_TDVDE_VAL = "+" & GM_TDVDE
                ELSE
                    GM_TDVDE_VAL = "-" & GM_TDVDE
                END IF
            ELSE
                GM_TDVDE_VAL = GM_TDVDE
            END IF
        END IF
    ELSEIF GM_FLAG="12" THEN
        IF GM_HNDFLAG="VS" THEN
            IF GM_VSCHB="Y" THEN
                GM_TDVDE_VAL = "vs"
            ELSE
                GM_TDVDE_VAL = GM_TDVDE
            END IF
        ELSEIF GM_HNDFLAG="UO" THEN
            GM_TDVDE_VAL = GM_TDVDE
        ELSEIF GM_HNDFLAG="CH" OR GM_HNDFLAG="CA" THEN
            IF GM_TDVDE<>"0" THEN
                IF GM_HNDFLAG="CA" THEN
                    GM_TDVDE_VAL = "+" & GM_TDVDE
                ELSE
                    GM_TDVDE_VAL = "-" & GM_TDVDE
                END IF
            ELSE
                GM_TDVDE_VAL = GM_TDVDE
            END IF
        END IF
    ELSEIF GM_FLAG="13" THEN
        IF GM_HNDFLAG="VS" THEN
            IF GM_VSCHB="Y" THEN
                GM_TDVDE_VAL = "vs"
            ELSE
                GM_TDVDE_VAL = FORMATNUMBER(GM_TDVDE,2)
            END IF
        ELSEIF GM_HNDFLAG="UO" THEN
            GM_TDVDE_VAL = GM_TDVDE
        ELSEIF GM_HNDFLAG="CH" OR GM_HNDFLAG="CA" THEN
            IF GM_TDVDE<>"0" THEN
                IF GM_HNDFLAG="CA" THEN
                    GM_TDVDE_VAL = "+" & GM_TDVDE
                ELSE
                    GM_TDVDE_VAL = "-" & GM_TDVDE
                END IF
            ELSE
                GM_TDVDE_VAL = GM_TDVDE
            END IF
        END IF
    END IF
    '//
    if gm_flag="11" then
        gubun_nm = "승무패"
    elseif gm_flag="10" then
        if gm_hndflag="UO" then
            gubun_nm = "오언"
        else
            gubun_nm = "핸디캡"
        end if
    elseif gm_flag="12" then
        gubun_nm = "스폐셜"
    elseif gm_flag="13" then
        gubun_nm = "스폐셜2"
    end if
    '//
%>

					<tr height=25 bgcolor=white align=left>
						<td align=center><%=j%></td>
						<td align=center><a href="btListCon.asp?flag=<%=gm_flag%>&gm_idx=<%=gm_idx%>&pgindex=<%=j%>&<%=param%>"><%=gm_datetime%></a></td>
						<td align=center><%=gm_jname%></td>
						<td align=center><%=gubun_nm%></td>
						<td>&nbsp;<a href="<%=iipgname%>Write.asp?gm_idx=<%=gm_idx%>&<%=param%>"><%=gm_hname & hicon%></td>
						<td>&nbsp;<a href="<%=iipgname%>Write.asp?gm_idx=<%=gm_idx%>&<%=param%>"><%=gm_aname & aicon%></td>
						<td align=center<%=gm_hdvde%></td>
						<td align=center><%=gm_tdvde_val%></td>
						<td align=center><%=gm_advde%></td>
						<td align=right><a href="gameAccList.asp?gm_idx=<%=gm_idx%>&ch_flag=H&bttflag=1"><font color=green><%=formatnumber(hapbttmoney_h,0)%></a>&nbsp;</td>
						<td align=right><a href="gameAccList.asp?gm_idx=<%=gm_idx%>&ch_flag=T&bttflag=1"><font color=red><%=formatnumber(hapbttmoney_t,0)%></a>&nbsp;</td>
						<td align=right><a href="gameAccList.asp?gm_idx=<%=gm_idx%>&ch_flag=A&bttflag=1"><font color=blue><%=formatnumber(hapbttmoney_a,0)%></a>&nbsp;</td>
						<td align=center><%=betCnt%></td>
						<td align=center><%=gm_state_val%></td>
						<td align=center><%=gmscore%></td>
						<td align=center>
                            <%if gm_state="50" then%>
                                <img src="/admin/images/play_result.gif" onclick='resultFrm("<%=gm_idx%>","<%=rst_htm_score%>","<%=rst_atm_score%>","")' style="cursor:pointer">
                                <img src="/admin/images/play_cancel.gif" onclick='resultFrm("<%=gm_idx%>","<%=rst_htm_score%>","<%=rst_atm_score%>","C")' style="cursor:pointer">
                            <%else%>
                                <!--img src="/admin/images/play_result.gif" onclick='alert("베팅마감시에만 결과입력이 가능합니다.")' style="cursor:pointer">
                                <img src="/admin/images/play_cancel.gif" onclick='alert("베팅마감시에만 경기취소가 가능합니다.")' style="cursor:pointer"-->
                            <%end if%>
                        </td>
					</tr>
					<tr bgcolor=white>
                        <td colspan=20 align=right><div id="resultHtm_<%=gm_idx%>"></div></td>
					</tr>

<%
    old_gm_lname = gm_lname
rs.movenext
j=j-1
i=i+1
loop
%>

</form>

				</table>

				<table border="0" cellpadding="0" cellspacing="0" width=100% bgcolor=white>
					<tr height=45>
						<td align=center>
							<!--#include virtual="/admin/include/paging.asp" -->
						</td>
					</tr>
				</table>
<%
Rs.close
Set rs=nothing
Db.close
Set db=nothing
%>