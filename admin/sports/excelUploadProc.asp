<!--#include virtual="/admin/include/aSessionend.asp" -->
<!--#include virtual="/db/db.asp" -->
<%
    '//
    asfilename = getDateTime(now()) &".XLS"
    '//
	Set upload = Server.CreateObject("DEXT.FileUpload")
	upload.DefaultPath = server.mappath("/fileupdown/excel")
    '//
	if upload("excelfile") = "" then call getPopup("2","엑셀파일이 없습니다.","")
    '//
    filename = upload("excelfile").FileName
    filepath = upload.DefaultPath & "\" & filename
    strName  = filename
    strEtc   = Mid(FileName, Instr(FileName, ".") + 1)  '//확장자
    '//
    If UCase(strEtc)<>"XLS" Then call getPopup("2","확장자 .xls 파일외에는 업로드 못합니다.","")
    '//
    filepath = upload.DefaultPath &"\"& asfilename
    upload("excelfile").SaveAs filepath
    Filename = Mid(filepath,instrRev(filepath,"\")+1)
    '//
    gm_flag = upload("gm_flag")
    '//
    Set xlDb = Server.CreateObject("ADODB.Connection")
    Set oRs  = Server.CreateObject("ADODB.RecordSet")
    '//
    connectString = "Provider=Microsoft.JET.OLEDB.4.0;Data Source="& filepath &"; Extended Properties=""Excel 8.0;HDR=YES;IMEX=1;"""
    xlDb.Open connectString
    '//
    Set oADOX = CreateObject("ADOX.Catalog") '시트명을 얻기위한 object
    oADOX.ActiveConnection = connectString
    For Each oTable in oADOX.Tables
        '//
        SheetName  = trim(oTable.Name)
        '//
        sQuery = "SELECT * FROM ["&SheetName&"] "
        oRs.Open sQuery,xlDb ,1
        If Not(oRs.Eof Or oRs.Bof) then
            arrTmp = oRs.GetRows()
        End if
        aacnt = oRs.recordcount
'response.write oRs.recordcount &"<BR>"
'response.write SheetName
'response.end
        oRs.Close

        '//
        IF IsArray(arrTmp) and aacnt>0 Then
        '//
            if SheetName="스페셜$" or SheetName="실시간$" then
                For i=0 To UBound(arrTmp, 2)
                    '//1.게임일시/2.리그/3.홈팀/4.홈배당/5.기준/6.원정배당/7.원정/8.IDX/9.게임타입
                    exl_val1 = getRevalue(trim(arrTmp(0,i)),"")
                    exl_val2 = trim(arrTmp(1,i))
                    exl_val3 = trim(arrTmp(2,i))
                    exl_val4 = trim(arrTmp(3,i))
                    exl_val5 = trim(arrTmp(4,i))
                    exl_val6 = getRevalue(trim(arrTmp(5,i)),0)
                    exl_val7 = getRevalue(trim(arrTmp(6,i)),0)
                    exl_val8 = getRevalue(trim(arrTmp(7,i)),0)
                    exl_val9 = getRevalue(trim(arrTmp(8,i)),"")
                    if SheetName="스페셜$" then
                        gm_flag  = "4"
                    else
                        gm_flag  = "5"
                    end if
                    '//
                    if exl_val1<>"" then
						SQL = "EXEC Proc_InsertExcelData "
						SQL = SQL & " @GM_FLAG	 = '"&rqVal(GM_FLAG)&"'"
						SQL = SQL & " ,@EXL_VAL1	 = '"&rqVal(EXL_VAL1)&"'"	     
						SQL = SQL & " ,@EXL_VAL2	 = '"&rqVal(EXL_VAL2)&"'"         
						SQL = SQL & " ,@EXL_VAL3	 = '"&rqVal(EXL_VAL3)&"'"      
	                    SQL = SQL & " ,@EXL_VAL4	 = '"&rqVal(EXL_VAL4)&"'"
	                    SQL = SQL & " ,@EXL_VAL5	 = '"&rqVal(EXL_VAL5)&"'"
	                    SQL = SQL & " ,@EXL_VAL6	 = '"&rqVal(EXL_VAL6)&"'"
	                    SQL = SQL & " ,@EXL_VAL7	 = '"&rqVal(EXL_VAL7)&"'"
	                    SQL = SQL & " ,@EXL_VAL8	 = '"&rqVal(EXL_VAL8)&"'"
	                    SQL = SQL & " ,@EXL_VAL9	 = '"&rqVal(EXL_VAL9)&"'"
	                    SQL = SQL & " ,@FILENAME	 = '"&rqVal(FILENAME)&"'"
	                    DB.EXECUTE SQL
                    end if
                    '//
                Next
            else
                For i=0 To UBound(arrTmp, 2)
                    '//게임일시/리그/홈팀/홈배당/기준/원정배당/원정/IDX
                    exl_val1 = getRevalue(trim(arrTmp(0,i)),"")
                    exl_val2 = trim(arrTmp(1,i))
                    exl_val3 = trim(arrTmp(2,i))
                    exl_val4 = trim(arrTmp(3,i))
                    exl_val5 = trim(arrTmp(4,i))
                    exl_val6 = getRevalue(trim(arrTmp(5,i)),0)
                    exl_val7 = replace(getRevalue(trim(arrTmp(6,i)),0),"'","")
                    exl_val8 = getRevalue(trim(arrTmp(7,i)),0)
                    '//
                    if SheetName="승무패$" then
                        gm_flag  = "1"
                    elseif SheetName="핸디캡$" then
                        gm_flag  = "2"
                    elseif SheetName="오버언더$" then
                        gm_flag  = "3"
                    elseif SheetName="실시간$" then
                        gm_flag  = "5"
                    end if
                    '//
                    if exl_val1<>"" then
						SQL = "EXEC Proc_InsertExcelData "
						SQL = SQL & " @GM_FLAG	 = '"&rqVal(GM_FLAG)&"'"
						SQL = SQL & " ,@EXL_VAL1	 = '"&rqVal(EXL_VAL1)&"'"	     
						SQL = SQL & " ,@EXL_VAL2	 = '"&rqVal(EXL_VAL2)&"'"         
						SQL = SQL & " ,@EXL_VAL3	 = '"&rqVal(EXL_VAL3)&"'"      
	                    SQL = SQL & " ,@EXL_VAL4	 = '"&rqVal(EXL_VAL4)&"'"
	                    SQL = SQL & " ,@EXL_VAL5	 = '"&rqVal(EXL_VAL5)&"'"
	                    SQL = SQL & " ,@EXL_VAL6	 = '"&rqVal(EXL_VAL6)&"'"
	                    SQL = SQL & " ,@EXL_VAL7	 = '"&rqVal(EXL_VAL7)&"'"
	                    SQL = SQL & " ,@EXL_VAL8	 = '"&rqVal(EXL_VAL8)&"'"
	                    SQL = SQL & " ,@FILENAME	 = '"&rqVal(FILENAME)&"'"                   
                        DB.EXECUTE SQL
                    end if
                    '//
                Next
            end if
        '//
        End IF
        '//
    Next
    '//
    xlDb.Close
    DB.CLOSE
    '//
    '//
    response.Redirect "uplodExcel.asp?flag="&gm_flag&"&Filename="&Filename
%>