<!--#include virtual="/admin/include/top.asp" -->

<%
	flag  = rq("flag")
	stxt1 = rq("stxt1")
	stxt2 = rq("stxt2")
	stxt3 = rq("stxt3")
	stxt4 = rq("stxt4")
	stxt5 = rq("stxt5")
	stxt6 = rq("stxt6")
	stxt7 = rq("stxt7")
	stxt8 = rq("stxt8")
	stxt9 = rq("stxt9")
	stxt10 = rq("stxt10")
	GotoPage = rq("GotoPage")
	gm_idx  = rq("gm_idx")
	'//
    titleMsg = "스페셜2 경기등록"
    '//
	If gm_idx<>"" then
		set rs = server.CreateObject("ADODB.Recordset")
		SQL = "select * from tb_spo_game where gm_idx = "& gm_idx
		rs.Open sql, db, 1
        gm_idx	     = rs("gm_idx")
        gm_flag	     = rs("gm_flag")
        gm_jcode	 = rs("gm_jcode")
        gm_lcode	 = rs("gm_lcode")
        gm_htcode	 = rs("gm_htcode")
        gm_atcode	 = rs("gm_atcode")
        gm_datetime	 = rs("gm_datetime")
        gm_hdvde	 = rs("gm_hdvde")
        gm_tdvde	 = rs("gm_tdvde")
        gm_advde	 = rs("gm_advde")
        gm_state     = rs("gm_state")
        gm_bonuscnt  = rs("gm_bonuscnt")
        wdate	     = rs("wdate")
		rs.close
        spo_date     = mid(gm_datetime,1,4) &"-"& mid(gm_datetime,5,2) &"-"& mid(gm_datetime,7,2)
        spo_hour     = mid(gm_datetime,9,2)
        spo_minute   = mid(gm_datetime,11,2)
	End if
    '//
    if spo_date="" then spo_date = left(now(),10)
%>

<table width="100%" border=0 cellspacing="0" cellpadding="0" height="100%">
	<tr>
		<td width="160" bgcolor='#F7F7F7' valign="top">
			<!--#include virtual="/admin/include/leftSports.asp" -->
		</td>
		<td valign="top" width=''>

    	<table width=1000 cellpadding=10 cellspacing=0 border=0>
			<tr>
				<td valign=top>

				<table width='100%' cellpadding=0 cellspacing=0 border=0>
					<tr>
						<td height=30 colspan=2 class=dotum_15><img src='/admin/images/icon_arrow0.gif' align=absmiddle>&nbsp;<%=titleMsg%></td>
					</tr>
					<tr height=2>
						<td width=200 bgcolor='#AF0B0A'></td>
						<td bgcolor='#F2F8FB'></td>
					</tr>
				</table>

<script language='JavaScript'>
<!--

    function formChb(val1){
        if (form.ch_jcode.value == ''){alert('종목을 선택해 주세요.');form.ch_jcode.focus();	return false;}
        if (form.ch_lcode.value == ''){alert('리그를 선택해 주세요.');form.ch_lcode.focus();	return false;}
        if (form.spo_hour.value == ''){alert('경기일시를 선택해 주세요.');form.spo_hour.focus();	return false;}
        if (form.spo_minute.value == ''){alert('경기일시를 선택해 주세요.');form.spo_minute.focus();	return false;}
        if (form.gm_htcode.value == ''){alert('홈팀(선수)를 선택해 주세요.');form.gm_htcode.focus();	return false;}
        if (form.gm_atcode.value == ''){alert('원정팀(선수)를 선택해 주세요.');form.gm_atcode.focus();	return false;}

        if (form.gm_hndflag.value == ''){alert('핸디캡/언더오버를 선택해 주세요.');form.gm_hndflag.focus();	return false;}

        if (form.gm_hdvde.value == ''){alert('홈팀 배당율을 입력해 주세요.');form.gm_hdvde.focus();	return false;}
        if (form.gm_tdvde.value == ''){alert('핸디캡/언더오버 값을 입력해 주세요.');form.gm_tdvde.focus();	return false;}
        if (form.gm_advde.value == ''){alert('원정팀 배당율을 입력해 주세요.');form.gm_advde.focus();	return false;}
        if (form.gm_htcode.value == form.gm_atcode.value){alert('홈팀(선수)와 원정팀(선수)가 같을수 없습니다.');form.gm_htcode.focus();	return false;}

        form.dflag.value=val1;
        form.action="../writeProc.asp";
        form.submit();
        return false;
    }

	function load_league(){
		var ch_jcode = form.ch_jcode.value;
        var ch_lcode = "<%=gm_lcode%>";

        $.ajax({
				type:"POST"
				, async:true
				, url:"../ajax/ajax_load_league.asp"
				, dataType:"html"
				, timeout:30000
				, cache:false
				, data:"ch_jcode="+ch_jcode+"&ch_lcode="+ch_lcode+"&jsflag=1&flag=<%=flag%>&gm_bonuscnt=<%=gm_bonuscnt%>"
				, contentType:"application/x-www-form-urlencoded; charset=UTF-8"
				, error: function(request, status, error){
						alert("code : " + request.status + "\r\nmessage : " + request.responseText);
						//$("body").append(request.responseText);
					}
				, success: function(response, status, request){
						result_html = response;
                        document.getElementById("lgeSelectBox").innerHTML=result_html;

					}
				, beforeSend: function(){

					}
				, complete: function(){

					}
			});
	}

	function teamSelectBoxAjax(val1){
		var ch_jcode = form.ch_jcode.value;
		var ch_lcode = val1;
        var gm_idx   = "<%=gm_idx%>";

        $.ajax({
				type:"POST"
				, async:true
				, url:"../ajax/ajax_load_team_handicap.asp"
				, dataType:"html"
				, timeout:30000
				, cache:false
				, data:"ch_jcode="+ch_jcode+"&ch_lcode="+ch_lcode+"&gm_idx="+gm_idx+"&flag=<%=flag%>"
				, contentType:"application/x-www-form-urlencoded; charset=UTF-8"
				, error: function(request, status, error){
						alert("code : " + request.status + "\r\nmessage : " + request.responseText);
						//$("body").append(request.responseText);
					}
				, success: function(response, status, request){
						result_html = response;
                        document.getElementById("teamShow").innerHTML=result_html;

					}
				, beforeSend: function(){

					}
				, complete: function(){

					}
			});
	}

    function onlyNumKey2() {
      var key = event.keyCode;
      if(!(key==8||key==9||key==144||key==46||(key>=48&&key<=57)||(key<96&&key>105))) {
       event.returnValue = false;
      }
     }
//-->
</script>

<form name="form" method="post" action="?">
<input type="hidden" name="stxt1" value="<%=stxt1%>">
<input type="hidden" name="stxt2" value="<%=stxt2%>">
<input type="hidden" name="stxt3" value="<%=stxt3%>">
<input type="hidden" name="stxt4" value="<%=stxt4%>">
<input type="hidden" name="stxt5" value="<%=stxt5%>">
<input type="hidden" name="stxt6" value="<%=stxt6%>">
<input type="hidden" name="stxt7" value="<%=stxt7%>">
<input type="hidden" name="stxt8" value="<%=stxt8%>">
<input type="hidden" name="stxt9" value="<%=stxt9%>">
<input type="hidden" name="stxt10" value="<%=stxt10%>">
<input type="hidden" name="gotopage" value="<%=gotopage%>">
<input type="hidden" name="gm_idx"  value="<%=gm_idx%>">
<input type="hidden" name="flag"     value="<%=flag%>">
<input type="hidden" name="dflag">

				<BR>
				<table border="0" cellpadding="2" cellspacing="1" width=100% bgcolor="#CBE1EE">
					<tr height=30 bgcolor="#F2F2F2">
						<td align=center>종목선택</td>
						<td bgcolor=white><% call jmkSelectBox("ch_jcode",gm_jcode," onchange='load_league()'")%></td>
					</tr>
					<tr height=30 bgcolor="#F2F2F2">
						<td align=center>리그선택</td>
						<td bgcolor=white><div id="lgeSelectBox"></div></td>
					</tr>
					<tr height=30 bgcolor="#F2F2F2">
						<td align=center>경기일시</td>
						<td bgcolor=white>
                            <input type="text" name="spo_date" id="spo_date" value="<%=spo_date%>" size="10" readonly />
							&nbsp;<img src="/admin/images/calendar.gif" align="absmiddle" OnClick="calendar(event, 'spo_date')" style="cursor:pointer">
                            &nbsp;
                            <% call selMake("1","spo_hour",spo_hour)%>시
                            &nbsp;
                            <% call selMake("7","spo_minute",spo_minute)%>분
                            <%If gm_idx<>"" and (gm_state="50" or gm_state="20") then%>
                                &nbsp;
                                <input type=checkbox name="btt_ex" value="Y"><font color=blue>베팅연장
                            <%end if%>
                        </td>
					</tr>
				</table>

				<BR>

                <div id=teamShow></div>

                <BR>

				<table border="0" cellpadding="0" cellspacing="0" width=100% bgcolor=white>
					<tr height=45>
						<td align=center>
							<%If gm_idx="" then%>
								<input type=button value="저장" onclick="return formChb('')">
							<%else%>
								<input type=button value="수정" onclick="return formChb('3')">
								<!--input type=button value="삭제" onclick="return formChb('1')"-->
								<%if rq("delok")="1" then%><input type=button value="삭제" onclick="return formChb('1')"><%end if%>
								<input type=button value="새로등록" onclick="return formChb('2')">
							<%End if%>
							<input type=button value="돌아가기" onclick="history.back()">
						</td>
					</tr>
				</table>
</form>
<BR>

<script language='JavaScript'>
<!--

    function excelup(){
        if (frm.excelfile.value == ''){alert('엑셀파일을 선택해 주세요.');frm.excelfile.focus();	return false;}
        frm.submit();
        return false;
    }

//-->
</script>

<form name="frm" method="post" action="../excelUploadProc.asp" ENCTYPE="MULTIPART/FORM-DATA">
<input type="hidden" name="gm_flag" value="<%=flag%>">

				<table border="0" cellpadding="2" cellspacing="1" width=100% bgcolor="#CBE1EE">
					<tr height=30 bgcolor="#F2F2F2" align=center>
						<td width=15%>엑셀파일 등록</td>
						<td bgcolor=white align=left><input type=file name="excelfile" style="width:80%;height:24px"> <input type=button value="등록" onclick="return excelup()"></td>
						<td bgcolor=white width=15%><a href="/fileupdown/sample/핸디캡.xls">샘플파일</a></td>
					</tr>
				</table>
</form>

		 		</td>
			</tr>
		</table>
 		</td>
	</tr>
</table>

<!--#include virtual="/admin/include/bottom.asp" -->

<script type="text/javascript">
	window.onload = function() {
        load_league();
        teamSelectBoxAjax('<%=gm_lcode%>');
	}
</script>