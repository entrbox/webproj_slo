<!--#include virtual="/admin/include/top.asp" -->

<table style="width:100%;min-width:1200px!important;height:100%;min-height:600px;border:0px;padding:0px;margin:0px;">
	<tr>
		<td style="width:160px!important;min-width:160px!important;background-color:#F7F7F7" valign="top">
			<!--#include virtual="/admin/include/leftMember.asp" -->
		</td>
		<td valign="top" style="min-width:1040px!important;">

    	<table width="100%" cellpadding=10 cellspacing=0 border=0>
			<tr>
				<td valign=top>
					<div class="w3-container w3-card-2 w3-green w3-text-16 w3-padding-large w3-bolder" style="margin-bottom:10px;">
					  회원별 레벨설정
					</div>


<%
	'//
	set rs = server.CreateObject("ADODB.Recordset")
	SQL = "SELECT * FROM TB_MEMBER_LEV_REG ORDER BY LEV_IDX ASC "
	rs.Open sql, db, 1
	'//
%>

<script language='JavaScript'>
<!--
function formChb(){
	form.submit();
	return false;
}

function onlyNumber(){
   if((event.keyCode<48)||(event.keyCode>57))
      event.returnValue=false;
}
//-->
</script>

<form name="form" method="post" action="memlevelregProc.asp">


				<table class="w3-table-all w3-bordered w3-centered w3-card-2" style="width:1000px;margin:24 auto;">
					<tr class="w3-green w3-opacity-min">
						<th class="w3-dark-grey w3-small w3-padding-large" style="white-space:nowrap">회원레벨</th>
						<th class="w3-dark-grey w3-small w3-padding-large" style="white-space:nowrap">최소베팅액</th>
						<th class="w3-dark-grey w3-small w3-padding-large" style="white-space:nowrap">최대베팅액</th>
						<th class="w3-dark-grey w3-small w3-padding-large" style="white-space:nowrap">상한가</th>
						<th class="w3-dark-grey w3-small w3-padding-large" style="white-space:nowrap">회원낙첨금(%)</th>
						<th class="w3-dark-grey w3-small w3-padding-large" style="white-space:nowrap">추천인낙첨금(%)</th>
						<th class="w3-dark-grey w3-small w3-padding-large" style="white-space:nowrap">하루충전시(%)</th>
					</tr>

<%
i=1
do until rs.EOF or i>rs.pagesize
    '//
    lev_idx	       = rs("lev_idx")
    btt_min_money  = rs("btt_min_money")
    btt_max_money  = rs("btt_max_money")
    max_money	   = rs("max_money")
    mem_lose_per   = rs("mem_lose_per")
    cmem_lose_per  = rs("cmem_lose_per")
    day_charge_per = rs("day_charge_per")
    '//
%>

<input type=hidden name=lev_idx value="<%=lev_idx%>">

					<tr>
						<td class="w3-text-blue" style="white-space:nowrap;font-weight: bold;"><%=lev_idx%>레벨</td>
						<td style="white-space:nowrap"><input type=text name=btt_min_money value="<%=btt_min_money%>"   onkeypress="onlyNumber()" size=12 style="text-align:center"> 원</td>
						<td style="white-space:nowrap"><input type=text name=btt_max_money value="<%=btt_max_money%>"   onkeypress="onlyNumber()" size=12 style="text-align:center"> 원</td>
						<td style="white-space:nowrap"><input type=text name=max_money value="<%=max_money%>"           onkeypress="onlyNumber()" size=12 style="text-align:center"> 원</td>
						<td style="white-space:nowrap"><input type=text name=mem_lose_per value="<%=mem_lose_per%>"     onkeypress="onlyNumber()" size=12 style="text-align:center"> %</td>
						<td style="white-space:nowrap"><input type=text name=cmem_lose_per value="<%=cmem_lose_per%>"   onkeypress="onlyNumber()" size=12 style="text-align:center"> %</td>
						<td style="white-space:nowrap"><input type=text name=day_charge_per value="<%=day_charge_per%>" onkeypress="onlyNumber()" size=12 style="text-align:center"> %</td>
					</tr>

<%
rs.movenext
j=j-1
i=i+1
loop
%>

				</table>

				<div class="w3-center w3-padding-xlarge">
					<button type=button class="w3-btn w3-blue w3-padding w3-small w3-bold" style="width:125px"  onclick="return formChb()">저장하기</button>
					<button type=button class="w3-btn w3-dark-grey w3-padding w3-small w3-bold" style="width:125px"  onclick="history.back()">돌아가기</button>
				</div>

				<!--table border="0" cellpadding="0" cellspacing="0" width=100% bgcolor=white>
					<tr height=45>
						<td align=center>
    						<input type=button value="저장" onclick="return formChb()">
						</td>
					</tr>
				</table-->
</form>

		 		</td>
			</tr>
		</table>
 		</td>
	</tr>
</table>

<!--#include virtual="/admin/include/bottom.asp" -->