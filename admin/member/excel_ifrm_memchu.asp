<%
	response.buffer=true
	response.contenttype="application/vnd.ms-excel" 
	Response.AddHeader "Content-Disposition","attachment;filename=excell5.xls"
%>
<!--#include virtual="/db/db.asp" -->
<table border="0" cellpadding="0" cellspacing="0" width=100% bgcolor=white>
	<tr height=30>
		<td align=left>* 최근 한달간 추천내역</td>
	</tr>
</table>

<table width="100%" border="0" cellspacing="1" cellpadding="1" bgcolor=#CCCCCC>
	<tr height=30 align=center bgcolor=#F7F7F7>
		<td>No.</td>
		<td>회차</td>
		<td>게임</td>
		<td>게임아이디</td>
		<td>추천아이디</td>
		<td>구매건수</td>
		<td>구매금액</td>
		<td>비율</td>
		<td>적립포인트</td>
	</tr>

<%
	cflag = "1"
	stxt1 = rq("stxt1")
	stxt2 = rq("stxt2")
	stxt3 = rq("stxt3")
	stxt4 = rq("stxt4")
	stxt5 = rq("stxt5")
	stxt6 = rq("stxt6")
	stxt7 = rq("stxt7")
	stxt8 = rq("stxt8")
	stxt9 = rq("stxt9")
	stxt10 = rq("stxt10")
	GotoPage = rq("GotoPage")
	'//
	cflag   = stxt2
	nowdate = now()
	bfmdate = dateadd("m",-1,nowdate)
	bfmdate = Replace(Left(bfmdate,10),"-","")
	if GotoPage = "" Then GotoPage = 1
	'//
	set rs = server.CreateObject("ADODB.Recordset")
	sql = " Select "
	sql = sql & " b.gmcode,d.gname,a.suserid,a.guserid, "
	sql = sql & " c.chCnt,(c.chCnt*1000) as ormoney,a.lper,a.lmoney "
	sql = sql & " From tb_order_game_fail a "
	sql = sql & " left outer join tb_order_info b on a.orseq  = b.orseq  "
	sql = sql & " left outer join tb_order_game c on a.gseq   = c.gseq  "
	sql = sql & " left outer join tb_game_code  d on c.gmkind = d.gcode "
	sql = sql & " where guserid = '"& stxt1 &"' "
	sql = sql & " and '20'+substring(convert(varchar,b.gmcode),1,6) >= '"& bfmdate &"' "
	sql = sql & " order by a.orseq desc, a.gseq asc  "
	rs.Open sql, db, 1
	schCnt = rs.recordcount
	'//
	j=schCnt-((gotopage-1)*50)
	i=1
	do until rs.EOF
		If (i Mod 2) = 0 Then
			bgcolor="#FFFFFF"
		Else
			bgcolor="#EEEEEE"		
		End If
		'//
		gmcode = rs(0)
		gname = rs(1)
		suserid = rs(2)
		guserid = rs(3)
		chCnt = rs(4)
		ormoney = rs(5)
		lper = rs(6)
		lmoney = rs(7)
%>

	<tr height=25 align=center bgcolor=<%=bgcolor%>>
		<td><%=j%></td>
		<td><%=gmcode%></td>
		<td><%=gname%></td>
		<td><%=suserid%></td>
		<td><%=guserid%></td>
		<td><%=chCnt%></td>
		<td align=right><%=ormoney%>&nbsp;</td>
		<td><%=lper%>%</td>
		<td align=right><%=lmoney%>&nbsp;</td>
	</tr>

<%
rs.movenext
j=j-1
i=i+1
loop
%>

