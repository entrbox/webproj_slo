<%
	response.buffer=true
	response.contenttype="application/vnd.ms-excel"
	Response.AddHeader "Content-Disposition","attachment;filename=excell.xls"
%>
<!--#include virtual="/db/db.asp" -->
<table border="0" cellpadding="0" cellspacing="0" width=100% bgcolor=white>
	<tr height=30>
		<td align=left>* 최근 한달간 구매내역</td>
	</tr>
</table>

<%
	flag  = rq("flag")    '승무패
	stxt1 = rq("stxt1")
	stxt2 = rq("stxt2")
	stxt3 = rq("stxt3")
	stxt4 = rq("stxt4")
	stxt5 = rq("stxt5")
	stxt6 = rq("stxt6")
	stxt7 = rq("stxt7")
	stxt8 = rq("stxt8")
	stxt9 = rq("stxt9")
	stxt10 = rq("stxt10")
	GotoPage = rq("GotoPage")
	If gotopage="" Then gotopage=1
    '//
    set rs = server.CreateObject("ADODB.Recordset")
    SQL = "       SELECT A.BTT_IDX, "
    SQL = SQL & "        B.BTS_IDX, "
    SQL = SQL & "  	     A.WDATE, "
    SQL = SQL & "        C.MB_ID, "
    SQL = SQL & "        C.MB_NICK, "
    SQL = SQL & "        D.GM_DATETIME, "
    SQL = SQL & "        E.SPO_NAME HTEAMNM, "
    SQL = SQL & "        F.SPO_NAME ATEAMNM, "
    SQL = SQL & "        CONVERT(VARCHAR, D.RST_HTM_SCORE) + ':' + CONVERT(VARCHAR, D.RST_ATM_SCORE) GMSCORE, "
    SQL = SQL & "        B.BTS_STATE, "
    SQL = SQL & "        A.BTT_TOT_DIVIDEND, "
    SQL = SQL & "        A.BTT_MONEY, "
    SQL = SQL & "        A.BTT_DIVIDEND_MONEY, "
    SQL = SQL & "        A.BTT_GEND_MONEY, "
    SQL = SQL & "        A.BTT_ACCFLAG, "
    SQL = SQL & "        A.BTT_CNT,B.BTS_CHFLAG, D.GM_HNDFLAG, D.GM_FLAG, G.SPO_TIE, "
    SQL = SQL & "        D.GM_HDVDE,D.GM_TDVDE,D.GM_ADVDE "
    SQL = SQL & "   FROM TB_SPO_BETTING_MASTER A  "
    SQL = SQL & "  INNER JOIN TB_SPO_BETTING_INFO B ON A.BTT_IDX=B.BTT_IDX "
    SQL = SQL & "  INNER JOIN TB_MEMBER   C ON A.USERID    = C.MB_ID "
    SQL = SQL & "  INNER JOIN tb_spo_game D ON B.GM_IDX    = D.GM_IDX "
    SQL = SQL & "  INNER JOIN tb_spo_kind E ON D.GM_HTCODE = E.SPO_CODE "
    SQL = SQL & "  INNER JOIN tb_spo_kind F ON D.GM_ATCODE = F.SPO_CODE "
	SQL = SQL & "  INNER JOIN TB_SPO_KIND G ON D.GM_JCODE  = G.SPO_CODE AND G.SPO_FLAG='J' "
    SQL = SQL & "  WHERE A.BTT_IDX=B.BTT_IDX "
    SQL = SQL & "    AND A.USERID     = '"& stxt1 &"' "
    '//
    SQL = SQL & "    AND SUBSTRING(A.WDATE,1,8) >= '"& REPLACE(LEFT(NOW()-30,10),"-","") &"' "
    '//
    SQL = SQL & "  ORDER BY A.WDATE DESC "
	rs.PageSize=50
	rs.Open sql, db, 1
	if not rs.EOF Then rs.AbsolutePage=int(gotopage)
	schCnt = rs.recordcount
    '//
%>
<!--#include virtual="/admin/sports/inc/js.asp" -->

<form name="form" id="frm" method="post" action="?">
<input type="hidden" name="flag" value="<%=flag%>">
<input type="hidden" name="accFlag">
<input type="hidden" name="pagenm" value="<%=pagenm%>">


				<table border="0" cellpadding="1" cellspacing="1" width=100% bgcolor="#CBE1EE">
					<tr height=35 bgcolor="#F2F2F2" align=center>
						<td width=3%>No.</td>
						<td width=5%>베팅일시</td>

                        <td width=7% bgcolor="#C6C6C6">경기일시</td>
                        <td width=26% bgcolor="#C6C6C6">홈팀</td>
                        <td width=4% bgcolor="#C6C6C6">기준</td>
                        <td width=26% bgcolor="#C6C6C6">원정팀</td>
                        <td width=5% bgcolor="#C6C6C6">스코어</td>
                        <td width=4% bgcolor="#C6C6C6">결과</td>

						<td width=4%>배당율</td>
						<td width=6%>베팅금액</td>
						<td width=6%>예상당첨금</td>
						<td width=5%>당첨금</td>
					</tr>

<%
    '//
    i=1
    p=1
    old_btt_idx=0
    listidx = 1
    totlist = rs.recordcount
    ing_icnt    = 0
    ing_ccnt    = 0
    ing_wcnt    = 0
    ing_lcnt    = 0
    ing_tcnt    = 0
    '//
    do until rs.eof or i>rs.pagesize
        '//
        btt_idx = rs("btt_idx")
        bts_idx = rs("bts_idx")
        wdate   = rs("wdate")
        mb_id   = rs("mb_id")
        mb_nick = rs("mb_nick")
        gm_datetime = rs("gm_datetime")
        hteamnm   = rs("hteamnm")
        ateamnm   = rs("ateamnm")
        gmscore   = rs("gmscore")
        bts_state = rs("bts_state")
        btt_tot_dividend   = rs("btt_tot_dividend")
        btt_money          = rs("btt_money")
        btt_dividend_money = rs("btt_dividend_money")
        btt_gend_money = rs("btt_gend_money")
        btt_accflag    = rs("btt_accflag")
        btt_cnt        = rs("btt_cnt")
        bts_chflag     = rs("bts_chflag")
        gm_hndflag     = rs("gm_hndflag")
        gm_flag        = rs("gm_flag")
        spo_tie        = rs("spo_tie")
        gm_hdvde = formatnumber(rs("gm_hdvde"),2)
        gm_tdvde = rs("gm_tdvde")
        gm_advde = formatnumber(rs("gm_advde"),2)
        '//
        wdate2      = mid(getDateRe("8",wdate,"-"),6)
        wdate       = mid(getDateRe("8",wdate,"-"),6)
        gm_datetime = mid(getDateRe("8",gm_datetime,"-"),6)
        gm_datetime = left(gm_datetime,len(gm_datetime)-1)
        if not isnull(btt_gend_money) then
            btt_gend_money = formatnumber(btt_gend_money,0)
        else
            btt_gend_money="0"
        end if
        '//
        winflag    = getWinText(bts_state)
        '//
        if bts_state="I" then
            ing_icnt = ing_icnt+1
        elseif bts_state="C" then
            ing_ccnt = ing_ccnt+1
        elseif bts_state="W" then
            ing_wcnt = ing_wcnt+1
        elseif bts_state="L" then
            ing_lcnt = ing_lcnt+1
        elseif bts_state="T" then
            ing_tcnt = ing_tcnt+1
        end if
        '//
        h_ftcolor = " bgcolor='#5E5E5E'"
        t_ftcolor = " bgcolor='#5E5E5E'"
        a_ftcolor = " bgcolor='#5E5E5E'"
        hicon     = ""
        aicon     = ""
        '//
        if bts_chflag="H" then h_ftcolor = " bgcolor='red'"
        if bts_chflag="T" then t_ftcolor = " bgcolor='red'"
        if bts_chflag="A" then a_ftcolor = " bgcolor='red'"
        if gm_hndflag="UO" then
            hicon = "[오버]<font color=#FFFF00>▲</font>"
            aicon = "<font color=#00D3F9>▼</font>[언더]"
        end if
        '//
        gm_tdvde_val=""
        IF GM_FLAG="11" THEN
            IF SPO_TIE<>"Y" THEN
                GM_TDVDE_VAL = "VS"
            ELSE
                GM_TDVDE_VAL = GM_TDVDE
            END IF
        ELSEIF GM_FLAG="10" THEN
            IF GM_HNDFLAG="VS" THEN
            ELSEIF GM_HNDFLAG="UO" THEN
                GM_TDVDE_VAL = GM_TDVDE
            ELSEIF GM_HNDFLAG="CH" OR GM_HNDFLAG="CA" THEN
                IF GM_TDVDE<>"0" THEN
                    IF GM_HNDFLAG="CA" THEN
                        GM_TDVDE_VAL = "+" & GM_TDVDE
                    ELSE
                        GM_TDVDE_VAL = "-" & GM_TDVDE
                    END IF
                ELSE
                    GM_TDVDE_VAL = GM_TDVDE
                END IF
            END IF
        ELSEIF GM_FLAG="12" THEN
            IF GM_HNDFLAG="VS" THEN
                IF GM_VSCHB="Y" THEN
                    GM_TDVDE_VAL = "VS"
                ELSE
                    GM_TDVDE_VAL = GM_TDVDE
                END IF
            ELSEIF GM_HNDFLAG="UO" THEN
                GM_TDVDE_VAL = GM_TDVDE
            ELSEIF GM_HNDFLAG="CH" OR GM_HNDFLAG="CA" THEN
                IF GM_TDVDE<>"0" THEN
                    IF GM_HNDFLAG="CA" THEN
                        GM_TDVDE_VAL = "+" & GM_TDVDE
                    ELSE
                        GM_TDVDE_VAL = "-" & GM_TDVDE
                    END IF
                ELSE
                    GM_TDVDE_VAL = GM_TDVDE
                END IF
            END IF
        ELSEIF GM_FLAG="13" THEN
            IF GM_HNDFLAG="VS" THEN
                IF GM_VSCHB="Y" THEN
                    GM_TDVDE_VAL = "VS"
                ELSE
                    GM_TDVDE_VAL = FORMATNUMBER(GM_TDVDE,2)
                END IF
            ELSEIF GM_HNDFLAG="UO" THEN
                GM_TDVDE_VAL = GM_TDVDE
            ELSEIF GM_HNDFLAG="CH" OR GM_HNDFLAG="CA" THEN
                IF GM_TDVDE<>"0" THEN
                    IF GM_HNDFLAG="CA" THEN
                        GM_TDVDE_VAL = "+" & GM_TDVDE
                    ELSE
                        GM_TDVDE_VAL = "-" & GM_TDVDE
                    END IF
                ELSE
                    GM_TDVDE_VAL = GM_TDVDE
                END IF
            END IF
        END IF
        '//
        if winflag="적중" then
            winflag = "<font color=red>" & winflag
        elseif winflag="취소" then
            winflag = "<font color=green>" & winflag
        elseif winflag="적특" then
            winflag = "<font color=blue>" & winflag
        end if
        '//
        rowspanval = ""
        if btt_cnt>1 then rowspanval = " rowspan='"& btt_cnt &"'"
        '//
%>

                <%if p=1 then%>
					<tr height=25 bgcolor=white align=center>
						<td <%=rowspanval%> align=center><%=listidx%></td>
                        <td <%=rowspanval%>><%=wdate%></td>

                        <td><%=gm_datetime%></td>
                        <td <%=h_ftcolor%> align=left>
                        <table border="0" cellpadding="0" cellspacing="0" width=100%>
                            <tr>
                                <td><font color=white><%=hteamnm%><%=hicon%></td>
                                <td width=22><font color=white><%=gm_hdvde%></td>
                            </tr>
                        </table>
                        </td>
                        <td <%=t_ftcolor%>><font color=white><%=gm_tdvde_val%></td>
                        <td <%=a_ftcolor%> align=right>
                        <table border=0 cellpadding="0" cellspacing="0" width=100%>
                            <tr>
                                <td width=22><font color=white><%=gm_advde%></td>
                                <td align=right><font color=white><%=aicon%><%=ateamnm%></td>
                            </tr>
                        </table>
                        </td>
                        <td><%=gmscore%></td>
                        <td><%=winflag%></td>

                        <td <%=rowspanval%>><%=formatnumber(btt_tot_dividend,2)%></td>
                        <td <%=rowspanval%> align=right><%=formatnumber(btt_money,0)%>&nbsp;</td>
                        <td <%=rowspanval%> align=right><%=formatnumber(btt_dividend_money,0)%>&nbsp;</td>
                        <td <%=rowspanval%> align=right><div id="jsmoney1_<%=btt_idx%>">0&nbsp;</div></td>
					</tr>
                <%else%>
					<tr height=25 bgcolor=white align=center>
                        <td><%=gm_datetime%></td>
                        <td <%=h_ftcolor%> align=left>
                        <table border="0" cellpadding="0" cellspacing="0" width=100%>
                            <tr>
                                <td><font color=white><%=hteamnm%><%=hicon%></td>
                                <td width=22><font color=white><%=gm_hdvde%></td>
                            </tr>
                        </table>
                        </td>
                        <td <%=t_ftcolor%>><font color=white><%=gm_tdvde_val%></td>
                        <td <%=a_ftcolor%> align=right>
                        <table border="0" cellpadding="0" cellspacing="0" width=100%>
                            <tr>
                                <td width=22><font color=white><%=gm_advde%></td>
                                <td align=right><font color=white><%=aicon%><%=ateamnm%></td>
                            </tr>
                        </table>
                        </td>
                        <td><%=gmscore%></td>
                        <td><%=winflag%></td>
					</tr>
                <%end if%>

<%
        if p=btt_cnt then
            '//
            all_tot_cnt = btt_cnt - ing_ccnt - ing_tcnt     '//베팅수 - 취소수 - 적특수
            allwinflag=""
            if all_tot_cnt=ing_wcnt then
                allwinflag = "당첨"
                '//
                if hiddenVal_W  = "" then
                    hiddenVal_W = btt_idx
                    hiddenVal_M = formatnumber(btt_dividend_money,0)
                else
                    hiddenVal_W = hiddenVal_W &","& btt_idx
                    hiddenVal_M = hiddenVal_M &"&"& formatnumber(btt_dividend_money,0)
                end if
                '//
            elseif ing_lcnt>0 then
                allwinflag = "미당첨"
                '//
                if hiddenVal_L  = "" then
                    hiddenVal_L = btt_idx
                else
                    hiddenVal_L = hiddenVal_L &","& btt_idx
                end if
                '//
            elseif btt_cnt=ing_ccnt then
                allwinflag = "경기취소"
                '//
                if hiddenVal_C  = "" then
                    hiddenVal_C = btt_idx
                else
                    hiddenVal_C = hiddenVal_C &","& btt_idx
                end if
                '//
            elseif btt_cnt=ing_tcnt then
                allwinflag = "적특"
                if hiddenVal_T  = "" then
                    hiddenVal_T = btt_idx
                else
                    hiddenVal_T = hiddenVal_T &","& btt_idx
                end if
            else
                allwinflag = "게임중"
            end if
            '//
            haptrhtm = getBttTrHtml(wdate2,btt_money,btt_tot_dividend,btt_dividend_money,btt_gend_money,allwinflag)
            response.write haptrhtm
            '//
            p=1
            listidx = listidx + 1
            ing_icnt    = 0
            ing_ccnt    = 0
            ing_wcnt    = 0
            ing_lcnt    = 0
            ing_tcnt    = 0
        else
            p=p+1
        end if
        '//
    i=i+1
    old_btt_idx = btt_idx
    rs.movenext
    loop
%>

					<tr height=10 bgcolor=white>
						<td colspan=20></td>
                    </tr>

<input type=hidden name=hiddenVal_W value="<%=hiddenVal_W%>">
<input type=hidden name=hiddenVal_L value="<%=hiddenVal_L%>">
<input type=hidden name=hiddenVal_C value="<%=hiddenVal_C%>">
<input type=hidden name=hiddenVal_T value="<%=hiddenVal_T%>">
</form>

				</table>


<script type="text/javascript" language="javascript">
<!--
$(document).ready(function(){
    var imsival  = "<%=hiddenVal_W%>";
    var imsival2 = "<%=hiddenVal_M%>";
	var imsiArr  = imsival.split(',');
	var imsiArr2 = imsival2.split('&');
	//
    for (i=0; i<imsiArr.length; i++){
        var realval  = imsiArr[i];
        var realval2 = imsiArr2[i];
//        $('#jsmoney1_'+realval).append('<font color=red>'+realval2+'</font>');
//        $('#jsmoney2_'+realval).append('<font color=red>'+realval2+'</font>');
        $('#jsmoney1_'+realval).replaceWith('<font color=red>'+realval2+'</font>');
        $('#jsmoney2_'+realval).replaceWith('<font color=red>'+realval2+'</font>');


    }
});
-->
</script>
