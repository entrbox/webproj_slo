<!--#include virtual="/admin/include/top.asp" -->

<table style="width:100%;min-width:1200px!important;height:100%;min-height:600px;border:0px;padding:0px;margin:0px;">
	<tr>
		<td style="width:160px!important;min-width:160px!important;background-color:#F7F7F7" valign="top">
			<!--#include virtual="/admin/include/leftCharge.asp" -->
		</td>
		<td valign="top" style="min-width:1040px!important;">

    	<table width=100% cellpadding=10 cellspacing=0 border=0>
			<tr>
				<td valign=top>
					<div class="w3-container w3-card-2 w3-deep-orange w3-text-16 w3-padding-large w3-bolder" style="margin-bottom:10px;">
					  충환전합계표
					</div>

				

<%
	stxt1 = rq("stxt1")
	stxt2 = rq("stxt2")
	stxt3 = rq("stxt3")
	stxt4 = rq("stxt4")
	stxt5 = rq("stxt5")
	stxt6 = rq("stxt6")
	stxt7 = rq("stxt7")
	stxt8 = rq("stxt8")
	stxt9 = rq("stxt9")
	stxt10 = rq("stxt10")
	GotoPage = rq("GotoPage")
	'//
	If stxt1="" then stxt1 = Left(now(),7) & "-01"
	If stxt2="" then stxt2 = Left(now(),7) & "-31"
	If stxt3="" then stxt3 = "3"
	if GotoPage = "" Then GotoPage = 1
	stxt11 = Replace(stxt1,"-","")
	stxt22 = Replace(stxt2,"-","")
	'//
	imsisort = "desc"	
	If stxt9="" then stxt9 = "a.mb_id"
	If stxt10="" then stxt10 = "asc"
	If stxt10="desc" then imsisort = "asc"
	'//
	set rs = server.CreateObject("ADODB.Recordset")
	SQL = "select isnull(count(mb_id),0), isnull(sum(mb_money),0) from tb_member where mb_outFlag = 'y' "
	rs.Open sql, db, 1
	totCnt = rs(0)
	totMny = rs(1)
	rs.close
	'//
	mSeachSql = getAmemSch(stxt3)
	'//
	set rs = server.CreateObject("ADODB.Recordset")
	SQL = " select a.mb_id,a.mb_nick "
	SQL = SQL & " ,isnull(b.c_cmoney,0) as c_cmoney "
	SQL = SQL & " ,isnull(c.winMoney,0) as winMoney "
	SQL = SQL & " ,isnull(b.h_cmoney,0) as h_cmoney "
	SQL = SQL & " ,isnull(b.c_mmoney,0) as c_mmoney "
	SQL = SQL & " ,isnull(b.h_mmoney,0) as h_mmoney "
	SQL = SQL & " ,isnull(b.c_pmoney,0) as c_pmoney "
	SQL = SQL & " ,(isnull(b.c_cmoney,0)-isnull(b.h_cmoney,0)) as ch_money "
	SQL = SQL & " ,a.mb_money "
	SQL = SQL & " from tb_member  a "
	SQL = SQL & " left outer join ( "
	SQL = SQL & " 				select a.userid "
	SQL = SQL & " 				,sum(isnull(a.c_cmoney,0)) as c_cmoney "
	SQL = SQL & " 				,sum(isnull(a.c_mmoney,0)) as c_mmoney "
	SQL = SQL & " 				,sum(isnull(a.c_pmoney,0)) as c_pmoney "
	SQL = SQL & " 				,sum(isnull(a.h_cmoney,0)) as h_cmoney "
	SQL = SQL & " 				,sum(isnull(a.h_mmoney,0)) as h_mmoney "
	SQL = SQL & " 				from "
	SQL = SQL & " 					( "
	SQL = SQL & " 					select userid,cflag,ingflag "
	SQL = SQL & " 					,(case when cflag='1' and ingflag='5' then SUM(cmoney) end) as c_cmoney "	'--충전금액
	SQL = SQL & " 					,(case when cflag='1' and ingflag='7' then SUM(cmoney) end) as c_mmoney "	'--머니추가
	SQL = SQL & " 					,(case when cflag='1' and ingflag='8' then SUM(cmoney) end) as c_pmoney "	'--포인트전환
	SQL = SQL & " 					,(case when cflag='2' and ingflag='5' then SUM(cmoney) end) as h_cmoney "	'--환전금액
	SQL = SQL & " 					,(case when cflag='2' and ingflag='7' then SUM(cmoney) end) as h_mmoney "	'--머니삭감
	SQL = SQL & " 					from tb_charge "
	SQL = SQL & " 					where (ingflag='5' or ingflag='7' or ingflag='8') "
	SQL = SQL & " 					group by userid,cflag,ingflag "
	SQL = SQL & " 					) a "
	SQL = SQL & " 				group by a.userid "
	SQL = SQL & " 			    ) b on a.mb_id = b.userid "
	SQL = SQL & " left outer join ( "
	SQL = SQL & " 				select a.userid, sum(b.winMoney) as winMoney "
	SQL = SQL & " 				from tb_order_info a, tb_order_Game b "
	SQL = SQL & " 				where a.orSeq = b.orseq "
	SQL = SQL & " 				group by a.userid "
	SQL = SQL & " 				) c on a.mb_id = c.userid "
	'//
	If stxt4<>"" And stxt5<>"" Then SQL = SQL & " and "& stxt4 &" like '%"& stxt5 &"%' "
	'//
	SQL = SQL & " order by " & stxt9 & " " & stxt10
	rs.PageSize=20
	rs.Open sql, db, 1
	if not rs.EOF Then rs.AbsolutePage=int(gotopage)
	schCnt = rs.recordcount
	'//
%>

<script language='JavaScript'>
<!--
function searChb(){
	if ((form.stxt4.value != '')&&(form.stxt5.value == '')){alert('검색구분을 선택해 주세요.');form.stxt5.focus();	return false;}
	if ((form.stxt4.value == '')&&(form.stxt5.value != '')){alert('검색어를 입력해 주세요.');form.stxt4.focus();	return false;}
	form.submit();
}
//-->
</script>


<form name="form" method="post" action="?" onsubmit="return searChb()">
					<div class="w3-container w3-leftbar w3-light-grey w3-padding w3-border" style="margin-bottom:10px;">		
						<div class="w3-right">
							<div class="w3-left" style="margin-left: 3px;">
								<select class="w3-select w3-border w3-small w3-padding" name="stxt4" style="min-width:125px;max-width:250px">
									<option value="">검색구분</option>
								<option value="a.mb_id"   <%If stxt4="a.mb_id"   Then Response.write " selected"%>>회원ID</option>
								<option value="a.mb_nick" <%If stxt4="a.mb_nick" Then Response.write " selected"%>>닉네임</option>
								</select>						
							</div>
							
							<div class="w3-left" style="margin-left: 3px;"><input class="w3-input w3-padding w3-border w3-left" type="text" placeholder="검색어" name="stxt5" value="<%=stxt5%>" style="width:378px"></div>
							<div class="w3-left" style="margin-left: 3px;"><button class="w3-btn w3-padding w3-black w3-small w3-left w3-bold" type="submit" style="width:125px">검색하기</button></div>
						</div>						
					</div>	
</form>

				
			  <div class="w3-panel w3-row w3-padding-small w3-sand w3-leftbar w3-border-green">
			    <div class="w3-col s6 w3-padding-small">
				    <!--span class="w3-text-dark-grey w3-text-14">전체:</span> <span class="w3-text-green w3-text-14 w3-bold"><%=FormatNumber(totCnt,0)%></span>
				    <span class="w3-text-grey w3-text-14 w3-padding-small">/</span-->
				    <span class="w3-text-dark-grey w3-text-14">검색결과:</span> <span class="w3-text-blue w3-text-14 w3-bold"><%=FormatNumber(schCnt,0)%></span> 
				    <!--span class="w3-text-grey w3-text-14 w3-padding-small">/</span>
				    <span class="w3-text-dark-grey w3-text-14">보유머니합계:</span> <span class="w3-text-deep-orange w3-text-14 w3-bold"><%=FormatNumber(totMny,0)%></span--> 
				</div>
			    <div class="w3-col s6">
				    <div class="w3-right">
					    <button type=button class="w3-btn w3-green w3-bold" onclick="listexcel();">EXCEL 파일 다운로드</button>
				    </div>			
			    </div>	    
			  </div>	

<!--table border="0" cellpadding="3" cellspacing="1" style="width:100%; border:1px solid #CBE1EE;">
<form name="form" method="post" action="?">

					<tr>
						<td bgcolor=#F2F2F2 width=15% align=center>조건검색</td>
						<td width=85%>
							<select name=stxt4>
								<option value="">--전체--
								<option value="a.mb_id"   <%If stxt4="a.mb_id"   Then Response.write " selected"%>>회원ID
								<option value="a.mb_nick" <%If stxt4="a.mb_nick" Then Response.write " selected"%>>닉네임
							</select>
							<input type=text name=stxt5 value="<%=stxt5%>" size=20>
							<input type="image" src="/admin/images/btn_search1.gif" align=bottom onclick="return searChb()">
							<input name="btnExcel" type="button" value="EXCEL 파일 다운로드" onclick="listexcel();" />
						</td>
					</tr>

</form>

				</table>

				<BR>
				<table width=100% cellpadding=3 cellspacing=0 border=0>
					<tr>
						<td>
							<img src='/admin/images/icon_arrow3.gif' align=absmiddle>&nbsp;검색결과 <font color=blue><%=FormatNumber(schCnt,0)%></font> 명
							&nbsp;/ 보유머니합계 <font color='#FF6600'>278,790,814</font> 원

						</td>
				</table-->

				<table class="w3-table-all w3-centered w3-card-2" style="min-width: 1000px !important;margin-bottom:10px;">
					<tr class="w3-deep-orange w3-opacity-min">
						<th class="w3-dark-grey w3-small w3-padding-large" width=5%>No.</td>
						<th class="w3-dark-grey w3-small w3-padding-large" width=7%><a href="?stxt9=a.mb_id&stxt10=<%=imsisort%>" class="w3-text-white">ID</a></th>
						<th class="w3-dark-grey w3-small w3-padding-large" width=7%><a href="?stxt9=a.mb_nick&stxt10=<%=imsisort%>" class="w3-text-white">닉네임</a></th>
						<th class="w3-dark-grey w3-small w3-padding-large" width=10%><a href="?stxt9=b.c_cmoney&stxt10=<%=imsisort%>" class="w3-text-white">충전금액</a></th>
						<th class="w3-dark-grey w3-small w3-padding-large" width=10%><a href="?stxt9=c.winMoney&stxt10=<%=imsisort%>" class="w3-text-white">당첨금액</a></th>
						<th class="w3-dark-grey w3-small w3-padding-large" width=10%><a href="?stxt9=b.h_cmoney&stxt10=<%=imsisort%>" class="w3-text-white">환전금액</a></th>
						<th class="w3-dark-grey w3-small w3-padding-large" width=10%><a href="?stxt9=b.c_mmoney&stxt10=<%=imsisort%>" class="w3-text-white">머니추가</a></th>
						<th class="w3-dark-grey w3-small w3-padding-large" width=10%><a href="?stxt9=b.h_mmoney&stxt10=<%=imsisort%>" class="w3-text-white">머니삭감</a></th>
						<th class="w3-dark-grey w3-small w3-padding-large" width=10%><a href="?stxt9=b.c_pmoney&stxt10=<%=imsisort%>" class="w3-text-white">포인트</a></th>
						<th class="w3-dark-grey w3-small w3-padding-large" width=10%><a href="?stxt9=ch_money&stxt10=<%=imsisort%>" class="w3-text-white">충전-환전</a></th>
						<th class="w3-dark-grey w3-small w3-padding-large" width=11%><a href="?stxt9=a.userid&stxt10=<%=imsisort%>" class="w3-text-white">보유머니</a></th>
					</tr>

<%
imsifontB = "<font color=blue>"
imsifontR = "<font color=red>"
imsifont  = "</font>"
j=schCnt-((gotopage-1)*20)
i=1
do until rs.EOF or i>rs.pagesize
	mb_id    = rs(0)
	mb_nick  = rs(1)
	c_cmoney = FormatNumber(rs(2),0)
	winMoney = FormatNumber(rs(3),0)
	h_cmoney = FormatNumber(rs(4),0)
	c_mmoney = FormatNumber(rs(5),0)
	h_mmoney = FormatNumber(rs(6),0)
	c_pmoney = FormatNumber(rs(7),0)
	ch_money = FormatNumber(rs(8),0)
	mb_money = FormatNumber(rs(9),0)
	'//
%>

					<tr height=25 bgcolor=white align=center>
						<td><%=j%></td>
						<td><%=mb_id%></td>
						<td><%=mb_nick%></td>
						<td style="text-align:right"><%=imsifontB%><%=c_cmoney%><%=imsifont%>   원&nbsp;</td>
						<td style="text-align:right"><%=imsifontR%><%=winMoney%><%=imsifont%>   원&nbsp;</td>
						<td style="text-align:right"><%=imsifontB%><%=h_cmoney%><%=imsifont%>  원&nbsp;</td>
						<td style="text-align:right"><%=imsifontR%><%=c_mmoney%><%=imsifont%>     원&nbsp;</td>
						<td style="text-align:right"><%=imsifontB%><%=h_mmoney%><%=imsifont%>   원&nbsp;</td>
						<td style="text-align:right"><%=imsifontR%><%=c_pmoney%><%=imsifont%> 원&nbsp;</td>
						<td style="text-align:right"><%=ch_money%><%=imsifont%> 원&nbsp;</td>
						<td style="text-align:right"><%=imsifontB%><%=mb_money%><%=imsifont%> 원&nbsp;</td>
					</tr>

<%
rs.movenext
j=j-1
i=i+1
loop
%>

				</table>

				<table border="0" cellpadding="0" cellspacing="0" width=100% bgcolor=white>
					<tr height=45>
						<td align=center>
							<!--#include virtual="/admin/include/paging.asp" -->
						</td>
					</tr>
				</table>

		 		</td>
			</tr>
		</table>
 		</td>
	</tr>
</table>

<!--#include virtual="/admin/include/bottom.asp" -->