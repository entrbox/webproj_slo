<!--#include virtual="/admin/include/top.asp" -->

<table width="100%" border=0 cellspacing="0" cellpadding="0" height="100%">
	<tr>
		<td width="160" bgcolor='#F7F7F7' valign="top">
			<!--#include virtual="/admin/include/leftCharge.asp" -->
		</td>
		<td valign="top" width=''>

    	<table width=1000 cellpadding=10 cellspacing=0 border=0>
			<tr>
				<td valign=top>

				<table width='100%' cellpadding=0 cellspacing=0 border=0>
					<tr>
						<td height=30 colspan=2 class=dotum_15><img src='/admin/images/icon_arrow0.gif' align=absmiddle>&nbsp;일일정산통계</td>
					</tr>
					<tr height=2>
						<td width=200 bgcolor='#AF0B0A'></td>
						<td bgcolor='#F2F8FB'></td>
					</tr>
				</table>

				<table border="0" cellpadding="3" cellspacing="1" style="width:100%; border:1px solid #CBE1EE;">

<%
	cflag = "1"
	stxt1 = rq("stxt1")
	stxt2 = rq("stxt2")
	stxt3 = rq("stxt3")
	stxt4 = rq("stxt4")
	stxt5 = rq("stxt5")
	stxt6 = rq("stxt6")
	stxt7 = rq("stxt7")
	stxt8 = rq("stxt8")
	stxt9 = rq("stxt9")
	stxt10 = rq("stxt10")
	GotoPage = rq("GotoPage")
	'//
	If stxt1="" then stxt1 = Left(dateadd("m",-1,now()),10)
	If stxt2="" then stxt2 = Left(now(),10)
	if GotoPage = "" Then GotoPage = 1
	stxt11 = Replace(stxt1,"-","")
	stxt22 = Replace(stxt2,"-","")
	'//
	set rs = server.CreateObject("ADODB.Recordset")
	SQL = "select a.Ymd, "
	SQL = SQL & " isnull(b.cSummoney,0) as cSummoney, "
	SQL = SQL & " isnull(c.hSummoney,0) as hSummoney, "
	SQL = SQL & " (isnull(b.cSummoney,0)-isnull(c.hSummoney,0)) as Summoney, "
	SQL = SQL & " isnull(d.pSummoney,0) as pSummoney "
	SQL = SQL & " from "
	SQL = SQL & " (select    substring(swdate,1,8) as Ymd from tb_charge where ingflag='5' group by substring(swdate,1,8)) a "
	SQL = SQL & " left outer join  "
	SQL = SQL & " 		   ( "
	SQL = SQL & " 		   select substring(swdate,1,8) as cYmd, SUM(cmoney) as cSummoney "
	SQL = SQL & " 		   from tb_charge  "
	SQL = SQL & "            where ingflag='5' "	'--충전완료 "
	SQL = SQL & "            and cflag = '1' "
	SQL = SQL & " 		   group by substring(swdate,1,8) "
	SQL = SQL & "            ) b on a.Ymd = b.cYmd "
	SQL = SQL & " left outer join  "
	SQL = SQL & " 		   ( "
	SQL = SQL & " 		   select substring(swdate,1,8) as hYmd, SUM(cmoney) as hSummoney "
	SQL = SQL & " 		   from tb_charge  "
	SQL = SQL & "            where ingflag='5' "	'--환전완료 "
	SQL = SQL & "            and cflag = '2' "
	SQL = SQL & " 		   group by substring(swdate,1,8) "
	SQL = SQL & "            ) c on a.Ymd = c.hYmd "
	SQL = SQL & " left outer join  "
	SQL = SQL & " 		   ( "
	SQL = SQL & " 		   select substring(swdate,1,8) as pYmd, SUM(cmoney) as pSummoney "
	SQL = SQL & " 		   from tb_charge  "
	SQL = SQL & "            where ingflag='7' or ingflag='8' "	'--머니추가,머니삭감 - 7 / 포인트전환 - 8
	SQL = SQL & " 		   group by substring(swdate,1,8) "
	SQL = SQL & "            ) d on a.Ymd = d.pYmd "
	SQL = SQL & " and a.Ymd >= '"& stxt11 &"' and a.Ymd <= '"& stxt22 &"' "
	SQL = SQL & " order by   a.Ymd asc "
	rs.Open sql, db, 1
	'//
%>

<script language='JavaScript'>
<!--
function searChb(){
	form.submit();
}

//-->
</script>

<form name="form" method="post" action="?">

					<tr>
						<td bgcolor=#F2F2F2 width=15% align=center>조건검색</td>
						<td height=30>
							<input type="text" name="stxt1" id="stxt1" value="<%=stxt1%>" size="10" readonly />
							&nbsp;<img src="/admin/images/calendar.gif" align="absmiddle" OnClick="calendar(event, 'stxt1')" style="cursor:pointer">
							~
							<input type="text" name="stxt2" id="stxt2" value="<%=stxt2%>" size="10" readonly />
							&nbsp;<img src="/admin/images/calendar.gif" align="absmiddle" OnClick="calendar(event, 'stxt2')" style="cursor:pointer">
							<input type="image" src="/admin/images/btn_search1.gif" align=bottom onclick="return searChb()">
						</td>
					</tr>

</form>

				</table>

				<BR>

				<table border="0" cellpadding="1" cellspacing="1" width=100% bgcolor="#CBE1EE">
					<tr height=35 bgcolor="#F2F2F2" align=center>
						<td width=5%>No.</td>
						<td width=15%>정산일자</td>
						<td width=20%>베팅횟수</td>
						<td width=20%>베팅금액</td>
						<td width=20%>정산금액</td>
						<td width=20%>수익금</td>
					</tr>

<%
i=1
t_cSummoney = 0
t_hSummoney = 0
t_Summoney  = 0
t_pSummoney = 0
'//
do until rs.EOF
	Ymd       = rs("Ymd")
	cSummoney = rs("cSummoney")
	hSummoney = rs("hSummoney")
	Summoney  = rs("Summoney")
	pSummoney = rs("pSummoney")
	'//
	t_cSummoney = t_cSummoney + cSummoney
	t_hSummoney = t_hSummoney + hSummoney
	t_Summoney  = t_Summoney + Summoney
	t_pSummoney = t_pSummoney + pSummoney
%>

					<tr height=25 bgcolor=white align=center>
						<td><%=i%></td>
						<td><%=ymd%></td>
						<td><%=FormatNumber(cSummoney,0)%></font></td>
						<td align=right><font color='brown'><%=FormatNumber(hSummoney,0)%></font> 원&nbsp;</td>
						<td align=right><font color='#FF6600'><%=FormatNumber(Summoney,0)%></font> 원&nbsp;</td>
						<td align=right><font color='deeppink'><%=FormatNumber(pSummoney,0)%> 원&nbsp;</td>
					</tr>

<%
rs.movenext
i=i+1
Loop
rs.close
%>

					<tr height=25 bgcolor=white align=center>
						<td colspan=2 align=right><B>합 계&nbsp;</td>
						<td align=right><font color='blue'><%=FormatNumber(t_cSummoney,0)%></font> 원&nbsp;</td>
						<td align=right><font color='red'><%=FormatNumber(t_hSummoney,0)%></font> 원&nbsp;</td>
						<td align=right><font color='blue'><%=FormatNumber(t_Summoney,0)%></font> 원&nbsp;</td>
						<td align=right><font color='blue'><%=FormatNumber(t_pSummoney,0)%> 원&nbsp;</td>
					</tr>

				</table>

		 		</td>
			</tr>
		</table>
 		</td>
	</tr>
</table>

<!--#include virtual="/admin/include/bottom.asp" -->