<!--#include virtual="/admin/include/aidIPchb.asp" -->
<html>
<head>
<title>H-SPORTS MASTER</title>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="/admin/css/style.css" type="text/css" />
<link rel="stylesheet" href="/admin/css/w3.css" type="text/css" />

<script language='JavaScript'>
	<!--
	function frm_check(){
		var frm = document.LoginForm;

		if(frm.pin_no.value == ''){
			alert('PIN NO 입력하세요');
			frm.pin_no.focus();
			return  ;
		}
		frm.submit();
	}

	function keyDown() { 
		var keyValue = event.keyCode; 
		if(keyValue==13) //as enter key ascii code 
		frm_check(); 
	
	} 
	//-->	
</script>
</head>

<body>
	
<div class="w3-container">
  <div id="id00" style="padding-top: 150px; text-align: center;">
  <h2>마스터 관리자페이지</h2>
  <button onclick="document.getElementById('id01').style.display='block'" class="w3-btn w3-green w3-large">Pin 번호 인증하기</button>
  </div>
  
  <div id="id01" class="w3-modal">
    <div class="w3-modal-content w3-card-8  w3-round-xlarge" style="max-width:500px">
  
	  <div class="w3-center">
        <img src="./images/img_avatar4.png" alt="Avatar" style="width:30%" class="w3-circle w3-margin-top">
      </div>  
  
      <form class="w3-container" name=LoginForm method=post action='pinStepChb.asp' onsubmit='return frm_check();'>
        <div class="w3-section">
          <label><b>Pin 번호는 매일 변경됩니다.</b></label>
          <input class="w3-input w3-border w3-margin-bottom w3-text-15 w3-bold" type="text" placeholder="Enter Pin no" name="pin_no" id="pin_no" required>
          <button class="w3-btn-block w3-green w3-section w3-padding w3-text-16 w3-bolder" type="submit">Pin 번호 인증하기</button>
        </div>
      </form>

      <!--div class="w3-container w3-border-top w3-padding-16 w3-light-grey  w3-round-xlarge">
        <button onclick="document.getElementById('id01').style.display='none'" type="button" class="w3-btn w3-red w3-right w3-padding">취소</button>
      </div-->

    </div>
  </div>
</div>

<script language='JavaScript'>
	document.getElementById('id01').style.display='block';
</script>	
	
</html>