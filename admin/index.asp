<!--#include virtual="/admin/include/aidIPchb.asp" -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>H-SPORTS MASTER</title>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="/admin/css/style.css" type="text/css" />
<link rel="stylesheet" href="/admin/css/w3.css" type="text/css" />

<script language='JavaScript'>
	<!--
	function frm_check(){
		var frm = document.LoginForm;
		if(frm.admin_id.value == ''){
			alert('아이디를 입력하세요');
			frm.admin_id.focus();
			return false;
		}
		if(frm.admin_pwd.value == ''){
			alert('비밀번호를 입력하세요');
			frm.admin_pwd.focus();
			return false;
		}
		frm.submit();
		return false;
	}
//-->
function keyDown() { 
	var keyValue = event.keyCode; 
	if(keyValue==13) //as enter key ascii code 
	frm_check(); 
} 
</script>
</head>

<body>

<div class="w3-container">
  <div id="id00" style="padding-top: 150px; text-align: center;">
  <h2>마스터 관리자페이지</h2>
  <button onclick="document.getElementById('id01').style.display='block'" class="w3-btn w3-green w3-large">관리자페이지 로그인하기</button>
  </div>
  
  <div id="id01" class="w3-modal">
    <div class="w3-modal-content w3-card-8  w3-round-xlarge w3-animate-zoom" style="max-width:500px">
  
	  <div class="w3-center">
        <img src="./images/img_avatar2.png" alt="Avatar" style="width:30%" class="w3-circle w3-margin-top">
      </div>  
  
      <form class="w3-container" id="LoginForm" name="LoginForm" method="post" action="login_submit.asp" onsubmit="return frm_check();">
        <div class="w3-section">
          <label><b>아이디</b></label>
          <input class="w3-input w3-border w3-margin-bottom w3-text-15 w3-bold" type="text" placeholder="Enter ID" name="admin_id" id="admin_id" required>
          <label><b>비밀번호</b></label>
          <input class="w3-input w3-border w3-text-15 w3-bold" type="password" placeholder="Enter Password" name="admin_pwd" id="admin_pwd"  required>
          <button class="w3-btn-block w3-green w3-section w3-padding w3-text-16 w3-bolder" type="submit">관리자 로그인</button>
          <!--input class="w3-check w3-margin-top" type="checkbox" checked="checked"> Remember me!-->
        </div>
      </form>

      <div class="w3-container w3-border-top w3-padding-16 w3-light-grey  w3-round-xlarge">
        <button onclick="document.getElementById('id01').style.display='none'" type="button" class="w3-btn w3-red w3-right w3-padding">취소</button>
        <!--span class="w3-right w3-padding w3-hide-small">Forgot <a href="#">password?</a></span-->
      </div>

    </div>
  </div>
</div>


<script language='JavaScript'>
	//document.getElementById('id01').style.display='block';
</script>

</body>
</html>
