<nav class="w3-sidenav w3-white  w3-animate-left w3-card-2" style="z-index:1;width:155px" id="mySidenav">
  <a href="#" class="w3-border-bottom w3-text-18 w3-blue w3-padding-large w3-grayscale-min w3-ultrabold">SPORTS</a>
  <a href="#" class="w3-border-bottom w3-light-grey w3-text-15 w3-padding-large w3-bolder">게임관리</a>
  <a href="javascript:ClickOnTD(this,'/admin/sports/WinTieLose/List.asp?flag=13');" class="w3-text-13 w3-padding w3-bold">경기목록</a>
  <a href="javascript:ClickOnTD(this,'/admin/sports/WinTieLose/Write.asp?flag=13');" class="w3-text-13 w3-padding w3-bold">경기등록</a>
  <a href="javascript:ClickOnTD(this,'/admin/sports/uplodExcel.asp');" class="w3-text-13 w3-padding w3-bold">엑셀등록 목록</a>
  <a href="javascript:ClickOnTD(this,'/admin/sports/WinTieLose/btList.asp?flag=13');" class="w3-text-13 w3-padding w3-bold">배팅현황</a>
  <a href="javascript:ClickOnTD(this,'/admin/sports/WinTieLose/btAccount.asp?flag=13');" class="w3-text-13 w3-padding w3-bold">배팅정산</a>
  <a href="javascript:ClickOnTD(this,'/admin/sports/WinTieLose/dayAccount.asp?flag=13');" class="w3-text-13 w3-padding w3-bold">일일정산</a>
  <a href="javascript:ClickOnTD(this,'/admin/sports/WinTieLose/accountEnd.asp?flag=13');" class="w3-text-13 w3-padding w3-bold">정산완료</a>
  
  <a href="#" class="w3-border-bottom w3-light-grey w3-text-15 w3-padding-large w3-bolder">게임 기본설정</a>
  <a href="javascript:ClickOnTD(this,'/admin/sports/gameKindList.asp');" class="w3-text-13 w3-padding w3-bold">종목관리</a>
  <a href="javascript:ClickOnTD(this,'/admin/sports/teamList.asp');" class="w3-text-13 w3-padding w3-bold">팀관리</a>
  <a href="javascript:ClickOnTD(this,'/admin/sports/leagueList.asp');" class="w3-text-13 w3-padding w3-bold">리그관리</a>
  <a href="javascript:ClickOnTD(this,'/admin/sports/nationList.asp');" class="w3-text-13 w3-padding w3-bold">국가관리</a>
  <a href="javascript:ClickOnTD(this,'/admin/sports/LosePointReg.asp');" class="w3-text-13 w3-padding w3-bold">낙찰포인트</a>
  <a href="javascript:ClickOnTD(this,'/admin/sports/cpmoneyreg.asp');" class="w3-text-13 w3-padding w3-bold">쿠폰머니</a>
</nav>
