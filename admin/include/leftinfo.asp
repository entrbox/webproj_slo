<nav class="w3-sidenav w3-white  w3-animate-left w3-card-2" style="z-index:1;width:155px;" id="mySidenav">
  <a href="#" class="w3-border-bottom w3-text-18 w3-brown w3-padding-large w3-grayscale-min w3-ultrabold">SITE INFO</a>
  <a href="#" class="w3-border-bottom w3-light-grey w3-text-15 w3-padding-large w3-bolder">사이트관리</a>
  <a href="javascript:ClickOnTD(this,'/admin/info/hnews.asp');" class="w3-text-13 w3-padding w3-bold">핫뉴스</a>
  <a href="javascript:ClickOnTD(this,'/admin/info/hnews2.asp');" class="w3-text-13 w3-padding w3-bold">메인뉴스</a>
  <a href="javascript:ClickOnTD(this,'/admin/info/popup.asp');" class="w3-text-13 w3-padding w3-bold">팝업창1 관리</a>
  <a href="javascript:ClickOnTD(this,'/admin/info/popup2.asp');" class="w3-text-13 w3-padding w3-bold">팝업창2 관리</a>
  <a href="javascript:ClickOnTD(this,'/admin/info/admininfo.asp');" class="w3-text-13 w3-padding w3-bold">관리자정보 변경</a>
  <a href="javascript:ClickOnTD(this,'/admin/info/adminlevel.asp');" class="w3-text-13 w3-padding w3-bold">관리자권한 관리</a>
  <a href="javascript:ClickOnTD(this,'/admin/info/adminip.asp');" class="w3-text-13 w3-padding w3-bold">관리자 접속IP</a>
  <!--a href="javascript:ClickOnTD(this,'/admin/info/chuchun.asp');" class="w3-text-13 w3-padding w3-bold">추천장등록</a-->
  <a href="javascript:ClickOnTD(this,'/admin/info/pinSave.asp');" class="w3-text-13 w3-padding w3-bold">PIN 등록</a>
  <a href="javascript:ClickOnTD(this,'/admin/info/cacaoSave.asp');" class="w3-text-13 w3-padding w3-bold">카카오톡ID 등록</a>
  <a href="javascript:ClickOnTD(this,'/admin/info/smsinfo.asp');" class="w3-text-13 w3-padding w3-bold">문자번호/아이디 변경</a>
  <a href="javascript:ClickOnTD(this,'/admin/info/messege.asp');" class="w3-text-13 w3-padding w3-bold">쪽지 관리</a>
  <a href="javascript:ClickOnTD(this,'/admin/info/server.asp');" class="w3-text-13 w3-padding w3-bold">서버점검 설정</a>
  <a href="javascript:ClickOnTD(this,'/admin/info/point.asp');" class="w3-text-13 w3-padding w3-bold">포인트적립</a>
  <a href="javascript:ClickOnTD(this,'/admin/info/member.asp');" class="w3-text-13 w3-padding w3-bold">회원가입 관리</a>
  <a href="javascript:ClickOnTD(this,'/admin/info/autoQnaSet.asp');" class="w3-text-13 w3-padding w3-bold">자동답변 관리</a>
  <!--a href="javascript:ClickOnTD(this,'/admin/info/datadel.asp');" class="w3-text-13 w3-padding w3-bold">지난데이타 삭제</a-->
</nav>
