<nav class="w3-sidenav w3-white  w3-animate-left w3-card-2" style="z-index:1;width:155px;" id="mySidenav">
  <a href="#" class="w3-border-bottom w3-text-18 w3-green w3-padding-large w3-grayscale-min w3-ultrabold">MEMBERS</a>
  <a href="#" class="w3-border-bottom w3-light-grey w3-text-15 w3-padding-large w3-bolder">회원관리</a>
  <a href="javascript:ClickOnTD(this,'/admin/member/list.asp');" class="w3-text-13 w3-padding w3-bold">회원목록</a>
  <a href="javascript:ClickOnTD(this,'/admin/member/memlevelreg.asp');" class="w3-text-13 w3-padding w3-bold">회원레벨 설정</a>
  <a href="javascript:ClickOnTD(this,'/admin/member/nowConMem.asp');" class="w3-text-13 w3-padding w3-bold">현재 접속회원</a>
  <a href="javascript:ClickOnTD(this,'/admin/member/outMem.asp');" class="w3-text-13 w3-padding w3-bold">탈퇴회원</a>
  <a href="javascript:ClickOnTD(this,'/admin/member/blackIP.asp');" class="w3-text-13 w3-padding w3-bold">블랙IP</a>
  <a href="javascript:ClickOnTD(this,'/admin/member/msgmanage.asp');" class="w3-text-13 w3-padding w3-bold">쪽지관리</a>
  <a href="javascript:ClickOnTD(this,'/admin/member/smsSend.asp');" class="w3-text-13 w3-padding w3-bold">SMS 발송</a>
  <a href="javascript:ClickOnTD(this,'/admin/member/sameiP.asp');" class="w3-text-13 w3-padding w3-bold">중복아이피</a>
  <a href="javascript:ClickOnTD(this,'/admin/member/chuchun.asp');" class="w3-text-13 w3-padding w3-bold">추천인목록</a>
  <!--a href="javascript:ClickOnTD(this,'/admin/member/dayList.asp');" class="w3-text-13 w3-padding w3-bold">일일내역</a-->
  <a href="javascript:ClickOnTD(this,'/admin/member/chuList.asp');" class="w3-text-13 w3-padding w3-bold">추천장내역</a>
  <a href="javascript:ClickOnTD(this,'/admin/member/bankNum.asp');" class="w3-text-13 w3-padding w3-bold">계좌문의</a>
</nav>