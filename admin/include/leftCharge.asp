<nav class="w3-sidenav w3-white  w3-animate-left w3-card-2" style="z-index:1;width:155px;" id="mySidenav">
  <a href="#" class="w3-border-bottom w3-text-18 w3-deep-orange w3-padding-large w3-grayscale-min w3-ultrabold">MONEY</a>
  <a href="#" class="w3-border-bottom w3-light-grey w3-text-15 w3-padding-large w3-bolder">머니관리</a>
  <a href="javascript:ClickOnTD(this,'/admin/charge/clist.asp');" class="w3-text-13 w3-padding w3-bold">머니충전</a>
  <a href="javascript:ClickOnTD(this,'/admin/charge/hlist.asp');" class="w3-text-13 w3-padding w3-bold">머니환전</a>
  <a href="javascript:ClickOnTD(this,'/admin/charge/moneylist.asp');" class="w3-text-13 w3-padding w3-bold">구매머니 내역</a>
  <a href="javascript:ClickOnTD(this,'/admin/charge/pointlist.asp');" class="w3-text-13 w3-padding w3-bold">적립포인트 내역</a>
  <a href="javascript:ClickOnTD(this,'/admin/charge/cpointlist.asp');" class="w3-text-13 w3-padding w3-bold">쿠폰포인트 내역</a>
  <a href="javascript:ClickOnTD(this,'/admin/charge/sasMargin.asp');" class="w3-text-13 w3-padding w3-bold">수익금통계</a>
  <!--a href="javascript:ClickOnTD(this,'/admin/charge/sasDay.asp');" class="w3-small2 w3-padding">일일정산통계</a-->
  <!--a href="javascript:ClickOnTD(this,'/admin/charge/sasMoney.asp');" class="w3-small2 w3-padding">추가/삭감머니</a-->
  <a href="javascript:ClickOnTD(this,'/admin/charge/sasSum.asp');" class="w3-text-13 w3-padding w3-bold">충환전합계표</a>
</nav>