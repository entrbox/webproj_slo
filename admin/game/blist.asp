<!--#include virtual="/admin/include/top.asp" -->

<table width="100%" border=0 cellspacing="0" cellpadding="0" height="100%">
	<tr>
		<td width="160" bgcolor='#F7F7F7' valign="top">
			<!--#include virtual="/admin/include/leftGame.asp" -->
		</td>
		<td valign="top" width=''>

    	<table width=1000 cellpadding=10 cellspacing=0 border=0>
			<tr>
				<td valign=top>

				<table width='100%' cellpadding=0 cellspacing=0 border=0>
					<tr>
						<td height=30 colspan=2 class=dotum_15><img src='/admin/images/icon_arrow0.gif' align=absmiddle>&nbsp;구매현황</td>
					</tr>
					<tr height=2>
						<td width=200 bgcolor='#AF0B0A'></td>
						<td bgcolor='#F2F8FB'></td>
					</tr>
				</table>

				<table border="0" cellpadding="3" cellspacing="1" style="width:100%; border:1px solid #CBE1EE;">

<%
	cflag = "1"
	stxt1 = rq("stxt1")
	stxt2 = rq("stxt2")
	stxt3 = rq("stxt3")
	stxt4 = rq("stxt4")
	stxt5 = rq("stxt5")
	stxt6 = rq("stxt6")
	stxt7 = rq("stxt7")
	stxt8 = rq("stxt8")
	stxt9 = rq("stxt9")
	stxt10 = rq("stxt10")
	GotoPage = rq("GotoPage")
	'//
	If stxt1="" then stxt1 = Left(now(),10)
	If stxt2="" then stxt2 = Left(now(),10)
	if GotoPage = "" Then GotoPage = 1
	stxt11 = Replace(stxt1,"-","")
	stxt22 = Replace(stxt2,"-","")
	'//
	set rs = server.CreateObject("ADODB.Recordset")
	SQL = "select "
	SQL = SQL & " (aa.gday + right('0'+CONVERT(varchar,aa.gno),2)) as num, "
	SQL = SQL & " isnull(bb.chCnt,0) as chCnt, "
	SQL = SQL & " isnull(bb.useridcnt,0) as useridcnt, "
	SQL = SQL & " isnull(bb.exmoney,0) as exmoney "
	SQL = SQL & " from tb_game_ymdt aa "
	SQL = SQL & " left outer join ( "
	SQL = SQL & " 				select ('20'+CONVERT(varchar,a.gmCode)) as ornum,sum(b.chCnt) as chCnt,COUNT(distinct a.userid) as useridcnt, isnull(sum(b.exmoney),0) as exmoney "
	SQL = SQL & " 				from tb_order_info a,  "
	SQL = SQL & " 					 ( "
	SQL = SQL & " 					 select a.* , (a.chCnt*1000*b.gwinper) as exmoney "
	SQL = SQL & " 					 from tb_order_Game a, tb_game_code b  "
	SQL = SQL & " 					 where b.gcode=a.gmKind "
	SQL = SQL & " 					 ) b "
	SQL = SQL & " 				where a.orSeq = b.orseq "
	SQL = SQL & " 				group by ('20'+CONVERT(varchar,gmCode)) "
	SQL = SQL & " 				) bb  "
	SQL = SQL & " on (aa.gday + right('0'+CONVERT(varchar,aa.gno),2)) = bb.ornum "
	SQL = SQL & " where aa.gday  = '"& stxt11 &"' "
	SQL = SQL & " order by (aa.gday + right('0'+CONVERT(varchar,aa.gno),2)) asc "
	rs.Open sql, db, 1
	'//
%>

<script language='JavaScript'>
<!--
function searChb(){
	form.submit();
}

function showdetail(val1,val2){
	window.location.href="blist_detail.asp?stxt1="+val1+"&stxt6="+val2;
}
//-->
</script>

<form name="form" method="post" action="?">

					<tr>
						<td bgcolor=#F2F2F2 width=15% align=center>조건검색</td>
						<td height=30>
							<input type="text" name="stxt1" id="stxt1" value="<%=stxt1%>" size="10" readonly />
							&nbsp;<img src="/admin/images/calendar.gif" align="absmiddle" OnClick="calendar(event, 'stxt1')" style="cursor:pointer">
							<input type="image" src="/admin/images/btn_search1.gif" align=bottom onclick="return searChb()">
							<input name="btnExcel" type="button" value="EXCEL 파일 다운로드" onclick="listexcel();" />
						</td>
					</tr>

</form>

				</table>

				<BR>

				<table border="0" cellpadding="1" cellspacing="1" width=100% bgcolor="#CBE1EE">
					<tr height=35 bgcolor="#F2F2F2" align=center>
						<td width=25%>회차</td>
						<td width=25%>판매수량</td>
						<td width=25%>구매인원수</td>
						<td width=20%>예상적중금액</td>
					</tr>

<%
i=1
t_cSummoney = 0
t_hSummoney = 0
t_Summoney  = 0
t_pSummoney = 0
'//
do until rs.EOF
	num       = Mid(rs("num"),3)
	chCnt     = rs("chCnt")
	useridcnt = rs("useridcnt")
	exmoney   = rs("exmoney")
	'//
	t_cSummoney = t_cSummoney + chCnt
	t_hSummoney = t_hSummoney + useridcnt
	t_Summoney  = t_Summoney + CDbl(exmoney)
	'//
	imsival1 = mid(rs("num"),1,4) &"-"& mid(rs("num"),5,2) &"-"& mid(rs("num"),7,2)
	imsival2 = CDbl(right(rs("num"),2))
%>

					<tr height=25 bgcolor=white align=center <%If chCnt>0 then%>onclick="showdetail('<%=imsival1%>','<%=imsival2%>');" style="cursor:pointer;"<%End if%>>
						<td><%=num%></td>
						<td align=right><font color='brown'><%=FormatNumber(chCnt,0)%></font> 장&nbsp;</td>
						<td align=right><font color='brown'><%=FormatNumber(useridcnt,0)%></font> 명&nbsp;</td>
						<td align=right><font color='#FF6600'><%=FormatNumber(exmoney,0)%></font> 원&nbsp;</td>
					</tr>

<%
rs.movenext
i=i+1
Loop
rs.close
%>

					<tr height=25 bgcolor=white align=center>
						<td align=right><B>합 계&nbsp;</td>
						<td align=right><font color='blue'><%=FormatNumber(t_cSummoney,0)%></font> 장&nbsp;</td>
						<td align=right><font color='red'><%=FormatNumber(t_hSummoney,0)%></font> 명&nbsp;</td>
						<td align=right><font color='blue'><%=FormatNumber(t_Summoney,0)%></font> 원&nbsp;</td>
					</tr>

				</table>

		 		</td>
			</tr>
		</table>
 		</td>
	</tr>
</table>

<!--#include virtual="/admin/include/bottom.asp" -->
