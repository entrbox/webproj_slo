<!--#include virtual="/admin/include/top.asp" -->

<table width="100%" border=0 cellspacing="0" cellpadding="0" height="100%">
	<tr>
		<td width="160" bgcolor='#F7F7F7' valign="top">
			<!--#include virtual="/admin/include/leftGame.asp" -->
		</td>
		<td valign="top" width=''>

    	<table width=1000 cellpadding=10 cellspacing=0 border=0>
			<tr>
				<td valign=top>

				<table width='100%' cellpadding=0 cellspacing=0 border=0>
					<tr>
						<td height=30 colspan=2 class=dotum_15><img src='/admin/images/icon_arrow0.gif' align=absmiddle>&nbsp;전체통계</td>
					</tr>
					<tr height=2>
						<td width=200 bgcolor='#AF0B0A'></td>
						<td bgcolor='#F2F8FB'></td>
					</tr>
				</table>

				<table border="0" cellpadding="3" cellspacing="1" style="width:100%; border:1px solid #CBE1EE;">

<%
	cflag = "1"
	stxt1 = rq("stxt1")
	stxt2 = rq("stxt2")
	stxt3 = rq("stxt3")
	stxt4 = rq("stxt4")
	stxt5 = rq("stxt5")
	stxt6 = rq("stxt6")
	stxt7 = rq("stxt7")
	stxt8 = rq("stxt8")
	stxt9 = rq("stxt9")
	stxt10 = rq("stxt10")
	GotoPage = rq("GotoPage")
	'//
	If stxt1="" then stxt1 = Left(now(),10)
	If stxt2="" then stxt2 = Left(now(),10)
	if GotoPage = "" Then GotoPage = 1
	stxt11 = Replace(stxt1,"-","")
	stxt22 = Replace(stxt2,"-","")
	'//
	set rs = server.CreateObject("ADODB.Recordset")
	SQL = " select a.gday,a.gno,isnull(b.chCnt,0) as chCnt,isnull(b.chCnt*1000,0) as ormoney, "
	SQL = SQL & " isnull(b.winmoney,0) as winmoney, "
	SQL = SQL & " (isnull(b.chCnt*1000,0)-isnull(b.winmoney,0)) as pmoney "
	SQL = SQL & " from tb_game_ymdt a "
	SQL = SQL & " left outer join ( "
	SQL = SQL & " 				select a.gmCode, isnull(sum(b.chCnt),0) as chCnt, isnull(sum(b.winmoney),0) as winmoney "
	SQL = SQL & " 				from tb_order_info a, tb_order_Game b "
	SQL = SQL & " 				where a.orSeq = b.orseq "
	SQL = SQL & " 				and (b.winFlag = '5' or b.winFlag = '9') "
	SQL = SQL & " 				group by a.gmCode "
	SQL = SQL & " 				) b "
	SQL = SQL & " on (a.gday+right('0'+convert(varchar,a.gno),2)) = ('20'+convert(varchar,b.gmcode)) "
	SQL = SQL & " where a.gday  = '"& stxt11 &"' "
	SQL = SQL & " order by a.gno Asc "
	rs.Open sql, db, 1
	'//
%>

<script language='JavaScript'>
<!--
function searChb(){
	form.submit();
}

//-->
</script>

<form name="form" method="post" action="?">

					<tr>
						<td bgcolor=#F2F2F2 width=15% align=center>조건검색</td>
						<td height=30>
							<input type="text" name="stxt1" id="stxt1" value="<%=stxt1%>" size="10" readonly />
							&nbsp;<img src="/admin/images/calendar.gif" align="absmiddle" OnClick="calendar(event, 'stxt1')" style="cursor:pointer">
							<input type="image" src="/admin/images/btn_search1.gif" align=bottom onclick="return searChb()">
							<input name="btnExcel" type="button" value="EXCEL 파일 다운로드" onclick="listexcel();" />
						</td>
					</tr>

</form>

				</table>

				<BR>

				<table border="0" cellpadding="1" cellspacing="1" width=100% bgcolor="#CBE1EE">
					<tr height=35 bgcolor="#F2F2F2" align=center>
						<td width=15%>No.</td>
						<td width=15%>날짜</td>
						<td width=10%>회차</td>
						<td width=15%>구매매수</td>
						<td width=15%>구매금액</td>
						<td width=15%>당첨금액</td>
						<td width=15%>수익</td>
					</tr>

<%
i=1
t_cSummoney = 0
t_hSummoney = 0
t_Summoney  = 0
t_pSummoney = 0
'//
do until rs.EOF
	gday     = rs(0)
	gno      = rs(1)
	chCnt    = rs(2)
	ormoney  = rs(3)
	winmoney = rs(4)
	pmoney   = rs(5)
	'//
	gday = getDateRe("1",gday,"-")
	t_cSummoney = t_cSummoney + chCnt
	t_hSummoney = t_hSummoney + ormoney
	t_Summoney  = t_Summoney  + winmoney
	t_pSummoney = t_pSummoney + pmoney
%>

					<tr height=25 bgcolor=white align=center>
						<td><%=i%></td>
						<td><%=gday%></td>
						<td><%=gno%></td>
						<td align=right><%=FormatNumber(chCnt,0)%>&nbsp;</td>
						<td align=right><%=FormatNumber(ormoney,0)%></font>&nbsp;</td>
						<td align=right><%=FormatNumber(winmoney,0)%></font>&nbsp;</td>
						<td align=right><%=FormatNumber(pmoney,0)%></font>&nbsp;</td>
					</tr>

<%
rs.movenext
i=i+1
Loop
rs.close
%>

					<tr height=25 bgcolor=white align=center>
						<td align=right colspan=3><B>합 계&nbsp;</td>
						<td align=right><B><%=FormatNumber(t_cSummoney,0)%></font>&nbsp;</td>
						<td align=right><B><%=FormatNumber(t_hSummoney,0)%></font>&nbsp;</td>
						<td align=right><B><%=FormatNumber(t_Summoney,0)%></font>&nbsp;</td>
						<td align=right><B><%=FormatNumber(t_pSummoney,0)%></font>&nbsp;</td>
					</tr>

				</table>

		 		</td>
			</tr>
		</table>
 		</td>
	</tr>
</table>

<!--#include virtual="/admin/include/bottom.asp" -->
