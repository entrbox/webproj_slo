<!--#include virtual="/admin/include/top.asp" -->

<table style="width:100%;min-width:1200px!important;height:100%;min-height:600px;border:0px;padding:0px;margin:0px;">
	<tr>
		<td style="width:160px!important;min-width:160px!important;background-color:#F7F7F7" valign="top">
			<!--#include virtual="/admin/include/leftagent.asp" -->
		</td>
		<td valign="top" style="min-width:1040px!important;">

    	<table width="100%" cellpadding=10 cellspacing=0 border=0>
			<tr>
				<td valign=top>
					<div class="w3-container w3-card-2 w3-indigo w3-text-16 w3-padding-large w3-bolder" style="margin-bottom:10px;">
					  총판관리
					</div>


				

<%
	cflag = "1"
	stxt1 = rq("stxt1")
	stxt2 = rq("stxt2")
	stxt3 = rq("stxt3")
	stxt4 = rq("stxt4")
	stxt5 = rq("stxt5")
	stxt6 = rq("stxt6")
	stxt7 = rq("stxt7")
	stxt8 = rq("stxt8")
	stxt9 = rq("stxt9")
	stxt10 = rq("stxt10")
	GotoPage = rq("GotoPage")
	'//
	If stxt1="" then stxt1 = Left(now(),7) & "-01"
	If stxt2="" then stxt2 = Left(now(),10)
	if GotoPage = "" Then GotoPage = 1
	stxt11 = Replace(stxt1,"-","")
	stxt22 = Replace(stxt2,"-","")
	'//
	set rs = server.CreateObject("ADODB.Recordset")
	SQL = "select isnull(count(idx),0) from tb_agent "
	rs.Open sql, db, 1
	totCnt   = rs(0)
	rs.close
	'//
	mSeachSql = getAmemSch2(stxt3)
	'//
	set rs = server.CreateObject("ADODB.Recordset")
	SQL = "select a.*,b.mb_code_cnt, c.mb_money_hap, d.mb_point_hap  from tb_agent a"
	SQL = SQL & " left outer join (select mb_code, count(mb_code) as mb_code_cnt from tb_member group by mb_code) b on a.userid = b.mb_code "
	SQL = SQL & " left outer join (select mb_code, sum(mb_money) as mb_money_hap  from tb_member group by mb_code) c on a.userid = c.mb_code "
	SQL = SQL & " left outer join (select mb_code, sum(mb_point) as mb_point_hap  from tb_member group by mb_code) d on a.userid = d.mb_code "
	'SQL = SQL & " left outer join (select userid, sum(smoney) as cpay  from tb_agent_pay where flag='c' group by userid) b on a.userid = b.userid "
	'SQL = SQL & " left outer join (select userid, sum(smoney) as hpay from tb_agent_pay where flag='h' group by userid)  c on a.userid = c.userid "
	'//
	If stxt4<>"" And stxt5<>"" Then SQL = SQL & " where "& stxt4 &" like '%"& stxt5 &"%' "
	'//
	SQL = SQL & " order by a.idx desc "
	rs.PageSize=20
	rs.Open sql, db, 1
	if not rs.EOF Then rs.AbsolutePage=int(gotopage)
	schCnt = rs.recordcount
	'//
%>

<script language='JavaScript'>
<!--
function searChb(){
	if ((form.stxt4.value != '')&&(form.stxt5.value == '')){alert('검색구분을 선택해 주세요.');form.stxt5.focus();	return false;}
	if ((form.stxt4.value == '')&&(form.stxt5.value != '')){alert('검색어를 입력해 주세요.');form.stxt4.focus();	return false;}
	form.submit();
}

function trChb(val1,val2,val3){
	var msg;
	if (val1=="y"){
		msg=confirm("승인 처리하시겠습니까?");
	}else if (val1=="n"){
		msg=confirm("미승인 처리하시겠습니까?");
	}else if (val1=="d"){
		msg=confirm("삭제 하시겠습니까?\n총판관련 모든데이타가 삭제됩니다.");
	}
	if(msg) {
		frm.dflag.value = val1;
		frm.idx.value = val2;
		frm.userid.value = val3;
		frm.action = "writeProc.asp";
		frm.submit();
		return false ;
	}
}

//-->
</script>

<form name="form" method="post" action="?" onsubmit="return searChb()">
					<div class="w3-container w3-leftbar w3-light-grey w3-padding w3-border" style="margin-bottom:10px;text-align:right">
						<div class="w3-right" style="margin-left: 3px;padding-top: 5px">						
							<div class="w3-left" style="margin-left: 3px;">
								<select class="w3-select w3-border w3-small w3-padding" name="stxt4" style="min-width:125px;max-width:250px">
									<option value="">검색구분</option>
								<option value="a.userid" <%If stxt4="a.userid" Then Response.write " selected"%>>총판등록ID</option>
								<option value="a.aname"  <%If stxt4="a.aname"  Then Response.write " selected"%>>총판명</option>
								</select>						
							</div>							
							<div class="w3-left" style="margin-left: 3px;"><input class="w3-input w3-padding w3-border w3-left" type="text" placeholder="검색어" name="stxt5" value="<%=stxt5%>" style="width:378px"></div>
							<div class="w3-left" style="margin-left: 3px;"><button class="w3-btn w3-padding w3-black w3-small w3-left w3-bold" type="submit" style="width:125px">검색하기</button></div>
						</div>											
					</div>
</form>

			  <div class="w3-panel w3-row w3-padding-small w3-sand w3-leftbar w3-border-cyan">
			    <div class="w3-col s6 w3-padding-small">
				    <span class="w3-text-dark-grey w3-text-14">전체:</span> <span class="w3-text-green w3-text-14 w3-bold"><%=FormatNumber(totCnt,0)%></span>
				    <span class="w3-text-grey w3-large w3-padding-small">/</span>
				    <span class="w3-text-dark-grey w3-text-14">검색결과:</span> <span class="w3-text-blue w3-text-14 w3-bold"><%=FormatNumber(schCnt,0)%></span> 
				    <!--span class="w3-text-grey w3-large w3-padding-small">/</span>
				    <span class="w3-text-dark-grey w3-large">보유머니합계:</span> <span class="w3-text-deep-orange w3-large"><%=FormatNumber(totMny,0)%></span--> 
				</div>
			    <div class="w3-col s6">
				    <div class="w3-right">
				    </div>			
			    </div>	    
			  </div>
			  
<!--table border="0" cellpadding="3" cellspacing="1" style="width:100%; border:1px solid #CBE1EE;">
<form name="form" method="post" action="?">

					<tr>
						<td bgcolor=#F2F2F2 width=15% align=center>조건검색</td>
						<td height=30>
							<select name=stxt4>
								<option value="">--전체--
								<option value="a.userid" <%If stxt4="a.userid" Then Response.write " selected"%>>총판등록ID
								<option value="a.aname"  <%If stxt4="a.aname"  Then Response.write " selected"%>>총판명
							</select>
							<input type=text name=stxt5 value="<%=stxt5%>" size=20>
							<input type="image" src="/admin/images/btn_search1.gif" align=bottom onclick="return searChb()">
						</td>
					</tr>

</form>
				</table>

				<BR>
				<table width=100% cellpadding=3 cellspacing=0 border=0>
					<tr>
						<td>
							<img src='/admin/images/icon_arrow3.gif' align=absmiddle>&nbsp;Total <font color=blue><%=FormatNumber(totCnt,0)%></font>
							/ 검색결과 <font color=blue><%=FormatNumber(schCnt,0)%></font>
						</td>
						<td align=right width=20%><input type=button value="총판등록" onclick="location.href='write.asp'" name=btn22></td>
				</table-->

<form name="frm" method="post" action="?">
<input type="hidden" name="stxt1" value="<%=stxt1%>">
<input type="hidden" name="stxt2" value="<%=stxt2%>">
<input type="hidden" name="stxt3" value="<%=stxt3%>">
<input type="hidden" name="stxt4" value="<%=stxt4%>">
<input type="hidden" name="stxt5" value="<%=stxt5%>">
<input type="hidden" name="stxt6" value="<%=stxt6%>">
<input type="hidden" name="stxt7" value="<%=stxt7%>">
<input type="hidden" name="stxt8" value="<%=stxt8%>">
<input type="hidden" name="stxt9" value="<%=stxt9%>">
<input type="hidden" name="stxt10" value="<%=stxt10%>">
<input type="hidden" name="gotopage" value="<%=gotopage%>">
<input type="hidden" name="idx">
<input type="hidden" name="userid">
<input type="hidden" name="dflag">
</form>


				<table class="w3-htable w3-hoverable">
					<tr>
						<td width=5%>No.</td>
						<td width=10%>총판ID</td>
						<td width=17%>총판명</td>
						<td width=15%>전화번호</td>
						
						<td width=10%>보유회원수</td>
						<td width=10%>보유금액</td>
						<td width=10%>보유포인트</td>
						<td width=15%>등록일자</td>
					</tr>

<%
j=schCnt-((gotopage-1)*20)
i=1
do until rs.EOF or i>rs.pagesize
	idx		 = rs("idx")
	aname	 = rs("aname")
	userid	 = rs("userid")
	sper	 = rs("sper")
	atel	 = rs("atel")
	abank	 = rs("abank")
	abanknm	 = rs("abanknm")
	abanknum = rs("abanknum")
	memo	 = rs("memo")
	aingflag = rs("aingflag")
	wdate	 = rs("wdate")
	mb_code_cnt  = formatnumber(getRevalue(rs("mb_code_cnt"),0),0)
    mb_money_hap = formatnumber(getRevalue(rs("mb_money_hap"),0),0)
    mb_point_hap = formatnumber(getRevalue(rs("mb_point_hap"),0),0)
	'//
	wdate  = getDateRe("8",wdate,"-")
	If aingflag="y" Then
		aingflag2 = "승인"
	Else
		aingflag2 = "미승인"
	End if

%>

					<tr>
						<td><%=j%></td>
						<td><a href="write.asp?idx=<%=idx%>&gotopage=<%=gotopage%>&stxt1=<%=stxt1%>&stxt2=<%=stxt2%>&stxt3=<%=stxt3%>&stxt4=<%=stxt4%>&stxt5=<%=stxt5%>&stxt6=<%=stxt6%>&stxt7=<%=stxt7%>&stxt8=<%=stxt8%>&stxt9=<%=stxt9%>&stxt10=<%=stxt10%>" class="w3-bold w3-text-black"><%=userid%></td>
						<td class="w3-bold w3-text-blue"><%=aname%></td>
						<td><%=atel%></td>
						
						<td style="text-align: right"><a href="/admin/member/list.asp?stxt7=<%=userid%>"><B><%=mb_code_cnt%>&nbsp;</td>
						<td style="text-align: right"><%=mb_money_hap%>&nbsp;</td>
						<td style="text-align: right"><%=mb_point_hap%>&nbsp;</td>
						<td><%=wdate%></td>
					</tr>

<%
rs.movenext
j=j-1
i=i+1
loop
%>

				</table>

				<table border="0" cellpadding="0" cellspacing="0" width=100% bgcolor=white>
					<tr height=45>
						<td align=center>
							<!--#include virtual="/admin/include/paging.asp" -->
						</td>
					</tr>
				</table>

		 		</td>
			</tr>
		</table>
 		</td>
	</tr>
</table>

<!--#include virtual="/admin/include/bottom.asp" -->