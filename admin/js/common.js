/******************************************************************************
*	기본 스크립트
*******************************************************************************/
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
	
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}	
	
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

/******************************************************************************
*	Email Check
*******************************************************************************/
function check_mail(check_value) {
	emailEx1 = /[^@]+@[A-Za-z0-9_-]+.[A-Za-z]+/;
	emailEx2 = /[^@]+@[A-Za-z0-9_-]+.[A-Za-z0-9_-]+.[A-Za-z]+/;
	emailEx3 = /[^@]+@[A-Za-z0-9_-]+.[A-Za-z0-9_-]+.[A-Za-z0-9_-]+.[A-Za-z]+/;
	
	if(emailEx1.test(check_value)) return true;
	if(emailEx2.test(check_value)) return true;
	if(emailEx3.test(check_value)) return true;
	
	return ;
}

/******************************************************************************
*	Numeric Check(소수점 포함)
*******************************************************************************/
function check_float(thisnum){
	var isnum = true;

	for(var i=0; i < thisnum.length; i++)
	{
		substr = thisnum.substring(i, i+1);

		if( substr != "0" && substr != "1" && substr != "2" &&
			substr != "3" && substr != "4" && substr != "5" &&
			substr != "6" && substr != "7" && substr != "8" &&
			substr != "9" && substr != "." )
			{ isnum = false;}		
	}
	return isnum;
}

/******************************************************************************
*	Numeric Check(소수점 제외)
*******************************************************************************/
function check_num(thisnum){
	var isnum = true;

	for(var i=0; i < thisnum.length; i++)
	{
		substr = thisnum.substring(i, i+1);

		if( substr < "0" || substr > "9")
			{ isnum = false;}		
	}
	return isnum;
}
/******************************************************************************
*	주민번호 체크
*******************************************************************************/
function check_jumin(str1,str2){

	strYear	= str1.substring(0, 2);
	strMonth = str1.substring(2, 4);
	strDay = str1.substring(4, 6);
	if (strMonth < 1 || strMonth > 12 || strDay < 1 || strDay > 31) {
		alert("주민등록번호 앞자리 입력이 올바르지 않습니다")
		return false;
	}
	strGender = str2.substring(0, 1);
	if(strGender < 1 || strGender > 4) {
		alert("주민등록번호 뒷자리 입력이 올바르지 않습니다")
		return false;
	}
	li_value = new Array(13);
	var li_lastid,li_mod,li_minus,li_last;
	li_lastid		= parseFloat(str2.substring(6,7));
	li_value[0]		= parseFloat(str1.substring(0,1))  * 2;
	li_value[1]		= parseFloat(str1.substring(1,2))  * 3;
	li_value[2]		= parseFloat(str1.substring(2,3))  * 4;
	li_value[3]		= parseFloat(str1.substring(3,4))  * 5;
	li_value[4]		= parseFloat(str1.substring(4,5))  * 6;
	li_value[5]		= parseFloat(str1.substring(5,6))  * 7;
	li_value[6]		= parseFloat(str2.substring(0,1))  * 8;
	li_value[7]		= parseFloat(str2.substring(1,2))  * 9;
	li_value[8]		= parseFloat(str2.substring(2,3))  * 2;
	li_value[9]		= parseFloat(str2.substring(3,4))  * 3;
	li_value[10]	= parseFloat(str2.substring(4,5))  * 4;
	li_value[11]	= parseFloat(str2.substring(5,6))  * 5;
	li_value[12]	= 0;
	for (var i = 0; i<12;i++) {
		li_value[12] = li_value[12] + li_value[i];
	}   
	li_mod		= li_value[12] %11;
	li_minus	= 11 - li_mod;
	li_last		= li_minus % 10;
	if (li_last != li_lastid) {
		alert ("주민등록번호가 맞지 않습니다. 다시 확인해 주십시오");
		return false;
	}
	return true;
}

/******************************************************************************
*	사업자번호 체크
*******************************************************************************/
function check_compno(comp_no){

	 var ret; 
	 var tot = 0; 
	 var tmp; 
	 var chk_digit; 
 
	 tot = Number(comp_no.charAt(0)) * 1; 
	 tot += Number(comp_no.charAt(1)) * 3; 
	 tot += Number(comp_no.charAt(2)) * 7; 
	 tot += Number(comp_no.charAt(3)) * 1; 
	 tot += Number(comp_no.charAt(4)) * 3; 
	 tot += Number(comp_no.charAt(5)) * 7; 
	 tot += Number(comp_no.charAt(6)) * 1; 
	 tot += Number(comp_no.charAt(7)) * 3; 
 
	 tmp = Number(comp_no.charAt(8)) * 5; 
	 tot += ( Math.floor(tmp/10) + (tmp%10) ); 
 
	 chk_digit = 10 - (tot % 10); 
	 if(chk_digit == 10) chk_digit = 0; 
 
	 if( String(chk_digit) != comp_no.charAt(9))  {	
		alert ("사업자등록번호가 맞지 않습니다. 다시 확인해 주십시오");
		return false; 
	 }
	 return true;
}  

/******************************************************************************
*	숫자만을 기입받게 하는 방법
*******************************************************************************/
function onlyNumber()  // onkeypress="onlyNumber();"
{
   if((event.keyCode < 48)||(event.keyCode > 57))
	  event.returnValue=false;      
}

/******************************************************************************
*	JavaScript에는 없는 Trim 함수로 좌, 우 공백을 제거한 문자열을 Return
*******************************************************************************/
function trim(str)	{
		return str.replace(/(^\s*)|(\s*$)/g, "");	
}

/******************************************************************************
*	관리자페이지 검색폼 입력항목 체크
*******************************************************************************/
function chk_search(){
	if(document.SearchForm.category.value != ""){
		if(document.SearchForm.keyword.value == ""){
			alert("검색어를 입력하세요.");
			document.SearchForm.keyword.focus();
			return;
		}
	}
	document.SearchForm.submit();
}

/******************************************************************************
*	TD MouseOver BgColor Change
*******************************************************************************/

function defaultStatus(obj,bgcolor){
	obj.style.backgroundColor = bgcolor;
}
function mouseOnTD(obj,bgcolor){
	obj.style.backgroundColor = bgcolor;
	obj.style.cursor = "hand";
}
function ClickOnTD(obj, page){
	//obj.style.cursor = "hand";
	window.location.href = page; 
}

function MoneyFormat(str){
	var re="";
	str = str + "";
//	str=str.replace(/-/gi,"");
	str=str.replace(/ /gi,"");
	
	str2=str.replace(/-/gi,"");
	str2=str2.replace(/,/gi,"");
	str2=str2.replace(/\./gi,"");	
	
	if(isNaN(str2) && str!="-") return "";
	try
	{
		for(var i=0;i<str2.length;i++)
		{
			var c = str2.substring(str2.length-1-i,str2.length-i);
			re = c + re;
			if(i%3==2 && i<str2.length-1) re = "," + re;
		}
		
	}catch(e)
	{
		re="";
	}
	
	if(str.indexOf("-")==0)
	{
		re = "-" + re;
	}

	return re;
}
function trim(str)	{
		return str.replace(/(^\s*)|(\s*$)/g, "");	
}