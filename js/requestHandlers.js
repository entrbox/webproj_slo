var querystring = require("querystring"),
    fs = require("fs"),
    url = require("url"),
    formidable = require("formidable");

function start(response) {
  console.log("Request handler 'start' was called.");

  var body = '<html>'+
    '<head>'+
    '<meta http-equiv="Content-Type" content="text/html; '+
    'charset=UTF-8" />'+
    '</head>'+
    '<body>'+
    '<form action="/upload" enctype="multipart/form-data" '+
    'method="post">'+
    '<input type="file" name="upload" multiple="multiple">'+
    '<input type="submit" value="Upload file" />'+
    '</form>'+
    '</body>'+
    '</html>';

    response.writeHead(200, {"Content-Type": "text/html"});
    response.write(body);
    response.end();
}

function upload(response, request) {
  console.log("Request handler 'upload' was called.");
  var form = new formidable.IncomingForm();
  form.uploadDir = "tmp"

  form.parse(request, function(error, fields, files) {
    console.log("F.U.P: " + files.upload.path);
    setTimeout(function(){
		try{
		    fs.renameSync(files.upload.path, "tmp/" + files.upload.name);
		}catch(e){
			console.log(e);
    }

    response.writeHead(200, {"Content-Type": "text/html;charset=utf-8"});
	response.write("received image:<br/>");
    response.end( "<img src='/show?i=" + files.upload.name + "' />" );

    },1000
    );
  });
}


function show(response, request) {

   // console.log( "Request handler 'show' was called. " );


    var image = querystring.parse( url.parse( request.url ).query ).i;

	if( !image ){
        response.writeHead( 500, { "Content-Type" : "text/plain" } );
        response.end( " No Image in QueryString ");
        return;
    }

    fs.readFile( "tmp/" + image, "binary", function( error, file ) {

        if( error ){
            response.writeHead( 500, { "Content-Type" : "text/plain" } );
            response.end( error + "\n" );
            return;
         }
                
        //type = mime.lookup( file );

		response.writeHead( 200, { "Content-Type" : "image/gif;charset=utf-8;" } );
   console.log( "file : " + file );
		response.end( file, "binary" );

        //response.writeHead( 200, { "Content-Type" : type } );
        //response.end( file, "binary" );


    });

}


exports.start = start;
exports.upload = upload;
exports.show = show;
