﻿; (function ($) {
    $.modalStart = function (maskID, zIndex, bgColor, opacity, dialogID, modalCloseButtonID) {
        //Get the screen height and width
        var maskHeight = $(document).height();
        var maskWidth = $(window).width();

        //Set heigth and width to mask to fill up the whole screen
        $('#' + maskID).css({ 'width': maskWidth, 'height': maskHeight });
        $('#' + maskID).css({ 'position': 'absolute' });
        $('#' + maskID).css({ 'left': '0', 'top': '0' });
        $('#' + maskID).css({ 'z-index': zIndex });
        $('#' + maskID).css({ 'background-color': bgColor });
        $('#' + maskID).css({ 'display': 'none' });

        //transition effect		                
        $('#' + maskID).fadeTo("slow", opacity);

        //Get the window height and width
        var winH = $(window).height();
        var winW = $(window).width();

        //Set the popup window to center
        $("#" + dialogID).css('top', winH / 2 - $("#" + dialogID).height() / 2);
        $("#" + dialogID).css('left', winW / 2 - $("#" + dialogID).width() / 2);

        //transition effect
        $("#" + dialogID).fadeIn(2000);

        //Close
        /*$('#' + maskID).click(function () {
            $(this).hide();
            $("#" + dialogID).hide();
        });*/

        //if close button is clicked
        $('#' + modalCloseButtonID).click(function (e) {
            //Cancel the link behavior
            e.preventDefault();
            $('#' + maskID).hide();
            $("#" + dialogID).hide();
        });
    }
})(jQuery);
/*
    <style type="text/css">      
        .dialogCss 
        {
            position:absolute;
            left:0;
            top:0;
            width:440px;
            height:200px;
            display:none;
            z-index:9999;
            padding:20px;
            background-color:#ffffff;
            filter:alpha(opacity=70); 
            opacity:0.7; 
            -moz-opacity:0.3;
            border:solid thin silver;
        }
    </style>
 */        