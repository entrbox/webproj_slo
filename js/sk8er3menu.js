var WM_BOTTOM = "bottom";
var WM_RIGHT = "right";

var MARGIN_BOTTOM = 1;
var MARGIN_RIGHT = 1;
var currentItem = null;
var menuTrail = new Array();
var currentStyleOff = null;

function wmItemOn(item, level, styleOn, styleOff, submenuId, submenuPosition) {

  debug("level:" + level + ", styleOn:" + styleOn + ", styleOff:" + styleOff + ", submenu:" + submenuId + "/" + submenuPosition);

  stopOffTimer();

  // turn off previous item 
  if (currentItem != null) {
    if (styleOff != currentStyleOff && currentStyleOff != null) {
      currentItem.className = currentStyleOff;
    } else {
      currentItem.className = styleOff;
    }
  }

  // make this item new current item
  currentItem = item;
  item.className = styleOn;
  currentStyleOff = styleOff;


  if (submenuId != null) {

    // take care of attached submenu

    hide(level);

    var menu = document.getElementById(submenuId);

    // item dimensions: item.offsetHeight, item.offsetWidth
    if (submenuPosition == WM_BOTTOM) {
      menu.style.top = findOffsetTop(item) + item.offsetHeight + MARGIN_BOTTOM;
      menu.style.left = findOffsetLeft(item);
    }

    if (submenuPosition == WM_RIGHT) {
      menu.style.top = findOffsetTop(item);
      menu.style.left = findOffsetLeft(item) + item.offsetWidth + MARGIN_RIGHT;
    }

    menu.style.visibility = "visible";

    menuTrail[level] = menu;
  } else {
    hide(level);
  }

}

function hide(level) {
  for (var i = level; i < menuTrail.length; i++) {
    menuTrail[i].style.visibility = "hidden";
  }
}

var timerID = null;
var timerOn = false;
var timecount = 250;

function wmStartOffTimer() {
  if (timerOn == false) {
    timerID = setTimeout("offAll()", timecount);
    timerOn = true;
  }
}

function stopOffTimer() {
  if (timerOn) {
    clearTimeout(timerID);
    timerID = null;
    timerOn = false;
  }
}

function offAll() {
  hide(0);
  
  if (currentStyleOff != null) {
    currentItem.className = currentStyleOff;
  }

  debug("All off by timer.");
}

var debugId = "wmDebug";

function debug(text) {
  var debug = document.getElementById(debugId);
  if (debug != null) {
     debug.innerHTML = "&raquo; " + text + "<br>" + debug.innerHTML;
  }
}

function findOffsetLeft(obj){
  var curleft = 0;
  if (obj.offsetParent){
    while (obj.offsetParent){
      curleft += obj.offsetLeft
        obj = obj.offsetParent;
    }
  }else if (obj.x){
    curleft += obj.x;
  }

  return curleft;
}

// Find total top offset.
function findOffsetTop(obj){
  var curtop = 0;
  if (obj.offsetParent)    {
    while (obj.offsetParent){
      curtop += obj.offsetTop
      obj = obj.offsetParent;
    }
  }else if (obj.y){
    curtop += obj.y;
  }

  return curtop;
}