function getXMLHttpRequest() {
	if (window.ActiveXObject) {
		try {
			return new ActiveXObject("Msxml2.XMLHTTP");
		} catch(e) {
			try {
				return new ActiveXObject("Microsoft.XMLHTTP");
			} catch(e1) { return null; }
		}
	} else if (window.XMLHttpRequest) {
		return new XMLHttpRequest();
	} else {
		return null;
	}
}
var httpRequest = null;

function sendRequest(url, params, callback, method) {
	httpRequest = getXMLHttpRequest();
	var httpMethod = method ? method : 'GET';
	if (httpMethod != 'GET' && httpMethod != 'POST') {
		httpMethod = 'GET';
	}
	var httpParams = (params == null || params == '') ? null : params;
	var httpUrl = url;
	if (httpMethod == 'GET' && httpParams != null) {
		httpUrl = httpUrl + "?" + httpParams;
	}
	httpRequest.open(httpMethod, httpUrl, true);
	httpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	httpRequest.onreadystatechange = callback;
	httpRequest.send(httpMethod == 'POST' ? httpParams : null);
}

	var XMLHttpRequestObject=false;
	if(window.XMLHttpRequest){
	    XMLHttpRequestObject  = new XMLHttpRequest();
		XMLHttpRequestObject1 = new XMLHttpRequest();
		XMLHttpRequestObject2 = new XMLHttpRequest();
		XMLHttpRequestObject3 = new XMLHttpRequest();
		XMLHttpRequestObject4 = new XMLHttpRequest();
	}else if(window.ActiveXObject){
		XMLHttpRequestObject  = new ActiveXObject("Microsoft.XMLHTTP");
		XMLHttpRequestObject1 = new ActiveXObject("Microsoft.XMLHTTP");
		XMLHttpRequestObject2 = new ActiveXObject("Microsoft.XMLHTTP");
		XMLHttpRequestObject3 = new ActiveXObject("Microsoft.XMLHTTP");
		XMLHttpRequestObject4 = new ActiveXObject("Microsoft.XMLHTTP");
	}

	function getGameTitleInfo(val1){
		if(XMLHttpRequestObject1){
			XMLHttpRequestObject1.open("GET","/gameInc/getGameTitleinfo.asp?id="+Math.round(Math.random()* (new Date().getTime())));
			XMLHttpRequestObject1.onreadystatechange = function (){
				if(XMLHttpRequestObject1.readyState == 4 && XMLHttpRequestObject1.status == 200){
					if (XMLHttpRequestObject1.responseText){
						imsival = XMLHttpRequestObject1.responseText;
						if (imsival!=null){
							//alert(imsival)
							var imsiff = form.gmCode.value;
							var imsiArr = imsival.split('|');
							var aa = imsiArr[0];	//구분
							var bb = imsiArr[1];	//구매회차
							var cc = imsiArr[2];	//발표회차
							var dd = imsiArr[3];	//구매시간
							var ee = imsiArr[4];	//발표시간
							var ff = imsiArr[5];	//직렬1,직렬2,질렬3  - 번호 5개
							var gg = imsiArr[6];	//현재시간
							var notnumflag = form.notnumflag.value;	//notnumflag : y/n
							//
							//printInhtm("topRealTime",gg);					//현재시간
							printInhtm("gmtitleTime",ee);					//발표남은시간
							printInhtm("gmtitleNum","<font color=#aeaeae>제 " + cc + " 회</font>");		//발표회차
							printInhtm("gmtitleNum2","제 " + bb + " 회");	//구매회차
							printInhtm("orLastText",dd);					//구매남은시간
							//
							//최근당첨번호 / 공돌림
							var js_ballFullcnt = form.ballFullcnt.value;
							var js_listFlag    = form.listFlag.value;
							var js_endFlag     = form.endFlag.value;
							if (val1=="0"){
								getNewWinNumList();
								if (notnumflag=="y"){NotNumList();}
								form.listFlag.value="";
								//
								revolBallChb("2","","");
								form.ballFullcnt.value = "";
							}else{
								if (aa=="c1") {	//3분전
									if (js_ballFullcnt==""){
										revolBallChb("1",ff,"1");
										form.ballFullcnt.value = "1";
									}
									//
									if (js_listFlag==""){
										getNewWinNumList();
										if (notnumflag=="y"){NotNumList();}
										form.listFlag.value="1";
										//팝업띄우기
										//if (js_endFlag=="" && dd=="<font size=6>마감</font>"){
										if (js_endFlag=="" && dd=="<b>마감</b>"){
											lpopmake(cc,bb);
											form.endFlag.value="2";
										}
									}
								}else{			//c2
									if (aa=="a2"){
										getNewWinNumList();
										if (notnumflag=="y"){NotNumList();}
										form.listFlag.value="";
									}
									if (ff==",,,,"){
										if (js_ballFullcnt!=""){
											revolBallChb("2","","");
											form.ballFullcnt.value = "";
											form.endFlag.value="";
										}
									}else{
										if (js_ballFullcnt==""){
											revolBallChb("1",ff,"1");
											form.ballFullcnt.value = "1";
										}
										//////////////////////////////////////////////////////
										if (dd != "undefined" && dd != "" && dd != "null"){
											if (dd.indexOf(">10<")>0 || dd.indexOf(">09<")>0 || dd.indexOf(">08<")>0 || dd.indexOf(">07<")>0 || dd.indexOf(">06<")>0 || dd.indexOf(">05<")>0 || dd.indexOf(">04<")>0 || dd.indexOf(">03<")>0 || dd.indexOf(">02<")>0 || dd.indexOf(">01<")>0){
												playonline("count.wav");
											}
										}
									}
								}
							}
							//
							form.gmCode.value = bb;							//게임회차값
							//
						}
					}
				}
			}
			XMLHttpRequestObject1.send();
		}
		setTimeout("getGameTitleInfo();", 1000);
	}

	function getNewWinNumList(){
		if(XMLHttpRequestObject2){
			XMLHttpRequestObject2.open("GET","/gameInc/getGameNewNumList.asp?id="+Math.round(Math.random()* (new Date().getTime())));
			XMLHttpRequestObject2.onreadystatechange = function (){
				if(XMLHttpRequestObject2.readyState == 4 && XMLHttpRequestObject2.status == 200){
					if (XMLHttpRequestObject2.responseText){
						imsival = XMLHttpRequestObject2.responseText;
						if (imsival!=null){
							printInhtm("newWinNumList",imsival);
						}
					}
				}
			}
			XMLHttpRequestObject2.send();
		}
	}

	function NotNumList(){
		if(XMLHttpRequestObject3){
			XMLHttpRequestObject3.open("GET","/gameInc/getNotNumList.asp?id="+Math.round(Math.random()* (new Date().getTime())));
			XMLHttpRequestObject3.onreadystatechange = function (){
				if(XMLHttpRequestObject3.readyState == 4 && XMLHttpRequestObject3.status == 200){
					if (XMLHttpRequestObject3.responseText){
						imsival = XMLHttpRequestObject3.responseText;
						if (imsival!=null){
							//[[changeNum]]
							imsival1 = imsival.replace("[[changeNum]]","")
							imsival2 = imsival.replace("[[changeNum]]","2")
							//alert(imsival2)
							imsival3 = imsival.replace("[[changeNum]]","3")
							imsival4 = imsival.replace("[[changeNum]]","4")
							imsival5 = imsival.replace("[[changeNum]]","5")
							imsival6 = imsival.replace("[[changeNum]]","6")
							imsival7 = imsival.replace("[[changeNum]]","7")
							imsival8 = imsival.replace("[[changeNum]]","8")
							imsival9 = imsival.replace("[[changeNum]]","9")
							imsival10 = imsival.replace("[[changeNum]]","10")
							imsival11 = imsival.replace("[[changeNum]]","11")
							imsival12 = imsival.replace("[[changeNum]]","12")

							printInhtm("NotNumid",imsival1);
							printInhtm("NotNumid2",imsival2);
							printInhtm("NotNumid3",imsival3);
							printInhtm("NotNumid4",imsival4);
							printInhtm("NotNumid5",imsival5);
							printInhtm("NotNumid6",imsival6);
							printInhtm("NotNumid7",imsival7);
							printInhtm("NotNumid8",imsival8);
							printInhtm("NotNumid9",imsival9);
							printInhtm("NotNumid10",imsival10);
							printInhtm("NotNumid11",imsival11);
							printInhtm("NotNumid12",imsival12);
						}
					}
				}
			}
			XMLHttpRequestObject3.send();
		}
	}

	function playonline(url){
		var js_soriflag = form.soriflag.value;
		var tt;
		var obj=document.getElementById("bsound");

		if (js_soriflag=="on"){
			obj.src="/sori/"+url;
		}else{
			obj.src="";
		}
	}

	function printInhtm(val1,val2){
		//alert(val1)
		inhtm = document.getElementById(val1);
		inhtm.innerHTML = val2;
	}

	function revolBallChb(val1,val2,val3){
		if (val2!=null){
			var ffArr = val2.split(",");
			var ff1 = ffArr[0];
			var ff2 = ffArr[1];
			var ff3 = ffArr[2];
			var ff4 = ffArr[3];
			var ff5 = ffArr[4];
			//
			if (val1=="1"){
				if (ff1!=="" && val3=="1"){printInhtm("ballSee1","<img src='/images/main/ball/ball_88_87_red_"    + ff1 + ".jpg' width='52' height='52'>");playonline("count.wav");}
				if (ff2!=="" && val3=="2"){printInhtm("ballSee2","<img src='/images/main/ball/ball_88_87_yellow_" + ff2 + ".jpg' width='52' height='52'>");}
				if (ff3!=="" && val3=="3"){printInhtm("ballSee3","<img src='/images/main/ball/ball_88_87_blue_"   + ff3 + ".jpg' width='52' height='52'>");}
				if (ff4!=="" && val3=="4"){printInhtm("ballSee4","<img src='/images/main/ball/ball_88_87_green_"  + ff4 + ".jpg' width='52' height='52'>");}
				if (ff5!=="" && val3=="5"){printInhtm("ballSee5","<img src='/images/main/ball/ball_88_87_purple_" + ff5 + ".jpg' width='52' height='52'>");}
			}else if (val1=="2"){
				printInhtm("ballSee1","<img src='/images/main/ball/allRevol1.gif' width='52' height='52'>");
				printInhtm("ballSee2","<img src='/images/main/ball/allRevol2.gif' width='52' height='52'>");
				printInhtm("ballSee3","<img src='/images/main/ball/allRevol3.gif' width='52' height='52'>");
				printInhtm("ballSee4","<img src='/images/main/ball/allRevol4.gif' width='52' height='52'>");
				printInhtm("ballSee5","<img src='/images/main/ball/allRevol5.gif' width='52' height='52'>");
				form.ballFullcnt.value = "";
				//
				printInhtm("jlNum1","");	//직렬1 초기화
				printInhtm("jlNum2","");	//직렬2 초기화
				printInhtm("jlNum3","");	//직렬3 초기화
			}
		}
		val4 = Number(val3)+1;
		//alert(val4)
		if (val4<7 && val2!=""){
			if (val4==6){
				setTimeout("revolBallChb('"+val1+"','"+val2+"','"+val4+"');", 4000);
			}else{
				setTimeout("revolBallChb('"+val1+"','"+val2+"','"+val4+"');", 1500);
			}
			//최근당첨번호
			if (val4==6){
				var notnumflag = form.notnumflag.value;	//notnumflag : y/n
				setTimeout("getNewWinNumList();", 4000);
				if (notnumflag=="y"){setTimeout("NotNumList();", 1000);}
				form.listFlag.value="";
				//
				if (ff1!=""){printInhtm("jlNum1",ff1);}else{printInhtm("jlNum1","");}							//직렬1
				if (ff2!=""){printInhtm("jlNum2",ff1 + ', ' + ff2);}else{printInhtm("jlNum2","");}				//직렬2
				if (ff3!=""){printInhtm("jlNum3",ff1 + ', ' + ff2 + ', ' + ff3);}else{printInhtm("jlNum3","");}	//직렬3
			}
		}
	}

	function lpopmake(val1,val2){
		printInhtm("lypopnum1",val1);
		printInhtm("lypopnum2",val2);
		layer_open('layer2');
		setTimeout("lpopmakeOut();", 3000);
	}

	function lpopmakeOut(){
		$('.cbtn').click();
	}

	function soriChb(){
		var js_soriflag = form.soriflag.value;	//on,off
		if (js_soriflag=="on"){
			printInhtm("soriimg","<img src='/Images/main/btn_sound_mute.gif' width='56' height='22' onclick='soriChb()' style='cursor:pointer;'>");
			form.soriflag.value = "off";
		}else{
			printInhtm("soriimg","<img src='/images/main/btn_sound.png' width='56' height='22' onclick='soriChb()' style='cursor:pointer;'>");
			form.soriflag.value = "on";
		}
	}