﻿; (function ($) {
    $.viewPopup = function (dialogID, modalCloseButtonID, popIndex) {
        //Get the window height and width
        var winH = $(window).height();
        var winW = $(window).width();

        //Set the popup window to center
        //$("#" + dialogID).css('top', winH / 2 - $("#" + dialogID).height() / 2);
        if (popIndex=='2')
        {
            $("#" + dialogID).css('top', 150);
			$("#" + dialogID).css('left', winW / 2 + 5);
		}
		else
		{
	        $("#" + dialogID).css('top', 150);
			$("#" + dialogID).css('left', winW / 2 - $("#" + dialogID).width() - 5);			
		}
        //transition effect
        $("#" + dialogID).fadeIn(1000);

        //if close button is clicked
        $('#' + modalCloseButtonID).click(function (e) {
            //Cancel the link behavior
            e.preventDefault();
            $("#" + dialogID).hide();
        });
    }   
})(jQuery);
        