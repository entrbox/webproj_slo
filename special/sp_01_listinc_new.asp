<script type="text/javascript">
	var js_sp_price	   = "<%=sp_price%>";
	var js_sp_maxorcnt = "<%=sp_maxorcnt%>";
	var js_sp_hper     = "<%=sp_hper%>";	//홀
	var js_sp_jper     = "<%=sp_jper%>";	//짝
	var js_sp_dper     = "<%=sp_dper%>";	//대
	var js_sp_sper     = "<%=sp_sper%>";	//소

	var Hresetimg1_a = "/images/special/colorball_88_red_odd.png" ;
	var Hresetimg1_b = "/images/special/colorball_88_red_even.png" ;
	var Hresetimg2_a = "/images/special/colorball_88_yellow_odd.png" ;
	var Hresetimg2_b = "/images/special/colorball_88_yellow_even.png" ;
	var Hresetimg3_a = "/images/special/colorball_88_green_odd.png" ;
	var Hresetimg3_b = "/images/special/colorball_88_green_even.png" ;
	var Hresetimg4_a = "/images/special/colorball_88_blue_odd.png" ;
	var Hresetimg4_b = "/images/special/colorball_88_blue_even.png" ;
	var Hresetimg5_a = "/images/special/colorball_88_purple_odd.png" ;
	var Hresetimg5_b = "/images/special/colorball_88_purple_even.png" ;

	var Dresetimg1_a = "/images/special/colorball_88_red_big.png" ;
	var Dresetimg1_b = "/images/special/colorball_88_red_small.png" ;
	var Dresetimg2_a = "/images/special/colorball_88_yellow_big.png" ;
	var Dresetimg2_b = "/images/special/colorball_88_yellow_small.png" ;
	var Dresetimg3_a = "/images/special/colorball_88_green_big.png" ;
	var Dresetimg3_b = "/images/special/colorball_88_green_small.png" ;
	var Dresetimg4_a = "/images/special/colorball_88_blue_big.png" ;
	var Dresetimg4_b = "/images/special/colorball_88_blue_small.png" ;
	var Dresetimg5_a = "/images/special/colorball_88_purple_big.png" ;
	var Dresetimg5_b = "/images/special/colorball_88_purple_small.png" ;

	var imgPath     = "/images/special/";
	var hj_outImg_h = "odd_g.png";	//홀
	var hj_outImg_j = "even_g.png";
	var ds_outImg_d = "big_g.png";	//대
	var ds_outImg_s = "small_g.png";

	function onlyNumber(){
		if((event.keyCode<48)||(event.keyCode>57))
		event.returnValue=false;
	}

// 지정자리 반올림 (값, 자릿수)
function Round(n, pos) {
	var digits = Math.pow(10, pos);

	var sign = 1;
	if (n < 0) {
		sign = -1;
	}

	// 음수이면 양수처리후 반올림 한 후 다시 음수처리
	n = n * sign;
	var num = Math.round(n * digits) / digits;
	num = num * sign;

	return num.toFixed(pos);
}

// 지정자리 버림 (값, 자릿수)
function Floor(n, pos) {
	var digits = Math.pow(10, pos);

	var num = Math.floor(n * digits) / digits;

	return num.toFixed(pos);
}

// 지정자리 올림 (값, 자릿수)
function Ceiling(n, pos) {
	var digits = Math.pow(10, pos);

	var num = Math.ceil(n * digits) / digits;

	return num.toFixed(pos);
}

	function jsCommaChb(val1){
		aa = Number(val1).toLocaleString().split(".")[0];
		return aa;
	}

	function show_layer_new_s(obj, n, v){	 //show_layer(id, 갯수, 보여줄레이어)
		for (i=1;i<6;i++ ){
			document.getElementById('mm' + i).src = "/images/special/menu0" + i + "_off.jpg" ;
		}
		//
		form.gmKind.value     = v;	//겜구분
		form.gmKind_sub.value = "1";
		//
		sub_show_layer_reset();
		document.getElementById('mm' + v).src = "/images/special/menu0" + v + "_on.jpg" ;	
		document.getElementById(obj + v).style.display='block' ;
		sub_show_layer_class(v);
		sub_show_layer_subdiv(v);
		//
	}

	function sub_show_layer(val1,val2){
		if (val2=="1"){
			document.getElementById(val1+'1').className = "imitest";
			document.getElementById(val1+'2').className = "";
		}else{
			document.getElementById(val1+'1').className = "";
			document.getElementById(val1+'2').className = "imitest";
		}
		form.gmKind_sub.value=val2;	
		//
		document.getElementById("sgame1").style.display='none' ;
		document.getElementById("sgame2").style.display='none' ;
		document.getElementById("sgame3").style.display='none' ;
		document.getElementById("sgame4").style.display='none' ;
		document.getElementById("sgame5").style.display='none' ;
		document.getElementById("sgame6").style.display='none' ;
		document.getElementById("sgame7").style.display='none' ;
		document.getElementById("sgame8").style.display='none' ;
		document.getElementById("sgame9").style.display='none' ;
		document.getElementById("sgame10").style.display='none' ;
		//
		if (val1=="sub_aa" && val2=="1"){document.getElementById("sgame1").style.display='block' ;}
		else if (val1=="sub_aa" && val2=="2"){document.getElementById("sgame2").style.display='block' ;}
		else if (val1=="sub_bb" && val2=="1"){document.getElementById("sgame3").style.display='block' ;}
		else if (val1=="sub_bb" && val2=="2"){document.getElementById("sgame4").style.display='block' ;}
		else if (val1=="sub_cc" && val2=="1"){document.getElementById("sgame5").style.display='block' ;}
		else if (val1=="sub_cc" && val2=="2"){document.getElementById("sgame6").style.display='block' ;}
		else if (val1=="sub_dd" && val2=="1"){document.getElementById("sgame7").style.display='block' ;}
		else if (val1=="sub_dd" && val2=="2"){document.getElementById("sgame8").style.display='block' ;}
		else if (val1=="sub_ee" && val2=="1"){document.getElementById("sgame9").style.display='block' ;}
		else if (val1=="sub_ee" && val2=="2"){document.getElementById("sgame10").style.display='block' ;}
		//
		divChbReset();			//볼 초기화
		spPerChb("2",val2);
	}

	function sub_show_layer_reset(){
		document.getElementById("sub1").style.display='none' ;
		document.getElementById("sub2").style.display='none' ;
		document.getElementById("sub3").style.display='none' ;
		document.getElementById("sub4").style.display='none' ;
		document.getElementById("sub5").style.display='none' ;

		document.getElementById('sub_aa1').className = "";
		document.getElementById('sub_aa2').className = "";
		document.getElementById('sub_bb1').className = "";
		document.getElementById('sub_bb2').className = "";
		document.getElementById('sub_cc1').className = "";
		document.getElementById('sub_cc2').className = "";
		document.getElementById('sub_dd1').className = "";
		document.getElementById('sub_dd2').className = "";
		document.getElementById('sub_ee1').className = "";
		document.getElementById('sub_ee2').className = "";

		document.getElementById("sgame1").style.display='none' ;
		document.getElementById("sgame2").style.display='none' ;
		document.getElementById("sgame3").style.display='none' ;
		document.getElementById("sgame4").style.display='none' ;
		document.getElementById("sgame5").style.display='none' ;
		document.getElementById("sgame6").style.display='none' ;
		document.getElementById("sgame7").style.display='none' ;
		document.getElementById("sgame8").style.display='none' ;
		document.getElementById("sgame9").style.display='none' ;
		document.getElementById("sgame10").style.display='none' ;

		divChbReset();		//볼 초기화
		return;
	}

	function sub_show_layer_class(v){
		if (v=="1"){document.getElementById("sub_aa1").className = "imitest";}
		else if (v=="2"){document.getElementById("sub_bb1").className = "imitest";}
		else if (v=="3"){document.getElementById("sub_cc1").className = "imitest";}
		else if (v=="4"){document.getElementById("sub_dd1").className = "imitest";}
		else if (v=="5"){document.getElementById("sub_ee1").className = "imitest";}
		return;
	}

	function sub_show_layer_subdiv(v){
		if (v=="1"){document.getElementById("sub_aa1").className = "imitest";      document.getElementById("sgame1").style.display='block';}
		else if (v=="2"){document.getElementById("sub_bb1").className = "imitest"; document.getElementById("sgame3").style.display='block';}
		else if (v=="3"){document.getElementById("sub_cc1").className = "imitest"; document.getElementById("sgame5").style.display='block';}
		else if (v=="4"){document.getElementById("sub_dd1").className = "imitest"; document.getElementById("sgame7").style.display='block';}
		else if (v=="5"){document.getElementById("sub_ee1").className = "imitest"; document.getElementById("sgame9").style.display='block';}
		return;
	}

	function div1Chb(val1,val2,val3){
		if (val3=="1"){
			valImg1 = "/images/special/colorball_88_red_odd.png" ;
			valImg2 = "/images/special/colorball_88_red_even.png" ;
			form.choice1.value=val1;
		}else if (val3=="2"){
			valImg1 = "/images/special/colorball_88_yellow_odd.png" ;
			valImg2 = "/images/special/colorball_88_yellow_even.png" ;
			form.choice2.value=val1;
		}else if (val3=="3"){
			valImg1 = "/images/special/colorball_88_green_odd.png" ;
			valImg2 = "/images/special/colorball_88_green_even.png" ;
			form.choice3.value=val1;
		}else if (val3=="4"){
			valImg1 = "/images/special/colorball_88_blue_odd.png" ;
			valImg2 = "/images/special/colorball_88_blue_even.png" ;
			form.choice4.value=val1;
		}else if (val3=="5"){
			valImg1 = "/images/special/colorball_88_purple_odd.png" ;
			valImg2 = "/images/special/colorball_88_purple_even.png" ;
			form.choice5.value=val1;
		}
		//
		document.getElementById(val2 + "_1").src = valImg1 ;
		document.getElementById(val2 + "_2").src = valImg2 ;
		//
		if (val1=="1"){
			document.getElementById(val2 + "_2").src = "/images/special/even_g.png" ;
		}else{
			document.getElementById(val2 + "_1").src = "/images/special/odd_g.png" ;
		}
		spPerChb("3","");
	}

	function div2Chb(val1,val2,val3){
		if (val3=="1"){
			valImg1 = "/images/special/colorball_88_red_big.png" ;
			valImg2 = "/images/special/colorball_88_red_small.png" ;
			form.choice1.value=val1;
		}else if (val3=="2"){
			valImg1 = "/images/special/colorball_88_yellow_big.png" ;
			valImg2 = "/images/special/colorball_88_yellow_small.png" ;
			form.choice2.value=val1;
		}else if (val3=="3"){
			valImg1 = "/images/special/colorball_88_green_big.png" ;
			valImg2 = "/images/special/colorball_88_green_small.png" ;
			form.choice3.value=val1;
		}else if (val3=="4"){
			valImg1 = "/images/special/colorball_88_blue_big.png" ;
			valImg2 = "/images/special/colorball_88_blue_small.png" ;
			form.choice4.value=val1;
		}else if (val3=="5"){
			valImg1 = "/images/special/colorball_88_purple_big.png" ;
			valImg2 = "/images/special/colorball_88_purple_small.png" ;
			form.choice5.value=val1;
		}
		//
		document.getElementById(val2 + "_1").src = valImg1;
		document.getElementById(val2 + "_2").src = valImg2;
		//
		if (val1=="1"){
			document.getElementById(val2 + "_2").src = "/images/special/small_g.png" ;
		}else{
			document.getElementById(val2 + "_1").src = "/images/special/big_g.png" ;
		}
		spPerChb("3","");
	}

	function divChbReset(){
		document.getElementById("div1_1").src  = Hresetimg1_a;
		document.getElementById("div1_2").src  = Hresetimg1_b;

		document.getElementById("div2_1").src  = Dresetimg1_a;
		document.getElementById("div2_2").src  = Dresetimg1_b;

		document.getElementById("div3_1").src  = Hresetimg1_a;
		document.getElementById("div3_2").src  = Hresetimg1_b;
		document.getElementById("div32_1").src = Hresetimg2_a;
		document.getElementById("div32_2").src = Hresetimg2_b;

		document.getElementById("div4_1").src  = Dresetimg1_a;
		document.getElementById("div4_2").src  = Dresetimg1_b;
		document.getElementById("div42_1").src  = Dresetimg2_a;
		document.getElementById("div42_2").src  = Dresetimg2_b;

		document.getElementById("div5_1").src  = Hresetimg1_a;
		document.getElementById("div5_2").src  = Hresetimg1_b;
		document.getElementById("div52_1").src = Hresetimg2_a;
		document.getElementById("div52_2").src = Hresetimg2_b;
		document.getElementById("div53_1").src = Hresetimg3_a;
		document.getElementById("div53_2").src = Hresetimg3_b;

		document.getElementById("div6_1").src  = Dresetimg1_a;
		document.getElementById("div6_2").src  = Dresetimg1_b;
		document.getElementById("div62_1").src  = Dresetimg2_a;
		document.getElementById("div62_2").src  = Dresetimg2_b;
		document.getElementById("div63_1").src  = Dresetimg3_a;
		document.getElementById("div63_2").src  = Dresetimg3_b;

		document.getElementById("div7_1").src  = Hresetimg1_a;
		document.getElementById("div7_2").src  = Hresetimg1_b;
		document.getElementById("div72_1").src = Hresetimg2_a;
		document.getElementById("div72_2").src = Hresetimg2_b;
		document.getElementById("div73_1").src = Hresetimg3_a;
		document.getElementById("div73_2").src = Hresetimg3_b;
		document.getElementById("div74_1").src = Hresetimg4_a;
		document.getElementById("div74_2").src = Hresetimg4_b;

		document.getElementById("div8_1").src  = Dresetimg1_a;
		document.getElementById("div8_2").src  = Dresetimg1_b;
		document.getElementById("div82_1").src  = Dresetimg2_a;
		document.getElementById("div82_2").src  = Dresetimg2_b;
		document.getElementById("div83_1").src  = Dresetimg3_a;
		document.getElementById("div83_2").src  = Dresetimg3_b;
		document.getElementById("div84_1").src  = Dresetimg4_a;
		document.getElementById("div84_2").src  = Dresetimg4_b;

		document.getElementById("div9_1").src  = Hresetimg1_a;
		document.getElementById("div9_2").src  = Hresetimg1_b;
		document.getElementById("div92_1").src = Hresetimg2_a;
		document.getElementById("div92_2").src = Hresetimg2_b;
		document.getElementById("div93_1").src = Hresetimg3_a;
		document.getElementById("div93_2").src = Hresetimg3_b;
		document.getElementById("div94_1").src = Hresetimg4_a;
		document.getElementById("div94_2").src = Hresetimg4_b;
		document.getElementById("div95_1").src = Hresetimg5_a;
		document.getElementById("div95_2").src = Hresetimg5_b;

		document.getElementById("div10_1").src  = Dresetimg1_a;
		document.getElementById("div10_2").src  = Dresetimg1_b;
		document.getElementById("div102_1").src  = Dresetimg2_a;
		document.getElementById("div102_2").src  = Dresetimg2_b;
		document.getElementById("div103_1").src  = Dresetimg3_a;
		document.getElementById("div103_2").src  = Dresetimg3_b;
		document.getElementById("div104_1").src  = Dresetimg4_a;
		document.getElementById("div104_2").src  = Dresetimg4_b;
		document.getElementById("div105_1").src  = Dresetimg5_a;
		document.getElementById("div105_2").src  = Dresetimg5_b;
		//
		form.choice1.value="";
		form.choice2.value="";
		form.choice3.value="";
		form.choice4.value="";
		form.choice5.value="";
		//
		spPerChb("1",form.gmKind_sub.value);
	}

	function spPerChb(val1,val2){
		var js_gmKind     = form.gmKind.value;
		var js_gmKind_sub = form.gmKind_sub.value;
		var js_choice1    = form.choice1.value;
		var js_choice2    = form.choice2.value;
		var js_choice3    = form.choice3.value;
		var js_choice4    = form.choice4.value;
		var js_choice5    = form.choice5.value;
		var basicMoney = js_sp_price;

		if (val1=="1"){	    //리셋시
			if (val2=="1"){	//홀짝
				exWinMoney = Round(basicMoney * js_sp_hper,0);
			}else{			//대소
				exWinMoney = Round(basicMoney * js_sp_dper,0);
			}
		}else if (val1=="2"){	//서브메뉴 클릭시
			if (val2=="1"){	//홀짝
				exWinMoney = Round(basicMoney * js_sp_hper,0);
			}else{			//대소
				exWinMoney = Round(basicMoney * js_sp_dper,0);
			}
		}else if (val1=="3"){	//볼 클릭시
			gmchoicePerVal = gmchoicePerValChb();
			exWinMoney = Round(basicMoney * gmchoicePerVal,0);
		}
		//
		printInhtm("basicMoney_htm",jsCommaChb(basicMoney)+'원');
		printInhtm("exWinMoney_htm",jsCommaChb(exWinMoney)+'원');
	}

	var oTbl; 
	function sp_cartok(){
		//
		if (form.gmKind.value==""){	alert("게임을 선택해주세요.!");	return false;}
		if ((form.gmKind.value=="1")&&(form.choice1.value=="")){	alert("번호를 선택해주세요.!");	return false;}
		if ((form.gmKind.value=="2")&&(form.choice1.value==""||form.choice2.value=="")){alert("번호를 선택해주세요.!");	return false;}
		if ((form.gmKind.value=="3")&&(form.choice1.value==""||form.choice2.value==""||form.choice3.value=="")){alert("번호를 선택해주세요.!");return false;}
		if ((form.gmKind.value=="4")&&(form.choice1.value==""||form.choice2.value==""||form.choice3.value==""||form.choice4.value=="")){alert("번호를 선택해주세요.!");return false;}
		if ((form.gmKind.value=="5")&&(form.choice1.value==""||form.choice2.value==""||form.choice3.value==""||form.choice4.value==""||form.choice5.value=="")){alert("번호를 선택해주세요.!");return false;}
		//
		var gmtitlename    = gmtitleNameChb();		//겜타이틀
		var gmchoiceVal    = gmchoiceValChb();		//겜선택사항
		var gmchoicePerVal = gmchoicePerValChb()	//배율
		//
		oTbl = document.getElementById("addTable"); 
		var oRow = oTbl.insertRow(); 

		trCnt = oTbl.rows.length-1;	//전체tr갯수
		oRow.onmouseover=function(){oTbl.clickedRowIndex=this.rowIndex}; //clickedRowIndex - 클릭한 Row의 위치를 확인;
		//수량만 카운터
		var ingflag = "y";
		for(var i=1; i<oTbl.rows.length; i++) {
			if (document.getElementById("gmtitlename_" + i)){
				js_trval1 = document.getElementById("gmtitlename_" + i).value;
				js_trval2 = document.getElementById("gmchoiceVal_"+ i).value;

				if (js_trval1==gmtitlename && js_trval2==gmchoiceVal){
					js_orcnt = Number(document.getElementById("orCnt_" + i).value);
					js_orcnt = js_orcnt+1;

					document.getElementById("orCnt_" + i).value = js_orcnt; //수량변경
					inhtm = document.getElementById(i);
					inhtm.innerHTML = jsCommaChb(gmchoicePerVal*(js_orcnt*js_sp_price));
					ingflag="n";
				}
			}
		}
		//
		if (ingflag == "y"){
			var oCell1 = oRow.insertCell(0); 
			var oCell2 = oRow.insertCell(1); 
			var oCell3 = oRow.insertCell(2); 
			var oCell4 = oRow.insertCell(3); 
			var oCell5 = oRow.insertCell(4); 
			var oCell6 = oRow.insertCell(5); 
			var oCell7 = oRow.insertCell(6); 
			var oCell8 = oRow.insertCell(7); 
			var oCell9 = oRow.insertCell(8); 
			var oCell10 = oRow.insertCell(9); 
			var oCell11 = oRow.insertCell(10); 
	
			oCell1.setAttribute("class","box4_d"); 
			oCell2.setAttribute("class","area");
			oCell3.setAttribute("class","box4_d"); 
			oCell4.setAttribute("class","area"); 
			oCell5.setAttribute("class","box4_d"); 
			oCell6.setAttribute("class","area"); 
			oCell7.setAttribute("class","box4_d"); 
			oCell8.setAttribute("class","area"); 
			oCell9.setAttribute("class","box4_d"); 
			oCell10.setAttribute("class","area"); 
			oCell11.setAttribute("class","box4_d"); 

			oCell1.innerHTML = gmtitlename;
			oCell2.innerHTML = "<div></div>"; 
			oCell3.innerHTML = "<input name='orCnt[]' id='orCnt_"+trCnt+"' type='text' style='width:50px;text-align:center' value='1' class='input2' maxlength=4 onkeypress='onlyNumber()' onkeyup='cntEdit("+trCnt+",this.value);'>"; 
			oCell4.innerHTML = "<div></div>"; 
			oCell5.innerHTML = gmchoiceVal + "<input type=hidden name='orNum[]' value='"+gmchoiceVal+"' id='gmchoiceVal_"+trCnt+"'><input type=hidden name='gmKind[]' id='gmKind_"+trCnt+"' value='"+form.gmKind.value+"'><input type=hidden name='gmKind_sub[]' id='gmKind_sub_"+trCnt+"' value='"+form.gmKind_sub.value+"'><input type=hidden name='gmchoicePer[]' id='gmchoicePer_"+trCnt+"' value='"+gmchoicePerVal+"'><input type=hidden name='gmtitlename[]' id='gmtitlename_"+trCnt+"' value='"+gmtitlename+"'>";
			oCell6.innerHTML = "<div></div>"; 
			oCell7.innerHTML = gmchoicePerVal; 
			oCell8.innerHTML = "<div></div>"; 
			oCell9.innerHTML = "<span id='"+trCnt+"'>" + jsCommaChb(Round(gmchoicePerVal*js_sp_price,0)) + '</span>원'; 
			oCell10.innerHTML = "<div></div>"; 
			oCell11.innerHTML = "<img src='/images/special/icon_delet.gif' alt='삭제' width='21' height='19' align='absmiddle' onClick='removeRow()' style='cursor:pointer'>"; 
			trCnt = trCnt+1;
		}
		//초기화
		divChbReset();
		//계산다시
		totOrMoneychb();
	}

	//Row 삭제 
	function removeRow() { 
		oTbl.deleteRow(oTbl.clickedRowIndex);
		totOrMoneychb();
	} 

	function totOrMoneychb(){	//합계금액
		var totOrcnt = 0;
		for( var i = 0; i <= form.elements.length - 1; i++ ){ 
			if( form.elements[i].name == "orCnt[]" ){
				totOrcnt = totOrcnt + Number(form.elements[i].value);
			}
		}
		var totOrcntMoney = totOrcnt * js_sp_price;
		inhtm = document.getElementById("orMoney");
		inhtm.innerHTML = "총 구매금액 " + jsCommaChb(totOrcntMoney) + " 원";
		form.orderMoney.value=totOrcntMoney;
	}

	function cntEdit(val1,val2){	//수량수정시 예상가격 합계
		var bb="";
		var imsinum = Number(val1)-1;
		var ff = document.getElementById("gmchoicePer_"+val1).value;

		inhtm = document.getElementById(val1);
		inhtm.innerHTML = jsCommaChb(ff*(val2*js_sp_price));

		totOrMoneychb();
	}

	function gmtitleNameChb(){
		var js_gmKind       = form.gmKind.value;
		var js_gmKind_sub   = form.gmKind_sub.value;
		var js_gmKind_title = "직렬" +js_gmKind+ " 스페셜 ";
		if (js_gmKind_sub=="1"){
			js_gmKind_sub_title = "홀짝";
		}else{
			js_gmKind_sub_title = "대소";
		}
		ortitle = js_gmKind_title + js_gmKind_sub_title;
		return ortitle;
	}

	function gmchoiceValChb(){
		var js_gmKind_sub = form.gmKind_sub.value;
		var js_choice1    = form.choice1.value;
		var js_choice2    = form.choice2.value;
		var js_choice3    = form.choice3.value;
		var js_choice4    = form.choice4.value;
		var js_choice5    = form.choice5.value;
		var js_choice1Title = ""
		var js_choice2Title = ""
		var js_choice3Title = ""
		var js_choice4Title = ""
		var js_choice5Title = ""
		if (js_gmKind_sub=="1"){
			if (js_choice1 == "1"){js_choice1Title = "홀";}else if (js_choice1 == "2"){js_choice1Title = "짝";}
			if (js_choice2 == "1"){js_choice2Title = "홀";}else if (js_choice2 == "2"){js_choice2Title = "짝";}
			if (js_choice3 == "1"){js_choice3Title = "홀";}else if (js_choice3 == "2"){js_choice3Title = "짝";}
			if (js_choice4 == "1"){js_choice4Title = "홀";}else if (js_choice4 == "2"){js_choice4Title = "짝";}
			if (js_choice5 == "1"){js_choice5Title = "홀";}else if (js_choice5 == "2"){js_choice5Title = "짝";}
		}else{
			if (js_choice1 == "1"){js_choice1Title = "대";}else if (js_choice1 == "2"){js_choice1Title = "소";}
			if (js_choice2 == "1"){js_choice2Title = "대";}else if (js_choice2 == "2"){js_choice2Title = "소";}
			if (js_choice3 == "1"){js_choice3Title = "대";}else if (js_choice3 == "2"){js_choice3Title = "소";}
			if (js_choice4 == "1"){js_choice4Title = "대";}else if (js_choice4 == "2"){js_choice4Title = "소";}
			if (js_choice5 == "1"){js_choice5Title = "대";}else if (js_choice5 == "2"){js_choice5Title = "소";}
		}
		ortitle = js_choice1Title + js_choice2Title + js_choice3Title + js_choice4Title + js_choice5Title;
		return ortitle;
	}

	function gmchoicePerValChb(){
		var js_gmKind_sub = form.gmKind_sub.value;
		var js_choice1    = form.choice1.value;
		var js_choice2    = form.choice2.value;
		var js_choice3    = form.choice3.value;
		var js_choice4    = form.choice4.value;
		var js_choice5    = form.choice5.value;
		var js_choice1Per = 0;
		var js_choice2Per = 0;
		var js_choice3Per = 0;
		var js_choice4Per = 0;
		var js_choice5Per = 0;
		var ortitle = 1;

		if (js_gmKind_sub=="1"){
			if (js_choice1 == "1"){js_choice1Per = js_sp_hper;}else if (js_choice1 == "2"){js_choice1Per = js_sp_jper;}
			if (js_choice2 == "1"){js_choice2Per = js_sp_hper;}else if (js_choice2 == "2"){js_choice2Per = js_sp_jper;}
			if (js_choice3 == "1"){js_choice3Per = js_sp_hper;}else if (js_choice3 == "2"){js_choice3Per = js_sp_jper;}
			if (js_choice4 == "1"){js_choice4Per = js_sp_hper;}else if (js_choice4 == "2"){js_choice4Per = js_sp_jper;}
			if (js_choice5 == "1"){js_choice5Per = js_sp_hper;}else if (js_choice5 == "2"){js_choice5Per = js_sp_jper;}
		}else{
			if (js_choice1 == "1"){js_choice1Per = js_sp_dper;}else if (js_choice1 == "2"){js_choice1Per = js_sp_sper;}
			if (js_choice2 == "1"){js_choice2Per = js_sp_dper;}else if (js_choice2 == "2"){js_choice2Per = js_sp_sper;}
			if (js_choice3 == "1"){js_choice3Per = js_sp_dper;}else if (js_choice3 == "2"){js_choice3Per = js_sp_sper;}
			if (js_choice4 == "1"){js_choice4Per = js_sp_dper;}else if (js_choice4 == "2"){js_choice4Per = js_sp_sper;}
			if (js_choice5 == "1"){js_choice5Per = js_sp_dper;}else if (js_choice5 == "2"){js_choice5Per = js_sp_sper;}
		}
		if (js_choice1Per>0){	ortitle = js_choice1Per;}
		if (js_choice2Per>0){	ortitle = ortitle*js_choice2Per;}
		if (js_choice3Per>0){	ortitle = ortitle*js_choice3Per;}
		if (js_choice4Per>0){	ortitle = ortitle*js_choice4Per;}
		if (js_choice5Per>0){	ortitle = ortitle*js_choice5Per;}

		ortitle = Round(ortitle,2);
		return ortitle;
	}

	function frmSubmit(){
		if (form.gmCode.value == ''){alert('게임 회차가 없습니다.');form.gmCode.focus();return false;}
		if (form.gmKind.value == ''){alert('게임 종류가 없습니다.');form.gmKind.focus();return false;}
		if (form.gmKind_sub.value == ''){alert('게임 종류가 없습니다.');form.gmKind_sub.focus();return false;}

		oTbl = document.getElementById("addTable"); 
		trCnt = oTbl.rows.length;	//전체tr갯수 1부터 카운트
		if (trCnt==1){
			alert("선택하신 게임번호가 없습니다.\n번호를 선택해 주세요."); 
			return false; 
		}else{
			for( var i = 0; i <= form.elements.length - 1; i++ ){ 
				if( form.elements[i].name == "orCnt[]" ){ 
					if( !form.elements[i].value ){
						alert("선택하신 게임의 구매장수를 입력해 주세요."); 
						form.elements[i].focus(); 
						return false; 
					}
					if( form.elements[i].value==0 ){
						alert("선택하신 게임의 구매장수를 입력해 주세요."); 
						form.elements[i].focus(); 
						return false; 
					}
				}
			}
		}
		//
		form.action = "orderProc.asp";
		form.target = "oifrm"
		form.submit();
		return false;
	}
</script>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td height="40">
		<table width="98%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="10">&nbsp;</td>
				<td width="120"><img src="../images/special/title_special.jpg" width="120" height="30"></td>
				<td>&nbsp;</td>
				<td align="right"><span class="location_01">홈 > 스페셜 > </span><span class="location_02"> 스페셜 </span></td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td height="1" bgcolor="#e4e4e4"></td>
	</tr>
	<tr>
	<td align="center">

	<!--#include virtual="/special/gmMenu.asp" -->

	<table width="90%" border=0 cellpadding="0" cellspacing="0">
		<tr>
			<td>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="41%" height="256" valign="top" class="tab">
					<div>
						<ul><p style="text-align:left;color:white"><%=gmTopmsg%></p></ul>
					</div>
					</td>
					<td width=1%>&nbsp;</td>
					<td width="58%" height="337" align="center" valign="top">

					<!--#include virtual="/special/gmSet.asp" -->

					</td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td height="50" align="center" class="info_10">

				구매금액 <span style="color:#FF0000; font:bold;" id=basicMoney_htm></span>
				&nbsp;|&nbsp;
				예상  당첨금액 <span style="color:#ff9933; font:bold;" id=exWinMoney_htm></span>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td align="center"><img src="../images/special/btn_cart.jpg" width="216" height="39" onclick="sp_cartok();" style="cursor:pointer"></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
	</table>


</td>
</tr>
<tr>
<td align="center">
	<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
	<td align="center" class="t1">

<iframe name="oifrm" border=0 width=0 height=0></iframe>
<form name='form' method='post' onsubmit="return frmSubmit()">
<input type=hidden name=gmCode>
<input type=hidden name=gmKind>
<input type=hidden name=gmKind_sub>

<input type=hidden name=choice1>
<input type=hidden name=choice2>
<input type=hidden name=choice3>
<input type=hidden name=choice4>
<input type=hidden name=choice5>

<input type=hidden name=listFlag>
<input type=hidden name=ballFullcnt>
<input type=hidden name=endFlag>
<input type=hidden name=notnumflag value="n">

<input type=hidden name=orderMoney value=0 id=orderMoney>
<input type=hidden name=expWinMoney>
<input type=hidden name=soriflag value="on">
	<!--#include virtual="/special/gmCartList.asp" -->

	</td>
	</tr>
	<tr>
	<td height="50" align="center"><span style="font-size:14px; color:#F00; font-weight:bold" id=orMoney>총 구매금액 0 원</span></td>
	</tr>
	<tr>
	<td align="center"><img src="../images/special/btn_buy_red.jpg" width="127" height="34" onclick="return frmSubmit()" style="cursor:pointer;"></td>
	</tr>
	<tr>
	<td height="30" align="center"><span style="color:#763988; font-size:12px; font-weight:bold;">구매취소는 불가능 하오니 신중하게 구매해 주시기 바랍니다.</span></td>
	</tr>

</form>

	<tr>
	<td>&nbsp;</td>
	</tr>
	<tr>
	<td>&nbsp;</td>
	</tr>
	<tr>
	<td align="left">
	<img src="../images/special/title_special2.jpg" style="margin-left:5%;">
	</td>
	</tr>
	<tr>
	<td align="center" class="t2 info_11">
		<table width="90%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="100">게임종류</td>
		<td width="240">예시</td>
		<td>당첨규정</td>
		<td width="90">당첨배수</td>
		</tr>
		<tr>
		<td colspan="4" class="line1"></td>
		</tr>			
		<tr>
		<td>직렬1 스페셜</td>
		<td>&nbsp;</td>
		<td align="left"><span class="info_12">1번째</span> 추첨볼로 홀짝, 대소를 맞추는 방식입니다. </td>
		<td>&nbsp;</td>
		</tr>
		<tr>
		<td colspan="4" class="line1"></td>
		</tr>
		<tr>
		<td>직렬2 스페셜</td>
		<td>&nbsp;</td>
		<td align="left"><span class="info_12">1번째 2번째</span> 추첨볼로<br><span class="info_12">홀짝, 대소 구간을 선택</span>하여 순서에 맞게 맞추는 방식입니다. <br>당첨배수는 조합된 구간 배수가 x  되어 계산됩니다.<br>예) 홀 X 홀=<%=CDbl(sp_hper)*CDbl(sp_hper)%>배당 <BR>예) 소 X 대=<%=CDbl(sp_sper)*CDbl(sp_dper)%>배당</td>
		<td>&nbsp;</td>
		</tr>
		<tr>
		<td colspan="4" class="line1"></td>
		</tr>
		<tr>
		<td>직렬3 스페셜</td>
		<td>&nbsp;</td>
		<td align="left"><span class="info_12">1번째 2번째 3번째</span> 추첨볼로<br><span class="info_12">홀짝, 대소 구간을 선택</span>하여 순서에 맞게 맞추는 방식입니다.<br>당첨배수는 조합된 구간 배수가 x  되어 계산됩니다. <br>예) 홀 X 홀 X 홀=<%=FormatNumber((CDbl(sp_hper)*CDbl(sp_hper)*CDbl(sp_hper))-0.005,2)%>배당 <BR>예) 소 X 소 X 소=<%=FormatNumber((CDbl(sp_sper)*CDbl(sp_sper)*CDbl(sp_sper))-0.005,2)%>배당</td>
		<td>&nbsp;</td>
		</tr>
		<tr>
		<td colspan="4" class="line1"></td>
		</tr>
		<tr>
		<td>직렬4 스페셜</td>
		<td>&nbsp;</td>
		<td align="left"><span class="info_12">1번째 2번째 3번째 4번째</span> 추첨볼로<br><span class="info_12">홀짝,대소 구간을 선택</span>하여 순서에 맞게 맞추는 방식입니다.<br>당첨배수는 조합된 구간 배수가 x 되어 계산됩니다<br>예) 홀 X 홀 X 홀 X 홀=<%=FormatNumber((CDbl(sp_hper)*CDbl(sp_hper)*CDbl(sp_hper)*CDbl(sp_hper))-0.005,2)%>배당 <BR>예) 소 X 소 X 소 X 소=<%=FormatNumber((CDbl(sp_sper)*CDbl(sp_sper)*CDbl(sp_sper)*CDbl(sp_sper))-0.005,2)%>배당</td>
		<td>&nbsp;</td>
		</tr>
		<tr>
		<td colspan="4" class="line1"></td>
		</tr>
		<tr>
		<td>직렬5 스페셜</td>
		<td>&nbsp;</td>
		<td align="left"><span class="info_12">1번째 2번째 3번째 4번째 5번째</span> 추첨볼로<br><span class="info_12">홀짝,대소 구간을 선택</span>하여 순서에 맞게 맞추는 방식입니다.<br>당첨배수는 조합된 구간 배수가 x 되어 계산됩니다<br>예) 홀 X 홀 X 홀 X 홀 X 홀=<%=FormatNumber((CDbl(sp_hper)*CDbl(sp_hper)*CDbl(sp_hper)*CDbl(sp_hper)*CDbl(sp_hper))-0.005,2)%>배당 <BR>예) 소 X 소 X 소 X 소 X 소=<%=FormatNumber((CDbl(sp_sper)*CDbl(sp_sper)*CDbl(sp_sper)*CDbl(sp_sper)*CDbl(sp_sper))-0.005,2)%>배당</td>
		<td>&nbsp;</td>
		</tr>
		<tr>
		<td colspan="4" class="line1"></td>
		</tr>
		<tr bgcolor="#dedede">

		<td>홀수</td>
		<td><img src="../images/special/gameinfo_ball_01.png" width="206" height="30"></td>
		<td align="left">직렬 1,2,3 스페셜 중 원하시는 구간을 선택하여 <br><span class="info_12">홀</span>을 맞추는 방식입니다.</td>
		<td><%=sp_hper%>배</td>
		</tr>
		<tr>
		<td colspan="4" class="line2"></td>
		</tr>
		<tr bgcolor="#dedede">
		<td>짝수</td>
		<td><img src="../images/special/gameinfo_ball_02.png" width="172" height="30"></td>
		<td align="left">직렬 1,2,3 스페셜 중 원하시는 구간을 선택하여 <br><span class="info_12">짝</span>을 맞추는 방식입니다.</td>
		<td><%=sp_jper%>배</td>
		</tr>
		<tr>
		<td colspan="4" class="line2"></td>
		</tr>
		<tr bgcolor="#dedede">
		<td>소</td>
		<td><img src="../images/special/gameinfo_ball_03.png" width="172" height="31"></td>
		<td align="left">직렬 1,2,3 스페셜 중 원하시는 구간을 선택하여 <br><span class="info_12">낮은 구간의 숫자</span>를 맞추는 방식입니다.</td>
		<td><%=sp_sper%>배</td>
		</tr>
		<tr>
		<td colspan="4" class="line2"></td>
		</tr>
		<tr bgcolor="#dedede">
		<td>대</td>
		<td><img src="../images/special/gameinfo_ball_04.png" width="205" height="30"></td>
		<td align="left">직렬 1,2,3 스페셜 중 원하시는 구간을 선택하여 <br><span class="info_12">높은 구간의 숫자</span>를 맞추는 방식입니다.</td>
		<td><%=sp_dper%>배</td>
		</tr>
		<tr>
		<td colspan="4" class="line3"></td>
		</tr>
		</table>
	</td>
	</tr>
	</table>
</td>
</tr>
</table>