<!-- #include file="../include/sub_menu.asp" -->
<!-- #include file="../db/db.asp" -->
<%
	set rs = server.CreateObject("ADODB.Recordset")
	SQL    = "select * from tb_game_dividendSet "
	rs.Open sql, db, 1
	If Not rs.eof Then
		dvS1	 = rs("dvS1")
		dvS2	 = rs("dvS2")
		dvS3	 = rs("dvS3")
		dvS2R	 = rs("dvS2R")
		dvS3R	 = rs("dvS3R")
		dvR2	 = rs("dvR2")
		dvR3	 = rs("dvR3")
		dvR4	 = rs("dvR4")
		dvR5	 = rs("dvR5")
		dvR6	 = rs("dvR6")
		dvR7	 = rs("dvR7")
		dvR8	 = rs("dvR8")
		'//
		sp_price = rs("sp_price")
		sp_maxorcnt = rs("sp_maxorcnt")
		sp_hper = rs("sp_hper")
		sp_jper = rs("sp_jper")
		sp_dper = rs("sp_dper")
		sp_sper = rs("sp_sper")
	End If
	rs.close
%>
<!--contents-->
<tr>
<td align="center">
	<table width="1024" border="0" cellpadding="0" cellspacing="0">
	<tr>
	<td width="180" valign="top"><!-- #include file="../include/lmenu_03.asp" --> </td>
	<td width="10"></td>
	<td width="834" valign="top">
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td height="40">
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
			<td width="10">&nbsp;</td>
			<td width="120"><img src="../images/gameinfo/title_gameinfo_03.jpg" width="150" height="30"></td>
			<td>&nbsp;</td>
			<td align="right"><span class="location_01">홈 > 게임설명 > </span><span class="location_02"> 오백만스페셜 </span></td>
			</tr>
			</table>
		</td>
		</tr>
		 <tr>
		<td height="1" bgcolor="#e4e4e4"></td>
		</tr>
		<tr>
		<td>&nbsp;</td>
		</tr>
		<tr>
		<td align="center">
			<table width="800" border="0" cellpadding="0" cellspacing="0">
			<tr>
			<td style="background:url(../images/gameinfo/method_03.jpg) no-repeat;" height="542" valign="top">
			<ul style="margin:275px 0 0 43px;width:435px;line-height:200%;text-align:left;">
			<li style="list-style:disc !important;padding-bottom:20px;">한 게임 종류당 최소 구매는 <%=FormatNumber(sp_price,0)%>원이며,<br>최대 구매가능 금액은 게임 종류별로 회차 당 <%=FormatNumber(sp_price*sp_maxorcnt,0)%>원입니다.<br>
			<span style="color:red;">(스페셜 호응에 따라 최대구매금액은 변동 가능합니다.)</span>
			<li style="list-style:disc !important;">직렬 1,2,3,4,5*스페셜 중 원하시는 구간을 선택하여 구매진행하시면 됩니다.</li>
			<li style="list-style:disc !important;padding-bottom:20px;">게임종류는 (홀,짝,대,소)로 총 4가지 종류입니다.<li>
			<li style="list-style:disc !important;">당첨금액은 구매자가 선택한 스페셜 종류에 따라 다르며 구매내역이 당첨될 경우 아래의 당첨배수표에 따라 보유머니로 지급됩니다.</li>
			<li style="list-style:disc !important;">구매 후 구매취소는 어떠한 경우에도 불가능합니다.
			</ul>
			</td>
			</tr>
			<tr>
			<td align="center" height="50" align="left"><img src="../images/special/title_special2.jpg" width="256" height="31"></td>
			</tr>
			<tr>
			<td align="center">
				<table width="730" border="0" cellpadding="0" cellspacing="1" bgcolor="#333333">
				<tr>
				<td align="center" bgcolor="#f4f4f4">
					<table width="724" border="0" cellpadding="0" cellspacing="0" >
					<tr>
					<td height="44" bgcolor="#f4f4f4">
						<table width="720" border="0" cellpadding="0" cellspacing="0">
						<tr class="info_11">
						<td width="95" align="center">게임종류</td>
						<td width="240" align="center">예시</td>
						<td width="313" align="center" class="info_11">당첨규정</td>
						<td align="center">당첨배수</td>
						</tr>
						</table>
					</td>
					</tr>
					 <tr>
					<td height="1" bgcolor="#dedede"></td>
					</tr>
					<tr>
					<td height="49" bgcolor="#f4f4f4">
						<table width="720" border="0" cellpadding="0" cellspacing="0">
						<tr>
						<td width="95" align="center" class="info_11">직렬1 스페셜</td>
						<td width="240">&nbsp;</td>
						<td width="313" align="left" class="info_11"><span class="info_12">1번째</span> 추첨볼로 홀짝, 대소를 맞추는 방식입니다. </td>
						<td>&nbsp;</td>
						</tr>
						</table>
					</td>
					</tr>
					<tr>
					<td height="1" bgcolor="#dedede"></td>
					</tr>
					<tr>
					<td height="95" bgcolor="#f4f4f4">
						<table width="720" border="0" cellpadding="0" cellspacing="0">
						<tr>
						<td width="95" align="center" class="info_11">직렬2 스페셜</td>
						<td width="240">&nbsp;</td>
						<td width="312" align="left"><p class="info_11"><span class="info_12">1번째 2번째</span> 추첨볼로 <span class="info_12">홀짝, 대소 구간을 선택</span>하여 <br>순서에 맞게 맞추는 방식입니다. <br>당첨배수는 조합된 구간 배수가 x 되어 계산됩니다.<br>예) 홀 X 홀=<%=CDbl(sp_hper)*CDbl(sp_hper)%>배당 예) 소 X 대=<%=CDbl(sp_sper)*CDbl(sp_dper)%>배당</p><p></p></td>
						<td>&nbsp;</td>
						</tr>
						</table>
					</td>
					</tr>
					<tr>
					<td height="1" bgcolor="#dedede"></td>
					</tr>
					<tr>
					<td height="95" bgcolor="#f4f4f4">
						<table width="720" border="0" cellpadding="0" cellspacing="0">
						<tr>
						<td width="95" align="center" class="info_11">직렬3 스페셜</td>
						<td width="240">&nbsp;</td>
						<td width="311" align="left"><p><span class="info_12">1번째 2번째 3번째</span><span class="info_11"> 추첨볼로 </span><span class="info_12">홀짝, 대소 구간을 선택</span><span class="info_11">하여 순서에 맞게 맞추는 방식입니다.<br>당첨배수는 조합된 구간 배수가 x 되어 계산됩니다. <br>예) 홀 X 홀 X 홀=<%=FormatNumber((CDbl(sp_hper)*CDbl(sp_hper)*CDbl(sp_hper))-0.005,2)%>배당 <BR>예) 소 X 소 X 소=<%=FormatNumber((CDbl(sp_sper)*CDbl(sp_sper)*CDbl(sp_sper))-0.005,2)%>배당</span><br></p><p></p></td>
						<td>&nbsp;</td>
						</tr>
						</table>
					</td>
					</tr>
					<tr>
					<td height="1" bgcolor="#dedede"></td>
					</tr>
					<tr>
					<td height="110" bgcolor="#f4f4f4">
						<table width="720" border="0" cellpadding="0" cellspacing="0">
						<tr>
						<td width="95" align="center" class="info_11">직렬4 스페셜</td>
						<td width="240">&nbsp;</td>
						<td width="311" align="left"><p><span class="info_12">1번째 2번째 3번째 4번째</span><span class="info_11"> 추첨볼로 <br>
						</span><span class="info_12">홀짝,대소 구간을 선택</span><span class="info_11">하여<br>
						순서에 맞게 맞추는 방식입니다.<br>당첨배수는 조합된 구간 배수가 x되어 계산됩니다<br>예) 홀 X 홀 X 홀 X 홀=<%=FormatNumber((CDbl(sp_hper)*CDbl(sp_hper)*CDbl(sp_hper)*CDbl(sp_hper))-0.005,2)%>배당 <BR>예) 소 X 소 X 소 X 소=<%=FormatNumber((CDbl(sp_sper)*CDbl(sp_sper)*CDbl(sp_sper)*CDbl(sp_sper))-0.005,2)%>배당</span><br><br></p><p></p></td>
						<td>&nbsp;</td>
						</tr>
						</table>
					</td>
					</tr>
					<tr>
					<td height="1" bgcolor="#dedede"></td>
					</tr>
					<tr>
					<td height="110" bgcolor="#f4f4f4">
						<table width="720" border="0" cellpadding="0" cellspacing="0">
						<tr>
						<td width="95" align="center" class="info_11">직렬5 스페셜</td>
						<td width="240">&nbsp;</td>
						<td width="311" align="left"><p><span class="info_12">1번째 2번째 3번째 4번째 5번째</span><span class="info_11"> 추첨볼로 <br></span><span class="info_12">홀짝,대소 구간을 선택</span><span class="info_11">하여<br>순서에 맞게 맞추는 방식입니다.<br>당첨배수는 조합된 구간 배수가 x되어 계산됩니다<br>예) 홀 X 홀 X 홀 X 홀 X 홀=<%=FormatNumber((CDbl(sp_hper)*CDbl(sp_hper)*CDbl(sp_hper)*CDbl(sp_hper)*CDbl(sp_hper))-0.005,2)%>배당 <BR>예) 소 X 소 X 소 X 소 X 소=<%=FormatNumber((CDbl(sp_sper)*CDbl(sp_sper)*CDbl(sp_sper)*CDbl(sp_sper)*CDbl(sp_sper))-0.005,2)%>배당</span><br><br></p><p></p></td>
						<td>&nbsp;</td>
						</tr>
						</table>
					</td>
					</tr>
					<tr>
					<td height="1" bgcolor="#ffffff"></td>
					</tr>
					<tr>

					<td height="67" bgcolor="#dedede">
						<table width="720" border="0" cellpadding="0" cellspacing="0">
						<tr>
						<td width="91" align="center" class="info_11">홀수</td>
						<td width="240" align="center"><img src="../images/special/gameinfo_ball_01.png" width="206" height="30"></td>
						<td width="311" align="left"><p class="info_11">직렬 1,2,3 스페셜 중 원하시는 구간을 선택하여 <br>
						<span class="info_12">홀</span>을 맞추는 방식입니다.</p>
						<p></p></td>
						<td align="center" class="info_11"><%=sp_hper%>배</td>
						</tr>
						</table>
					</td>
					</tr>
					<tr>
					<td height="1" bgcolor="#ffffff"></td>
					</tr>
					<tr>
					<td height="56" bgcolor="#dedede">
						<table width="720" border="0" cellpadding="0" cellspacing="0">
						<tr>
						<td width="91" align="center" class="info_11">짝수</td>
						<td width="240" align="center"><img src="../images/special/gameinfo_ball_02.png" width="172" height="30"></td>
						<td width="311" align="left"><p class="info_11">직렬 1,2,3 스페셜 중 원하시는 구간을 선택하여 <br><span class="info_12">짝</span>을 맞추는 방식입니다.</p><p></p></td>
						<td align="center" class="info_11"><%=sp_jper%>배</td>
						</tr>
						</table>
					</td>
					</tr>
					<tr>
					<td height="1" bgcolor="#ffffff"></td>
					</tr>
					<tr>
					<td height="59" bgcolor="#dedede">
						<table width="720" border="0" cellpadding="0" cellspacing="0">
						<tr>
						<td width="91" align="center" class="info_11">소</td>
						<td width="240" align="center"><img src="../images/special/gameinfo_ball_03.png" width="172" height="31"></td>
						<td width="311" align="left"><p class="info_11">직렬 1,2,3 스페셜 중 원하시는 구간을 선택하여 <br>
						<span class="info_12">낮은 구간의 숫자</span>를 맞추는 방식입니다.</p><p></p></td>
						<td align="center" class="info_11"><%=sp_sper%>배</td>
						</tr>
						</table>
					</td>
					</tr>
					<tr>
					<td height="1" bgcolor="#ffffff"></td>
					</tr>
					<tr>
					<td height="54" bgcolor="#dedede">
						<table width="720" border="0" cellpadding="0" cellspacing="0">
						<tr>
						<td width="91" align="center" class="info_11">대</td>
						<td width="240" align="center"><img src="../images/special/gameinfo_ball_04.png" width="205" height="30"></td>
						<td width="311" align="left"><p class="info_11">직렬 1,2,3 스페셜 중 원하시는 구간을 선택하여 <br>
						<span class="info_12">높은 구간의 숫자</span>를 맞추는 방식입니다.</p><p></p></td>
						<td align="center" class="info_11"><%=sp_dper%>배</td>
						</tr>
						</table>
					</td>
					</tr>
					<tr>
					<td height="1" bgcolor="#CCCCCC"></td>
					</tr>
					</table>
				</td>
				</tr>
				</table>
			</td>
			</tr>
			<tr>
			<td height="50">&nbsp;</td>
			</tr>
			</table>
		</td>
		</tr>
		</table>
	</td>
	</tr>
	</table>
</td>
</tr>
 <!--footer-->
 <!-- #include file="../include/sub_footer.asp" --> 