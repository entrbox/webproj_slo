<!-- #include file="../include/sub_menu.asp" -->  
<!-- #include file="../db/db.asp" -->
<%
	set rs = server.CreateObject("ADODB.Recordset")
	SQL    = "select * from tb_game_dividendSet "
	rs.Open sql, db, 1
	If Not rs.eof Then
		dvS1	 = rs("dvS1")
		dvS2	 = rs("dvS2")
		dvS3	 = rs("dvS3")
		dvS2R	 = rs("dvS2R")
		dvS3R	 = rs("dvS3R")
		dvR2	 = rs("dvR2")
		dvR3	 = rs("dvR3")
		dvR4	 = rs("dvR4")
		dvR5	 = rs("dvR5")
		dvR6	 = rs("dvR6")
		dvR7	 = rs("dvR7")
		dvR8	 = rs("dvR8")
		'//
		sp_price = rs("sp_price")
		sp_maxorcnt = rs("sp_maxorcnt")
		sp_hper = rs("sp_hper")
		sp_jper = rs("sp_jper")
		sp_dper = rs("sp_dper")
		sp_sper = rs("sp_sper")
	End If
	rs.close
%>

      <!--contents-->
      <tr>
        <td align="center"><table width="1024" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="180" valign="top"><!-- #include file="../include/lmenu_03.asp" --> </td>
            <td width="10"></td>
            <td width="834" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td height="40"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                  <tr>
		<td width="120"><span class="new_tit_st">게임<span>설명</span></span></td>
                    <td>&nbsp;</td>
                    <td align="right"><span class="location_01">홈 > 게임셜명 > </span><span class="location_02"> 게임설명 </span></td>
                  </tr>
                </table></td>
              </tr>
               <tr>
                <td height="1" bgcolor="#292a2d"></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td align="center"><table width="744" border="0" cellpadding="0" cellspacing="0">
                  <tr>
                    <td><img src="../images/gameinfo/method_02.png" width="830" height="400"></td>
                  </tr>
                  <tr>
                    <td height="20">&nbsp;</td>
                  </tr>
                  <tr>
                    <td height="50" align="left"><img src="../images/gameinfo/method_02_02.jpg" width="265" height="34"></td>
                  </tr>
                  <tr>
                    <td align="center"><table width="750" border="0" cellpadding="0" cellspacing="1" style="background-color:e3e3e3">
                      <tr>
                        <td><table width="750" border="0" cellpadding="0" cellspacing="0">
                          <tr>
                            <td><table width="100%" border="0" cellpadding="0" cellspacing="0" style="background-image:url(../images/customer/board_bg_01.jpg)">
                              <tr>
                                <td width="127" height="35" align="center"><strong>게임종류</strong></td>
                                <td width="3" align="center"><img src="../images/board/board_line_01.gif" width="3" height="22"></td>
                                <td align="center"><strong>예시</strong></td>
                                <td width="3" align="center"><img src="../images/board/board_line_01.gif" width="3" height="22"></td>
                                <td width="350" align="center"><strong>당첨규정</strong></td>
                                <td width="3" align="center"><img src="../images/board/board_line_01.gif" width="3" height="22"></td>
                                <td width="100" align="center"><strong>당첨배수</strong></td>
                              </tr>
                            </table></td>
                          </tr>
                          <tr>
                            <td><table width="100%" border="0" cellpadding="0" cellspacing="0" >
                              <tr>
                                <td width="127" height="35" align="center"><strong>직렬 1</strong></td>
                                <td width="169" align="left" bgcolor="#292a2d" class="left">&nbsp;&nbsp;01</td>
                                <td width="353" align="left" bgcolor="#292a2d">&nbsp;&nbsp;첫번째 추첨볼 맞추기<br/></td>
                                <td width="101" align="center" bgcolor="#292a2d"><%=dvS1%> 배</td>
                              </tr>
                            </table></td>
                          </tr>
                          <tr>
                            <td height="1" bgcolor="#111113"></td>
                          </tr>
                          <tr>
                            <td><table width="100%" border="0" cellpadding="0" cellspacing="0" >
                              <tr>
                                <td width="127" height="35" align="center"><strong>직렬 2</strong></td>
                                <td width="169" align="left" bgcolor="#292a2d" class="left">&nbsp;&nbsp;01  02</td>
                                <td width="353" align="left" bgcolor="#292a2d">&nbsp;&nbsp;순서에 맞게 첫번째 두번째 추첨볼 맞추기</td>
                                <td width="101" align="center" bgcolor="#292a2d"><%=dvS2%> 배</td>
                              </tr>
                            </table></td>
                          </tr>
                          <tr>
                            <td height="1" bgcolor="#111113"></td>
                          </tr>
                          <tr>
                            <td><table width="100%" border="0" cellpadding="0" cellspacing="0" >
                              <tr bgcolor="#CCCCCC">
                                <th width="127" height="40" bgcolor="#292a2d">직렬 2임의</th>
                                <td width="169" align="left" bgcolor="#292a2d" class="left">&nbsp;&nbsp;01  02</td>
                                <td align="left" bgcolor="#292a2d">&nbsp;&nbsp;순서에 상관없이 첫번째 두번째 추첨볼 맞추기</td>
                                <td width="101" align="center" bgcolor="#292a2d"><%=dvS2R%> 배</td>
                              </tr>
                            </table></td>
                          </tr>
                          <tr>
                            <td height="1" bgcolor="#111113"></td>
                          </tr>
                          <tr>
                            <td><table width="100%" border="0" cellpadding="0" cellspacing="0" >
                              <tr bgcolor="#CCCCCC">
                                <th width="127" height="40" bgcolor="#292a2d">직렬 3</th>
                                <td width="169" align="left" bgcolor="#292a2d" class="left">&nbsp;&nbsp;01  02  03</td>
                                <td align="left" bgcolor="#292a2d">&nbsp;&nbsp;순서에 맞게 첫번째 두번째 세번째 추첨볼 맞추기</td>
                                <td width="101" align="center" bgcolor="#292a2d"><%=dvS3%> 배</td>
                              </tr>
                            </table></td>
                          </tr>
                          <tr>
                            <td height="1" bgcolor="#111113"></td>
                          </tr>
                          <tr>
                            <td><table width="100%" border="0" cellpadding="0" cellspacing="0" >
                              <tr bgcolor="#CCCCCC">
                                <th width="127" height="40" bgcolor="#292a2d">직렬 3임의</th>
                                <td width="169" align="left" bgcolor="#292a2d" class="left">&nbsp;&nbsp;01  02  03</td>
                                <td align="left" bgcolor="#292a2d">&nbsp;&nbsp;순서에 상관없이 첫번째 두번째 세번째 추첨볼 맞추기</td>
                                <td width="101" align="center" bgcolor="#292a2d"><%=dvS3R%> 배</td>
                              </tr>
                            </table></td>
                          </tr>
                          <tr>
                            <td height="1" bgcolor="#111113"></td>
                          </tr>
                          <tr>
                            <td><table width="100%" border="0" cellpadding="0" cellspacing="0" >
                              <tr bgcolor="#CCCCCC">
                                <th width="127" height="40" bgcolor="#292a2d">임의 2</th>
                                <td width="169" align="left" bgcolor="#292a2d" class="left">&nbsp;&nbsp;01  02 </td>
                                <td align="left" bgcolor="#292a2d">&nbsp;&nbsp;추첨볼 5개중 2개의 숫자 맞추기</td>
                                <td width="101" align="center" bgcolor="#292a2d"><%=dvR2%> 배</td>
                              </tr>
                            </table></td>
                          </tr>
                          <tr>
                            <td height="1" bgcolor="#111113"></td>
                          </tr>
                          <tr>
                            <td><table width="100%" border="0" cellpadding="0" cellspacing="0" >
                              <tr bgcolor="#CCCCCC">
                                <th width="127" height="40" bgcolor="#292a2d">임의 3</th>
                                <td width="169" align="left" bgcolor="#292a2d" class="left">&nbsp;&nbsp;01  02  03 </td>
                                <td align="left" bgcolor="#292a2d">&nbsp;&nbsp;추첨볼 5개중 3개의 숫자 맞추기</td>
                                <td width="101" align="center" bgcolor="#292a2d"><%=dvR3%> 배</td>
                              </tr>
                            </table></td>
                          </tr>
                          <tr>
                            <td height="1" bgcolor="#111113"></td>
                          </tr>
                          <tr>
                            <td><table width="100%" border="0" cellpadding="0" cellspacing="0" >
                              <tr bgcolor="#CCCCCC">
                                <th width="127" height="40" bgcolor="#292a2d">임의 4</th>
                                <td width="169" align="left" bgcolor="#292a2d" class="left">&nbsp;&nbsp;01  02  03  04</td>
                                <td align="left" bgcolor="#292a2d">&nbsp;&nbsp;추첨볼 5개중 4개의 숫자 맞추기</td>
                                <td width="101" align="center" bgcolor="#292a2d"><%=dvR4%> 배</td>
                              </tr>
                            </table></td>
                          </tr>
                          <tr>
                            <td height="1" bgcolor="#111113"></td>
                          </tr>
                          <tr>
                            <td><table width="100%" border="0" cellpadding="0" cellspacing="0" >
                              <tr bgcolor="#CCCCCC">
                                <th width="127" height="40" bgcolor="#292a2d">임의 5</th>
                                <td width="169" align="left" bgcolor="#292a2d" class="left">&nbsp;&nbsp;01  02  03  04  05</td>
                                <td align="left" bgcolor="#292a2d">&nbsp;&nbsp;추첨볼 5개중 5개의 숫자 맞추기</td>
                                <td width="101" align="center" bgcolor="#292a2d"><%=dvR5%> 배</td>
                              </tr>
                            </table></td>
                          </tr>
                          <tr>
                            <td height="1" bgcolor="#111113"></td>
                          </tr>
                          <tr>
                            <td><table width="100%" border="0" cellpadding="0" cellspacing="0" >
                              <tr bgcolor="#CCCCCC">
                                <th width="127" height="40" bgcolor="#292a2d">임의 6</th>
                                <td width="169" align="left" bgcolor="#292a2d" class="left">&nbsp;&nbsp;01  02  03  04  05  06</td>
                                <td align="left" bgcolor="#292a2d">&nbsp;&nbsp;추첨볼 5개중 임의로 6개 숫자선택하여 5개 맞추기</td>
                                <td width="101" align="center" bgcolor="#292a2d"><%=dvR6%> 배</td>
                              </tr>
                            </table></td>
                          </tr>
                          <tr>
                            <td height="1" bgcolor="#111113"></td>
                          </tr>
                          <tr>
                            <td><table width="100%" border="0" cellpadding="0" cellspacing="0" >
                              <tr bgcolor="#CCCCCC">
                                <th width="127" height="40" bgcolor="#292a2d">임의 7</th>
                                <td width="169" align="left" bgcolor="#292a2d" class="left">&nbsp;&nbsp;01  02  03  04  05  06  07</td>
                                <td align="left" bgcolor="#292a2d">&nbsp;&nbsp;추첨볼 5개중 임의로 7개 숫자선택하여 5개 맞추기</td>
                                <td width="101" align="center" bgcolor="#292a2d"><%=dvR7%> 배</td>
                              </tr>
                            </table></td>
                          </tr>
                          <tr>
                            <td height="1" bgcolor="#111113"></td>
                          </tr>
                          <tr>
                            <td><table width="100%" border="0" cellpadding="0" cellspacing="0" >
                              <tr bgcolor="#CCCCCC">
                                <th width="127" height="40" bgcolor="#292a2d">임의 8</th>
                                <td width="169" align="left" bgcolor="#292a2d" class="left">&nbsp;&nbsp;01  02  03  04  05  06  07  08</td>
                                <td align="left" bgcolor="#292a2d">&nbsp;&nbsp;추첨볼 5개중 임의로 8개 숫자선택하여 5개 맞추기</td>
                                <td width="101" align="center" bgcolor="#292a2d"><%=dvR8%> 배</td>
                              </tr>
          
                            </table></td>
                          </tr>
                        </table></td>
                      </tr>
                                           <tr>
                            <td height="1" bgcolor="#111113"></td>
                          </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td height="50" align="center">&nbsp;</td>
                  </tr>
                </table>
                </td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
           <!--footer-->
     <!-- #include file="../include/sub_footer.asp" --> 