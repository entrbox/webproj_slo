<!-- #include file="../include/sub_menu.asp" -->  
      <!--contents-->
      <tr>
        <td align="center"><table width="1024" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="180" valign="top"><!--#include file="../include/lmenu_10.asp" --> </td>
            <td width="10"></td>
            <td width="834" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td height="40"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                  <tr>
                    <td width="10">&nbsp;</td>
                    <td width="150"><img src="../images/mypage/title_mypage_06.jpg" width="520" height="30"></td>
                    <td>&nbsp;</td>
                    <td align="right"><span class="location_01">홈 > 마이페이지 > </span><span class="location_02"> 구매내역(스포츠) </span></td>
                  </tr>
                </table></td>
              </tr>
              <tr>
                <td height="1" bgcolor="#e4e4e4"></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td align="center">
                <table width="800" border="0" cellpadding="0" cellspacing="0">     
                <!-- 상단탭 -->
                <tr><td>
                <table width="800%" border="0" cellspacing="0" cellpadding="0" style="background:url(../images/sports/tab_bg.jpg) repeat-x">
                  <tr>
                    <td width="165" height="35" align="center"><a href="../mypage/mypage_sports.asp" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image31','','../images/sports/result_tab_01_on.gif',1)"><img src="../images/sports/result_tab_01.gif" name="Image31" width="153" height="35" id="Image31"></a></td>
                    <td width="165" align="center"><a href="../mypage/mypage_sports_02.asp" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image32','','../images/sports/result_tab_02_on.gif',1)"><img src="../images/sports/result_tab_02.gif" name="Image32" width="153" height="35" id="Image32"></a></td>
                    <td width="165" align="center"><a href="../mypage/mypage_sports_03.asp"><img src="../images/sports/result_tab_03_on.gif" width="153" height="35"></a></td>
                    <td>&nbsp;</td>
                  </tr>
                </table>
                </td></tr>  
                <!-- 상단탭 -->           
                  <tr>
                    <td>
                    <!-- 승무패 -->
                    <table width="800" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td><img src="../images/sports/01_table_tit2.jpg" width="800" height="45"></td>
                      </tr>
                      <tr>
                        <td>
                        <table width="800" border="0" cellspacing="0" cellpadding="0" style="border-color:#ffffff; border-style:solid; border-width:1px;">
                          <tr>
                            <td>
                            <table width="800" border="0" cellspacing="0" cellpadding="0">
                              
                              <!-- 경기 -->                           
                              <tr>
                                <td>
                                <table width="800" border="0" cellspacing="0" cellpadding="0">
                                 <!--경기제목-->
                                        <tr>
                                          <th height="26" bgcolor="#ededed" class="s_tit"><img src="../images/sports/icon_kbo.png" width="18" height="13"> KBO 프로야구</th>
                                          </tr>
                                        <tr><td height="2"></td></tr>
                                        <!--//경기제목-->
                              <!-- 경기결과 --> 
                                  <tr>
                                    <td>
                                    <table width="800" border="0" cellspacing="0" cellpadding="0" class="s_bg_ing">                                    
                                      <tr>
                                        <th width="20" class="s_date" ><input type="checkbox" name="checkbox" id="checkbox"></th>
                                        <th width="110" class="s_date" >11/01 [월] 18:00</th>
                                        <td width="580">
                                        <!-- 홈팀우승 -->
                                        <table width="580" cellpadding="0" cellspacing="0" style="background:url(../images/sports/home_win_bg.gif);">
                                        <tr>
                                        <th width="210" class="s_home_name">롯데자이언트</th>
                                        <th width="50" class="s_home_point">2.85</th>                                     
                                        <th width="60" class="s_vs_stop">vs</th>
                                        <th width="50" class="s_away_point" >2.15</th>
                                        <th width="210" class="s_away_name" >기아타이거즈</th>
                                        </tr>
                                        </table>
                                        <!-- //홈팀우승 -->
                                        </td>
                                        <th width="45" class="s_result">1:0</th>
                                        <th width="45" class="s_result"><span class="s_result_right">적중</span></th>
                                      </tr>                                     
                                    </table>
                                    </td>
                                  </tr> 
                                  <tr><td height="2"></td></tr>
                                  <!-- //경기결과  -->
                                <!-- 경기결과 --> 
                                  <tr>
                                    <td>
                                    <table width="800" border="0" cellspacing="0" cellpadding="0" class="s_bg_ing">                                    
                                      <tr>
                                        <th width="20" class="s_date" ><input type="checkbox" name="checkbox2" id="checkbox2"></th>
                                        <th width="110" class="s_date" >11/01 [월] 18:00</th>
                                        <td width="580">
                                        <!-- 원정우승 -->
                                        <table width="580" cellpadding="0" cellspacing="0" style="background:url(../images/sports/away_win_bg.gif);">
                                        <tr>
                                        <th width="210" class="s_home_name">롯데자이언트</th>
                                        <th width="50" class="s_home_point">2.85</th>                                     
                                        <th width="60" class="s_vs_stop">vs</th>
                                        <th width="50" class="s_away_point" >2.15</th>
                                        <th width="210" class="s_away_name" >기아타이거즈</th>
                                        </tr>
                                        </table>
                                        <!-- //원정우승 -->
                                        </td>
                                        <th width="45" class="s_result">52:63</th>
                                        <th width="45" class="s_result"><span class="s_result_miss">미적중</span></th>
                                      </tr>                                     
                                    </table>
                                    </td>
                                  </tr> 
                                  <tr><td height="2"></td></tr>
                                  <!-- 결과산출-->
                                  <tr>
                                    <td height="30">
                                    <table width="800" border="0" cellspacing="0" cellpadding="0" bgcolor="#9e1940" class="s_result_cal " >
                                      <tr>
                                        <td height="26">배팅날짜 :<span class="s_result_cal2"> 2014 11/01 18:00</span></td>
                                        <td>배팅금액 : <span class="s_result_cal2">100,000</span></td>
                                        <td>배당률 : <span class="s_result_cal2">11.71</span></td>
                                        <td>예상배당금 : <span class="s_result_cal2">1,171,350</span></td>
                                        <td>당첨금 : <span class="s_result_cal2">0</span></td>
                                        <td><span class="s_result_cal2">게임종료</span></td>
                                      </tr>
                                    </table></td></tr>
                                  <!-- //결과산출-->
                                  <!-- //경기결과  -->
                                  <tr><td height="5"></td></tr>
                                </table>
                                </td>
                              </tr>
                              <!-- //경기 --> 
                            <!-- 경기 -->                           
                              <tr>
                                <td>
                                <table width="800" border="0" cellspacing="0" cellpadding="0">
                                 <!--경기제목-->
                                        <tr>
                                          <th height="26" bgcolor="#ededed" class="s_tit"><img src="../images/sports/icon_kbo.png" width="18" height="13"> KBO 프로야구</th>
                                          </tr>
                                        <tr><td height="2"></td></tr>
                                        <!--//경기제목-->
                              <!-- 경기결과 --> 
                                  <tr>
                                    <td>
                                    <table width="800" border="0" cellspacing="0" cellpadding="0" class="s_bg_ing">                                    
                                      <tr>
                                        <th width="20" class="s_date" ><input type="checkbox" name="checkbox" id="checkbox"></th>
                                        <th width="110" class="s_date" >11/01 [월] 18:00</th>
                                        <td width="580">
                                        <!-- 홈팀우승 -->
                                        <table width="580" cellpadding="0" cellspacing="0" style="background:url(../images/sports/home_win_bg.gif);">
                                        <tr>
                                        <th width="210" class="s_home_name">롯데자이언트</th>
                                        <th width="50" class="s_home_point">2.85</th>                                     
                                        <th width="60" class="s_vs_stop">vs</th>
                                        <th width="50" class="s_away_point" >2.15</th>
                                        <th width="210" class="s_away_name" >기아타이거즈</th>
                                        </tr>
                                        </table>
                                        <!-- //홈팀우승 -->
                                        </td>
                                        <th width="45" class="s_result">1:0</th>
                                        <th width="45" class="s_result"><span class="s_result_right">적중</span></th>
                                      </tr>                                     
                                    </table>
                                    </td>
                                  </tr> 
                                  <tr><td height="2"></td></tr>
                                  <!-- //경기결과  -->
                                <!-- 경기결과 --> 
                                  <tr>
                                    <td>
                                    <table width="800" border="0" cellspacing="0" cellpadding="0" class="s_bg_ing">                                    
                                      <tr>
                                        <th width="20" class="s_date" ><input type="checkbox" name="checkbox2" id="checkbox2"></th>
                                        <th width="110" class="s_date" >11/01 [월] 18:00</th>
                                        <td width="580">
                                        <!-- 원정우승 -->
                                        <table width="580" cellpadding="0" cellspacing="0" style="background:url(../images/sports/away_win_bg.gif);">
                                        <tr>
                                        <th width="210" class="s_home_name">롯데자이언트</th>
                                        <th width="50" class="s_home_point">2.85</th>                                     
                                        <th width="60" class="s_vs_stop">vs</th>
                                        <th width="50" class="s_away_point" >2.15</th>
                                        <th width="210" class="s_away_name" >기아타이거즈</th>
                                        </tr>
                                        </table>
                                        <!-- //원정우승 -->
                                        </td>
                                        <th width="45" class="s_result">52:63</th>
                                        <th width="45" class="s_result"><span class="s_result_miss">미적중</span></th>
                                      </tr>                                     
                                    </table>
                                    </td>
                                  </tr> 
                                  <tr><td height="2"></td></tr>
                                  <!-- 결과산출-->
                                  <tr>
                                    <td height="30">
                                    <table width="800" border="0" cellspacing="0" cellpadding="0" bgcolor="#9e1940" class="s_result_cal " >
                                      <tr>
                                        <td height="26">배팅날짜 :<span class="s_result_cal2"> 2014 11/01 18:00</span></td>
                                        <td>배팅금액 : <span class="s_result_cal2">100,000</span></td>
                                        <td>배당률 : <span class="s_result_cal2">11.71</span></td>
                                        <td>예상배당금 : <span class="s_result_cal2">1,171,350</span></td>
                                        <td>당첨금 : <span class="s_result_cal2">0</span></td>
                                        <td><span class="s_result_cal2">게임종료</span></td>
                                      </tr>
                                    </table></td></tr>
                                  <!-- //결과산출-->
                                  <!-- //경기결과  -->
                                  <tr><td height="5"></td></tr>
                                </table>
                                </td>
                              </tr>
                              <!-- //경기 --> <!-- 경기 -->                           
                              <tr>
                                <td>
                                <table width="800" border="0" cellspacing="0" cellpadding="0">
                                 <!--경기제목-->
                                        <tr>
                                          <th height="26" bgcolor="#ededed" class="s_tit"><img src="../images/sports/icon_kbo.png" width="18" height="13"> KBO 프로야구</th>
                                          </tr>
                                        <tr><td height="2"></td></tr>
                                        <!--//경기제목-->
                              <!-- 경기결과 --> 
                                  <tr>
                                    <td>
                                    <table width="800" border="0" cellspacing="0" cellpadding="0" class="s_bg_ing">                                    
                                      <tr>
                                        <th width="20" class="s_date" ><input type="checkbox" name="checkbox" id="checkbox"></th>
                                        <th width="110" class="s_date" >11/01 [월] 18:00</th>
                                        <td width="580">
                                        <!-- 홈팀우승 -->
                                        <table width="580" cellpadding="0" cellspacing="0" style="background:url(../images/sports/home_win_bg.gif);">
                                        <tr>
                                        <th width="210" class="s_home_name">롯데자이언트</th>
                                        <th width="50" class="s_home_point">2.85</th>                                     
                                        <th width="60" class="s_vs_stop">vs</th>
                                        <th width="50" class="s_away_point" >2.15</th>
                                        <th width="210" class="s_away_name" >기아타이거즈</th>
                                        </tr>
                                        </table>
                                        <!-- //홈팀우승 -->
                                        </td>
                                        <th width="45" class="s_result">1:0</th>
                                        <th width="45" class="s_result"><span class="s_result_right">적중</span></th>
                                      </tr>                                     
                                    </table>
                                    </td>
                                  </tr> 
                                  <tr><td height="2"></td></tr>
                                  <!-- //경기결과  -->
                                <!-- 경기결과 --> 
                                  <tr>
                                    <td>
                                    <table width="800" border="0" cellspacing="0" cellpadding="0" class="s_bg_ing">                                    
                                      <tr>
                                        <th width="20" class="s_date" ><input type="checkbox" name="checkbox2" id="checkbox2"></th>
                                        <th width="110" class="s_date" >11/01 [월] 18:00</th>
                                        <td width="580">
                                        <!-- 원정우승 -->
                                        <table width="580" cellpadding="0" cellspacing="0" style="background:url(../images/sports/away_win_bg.gif);">
                                        <tr>
                                        <th width="210" class="s_home_name">롯데자이언트</th>
                                        <th width="50" class="s_home_point">2.85</th>                                     
                                        <th width="60" class="s_vs_stop">vs</th>
                                        <th width="50" class="s_away_point" >2.15</th>
                                        <th width="210" class="s_away_name" >기아타이거즈</th>
                                        </tr>
                                        </table>
                                        <!-- //원정우승 -->
                                        </td>
                                        <th width="45" class="s_result">52:63</th>
                                        <th width="45" class="s_result"><span class="s_result_miss">미적중</span></th>
                                      </tr>                                     
                                    </table>
                                    </td>
                                  </tr> 
                                  <tr><td height="2"></td></tr>
                                  <!-- 결과산출-->
                                  <tr>
                                    <td height="30">
                                    <table width="800" border="0" cellspacing="0" cellpadding="0" bgcolor="#9e1940" class="s_result_cal " >
                                      <tr>
                                        <td height="26">배팅날짜 :<span class="s_result_cal2"> 2014 11/01 18:00</span></td>
                                        <td>배팅금액 : <span class="s_result_cal2">100,000</span></td>
                                        <td>배당률 : <span class="s_result_cal2">11.71</span></td>
                                        <td>예상배당금 : <span class="s_result_cal2">1,171,350</span></td>
                                        <td>당첨금 : <span class="s_result_cal2">0</span></td>
                                        <td><span class="s_result_cal2">게임종료</span></td>
                                      </tr>
                                    </table></td></tr>
                                  <!-- //결과산출-->
                                  <!-- //경기결과  -->
                                  <tr><td height="5"></td></tr>
                                </table>
                                </td>
                              </tr>
                              <!-- //경기 --> <!-- 경기 -->                           
                              <tr>
                                <td>
                                <table width="800" border="0" cellspacing="0" cellpadding="0">
                                 <!--경기제목-->
                                        <tr>
                                          <th height="26" bgcolor="#ededed" class="s_tit"><img src="../images/sports/icon_kbo.png" width="18" height="13"> KBO 프로야구</th>
                                          </tr>
                                        <tr><td height="2"></td></tr>
                                        <!--//경기제목-->
                              <!-- 경기결과 --> 
                                  <tr>
                                    <td>
                                    <table width="800" border="0" cellspacing="0" cellpadding="0" class="s_bg_ing">                                    
                                      <tr>
                                        <th width="20" class="s_date" ><input type="checkbox" name="checkbox" id="checkbox"></th>
                                        <th width="110" class="s_date" >11/01 [월] 18:00</th>
                                        <td width="580">
                                        <!-- 홈팀우승 -->
                                        <table width="580" cellpadding="0" cellspacing="0" style="background:url(../images/sports/home_win_bg.gif);">
                                        <tr>
                                        <th width="210" class="s_home_name">롯데자이언트</th>
                                        <th width="50" class="s_home_point">2.85</th>                                     
                                        <th width="60" class="s_vs_stop">vs</th>
                                        <th width="50" class="s_away_point" >2.15</th>
                                        <th width="210" class="s_away_name" >기아타이거즈</th>
                                        </tr>
                                        </table>
                                        <!-- //홈팀우승 -->
                                        </td>
                                        <th width="45" class="s_result">1:0</th>
                                        <th width="45" class="s_result"><span class="s_result_right">적중</span></th>
                                      </tr>                                     
                                    </table>
                                    </td>
                                  </tr> 
                                  <tr><td height="2"></td></tr>
                                  <!-- //경기결과  -->
                                <!-- 경기결과 --> 
                                  <tr>
                                    <td>
                                    <table width="800" border="0" cellspacing="0" cellpadding="0" class="s_bg_ing">                                    
                                      <tr>
                                        <th width="20" class="s_date" ><input type="checkbox" name="checkbox2" id="checkbox2"></th>
                                        <th width="110" class="s_date" >11/01 [월] 18:00</th>
                                        <td width="580">
                                        <!-- 원정우승 -->
                                        <table width="580" cellpadding="0" cellspacing="0" style="background:url(../images/sports/away_win_bg.gif);">
                                        <tr>
                                        <th width="210" class="s_home_name">롯데자이언트</th>
                                        <th width="50" class="s_home_point">2.85</th>                                     
                                        <th width="60" class="s_vs_stop">vs</th>
                                        <th width="50" class="s_away_point" >2.15</th>
                                        <th width="210" class="s_away_name" >기아타이거즈</th>
                                        </tr>
                                        </table>
                                        <!-- //원정우승 -->
                                        </td>
                                        <th width="45" class="s_result">52:63</th>
                                        <th width="45" class="s_result"><span class="s_result_miss">미적중</span></th>
                                      </tr>                                     
                                    </table>
                                    </td>
                                  </tr> 
                                  <tr><td height="2"></td></tr>
                                  <!-- 결과산출-->
                                  <tr>
                                    <td height="30">
                                    <table width="800" border="0" cellspacing="0" cellpadding="0" bgcolor="#9e1940" class="s_result_cal " >
                                      <tr>
                                        <td height="26">배팅날짜 :<span class="s_result_cal2"> 2014 11/01 18:00</span></td>
                                        <td>배팅금액 : <span class="s_result_cal2">100,000</span></td>
                                        <td>배당률 : <span class="s_result_cal2">11.71</span></td>
                                        <td>예상배당금 : <span class="s_result_cal2">1,171,350</span></td>
                                        <td>당첨금 : <span class="s_result_cal2">0</span></td>
                                        <td><span class="s_result_cal2">게임종료</span></td>
                                      </tr>
                                    </table></td></tr>
                                  <!-- //결과산출-->
                                  <!-- //경기결과  -->
                                  <tr><td height="5"></td></tr>
                                </table>
                                </td>
                              </tr>
                              <!-- //경기 --> <!-- 경기 -->                           
                              <tr>
                                <td>
                                <table width="800" border="0" cellspacing="0" cellpadding="0">
                                 <!--경기제목-->
                                        <tr>
                                          <th height="26" bgcolor="#ededed" class="s_tit"><img src="../images/sports/icon_kbo.png" width="18" height="13"> KBO 프로야구</th>
                                          </tr>
                                        <tr><td height="2"></td></tr>
                                        <!--//경기제목-->
                              <!-- 경기결과 --> 
                                  <tr>
                                    <td>
                                    <table width="800" border="0" cellspacing="0" cellpadding="0" class="s_bg_ing">                                    
                                      <tr>
                                        <th width="20" class="s_date" ><input type="checkbox" name="checkbox" id="checkbox"></th>
                                        <th width="110" class="s_date" >11/01 [월] 18:00</th>
                                        <td width="580">
                                        <!-- 홈팀우승 -->
                                        <table width="580" cellpadding="0" cellspacing="0" style="background:url(../images/sports/home_win_bg.gif);">
                                        <tr>
                                        <th width="210" class="s_home_name">롯데자이언트</th>
                                        <th width="50" class="s_home_point">2.85</th>                                     
                                        <th width="60" class="s_vs_stop">vs</th>
                                        <th width="50" class="s_away_point" >2.15</th>
                                        <th width="210" class="s_away_name" >기아타이거즈</th>
                                        </tr>
                                        </table>
                                        <!-- //홈팀우승 -->
                                        </td>
                                        <th width="45" class="s_result">1:0</th>
                                        <th width="45" class="s_result"><span class="s_result_right">적중</span></th>
                                      </tr>                                     
                                    </table>
                                    </td>
                                  </tr> 
                                  <tr><td height="2"></td></tr>
                                  <!-- //경기결과  -->
                                <!-- 경기결과 --> 
                                  <tr>
                                    <td>
                                    <table width="800" border="0" cellspacing="0" cellpadding="0" class="s_bg_ing">                                    
                                      <tr>
                                        <th width="20" class="s_date" ><input type="checkbox" name="checkbox2" id="checkbox2"></th>
                                        <th width="110" class="s_date" >11/01 [월] 18:00</th>
                                        <td width="580">
                                        <!-- 원정우승 -->
                                        <table width="580" cellpadding="0" cellspacing="0" style="background:url(../images/sports/away_win_bg.gif);">
                                        <tr>
                                        <th width="210" class="s_home_name">롯데자이언트</th>
                                        <th width="50" class="s_home_point">2.85</th>                                     
                                        <th width="60" class="s_vs_stop">vs</th>
                                        <th width="50" class="s_away_point" >2.15</th>
                                        <th width="210" class="s_away_name" >기아타이거즈</th>
                                        </tr>
                                        </table>
                                        <!-- //원정우승 -->
                                        </td>
                                        <th width="45" class="s_result">52:63</th>
                                        <th width="45" class="s_result"><span class="s_result_miss">미적중</span></th>
                                      </tr>                                     
                                    </table>
                                    </td>
                                  </tr> 
                                  <tr><td height="2"></td></tr>
                                  <!-- 결과산출-->
                                  <tr>
                                    <td height="30">
                                    <table width="800" border="0" cellspacing="0" cellpadding="0" bgcolor="#9e1940" class="s_result_cal " >
                                      <tr>
                                        <td height="26">배팅날짜 :<span class="s_result_cal2"> 2014 11/01 18:00</span></td>
                                        <td>배팅금액 : <span class="s_result_cal2">100,000</span></td>
                                        <td>배당률 : <span class="s_result_cal2">11.71</span></td>
                                        <td>예상배당금 : <span class="s_result_cal2">1,171,350</span></td>
                                        <td>당첨금 : <span class="s_result_cal2">0</span></td>
                                        <td><span class="s_result_cal2">게임종료</span></td>
                                      </tr>
                                    </table></td></tr>
                                  <!-- //결과산출-->
                                  <!-- //경기결과  -->
                                  <tr><td height="5"></td></tr>
                                </table>
                                </td>
                              </tr>
                              <!-- //경기 --> <!-- 경기 -->                           
                              <tr>
                                <td>
                                <table width="800" border="0" cellspacing="0" cellpadding="0">
                                 <!--경기제목-->
                                        <tr>
                                          <th height="26" bgcolor="#ededed" class="s_tit"><img src="../images/sports/icon_kbo.png" width="18" height="13"> KBO 프로야구</th>
                                          </tr>
                                        <tr><td height="2"></td></tr>
                                        <!--//경기제목-->
                              <!-- 경기결과 --> 
                                  <tr>
                                    <td>
                                    <table width="800" border="0" cellspacing="0" cellpadding="0" class="s_bg_ing">                                    
                                      <tr>
                                        <th width="20" class="s_date" ><input type="checkbox" name="checkbox" id="checkbox"></th>
                                        <th width="110" class="s_date" >11/01 [월] 18:00</th>
                                        <td width="580">
                                        <!-- 홈팀우승 -->
                                        <table width="580" cellpadding="0" cellspacing="0" style="background:url(../images/sports/home_win_bg.gif);">
                                        <tr>
                                        <th width="210" class="s_home_name">롯데자이언트</th>
                                        <th width="50" class="s_home_point">2.85</th>                                     
                                        <th width="60" class="s_vs_stop">vs</th>
                                        <th width="50" class="s_away_point" >2.15</th>
                                        <th width="210" class="s_away_name" >기아타이거즈</th>
                                        </tr>
                                        </table>
                                        <!-- //홈팀우승 -->
                                        </td>
                                        <th width="45" class="s_result">1:0</th>
                                        <th width="45" class="s_result"><span class="s_result_right">적중</span></th>
                                      </tr>                                     
                                    </table>
                                    </td>
                                  </tr> 
                                  <tr><td height="2"></td></tr>
                                  <!-- //경기결과  -->
                                <!-- 경기결과 --> 
                                  <tr>
                                    <td>
                                    <table width="800" border="0" cellspacing="0" cellpadding="0" class="s_bg_ing">                                    
                                      <tr>
                                        <th width="20" class="s_date" ><input type="checkbox" name="checkbox2" id="checkbox2"></th>
                                        <th width="110" class="s_date" >11/01 [월] 18:00</th>
                                        <td width="580">
                                        <!-- 원정우승 -->
                                        <table width="580" cellpadding="0" cellspacing="0" style="background:url(../images/sports/away_win_bg.gif);">
                                        <tr>
                                        <th width="210" class="s_home_name">롯데자이언트</th>
                                        <th width="50" class="s_home_point">2.85</th>                                     
                                        <th width="60" class="s_vs_stop">vs</th>
                                        <th width="50" class="s_away_point" >2.15</th>
                                        <th width="210" class="s_away_name" >기아타이거즈</th>
                                        </tr>
                                        </table>
                                        <!-- //원정우승 -->
                                        </td>
                                        <th width="45" class="s_result">52:63</th>
                                        <th width="45" class="s_result"><span class="s_result_miss">미적중</span></th>
                                      </tr>                                     
                                    </table>
                                    </td>
                                  </tr> 
                                  <tr><td height="2"></td></tr>
                                  <!-- 결과산출-->
                                  <tr>
                                    <td height="30">
                                    <table width="800" border="0" cellspacing="0" cellpadding="0" bgcolor="#9e1940" class="s_result_cal " >
                                      <tr>
                                        <td height="26">배팅날짜 :<span class="s_result_cal2"> 2014 11/01 18:00</span></td>
                                        <td>배팅금액 : <span class="s_result_cal2">100,000</span></td>
                                        <td>배당률 : <span class="s_result_cal2">11.71</span></td>
                                        <td>예상배당금 : <span class="s_result_cal2">1,171,350</span></td>
                                        <td>당첨금 : <span class="s_result_cal2">0</span></td>
                                        <td><span class="s_result_cal2">게임종료</span></td>
                                      </tr>
                                    </table></td></tr>
                                  <!-- //결과산출-->
                                  <!-- //경기결과  -->
                                  <tr><td height="5"></td></tr>
                                </table>
                                </td>
                              </tr>
                              <!-- //경기 --> <!-- 경기 -->                           
                              <tr>
                                <td>
                                <table width="800" border="0" cellspacing="0" cellpadding="0">
                                 <!--경기제목-->
                                        <tr>
                                          <th height="26" bgcolor="#ededed" class="s_tit"><img src="../images/sports/icon_kbo.png" width="18" height="13"> KBO 프로야구</th>
                                          </tr>
                                        <tr><td height="2"></td></tr>
                                        <!--//경기제목-->
                              <!-- 경기결과 --> 
                                  <tr>
                                    <td>
                                    <table width="800" border="0" cellspacing="0" cellpadding="0" class="s_bg_ing">                                    
                                      <tr>
                                        <th width="20" class="s_date" ><input type="checkbox" name="checkbox" id="checkbox"></th>
                                        <th width="110" class="s_date" >11/01 [월] 18:00</th>
                                        <td width="580">
                                        <!-- 홈팀우승 -->
                                        <table width="580" cellpadding="0" cellspacing="0" style="background:url(../images/sports/home_win_bg.gif);">
                                        <tr>
                                        <th width="210" class="s_home_name">롯데자이언트</th>
                                        <th width="50" class="s_home_point">2.85</th>                                     
                                        <th width="60" class="s_vs_stop">vs</th>
                                        <th width="50" class="s_away_point" >2.15</th>
                                        <th width="210" class="s_away_name" >기아타이거즈</th>
                                        </tr>
                                        </table>
                                        <!-- //홈팀우승 -->
                                        </td>
                                        <th width="45" class="s_result">1:0</th>
                                        <th width="45" class="s_result"><span class="s_result_right">적중</span></th>
                                      </tr>                                     
                                    </table>
                                    </td>
                                  </tr> 
                                  <tr><td height="2"></td></tr>
                                  <!-- //경기결과  -->
                                <!-- 경기결과 --> 
                                  <tr>
                                    <td>
                                    <table width="800" border="0" cellspacing="0" cellpadding="0" class="s_bg_ing">                                    
                                      <tr>
                                        <th width="20" class="s_date" ><input type="checkbox" name="checkbox2" id="checkbox2"></th>
                                        <th width="110" class="s_date" >11/01 [월] 18:00</th>
                                        <td width="580">
                                        <!-- 원정우승 -->
                                        <table width="580" cellpadding="0" cellspacing="0" style="background:url(../images/sports/away_win_bg.gif);">
                                        <tr>
                                        <th width="210" class="s_home_name">롯데자이언트</th>
                                        <th width="50" class="s_home_point">2.85</th>                                     
                                        <th width="60" class="s_vs_stop">vs</th>
                                        <th width="50" class="s_away_point" >2.15</th>
                                        <th width="210" class="s_away_name" >기아타이거즈</th>
                                        </tr>
                                        </table>
                                        <!-- //원정우승 -->
                                        </td>
                                        <th width="45" class="s_result">52:63</th>
                                        <th width="45" class="s_result"><span class="s_result_miss">미적중</span></th>
                                      </tr>                                     
                                    </table>
                                    </td>
                                  </tr> 
                                  <tr><td height="2"></td></tr>
                                  <!-- 결과산출-->
                                  <tr>
                                    <td height="30">
                                    <table width="800" border="0" cellspacing="0" cellpadding="0" bgcolor="#9e1940" class="s_result_cal " >
                                      <tr>
                                        <td height="26">배팅날짜 :<span class="s_result_cal2"> 2014 11/01 18:00</span></td>
                                        <td>배팅금액 : <span class="s_result_cal2">100,000</span></td>
                                        <td>배당률 : <span class="s_result_cal2">11.71</span></td>
                                        <td>예상배당금 : <span class="s_result_cal2">1,171,350</span></td>
                                        <td>당첨금 : <span class="s_result_cal2">0</span></td>
                                        <td><span class="s_result_cal2">게임종료</span></td>
                                      </tr>
                                    </table></td></tr>
                                  <!-- //결과산출-->
                                  <!-- //경기결과  -->
                                  <tr><td height="5"></td></tr>
                                </table>
                                </td>
                              </tr>
                              <!-- //경기 --> <!-- 경기 -->                           
                              <tr>
                                <td>
                                <table width="800" border="0" cellspacing="0" cellpadding="0">
                                 <!--경기제목-->
                                        <tr>
                                          <th height="26" bgcolor="#ededed" class="s_tit"><img src="../images/sports/icon_kbo.png" width="18" height="13"> KBO 프로야구</th>
                                          </tr>
                                        <tr><td height="2"></td></tr>
                                        <!--//경기제목-->
                              <!-- 경기결과 --> 
                                  <tr>
                                    <td>
                                    <table width="800" border="0" cellspacing="0" cellpadding="0" class="s_bg_ing">                                    
                                      <tr>
                                        <th width="20" class="s_date" ><input type="checkbox" name="checkbox" id="checkbox"></th>
                                        <th width="110" class="s_date" >11/01 [월] 18:00</th>
                                        <td width="580">
                                        <!-- 홈팀우승 -->
                                        <table width="580" cellpadding="0" cellspacing="0" style="background:url(../images/sports/home_win_bg.gif);">
                                        <tr>
                                        <th width="210" class="s_home_name">롯데자이언트</th>
                                        <th width="50" class="s_home_point">2.85</th>                                     
                                        <th width="60" class="s_vs_stop">vs</th>
                                        <th width="50" class="s_away_point" >2.15</th>
                                        <th width="210" class="s_away_name" >기아타이거즈</th>
                                        </tr>
                                        </table>
                                        <!-- //홈팀우승 -->
                                        </td>
                                        <th width="45" class="s_result">1:0</th>
                                        <th width="45" class="s_result"><span class="s_result_right">적중</span></th>
                                      </tr>                                     
                                    </table>
                                    </td>
                                  </tr> 
                                  <tr><td height="2"></td></tr>
                                  <!-- //경기결과  -->
                                <!-- 경기결과 --> 
                                  <tr>
                                    <td>
                                    <table width="800" border="0" cellspacing="0" cellpadding="0" class="s_bg_ing">                                    
                                      <tr>
                                        <th width="20" class="s_date" ><input type="checkbox" name="checkbox2" id="checkbox2"></th>
                                        <th width="110" class="s_date" >11/01 [월] 18:00</th>
                                        <td width="580">
                                        <!-- 원정우승 -->
                                        <table width="580" cellpadding="0" cellspacing="0" style="background:url(../images/sports/away_win_bg.gif);">
                                        <tr>
                                        <th width="210" class="s_home_name">롯데자이언트</th>
                                        <th width="50" class="s_home_point">2.85</th>                                     
                                        <th width="60" class="s_vs_stop">vs</th>
                                        <th width="50" class="s_away_point" >2.15</th>
                                        <th width="210" class="s_away_name" >기아타이거즈</th>
                                        </tr>
                                        </table>
                                        <!-- //원정우승 -->
                                        </td>
                                        <th width="45" class="s_result">52:63</th>
                                        <th width="45" class="s_result"><span class="s_result_miss">미적중</span></th>
                                      </tr>                                     
                                    </table>
                                    </td>
                                  </tr> 
                                  <tr><td height="2"></td></tr>
                                  <!-- 결과산출-->
                                  <tr>
                                    <td height="30">
                                    <table width="800" border="0" cellspacing="0" cellpadding="0" bgcolor="#9e1940" class="s_result_cal " >
                                      <tr>
                                        <td height="26">배팅날짜 :<span class="s_result_cal2"> 2014 11/01 18:00</span></td>
                                        <td>배팅금액 : <span class="s_result_cal2">100,000</span></td>
                                        <td>배당률 : <span class="s_result_cal2">11.71</span></td>
                                        <td>예상배당금 : <span class="s_result_cal2">1,171,350</span></td>
                                        <td>당첨금 : <span class="s_result_cal2">0</span></td>
                                        <td><span class="s_result_cal2">게임종료</span></td>
                                      </tr>
                                    </table></td></tr>
                                  <!-- //결과산출-->
                                  <!-- //경기결과  -->
                                  <tr><td height="5"></td></tr>
                                </table>
                                </td>
                              </tr>
                              <!-- //경기 --> 
                            </table>
                            </td>
                          </tr>
                        </table>
                        </td>
                      </tr>
                    </table>
                    <!--//승무패 -->
                    </td>
                  </tr>
                  <tr>
                    <td height="50">&nbsp;</td>
                  </tr>
                </table>
                </td>
              </tr>
              <tr>
                <td align="center">
                
                </td>
              </tr>
              <tr>
                <td height="50" align="center"></td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
           <!--footer-->
     <!-- #include file="../include/sub_footer.asp" --> 