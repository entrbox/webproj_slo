<!--#include virtual="/include/Sessionend.asp" -->
<!--#include virtual="/db/db.asp" -->
<%
    '//
	userid  = session("userid")
	btt_idx = rq("btt_idx")
    '//
    set rs = server.CreateObject("ADODB.Recordset")
    SQL = "       SELECT TOP 1 A.BTT_IDX, "
    SQL = SQL & "        B.BTS_IDX, "
    SQL = SQL & "  	     A.WDATE, "
    SQL = SQL & "        C.MB_ID, "
    SQL = SQL & "        C.MB_NICK, "
    SQL = SQL & "        D.GM_DATETIME, "
    SQL = SQL & "        E.SPO_NAME HTEAMNM, "
    SQL = SQL & "        F.SPO_NAME ATEAMNM, "
    SQL = SQL & "        CONVERT(VARCHAR, D.RST_HTM_SCORE) + ':' + CONVERT(VARCHAR, D.RST_ATM_SCORE) GMSCORE, "
    SQL = SQL & "        B.BTS_STATE, "
    SQL = SQL & "        A.BTT_TOT_DIVIDEND, "
    SQL = SQL & "        A.BTT_MONEY, "
    SQL = SQL & "        A.BTT_DIVIDEND_MONEY, "
    SQL = SQL & "        A.BTT_GEND_MONEY, "
    SQL = SQL & "        A.BTT_ACCFLAG, "
    SQL = SQL & "        A.BTT_CNT,B.BTS_CHFLAG, D.GM_HNDFLAG, D.GM_FLAG, G.SPO_TIE, "
    SQL = SQL & "        D.GM_HDVDE,D.GM_TDVDE,D.GM_ADVDE,D.GM_STATE, "
    SQL = SQL & "        B.BTS_HDVDE, B.BTS_TDVDE, B.BTS_ADVDE, D.GM_VSCHB,D.GM_JCODE,H.CPNM "
    SQL = SQL & "   FROM TB_SPO_BETTING_MASTER A  "
    SQL = SQL & "  INNER JOIN TB_SPO_BETTING_INFO B ON A.BTT_IDX=B.BTT_IDX "
    SQL = SQL & "  INNER JOIN TB_MEMBER   C ON A.USERID    = C.MB_ID "
    SQL = SQL & "  INNER JOIN tb_spo_game D ON B.GM_IDX    = D.GM_IDX "
    SQL = SQL & "  INNER JOIN tb_spo_kind E ON D.GM_HTCODE = E.SPO_CODE "
    SQL = SQL & "  INNER JOIN tb_spo_kind F ON D.GM_ATCODE = F.SPO_CODE "
	SQL = SQL & "  INNER JOIN TB_SPO_KIND G ON D.GM_JCODE  = G.SPO_CODE AND G.SPO_FLAG='J' "
	SQL = SQL & "  LEFT OUTER JOIN TB_SPO_CP_USE H ON A.CP_IDX  = H.CP_IDX "
    SQL = SQL & "  WHERE A.BTT_IDX=B.BTT_IDX "
    SQL = SQL & "    AND A.BTT_IDX  = "& BTT_IDX &" "
    SQL = SQL & "    AND A.USERID  = '"& USERID &"'  ORDER BY D.GM_DATETIME ASC"
	rs.Open sql, db, 1


    
    '//
    if not rs.eof then
        btt_idx = rs("btt_idx")
        bts_idx = rs("bts_idx")
        wdate   = rs("wdate")
        mb_id   = rs("mb_id")
        mb_nick = rs("mb_nick")
        gm_datetime = rs("gm_datetime")
        hteamnm   = rs("hteamnm")
        ateamnm   = rs("ateamnm")
        gmscore   = rs("gmscore")
        bts_state = rs("bts_state")
        btt_tot_dividend   = rs("btt_tot_dividend")
        btt_money          = rs("btt_money")
        btt_dividend_money = rs("btt_dividend_money")
        btt_gend_money = rs("btt_gend_money")
        btt_accflag    = rs("btt_accflag")
        btt_cnt        = rs("btt_cnt")
        bts_chflag     = rs("bts_chflag")
        gm_hndflag     = rs("gm_hndflag")
        gm_flag        = rs("gm_flag")
        spo_tie        = rs("spo_tie")
    	gm_vschb = rs("gm_vschb")
        gm_jcode = rs("gm_jcode")
        cpnm = rs("cpnm")
        '//
        gm_state = rs("gm_state")
        '//
        if con_ord_cancel_min="" or isnull(con_ord_cancel_min) then con_ord_cancel_min=0
        max_cancel_wdate = getDateRe("8",wdate,"-")
        max_cancel_wdate = getDateTime2(DateAdd("n",con_ord_cancel_min,max_cancel_wdate))
        '//
        wdate2      = mid(getDateRe("8",wdate,"-"),6)
        wdate       = mid(getDateRe("8",wdate,"-"),6)
        gm_datetime = mid(getDateRe("8",gm_datetime,"-"),6)
        gm_datetime = left(gm_datetime,len(gm_datetime)-1)
        '//
        cancelflag=""
        now_wdate = getDateTime(now())
        if btt_accflag<>"9" then
                if gm_state = "10" or gm_state="20" then
                    if max_cancel_wdate > now_wdate then
                        cancelflag="1"
                    end if
                end if
        end if
        '//
        if btt_accflag="9" then
            cancelflag="1"
        else
            '//
            sqlff = ""
            game_stat_date   = rs("gm_datetime")&"00"
            betting_date     = rs("wdate")
            game_stat_date_r = getDateRe("8",game_stat_date,"-")
            betting_date_r   = getDateRe("8",betting_date,"-")

            btt_60m_after_date  = getDateTime2(DateAdd("n",60,betting_date_r))
            btt_30m_after_date  = getDateTime2(DateAdd("n",30,betting_date_r))
          
            imsidate = getDateRe("8", getGmDatetimeMin(btt_idx) & "00", "-")
            game_30m_bfore_date = getDateTime2(DateAdd("n",-30,imsidate)) '//게임시작 30분전
            game_40m_after_date = getDateTime2(DateAdd("n",40,imsidate))       
            '//
            if cpnm="cp_notmin_30" then
                if now_wdate < btt_60m_after_date and now_wdate < game_30m_bfore_date then
                    cancelflag="1"   
                end if
            else
                aaaa = getCpKindChb_sql(btt_idx,sqlff)

                if instr(aaaa,cpnm)>0 then
                    if now_wdate < game_40m_after_date then 
                    	cancelflag=1 
	                end if	
                end if
            end if
            '//
        end if
    end if
    rs.close
    '//


    if cancelflag="1" then
        '//
        call cancelOrderSave(btt_idx)
        '//
        sql = " delete tb_spo_betting_info where btt_idx = "& btt_idx
        db.execute sql
        sql = " delete tb_spo_betting_master where btt_idx = "& btt_idx
        db.execute sql
        '//
        gmnm = getGmflagRe(gm_flag)
        Call subMemUmoneyLog_spo(userid,btt_money,"1","[" & gmnm & "] - 게임취소 환불 !!",btt_idx)		'아이디 / 사용포인트 / 가감구분(1+,2-) / 사용내역
        '//
        if cdbl(cp_idx)>0 then
            SQL = "UPDATE TB_SPO_CP_USE SET UFLAG = 'Y', UWDATE = NULL WHERE CP_IDX = "& CP_IDX &" "
            DB.EXECUTE SQL
        end if
        '//
    else
        Call getPopup("4","베팅취소가 불가능합니다.","/mypage/mypage_sports.asp")
    end if
    '//
	db.close
	'//
	Call getPopup("4","베팅취소가 되었습니다.","mypage_sports.asp")
%>



