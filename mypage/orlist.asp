<table width="800" border=0 cellpadding="0" cellspacing="0">
	<tr>
		<td height="35">
		<table width="100%" border="0" cellpadding="0" cellspacing="0" style="background-image:url(../images/customer/board_bg_01.jpg)">
			<COLGROUP>
            <COL width="90">
            <COL width="70">
            <COL width="105">
            <COL>
            <COL width="210">
            <COL width="65">
            </COLGROUP>
             <THEAD>
             <TR>
				<TH width="5%" height="35">NO.</TH>
                <td width="3" align="center"><img src="../images/board/board_line_01.gif" width="3" height="22"></td>
                <TH width="8%">추첨시간</TH>
                <td width="3" align="center"><img src="../images/board/board_line_01.gif" width="3" height="22"></td>
                <TH width="9%">회차</TH>
                <td width="3" align="center"><img src="../images/board/board_line_01.gif" width="3" height="22"></td>
                <TH width="8%">게임종류</TH>
                <td width="3" align="center"><img src="../images/board/board_line_01.gif" width="3" height="22"></td>
                <TH width="5%">매수</TH>
                <td width="3" align="center"><img src="../images/board/board_line_01.gif" width="3" height="22"></td>
                <TH width="34%">구매내역</TH>
                <td width="3" align="center"><img src="../images/board/board_line_01.gif" width="3" height="22"></td>
                <TH width="22%">추첨결과</TH>
                <td width="3" align="center"><img src="../images/board/board_line_01.gif" width="3" height="22"></td>
                <TH width="9%">당첨결과</TH>
			</TR>
            </THEAD>
		</TABLE>
		</td>
	</tr>
    <tr>
		<td height="10">&nbsp;</td>
	</tr>
	<tr>
		<td height="1" bgcolor="#292a2d"></td>
	</tr>

<%
	gotopage = rq("gotopage")
	userid   = session("userid")
	If gotopage="" Then gotopage=1
	'//
	set rs = server.CreateObject("ADODB.Recordset")
	SQL = "select  "
	SQL = SQL & " (substring(a.wintime,1,2)+':'+substring(a.wintime,3,2)+':'+substring(a.wintime,5,2)) as wintime, "
	SQL = SQL & " a.gmCode, c.gname, b.chCnt, b.chNumber, d.wNum, b.winFlag, "
	SQL = SQL & " (substring(a.wdate,9,2)+':'+substring(a.wdate,11,2)+':'+substring(a.wdate,13,2)) as ortime, "
	SQL = SQL & " (b.chCnt*1000) as betMoney, (b.chCnt*1000*c.gwinper) as exmoney,b.winmoney,a.orSeq,b.gSeq "
	SQL = SQL & " from tb_order_Info a "
	SQL = SQL & " left outer join tb_order_Game  b on a.orSeq  = b.orSeq "
	SQL = SQL & " left outer join tb_game_code   c on b.gmKind = c.gcode "
	SQL = SQL & " left outer join tb_game_result d on a.gmCode = d.gnum "
	SQL = SQL & " where a.userid = '"& userid &"' "
	SQL = SQL & " and b.delflag = 'n' "
	SQL = SQL & " and b.gmKind_sub = '0' "
	SQL = SQL & " order by a.orSeq desc , b.gSeq desc "
	rs.PageSize=20
	rs.Open sql, db, 1
	if not rs.EOF Then rs.AbsolutePage=int(gotopage)
	schCnt = rs.recordcount
	'//
	j=schCnt-((gotopage-1)*20)
	i=1
	do until rs.EOF or i>rs.pagesize
		wintime = rs("wintime")
		wintime2 = Left(wintime,5)
		gmCode  = rs("gmCode")
		gname   = rs("gname")
		chCnt   = rs("chCnt")
		chNumber= rs("chNumber")
		wNum    = rs("wNum")
		winFlag = rs("winFlag")
		ortime  = rs("ortime")
		betMoney= rs("betMoney")
		exmoney = rs("exmoney")
		winmoney= rs("winmoney")
		orSeq   = rs("orSeq")
		gSeq    = rs("gSeq")
		'//
		choimg = getImageChange("1",chNumber)
		winimg = getImageChange("2",wNum)
		'//
		winFlag = getWinFlagChange(winFlag,wintime,gmCode)
%>
	
	<tr>
		<td height="40">
		<table width="800" cellspacing="5" style="background-color:#1c1d1f">
			<tr>
				<td bgcolor="#292a2d">
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td height="50">
						<table width="100%" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td width="40" align="center"><%=j%></td>
                                <td width="65" align="center"><%=wintime2%></td>
                                <td width="75" align="center"><%=gmCode%></td>
                                <td width="65" align="center"><%=gname%></td>
                                <td width="45" align="center"><%=chCnt%></td>
                                <td align="center"><%=choimg%></td>
                                <td width="175" align="center"><span class="last"><%=winimg%></span></td>
                                <td width="70" align="center" class="info_12"><%If winFlag="미당첨" then%><font color=#9a9e9e><%End if%><%=winFlag%><%If winFlag="미당첨" Or winFlag="당첨" then%><img src='/images/btn_list_delete.gif' onclick="delchb3('<%=gSeq%>')" style="cursor:pointer;" align=top alt="삭제"><%End if%></td>
							</tr>
						</table>
						</td>
					</tr>
                    <tr>
						<td height="1" bgcolor="292a2d"></td>
					</tr>
                    <tr>
						<td height="40">
						<table width="700" border="0" align="right" cellpadding="0" cellspacing="0">
							<tr>
								<td align="center">구매시간 <span class="info_05"><%=ortime%></span></td>
                                <td align="center">매수 <span class="info_05"><%=chCnt%>장</span></td>
                                <td align="center">배팅금액 <span class="info_05"><%=FormatNumber(betMoney,0)%>원</span></td>
                                <td align="center">예상적중금액 <span class="info_05"><%=FormatNumber(exMoney,0)%>원</span></td>
                                <td align="center">당첨금액 <span class="info_05"><%If winFlag="당첨" then%><font color=red><%=FormatNumber(winMoney,0)%><%else%><%=FormatNumber(winMoney,0)%><%end if%></font>원</span></td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
				</td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td height="10"></td>
	</tr>

<%
rs.movenext
i=i+1
j=j-1
loop
%>


	<tr>
		<td height="50" align=center><!--#include virtual="/admin/include/paging.asp" --></td>
	</tr>
</table>
