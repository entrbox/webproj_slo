<!--#include virtual="/include/sessionend.asp" -->
<%
	userid = session("userid")
	'//
	set rs = server.CreateObject("ADODB.Recordset")
	SQL = "select * from tb_agent where userid = '"& userid &"' "
	rs.Open sql, db, 1
	If Not rs.eof then
		aingflag = rs("aingflag")
		aname    = rs("aname")
		sper     = rs("sper")
		If aingflag="n" Then Call getPopup("12","현재 미승인 상태입니다.\n\n관리자에게 문의 하세요.","")
	Else
		If imsiCnt=0 Then Call getPopup("12","해당 총판회원이 아닙니다.","")
	End if
	rs.close
	'//
	set rs = server.CreateObject("ADODB.Recordset")
	SQL = "select sum(isnull(b.cpay,0)-isnull(c.hpay,0)) as rpay from tb_agent a"
	SQL = SQL & " left outer join (select userid, sum(smoney) as cpay from tb_agent_pay where flag='c' group by userid) b on a.userid = b.userid "
	SQL = SQL & " left outer join (select userid, sum(smoney) as hpay from tb_agent_pay where flag='h' group by userid)  c on a.userid = c.userid "
	SQL = SQL & " where a.userid = '"& userid &"' "
	rs.Open sql, db, 1
	ppmoney  = rs(0)
	rs.close
	'//
	set rs = server.CreateObject("ADODB.Recordset")
	SQL = " select count(mb_cid) as mbcidcnt from tb_member where mb_cid = '"& userid &"' "
	rs.Open sql, db, 1
	mbcidcnt = rs(0)
	rs.close
	'//
	set rs = server.CreateObject("ADODB.Recordset")
	SQL = " select a.suserid "
	SQL = SQL & " ,isnull(b.orMoney,0) as orMoney "
	SQL = SQL & " ,isnull(sum(a.c_pmoney),0) as c_pmoney "
	SQL = SQL & " ,isnull(sum(a.h_pmoney),0) as h_pmoney "
	SQL = SQL & " ,c.mb_money "
	SQL = SQL & " ,(isnull(sum(a.c_smoney),0) - isnull(sum(a.h_smoney),0)) as smoney "
	SQL = SQL & " from  "
	SQL = SQL & " 	(select flag,suserid "
	SQL = SQL & " 		,(case flag when 'c' then SUM(pmoney) end) as c_pmoney "
	SQL = SQL & " 		,(case flag when 'h' then SUM(pmoney) end) as h_pmoney "
	SQL = SQL & " 		,(case flag when 'c' then SUM(smoney) end) as c_smoney "
	SQL = SQL & " 		,(case flag when 'h' then SUM(smoney) end) as h_smoney "
	SQL = SQL & " 		from tb_agent_pay  "
	SQL = SQL & " 		where userid = '"& userid &"' and substring(wdate,1,8) = '"& Replace(Left(now(),10),"-","") &"' " 
	SQL = SQL & " 		group by suserid,flag "
	SQL = SQL & " 	) a "
	SQL = SQL & " left outer join (select userid,isnull(sum(orMoney),0) as orMoney from tb_order_info where substring(wdate,1,8) = '"& Replace(Left(now(),10),"-","") &"' group by userid) b on a.suserid = b.userid "
	SQL = SQL & " left outer join tb_member c on a.suserid = c.mb_id "
	SQL = SQL & " group by a.suserid,b.orMoney,c.mb_money "
	SQL = SQL & " order by a.suserid asc "
	rs.Open sql, db, 1
	schCnt = rs.recordcount
	'//
%>

<HTML>
<HEAD><TITLE>ONESHOT - Partner.</TITLE>
<meta http-equiv='Content-Type' content='text/html; charset=euc-kr'>
<meta http-equiv='imagetoolbar' content='no'>
<style>
* {
	font-size:12px;
	font-family:dotum;
	color:#666;
}
body {padding:10px;}
table {
	border-collapse: collapse;
	border:0;
}
	#wrap {width:800px;margin:0 auto;}
	#header {width:100%;height:30px;padding-bottom:15px;}
	.title {float:left;width:13%;height:100%;background:url(../images/popup/partner_title.gif) no-repeat center left;}
	.box {float:right;width:86%;height:100%;border:1px solid #e6e5e5;}
	.box div {margin:6px 10px;}
	.box img {vertical-align:middle;}
	.name {color:#000000;font-weight:bold;}
	#header:after {content: "";clear:both;}
	#container {border:2px solid #4d4d4d;}
	.notice {width:100%;padding:20px 0; background:#4d4d4d;border:1px solid #7d838f;}
	.notice table {width:90%;margin:0 auto;border:1px solid #7d838f;}
	.notice table th,
	.notice table td {color:#fff;border-bottom:1px solid #7d838f;}
	.notice table th {width:103px;height:25px;background:url(../images/popup/partner_cell_bg.gif) no-repeat;text-align:center;}
	.notice table td {padding-left:10px;border-left:1px solid #7d838f;}
	.body {width:100%;margin:30px 0;}
	.body table {width:95%;margin:10px auto;text-align:center;border-top:1px solid #ccc;}
	.body table th {height:32px;background:url(../images/charge/charge_bg_01.jpg) repeat-x;}
	.body table td {height:30px; border-bottom:1px solid #cccccc}
</style>
<body>
<div id="wrap">
	<div id="header">
		<div class="title"></div>
		<div class="box">
			<div><img src="../images/menu/icon_01.gif"> 안녕하세요. [ <img src="../images/board/level_4.gif"> <span class="name"><%=aname%> 파트너님</span> ] 수익률은 <span style="color:red;"><%=sper%>%</span> 입니다.]</div>
		</div>
	</div>
	<div id="container">
		<div class="notice">
			<table cellpadding="0" cellspacing="1" border="0" >
				<tr>
					<th>내 추천인 수</th>
					<td><%=mbcidcnt%>명</td>
					<td rowspan="2" align="center">※ 수익금 정산은<br>고객센터에 문의해주시기 바랍니다.</td>
				</tr>
				<tr>
					<th>수익금액</th>
					<td><%=FormatNumber(ppmoney,0)%> p</td>
				</tr>
			</table>
		</div>
		<div class="body">
			<table width="99%" border="0" cellpadding="5" cellspacing="0">
			<tr>
			<th>회원목록</th>
			<th>금일베팅금액</th>
			<th>금일충전금액</th>
			<th>금일환전금액</th>
			<th>보유머니</th>
			<th>수익금</th>
			</tr>

<%
i=1
do until rs.EOF
	suserid  = rs(0)
	orMoney  = rs(1)
	c_pmoney = rs(2)
	h_pmoney = rs(3)
	mb_money = rs(4)
	smoney   = rs(5)
	'//
	bgcolor = ""
	If i Mod 2 = 0 Then bgcolor = "bgcolor=#ededed"
%>

			<tr <%=bgcolor%>>
			<td><%=Left(suserid,3)%>**</td>
			<td><%=FormatNumber(orMoney,0)%></td>
			<td><%=FormatNumber(c_pmoney,0)%></td>
			<td><%=FormatNumber(h_pmoney,0)%></td>
			<td><%=FormatNumber(mb_money,0)%></td>
			<td><%=FormatNumber(smoney,0)%></td>
			</tr>

<%
rs.movenext
i=i+1
Loop
rs.close
%>

			</table>
		</div>
	</div>
</div>
</body>
</HTML>