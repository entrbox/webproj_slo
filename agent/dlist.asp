<!--#include virtual="/agent/inc/top.asp" -->
<%
    FUNCTION getAgentPaySas_w(val1,val2,val3)
        SQL = "     SELECT A.IDX, "
       SQL = SQL & " 	   A.AID, "
       SQL = SQL & " 	   A.IN_MONEY, "
       SQL = SQL & " 	   A.OUT_MONEY, "
       SQL = SQL & " 	   A.END_MONEY, A.ORMONEY, "
       SQL = SQL & " 	   A.USER_MONEY, "
       SQL = SQL & " 	   A.WDATE, "
       SQL = SQL & " 	   A.ORHAPMONEY "
       SQL = SQL & "  FROM "
       SQL = SQL & " ( "

        SQL = SQL & " SELECT IDX, "
        SQL = SQL & " 	   AID, "
        SQL = SQL & " 	   IN_MONEY, "
        SQL = SQL & " 	   OUT_MONEY, "
        SQL = SQL & " 	   END_MONEY, "
        SQL = SQL & "      ORMONEY, "
        SQL = SQL & " 	   USER_MONEY, "
        SQL = SQL & " 	   SUBSTRING(WDATE,1,8) WDATE, "
        SQL = SQL & " 	   ORHAPMONEY "
        SQL = SQL & "   FROM TB_AGENT_DAYLIST "
        SQL = SQL & " UNION ALL  "

        SQL = SQL & " SELECT 0, T1.userid AID, ISNULL(T2.IN_MONEY,0), ISNULL(T2.OUT_MONEY,0), ISNULL(T2.END_MONEY,0), ISNULL(T2.ORMONEY,0), ISNULL(T3.USER_MONEY,0), "& REPLACE(LEFT(NOW(),10),"-","") &" WDATE, ISNULL(T2.ORHAPMONEY,0) "
        SQL = SQL & "   FROM tb_agent T1 "
        SQL = SQL & "   LEFT OUTER JOIN ( "

        SQL = SQL & " SELECT 0 IDX, "
        SQL = SQL & "        A.MB_CODE AID, "
        SQL = SQL & "        SUM(A.IN_MONEY)  IN_MONEY, "
        SQL = SQL & "        SUM(A.OUT_MONEY) OUT_MONEY, "
        SQL = SQL & "        (SUM(A.IN_MONEY)-SUM(A.OUT_MONEY)-SUM(A.ORMONEY)) END_MONEY, "
        SQL = SQL & "        SUM(A.ORMONEY) ORMONEY, "
        SQL = SQL & "        A.WDATE, "
        SQL = SQL & "        SUM(A.ORHAPMONEY) ORHAPMONEY "
        SQL = SQL & " FROM "
        SQL = SQL & "         ( "
        SQL = SQL & " 		SELECT SUM(A.CMONEY) IN_MONEY, 0 OUT_MONEY, 0 ORMONEY, B.MB_CODE, SUBSTRING(A.SWDATE,1,8) WDATE, 0 ORHAPMONEY "
        SQL = SQL & " 		  FROM TB_CHARGE A, TB_MEMBER B "
        SQL = SQL & " 		 WHERE A.USERID=B.MB_ID "
        SQL = SQL & " 		   AND A.CFLAG='1'  "
        SQL = SQL & " 		   AND A.INGFLAG='5'  "
        SQL = SQL & " 		 GROUP BY B.MB_CODE, SUBSTRING(A.SWDATE,1,8) "
        SQL = SQL & " 		 UNION ALL "
        SQL = SQL & " 		SELECT 0, SUM(A.CMONEY) OUT_MONEY, 0, B.MB_CODE, SUBSTRING(A.SWDATE,1,8) WDATE, 0 ORHAPMONEY "
        SQL = SQL & " 		  FROM TB_CHARGE A, TB_MEMBER B "
        SQL = SQL & " 		 WHERE A.USERID=B.MB_ID "
        SQL = SQL & " 		   AND A.CFLAG='2'  "
        SQL = SQL & " 		   AND A.INGFLAG='5'  "
        SQL = SQL & " 		 GROUP BY B.MB_CODE, SUBSTRING(A.SWDATE,1,8) "
        SQL = SQL & " 		 UNION ALL "
        SQL = SQL & " 		SELECT 0,0,SUM(A.ORMONEY) ORMONEY, A.MB_CODE, A.WDATE, 0 ORHAPMONEY "
        SQL = SQL & " 		  FROM ( "
        SQL = SQL & " 				SELECT ISNULL(SUM(ORMONEY),0) AS ORMONEY , B.MB_CODE, SUBSTRING(WDATE,1,8) WDATE "
        SQL = SQL & " 				  FROM TB_ORDER_INFO A, TB_MEMBER B "
        SQL = SQL & " 				 WHERE A.USERID=B.MB_ID "
        SQL = SQL & " 				   AND INGFLAG = '0'  "
        SQL = SQL & " 				 GROUP BY SUBSTRING(WDATE,1,8),B.MB_CODE "
        SQL = SQL & " 				UNION ALL "
        SQL = SQL & " 				SELECT ISNULL(SUM(BTT_MONEY),0) AS ORMONEY , B.MB_CODE, SUBSTRING(WDATE,1,8) WDATE "
        SQL = SQL & " 				  FROM TB_SPO_BETTING_MASTER A, TB_MEMBER B "
        SQL = SQL & " 				 WHERE A.USERID=B.MB_ID  "
        SQL = SQL & " 				   AND BTT_ACCFLAG = '1'  "
        SQL = SQL & " 				 GROUP BY SUBSTRING(WDATE,1,8),B.MB_CODE "
        SQL = SQL & " 			  ) A "
        SQL = SQL & " 		 GROUP BY A.WDATE, A.MB_CODE "
        '//
        SQL = SQL & " 		 UNION ALL "
        SQL = SQL & " 		 SELECT 0,0,0, A.MB_CODE, A.WDATE, SUM(A.ORMONEY) ORHAPMONEY "
        SQL = SQL & " 		 FROM ( "
        SQL = SQL & " 		 		SELECT ISNULL(SUM(BTT_MONEY),0) AS ORMONEY, B.MB_CODE, SUBSTRING(A.WDATE,1,8) WDATE "
        SQL = SQL & " 		 		FROM tb_spo_betting_MASTER A "
        SQL = SQL & " 		 		INNER JOIN TB_MEMBER B ON A.USERID=B.MB_ID "
        SQL = SQL & " 		 		GROUP BY B.MB_CODE, SUBSTRING(A.WDATE,1,8) "
        SQL = SQL & " 		 		UNION ALL "
        SQL = SQL & " 		 		SELECT ISNULL(SUM(ORMONEY),0) AS ORMONEY , B.MB_CODE, SUBSTRING(WDATE,1,8) WDATE "
        SQL = SQL & " 		 		FROM TB_ORDER_INFO A, TB_MEMBER B  "
        SQL = SQL & " 		 		WHERE A.USERID=B.MB_ID  "
        SQL = SQL & " 		 		GROUP BY SUBSTRING(WDATE,1,8),B.MB_CODE "
        SQL = SQL & " 		 	) A "
        SQL = SQL & " 		  GROUP BY A.WDATE, A.MB_CODE "
        '//
        SQL = SQL & "          ) A "
        SQL = SQL & " WHERE A.WDATE = '"& REPLACE(LEFT(NOW(),10),"-","") &"' "
        SQL = SQL & " GROUP BY A.MB_CODE, A.WDATE "

        SQL = SQL & " ) T2 ON T1.userid=T2.AID "

        SQL = SQL & " LEFT OUTER JOIN ( "
        SQL = SQL & " 				   SELECT SUM(MB_MONEY) USER_MONEY, MB_CODE  "
        SQL = SQL & " 					  FROM TB_MEMBER  "
        SQL = SQL & " 					 GROUP BY MB_CODE "
        SQL = SQL & " 			    ) T3 ON T1.USERID=T3.MB_CODE "



        SQL = SQL & " ) A "
        SQL = SQL & " WHERE A.WDATE >= '"& VAL1 &"' AND A.WDATE <= '"& VAL2 &"' "
        '//
        SQL = SQL & " AND A.AID = '"& SESSION("AGT_USERID") &"' "
        '//
        SQL = SQL & " GROUP BY A.IDX, A.AID, A.IN_MONEY, A.OUT_MONEY, A.END_MONEY, A.ORMONEY, A.USER_MONEY, A.WDATE, A.ORHAPMONEY  "
        SQL = SQL & " ORDER BY A.WDATE DESC, A.AID ASC "
        '//
        getAgentPaySas_w = SQL
        '//
    END FUNCTION	
%>	
<%
	stxt1 = rq("stxt1")
	stxt2 = rq("stxt2")
	stxt3 = rq("stxt3")
	stxt4 = rq("stxt4")
	stxt5 = rq("stxt5")
	stxt6 = rq("stxt6")
	stxt7 = rq("stxt7")
	stxt8 = rq("stxt8")
	stxt9 = rq("stxt9")
	stxt10 = rq("stxt10")
	GotoPage = rq("GotoPage")
	'//
	If stxt1="" then stxt1 = Left(now(),8) &"01"
	If stxt2="" then stxt2 = Left(now(),10)
	if GotoPage = "" Then GotoPage = 1
	stxt11 = Replace(stxt1,"-","")
	stxt22 = Replace(stxt2,"-","")
%>

<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td bgcolor=black height=35> &nbsp; <font color=white><B>날짜별현황 </td>
    </tr>
</table>

<script language='JavaScript'>
<!--
function searChb(){
	if ((form.stxt4.value != "")&&(form.stxt5.value == "")){alert('검색어를 선택해 주세요.');form.stxt5.focus();	return false;}
	if ((form.stxt4.value == "")&&(form.stxt5.value != "")){alert('검색구분을 입력해 주세요.');form.stxt4.focus();	return false;}
	form.submit();
}

//-->
</script>

<form name="form" method="post" action="?">

<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td height=30>
            <input type="text" name="stxt1" id="stxt1" value="<%=stxt1%>" size="10" readonly />
            &nbsp;<img src="/admin/images/calendar.gif" align="absmiddle" OnClick="calendar(event, 'stxt1')" style="cursor:pointer">
            ~
            <input type="text" name="stxt2" id="stxt2" value="<%=stxt2%>" size="10" readonly />
            &nbsp;<img src="/admin/images/calendar.gif" align="absmiddle" OnClick="calendar(event, 'stxt2')" style="cursor:pointer">
            <!--select name=stxt4>
                <option value="">--검색구분--
                <option value="b.MB_ID" <%If stxt4="b.MB_ID" Then Response.write " selected"%>>아이디
                <option value="b.MB_NICK" <%If stxt4="b.MB_NICK" Then Response.write " selected"%>>닉네임
            </select>
            <input type=text name=stxt5 value="<%=stxt5%>" size=20-->
            <input type="image" src="/admin/images/btn_search1.gif" align=bottom onclick="return searChb()">
        </td>
    </tr>
</table>

</form>

				<table border="0" cellpadding="1" cellspacing="1" width=100% bgcolor="#CBE1EE">
					<tr height=35 bgcolor="#F2F2F2" align=center>
						<td width=5%><font color=black>No.</td>
						<td width=12%><font color=black>일자</td>
						<td width=12%><font color=black>총판ID</td>
						<td width=10%><font color=black>입금</td>
						<td width=10%><font color=black>출금</td>
						<td width=13%><font color=black>베팅금액</td>
						<td width=13%><font color=black>구매중머니</td>
						<td width=13%><font color=black>정산</td>
						<td width=14%><font color=black>유저보유금액</td>
					</tr>

<%
    SQL = getAgentPaySas_w(stxt11,stxt22,stxt4)
	set rs = server.CreateObject("ADODB.Recordset")
	rs.Open sql, db, 1
	schCnt = rs.recordcount
	'//
    param = "gotopage="&gotopage&"&stxt1="&stxt1&"&stxt2="&stxt2&"&stxt3="&stxt3&"&stxt4="&stxt4&"&stxt5="&stxt5&"&stxt6="&stxt6&"&stxt7="&stxt7&"&stxt8="&stxt8&"&stxt9="&stxt9&"&stxt10="&stxt10

i=1
tot_in_money   = 0
tot_out_money  = 0
tot_end_money  = 0
tot_ormoney    = 0
tot_user_money = 0
do until rs.EOF
    idx        = rs(0)
    aid        = rs(1)
    in_money   = rs(2)
    out_money  = rs(3)
    end_money  = rs(4)
    ormoney    = rs(5)
    user_money = rs(6)
    wdate      = rs(7)
    orhapmoney = rs(8)
	'//
	wdate  = getDateRe("1",wdate,"-")
    '//
    tot_in_money   = tot_in_money   + in_money
    tot_out_money  = tot_out_money  + out_money
    tot_end_money  = tot_end_money  + end_money
    tot_ormoney    = tot_ormoney    + ormoney
    tot_user_money = tot_user_money + user_money
    tot_orhapmoney = tot_orhapmoney + orhapmoney
    '//
%>

					<tr height=25 bgcolor=white align=center>
						<td><font color=black><%=i%></td>
						<td><font color=black><%=wdate%></td>
						<td><font color=black><%=aid%></td>
						<td align=right><font color=#CC8800><%=formatnumber(in_money,0)%>&nbsp;</td>
						<td align=right><font color=black><%=FormatNumber(out_money,0)%>&nbsp;</td>
						<td align=right><font color=red><%=formatnumber(orhapmoney,0)%>&nbsp;</td>
						<td align=right><font color=#CC8800><%=FormatNumber(ormoney,0)%>&nbsp;</td>
						<td align=right><font color=blue><%=FormatNumber(end_money,0)%>&nbsp;</td>
						<td align=right><font color=black><%=FormatNumber(user_money,0)%>&nbsp;</td>
					</tr>


<%
rs.movenext
i=i+1
loop
%>

					<tr height=35 bgcolor=#CC8800 align=center>
						<td colspan=3><font color=white><b><%=old_wdate%></td>
						<td align=right bgcolor=#668800><font color=#CC8800><b><%=formatnumber(tot_in_money,0)%>&nbsp;</td>
						<td align=right bgcolor=#668800><font color=black><b><%=formatnumber(tot_out_money,0)%>&nbsp;</td>
						<td align=right bgcolor=#668800><font color=red><b><%=formatnumber(tot_orhapmoney,0)%>&nbsp;</td>
						<td align=right bgcolor=#668800><font color=#CC8800><b><%=FormatNumber(tot_ormoney,0)%>&nbsp;</td>
						<td align=right bgcolor=#668800><font color=blue><b><%=formatnumber(tot_end_money,0)%>&nbsp;</td>
						<td align=right bgcolor=#668800><font color=black><b><%=FormatNumber(tot_user_money,0)%>&nbsp;</td>
					</tr>

				</table>



<!--#include virtual="/agent/inc/bottom.asp" -->