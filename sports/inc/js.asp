<script type="text/javascript">

var js_gm_flag = "<%=gm_flag%>";

function clickChb(jsval1,jsval2,jsval3,jsval4) {
    var js_flag     = jsval1;   //H,T,A구분
    var js_idx      = jsval2;   //게임키
    var js_dividend = jsval3;   //배당율
    var js_gct_idx  = jsval4;   //카트키
    var cpcnt = $(":checkbox[name='cpuseyn']:checked").length;
    var choice_cp = form.cpusenm.value;

    $.ajax({
            type:"POST"
            , async:true
            , url:"/sports/ajax/cart_ajax_frm.asp"
            , dataType:"html"
            , timeout:30000
            , cache:false
            , data:"gm_idx="+js_idx+"&js_flag="+js_flag+"&gm_flag=<%=gm_flag%>&gct_dividend="+js_dividend+"&gct_idx="+js_gct_idx+"&cpcnt="+cpcnt+"&choice_cp="+choice_cp
            , contentType:"application/x-www-form-urlencoded; charset=UTF-8"
            , error: function(request, status, error){
                    alert("code : " + request.status + "\r\nmessage : " + request.responseText);
                    //$("aaa").append(request.responseText);
                    //document.getElementById("aaa").innerHTML=request.responseText;
                }
            , success: function(response, status, request){
                    result_html  = response;
          			var imsiArr  = result_html.split('&&^^&&');
			        var errorTxt = imsiArr[0];
			        var mainHtml = imsiArr[1];

                    if (errorTxt=="a"){
                        jsMsgFun('10건 초과!!');
                    }else if (errorTxt=="100"){
                        jsMsgFun('누적배당이 100배를 초과할 수 없습니다.');
                    }else if(errorTxt=="b"){
                        document.getElementById("right_banner").innerHTML=mainHtml;
                    }else if(errorTxt=="c"){
                        alert('축구종목 동일한 경기로 핸디캡 / 오버언더 베팅은 불가능합니다.');
                        document.getElementById("right_banner").innerHTML=mainHtml;
                    }else if(errorTxt.substring(0,1)=="d"){
                        var bonusarr = errorTxt.split(',');
                        var bonuscnt = bonusarr[1];
                        alert(bonuscnt+'경기 이상 선택하셔야 합니다.');
                        document.getElementById("right_banner").innerHTML=mainHtml;
                    }else if(errorTxt.substring(0,1)=="o"){
                        var bonusarr = errorTxt.split(',');
                        var bonuscnt = bonusarr[1];
                        alert('보너스배당은 '+bonuscnt+'경기 이상 선택하셔야 합니다.');
                    }else if(errorTxt=="p"){
                        alert('보너스배당은 1폴더만 베팅이 가능합니다.');
//                        document.getElementById("right_banner").innerHTML=mainHtml;
                    }else if(errorTxt=="e"){
                        alert('보너스배당 조합수가 설정되지 않았습니다.');
                        document.getElementById("right_banner").innerHTML=mainHtml;
                    }else if(errorTxt=="f"){
                        jsMsgFun('사다리게임은 사다리게임 조합만 가능합니다.');
                    }else if(errorTxt=="ff"){
                        jsMsgFun('다리다리는 다리다리 게임조합만 가능합니다.');
                    }else if(errorTxt=="f5"){
                        jsMsgFun('빙고게임은 빙고게임 조합만 가능합니다.');
                    }
                    else if(errorTxt=="q"){
                        jsMsgFun('사다리게임 동일회차는 단폴더 배팅만 가능합니다.');
                    }
                    else if(errorTxt=="qq"){
                        jsMsgFun('다리다리 동일회차는 단폴더 배팅만 가능합니다.');
                    }	
                    else if(errorTxt=="q5"){
                        jsMsgFun('빙고게임 동일회차는 단폴더 배팅만 가능합니다.');
                    }	                    				
                    else if(errorTxt=="r"){
                        jsMsgFun('파워볼 동일회차는 2개까지만 조합이 가능합니다.');
                    }                      
                    else if(errorTxt.substring(0,1)=="g"){
                        var errcntarr = errorTxt.split(',');
                        var errcntcnt1 = errcntarr[0];
                        var errcntcnt2 = errcntarr[1];

                        if (errcntcnt2=="20"){
                            alert('동일경기 1이닝득점/무득점과 오버언더는 조합은 안됩니다.');
                        }

                        if (errcntcnt2=="21" || errcntcnt2=="22"){
                            alert('동일경기 승무패에서 무, 오버언더는 조합은 안됩니다.');
                        }

                        if (errcntcnt2=="23"){
                            alert('동일경기 승,무,패 핸디캡 조합은 안됩니다.');
                        }
                        
                         if (errcntcnt2=="24"){
                            alert('동일경기 승무패, 오버언더 조합은 안됩니다.');
                        }
                                                
                    }else if(errorTxt=="h"){
                        jsMsgFun('승무패,핸디캡은 스페셜1,스페셜2와 조합이 안됩니다.');
                    }else if(errorTxt=="j1"){
                        jsMsgFun('네임드달팽이 동일회차는 1게임만 배팅이 가능합니다.');
                    }else if(errorTxt=="j"){
                        jsMsgFun('축구종목의 동일경기는 승무패, 언더오버 조합이 안됩니다.');
                    }else if(errorTxt=="i"){
                        jsMsgFun('동일경기는 3번까지만 베팅이 가능합니다.');
                    }else{
	                    
	                    if (js_flag=="H"){
	                        $('#homeTB_'+js_idx).addClass("s_home-sel");
	                        $('#homePO_'+js_idx).addClass("s_home_point-sel");
	                    }else if (js_flag=="T") {
							$('#tieTB_'+js_idx).addClass("s_vs-sel");
							$('#tiePO_'+js_idx).addClass("s_vs_point-sel");
	                    }else if (js_flag=="A") {
							$('#awayTB_'+js_idx).addClass("s_away-sel");
							$('#awayPO_'+js_idx).addClass("s_away_point-sel");
	                    }
	                    //
/*
	                    if (jsval4!=""){
	                        $('#homeTB_'+js_idx).removeClass("s_home-sel");
	                        $('#tieTB_'+js_idx).removeClass("s_vs-sel");
	                        $('#awayTB_'+js_idx).removeClass("s_away-sel");
	                    }	                    
*/
	                   document.getElementById("right_banner").innerHTML=mainHtml;
                        
	                   if(errorTxt=="ss2"){
	                        alert('회원님은 사다리게임 동일회차에 2개의 단폴더배팅 내역이 존재합니다.');
	                    }   
	                    else if(errorTxt=="ss1"){
	                        alert('회원님은 사다리게임 동일회차에 단폴더배팅 내역이 존재합니다.');
	                    }          
	                    else if(errorTxt=="sd1"){
	                        alert('회원님은 사다리게임 동일회차에 조합배팅 내역이 존재합니다.');
	                    }                                                 
	                   else if(errorTxt=="rs2"){
	                        alert('회원님은 다리다리 동일회차에 2개의 단폴더배팅 내역이 존재합니다.');
	                    }   
	                    else if(errorTxt=="rs1"){
	                        alert('회원님은 다리다리 동일회차에 단폴더배팅 내역이 존재합니다.');
	                    }          
	                    else if(errorTxt=="rd1"){
	                        alert('회원님은 다리다리 동일회차에 조합배팅 내역이 존재합니다.');
	                    }  
	                   else if(errorTxt=="bs2"){
	                        alert('회원님은 빙고게임 동일회차에 2개의 단폴더배팅 내역이 존재합니다.');
	                    }   
	                    else if(errorTxt=="bs1"){
	                        alert('회원님은 빙고게임 동일회차에 단폴더배팅 내역이 존재합니다.');
	                    }          
	                    else if(errorTxt=="bd1"){
	                        alert('회원님은 빙고게임 동일회차에 조합배팅 내역이 존재합니다.');
	                    }
                    }
                }
            , beforeSend: function(){
                    $('#homeTB_'+js_idx).removeClass("s_home-sel");$('#homePO_'+js_idx).removeClass("s_home_point-sel");
                    $('#tieTB_'+js_idx).removeClass("s_vs-sel");$('#tiePO_'+js_idx).removeClass("s_vs_point-sel");
                    $('#awayTB_'+js_idx).removeClass("s_away-sel");$('#awayPO_'+js_idx).removeClass("s_away_point-sel");
                }
            , complete: function(){

                }
        });
}

function load_right_banner() {
    $.ajax({
            type:"POST"
            , async:true
            , url:"/sports/ajax/cart_ajax_frm.asp"
            , dataType:"html"
            , timeout:30000
            , cache:false
            , data:""
            , contentType:"application/x-www-form-urlencoded; charset=UTF-8"
            , error: function(request, status, error){
                    alert("code : " + request.status + "\r\nmessage : " + request.responseText);
                    //$("body").append(request.responseText);
                }
            , success: function(response, status, request){
                    result_html  = response;
          			var imsiArr  = result_html.split('&&^^&&');
			        var errorTxt = imsiArr[0];
			        var mainHtml = imsiArr[1];

                    //document.getElementById("right_banner").innerHTML=mainHtml;
                    $("#right_banner").html(mainHtml);
                }
            , beforeSend: function(){

                }
            , complete: function(){

                }
        });
}

function bttMaxChb3(val1,val2) {
//alert(val1 +' - '+ val2)
    var maxmoney = val1 ;
    var diviPer  = val2 ;

    if (val2==0){
        var input_val = 0;
        var text_val  = 0;
    }else{
        var input_val = Math.floor(val1 / val2) ;
        var text_val  = Math.floor(input_val*val2) ;
    }

    document.getElementById("seeMoney").innerHTML=jsCommaChb(text_val); //예상
    $( "input[name='btt_money']" ).val( jsCommaChb(input_val) );        //input
}

function bttMaxChb(val1,val2) {
//alert(val1 +' - '+ val2)
    var maxmoney = val1 ;
    var diviPer  = val2 ;

    if (val2==0){
        var input_val = 0;
        var text_val  = 0;
    }
    else{
        var input_val = Math.floor(val1 / val2) ;
        if (input_val>1000000) input_val = 1000000
        var text_val  = Math.floor(input_val*val2) ;
    }

    document.getElementById("seeMoney").innerHTML=jsCommaChb(text_val); //예상
    $( "input[name='btt_money']" ).val( jsCommaChb(input_val) );        //input
}

function bttMaxChb2(val1,val2,val3) {
//alert(val1 +' - '+ val2)
    var maxmoney = val1 ;
    var diviPer  = val2 ;

    if (val2==0){
        var input_val = 0;
        var text_val  = 0;
    }
    else{
        var input_val = Math.floor(val1 / val2) ;
        if (input_val>1000000) input_val = 1000000;
        if (val3==1) input_val = 500000;
        var text_val  = Math.floor(input_val*val2) ;
    }

    document.getElementById("seeMoney").innerHTML=jsCommaChb(text_val); //예상
    $( "input[name='btt_money']" ).val( jsCommaChb(input_val) );        //input
}

function bttMaxChbLive(val1,val2) {
    var maxmoney = val1 ;
    var diviPer  = Number(val2) ;
    var js_btt_money=$('#btt_money').val();

    diviPer  = diviPer.toFixed(2);
    js_btt_money=js_btt_money.replace(/,/g,"");
    text_val = diviPer * js_btt_money ;
    //document.getElementById("seeMoney").innerHTML=jsCommaChb(text_val); //예상
        cpchangechb('');
}

function bttIngChb(val1,val2,val3,val4) {   //최대베팅금액 / 예상배당 / 보유머니 / 베팅건수
    var maxmoney = Number(val1) ;
    var diviPer  = Number(val2) ;
    
    if (diviPer>100)
    {
	    alert('누적배당이 100배를 초과할 수 없습니다.');
	    return false;
    }    
    
    diviPer  = diviPer.toFixed(2);

///*
	var sadariCart = Number(val4)
	var sadariSingle = Number($('#sadarisingle').val())
	var sadariDouble = Number($('#sadaridouble').val())
	var daridariSingle = Number($('#daridarisingle').val())
	var daridariDouble = Number($('#daridaridouble').val())		
	var bingoSingle = Number($('#bingosingle').val())
	var bingoDouble = Number($('#bingodouble').val())		
	
	if (sadariCart==1) // single bet
	{
		if (sadariSingle>1)
		{
			alert('사다리게임 동일회차의 \n단폴더배팅은 2회까지만 가능합니다. \n\n회원님은 이미 단폴더배팅 기회를  2회 사용하였습니다.');
			return false;
		}
		if (sadariDouble>0)
		{
			alert('사다리게임 동일회차의 \n조합배팅과 단폴더배팅을 동시에 사용할 수 없습니다.\n\n회원님은 이미 조합배팅 기회를 사용하였습니다.');
			return false;
		}

		if (daridariSingle>1)
		{
			alert('다리다리 동일회차의 \n단폴더배팅은 2회까지만 가능합니다. \n\n회원님은 이미 단폴더배팅 기회를  2회 사용하였습니다.');
			return false;
		}
		if (daridariDouble>0)
		{
			alert('다리다리 동일회차의 \n조합배팅과 단폴더배팅을 동시에 사용할 수 없습니다.\n\n회원님은 이미 조합배팅 기회를 사용하였습니다.');
			return false;
		}		
		
		if (bingoSingle>1)
		{
			alert('빙고게임 동일회차의 \n단폴더배팅은 2회까지만 가능합니다. \n\n회원님은 이미 단폴더배팅 기회를  2회 사용하였습니다.');
			return false;
		}
		if (bingoDouble>0)
		{
			alert('빙고게임 동일회차의 \n조합배팅과 단폴더배팅을 동시에 사용할 수 없습니다.\n\n회원님은 이미 조합배팅 기회를 사용하였습니다.');
			return false;
		}			
	}
	else if (sadariCart>1)
	{
		if (sadariSingle>0)
		{
			alert('사다리게임 동일회차의 \n조합배팅과 단폴더배팅을 동시에 사용할 수 없습니다.\n\n회원님은 이미 단폴더배팅 기회를 사용하였습니다.');
			return false;
		}
		if (daridariSingle>0)
		{
			alert('다리다리 동일회차의 \n조합배팅과 단폴더배팅을 동시에 사용할 수 없습니다.\n\n회원님은 이미 단폴더배팅 기회를 사용하였습니다.');
			return false;
		}	
		if (bingoSingle>0)
		{
			alert('빙고게임 동일회차의 \n조합배팅과 단폴더배팅을 동시에 사용할 수 없습니다.\n\n회원님은 이미 단폴더배팅 기회를 사용하였습니다.');
			return false;
		}				
	}
//*/

    //var myMoney  = val3 ;
    //var mxs_money  = 5000;      //최소베팅금액
    //var mxb_money  = 1000000;   //최대베팅금액
    //var mxbs_money = 500000;    //최대베팅금액
    //var exb_money  = 3000000;   //최대예상금액
    //var exbs_money = 1500000;   //최대예상금액

    var mxs_money  = <%=conlv_btt_min_money%>;   //최소베팅금액
    var mxb_money  = <%=conlv_btt_max_money%>;   //최대베팅금액
    var mxbs_money = <%=conlv_btt_max_money%>;   //최대베팅금액
    var exb_money  = <%=conlv_max_money%>;   //최대예상금액
    var exbs_money = 1500000;   //최대예상금액
    var mb_spoyn   = "<%=ConSet_mb_spoyn%>";
    //
    var cpuseyncnt = $(":checkbox[name='cpuseyn']:checked").length;
    var cpusenm    = form.cpusenm.value;
    if ((cpusenm=="cp_upmax")&&(cpuseyncnt==1)){
        exb_money  = exb_money + (exb_money/2);
        exbs_money = exb_money;
    }
    //
    if (js_gm_flag=="12" || js_gm_flag=="13"){
        mxb_money = mxbs_money;
        exb_money = exbs_money;
    }
    //
    if (val2==0){
        var input_val = 0;
        var text_val  = 0;
    }else{
        var input_val = (maxmoney / diviPer).toFixed(2) ;
        var text_val  = (input_val*diviPer).toFixed(2) ;
    }

    js_btt_money=$('#btt_money').val();
    js_btt_money=js_btt_money.replace(/,/g,"");

    ex_money = $("#seeMoney").text();
    ex_money = ex_money.replace(/,/g,"");   //실제예상배당금

	if(ex_money > exb_money){
		alert('최대 적중금상한 금액 '+ jsCommaChb(exb_money) +'원을 초과 하였습니다.1');
		return false;
	}

    var darigubun = form.darigubun.value
    if(mb_spoyn != "Y" && val4=="1"){
        if (darigubun=="Y"){
        }else{
            alert('단폴더 베팅은 불가합니다.\n2경기이상 선택하셔야 합니다.');
            return false;
        }
	}

	if(js_btt_money > mxb_money){
		alert('최대 베팅한도 금액 '+jsCommaChb(mxb_money)+'원을 초과하였습니다.');
		return false;
	}
	if(js_btt_money < mxs_money){
		alert('최소 베팅한도 금액은 '+jsCommaChb(mxs_money)+'원 입니다.');
		return false;
	}

	if(diviPer == '' || diviPer == '0' || diviPer == '0.00'){
		alert('베팅할 경기를 선택해 주십시요.');
		return false;
	}
	if(js_btt_money == '' || js_btt_money == '0'){
		alert('베팅금액을 입력해 주십시요.');
		$('#btt_money').focus();
		return false;
	}
	if(Number(val3) < Number(js_btt_money)){
		alert('보유머니가 부족합니다. 확인후 다시 베팅해주세요.');
		$('#btt_money').focus();
		return false;
	}

	if (window.confirm("★베팅내역을 확인 하시고 일치하시면 [확인] 버튼을 클릭해주세요. ★\n★한번 베팅하면 베팅취소는 불가능합니다.★")){
        js_paramval = "<%=gm_flag%>" + "^^" + val4 + "^^" + diviPer + "^^" + text_val

        $( "input[name='paramval']" ).val(js_paramval);

        $('#form').attr('action','bttProc.asp');
        $('#form').attr('method', 'get');
        $('#form').attr('target','bettifrm');
        $('#form').submit();
    }
}

function jsMsgFun(val1) {
    alert(val1);
}

function jsCommaChb(val1){
    aa = Number(val1).toLocaleString().split(".")[0];
    return aa;
}

function MoneyFormat(str){
	var re="";
	str = str + "";
//	str=str.replace(/-/gi,"");
	str=str.replace(/ /gi,"");

	str2=str.replace(/-/gi,"");
	str2=str2.replace(/,/gi,"");
	str2=str2.replace(/\./gi,"");

	if(isNaN(str2) && str!="-") return "";
	try
	{
		for(var i=0;i<str2.length;i++)
		{
			var c = str2.substring(str2.length-1-i,str2.length-i);
			re = c + re;
			if(i%3==2 && i<str2.length-1) re = "," + re;
		}

	}catch(e)
	{
		re="";
	}

	if(str.indexOf("-")==0)
	{
		re = "-" + re;
	}

	return re;
}

function trim(str)	{
		return str.replace(/(^\s*)|(\s*$)/g, "");
}

function cpchb(val1){
    aa = $(":checkbox[name='cpuseyn']:checked").length;
    if (aa=="1"){
        form.cpusenm.disabled=false;
    }else{
        form.cpusenm.disabled=true;
    }
    cpchangechb('');
}

function cpchangechb(val1){
    var old_per = form.tot_gct_ch_dividend_old.value;
    var new_per = form.tot_gct_ch_dividend.value;
    var btt_money = form.btt_money.value;
    if (btt_money==""){btt_money="0";}
    btt_money = btt_money.replace(/,/g,"");

    var cpuseyncnt = $(":checkbox[name='cpuseyn']:checked").length;
    var cpusenm    = form.cpusenm.value;
    if ((cpusenm=="cp_addper_3" || cpusenm=="cp_addper_5" || cpusenm=="cp_addper_10")&&(cpuseyncnt==1)){
        if (cpusenm=="cp_addper_3"){imsiper = 1.03;}
        if (cpusenm=="cp_addper_5"){imsiper = 1.05;}
        if (cpusenm=="cp_addper_10"){imsiper = 1.10;}
        see_per = Number(old_per)*imsiper;
        see_per = see_per.toFixed(2);
    }else{
        see_per = old_per;
    }
    //see_per = see_per.toFixed(2);
    btt_money = btt_money*see_per
    document.getElementById("seePer").innerHTML=see_per; //예상
    document.getElementById("seeMoney").innerHTML=jsCommaChb(btt_money); //예상
}

function load_right_banner2(aa) {
    $.ajax({
            type:"POST"
            , async:true
            , url:"/sports/ajax/cart_ajax_frm.asp"
            , dataType:"html"
            , timeout:30000
            , cache:false
            , data:"notaction="+aa
            , contentType:"application/x-www-form-urlencoded; charset=UTF-8"
            , error: function(request, status, error){
                    alert("code : " + request.status + "\r\nmessage : " + request.responseText);
                    //$("body").append(request.responseText);
                }
            , success: function(response, status, request){
                    result_html  = response;
          			var imsiArr  = result_html.split('&&^^&&');
			        var errorTxt = imsiArr[0];
			        var mainHtml = imsiArr[1];

                    //document.getElementById("right_banner").innerHTML=mainHtml;
                    $("#right_banner").html(mainHtml);
                }
            , beforeSend: function(){

                }
            , complete: function(){

                }
        });
}



$("document").ready(function() {

    var mobilechbymn = "<%=ConSet_MobileYn%>";
    //var mobilechbymn = "Y";

    //if (mobilechbymn=="N"){
        var currentPosition = parseInt($("#right_section").css("top"));
	       $(window).scroll(function() {
            var position = $(window).scrollTop(); // 현재 스크롤바의 위치값을 반환합니다.
            //alert("currentPosition : " +currentPosition + " Position : " + position);
            if (position<170){
                $("#right_section").stop().animate({"top":position+currentPosition+"px"},1000);
            }else{
                $("#right_section").stop().animate({"top":position+currentPosition-170+"px"},1000);
            }
        });
    //}

    load_right_banner();
});

function notactionchb(){
    aa = $(":checkbox[name='notaction']:checked").length;
    if (aa=="1"){
		mobilechbymn = "Y"
    }else{
		mobilechbymn = "N"
    }
	if (mobilechbymn=="N"){
        var currentPosition = parseInt($("#right_section").css("top"));
        $(window).scroll(function() {
			if (mobilechbymn=="N"){
				var position = $(window).scrollTop(); // 현재 스크롤바의 위치값을 반환합니다.
				$("#right_section").stop().animate({"top":position+0+"px"},1000);
			}
        });
    }else{
        var currentPosition = 0;
        $(window).scroll(function() {
            var position = $(window).scrollTop(); // 현재 스크롤바의 위치값을 반환합니다.
                //$("#right_section").stop().animate({"top":position+currentPosition+"px"},1000);
				$("#right_section").stop()
        });
	}

}

</script>

