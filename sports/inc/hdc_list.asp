<%
    '//
    gmdate  = left(ConSet_boardNowDate,8)
    gm_flag = rq("gm_flag")
    sdiflag = rq("sdiflag")
    '//
%>

<!-- #include file="js.asp" -->

<table border='0' cellspacing='0' cellpadding='0'>

<%
    if sdiflag = "" then
	    sql    = getListSql_crs("20","")
	elseif sdiflag = "1" then
	    sql    = getListSql_crs("20","KK")
	elseif sdiflag = "2" then
	    sql    = getListSql_crs("20","JJ")    
	elseif sdiflag = "3" then
	    sql    = getListSql_crs("20","dari")
	elseif sdiflag = "4" then
	    sql    = getListSql_crs("20","ald")	    
	elseif sdiflag = "5" then
	    sql    = getListSql_crs("20","bg")
	else
		sql    = getListSql_crs("20","")
	end if
	    
	Set Rs = Db.Execute(sql)

    i=1
    Do until rs.eof
        gm_idx     = rs("gm_idx")
        nation_img = rs("nation_img")
        jong_nm  = rs("SPOKIND_NM")
        league_nm  = rs("league_nm")
        gm_jcode = rs("gm_jcode")
        game_dt    = rs("game_dt")
        hteam_nm   = rs("hteam_nm")
        ateam_nm   = rs("ateam_nm")
        imsi_gm_flag    = rs("gm_flag")
        gm_hdvde   = formatnumber(rs("gm_hdvde"),2)

		If imsi_gm_flag="11" then
			gm_tdvde   = formatnumber(rs("gm_tdvde"),2)
		else
	        gm_tdvde   = rs("gm_tdvde")
		End If
		
        gm_advde   = formatnumber(rs("gm_advde"),2)
        state_nm   = rs("state_nm")
        spo_tie    = rs("spo_tie")
        gm_hndflag = rs("gm_hndflag")
        gm_vschb   = rs("gm_vschb")
        cart_gm_idx= rs("cart_gm_idx")
        cart_bttflag = rs("cart_bttflag")

        beforChb = "N"
        if CDBL(cart_gm_idx)=CDBL(gm_idx) then beforChb = "Y"

        stopflag = ""
        if nation_img<>"" then nation_img = "<img src='/fileupdown/icon/"& nation_img &"' class='s_league_img' />"

        class_nm = "s_bg_ing"        
        strTclass = "s_tit"
        
        if gm_jcode = "FOOT" then
	        stopflag = "_football"
	        strTclass = "s_tit_football"
	        class_nm = "s_bg_ing_football"
        elseif gm_jcode = "B1" then
	        stopflag = "_baseball"
	        strTclass = "s_tit_baseball"	
	        class_nm = "s_bg_ing_baseball"        
	    elseif gm_jcode = "sc" then
	    	stopflag = "_bonus"
	    	strTclass = "s_tit_bonus"
	    	class_nm = "s_bg_ing_bonus"
	    elseif gm_jcode = "BS" then
	    	stopflag = "_basketball"
	    	strTclass = "s_tit_basketball"	
	    	class_nm = "s_bg_ing_basketball"  
	    elseif gm_jcode = "VO" then
	    	stopflag = "_voball"
	    	strTclass = "s_tit_voball"	
	    	class_nm = "s_bg_ing_voball"  	    	  	
		end if        
        
        if league_nm<>old_league_nm then
            if i>1 then response.write "<tr><td height=5></td></tr>"
            response.write "    <tr>"
            response.write "        <th class='s_league_bg " & strTclass & "'>" & nation_img &" <div class='s_league_nm'>"& league_nm & "</div> <!--span class='h-text-grey'>{ "& jong_nm  & " }</span--> </th>"
            response.write "    </tr>"
            response.write "    <tr>"
            response.write "        <td height='5'></td>"
            response.write "    </tr>"
        end if
        '//
        hicon      = ""
        aicon      = ""
        betting_yn = ""
        '//

        if gm_hndflag="CH" then
            if gm_tdvde="0" or gm_tdvde="0.00" then
                titlemsg = gm_tdvde
            else
                titlemsg = "-" & gm_tdvde
            end if
        elseif gm_hndflag="CA" then
            if gm_tdvde="0" or gm_tdvde="0.00" then
                titlemsg = gm_tdvde
            else
                titlemsg = "+" & gm_tdvde
            end if
        elseif gm_hndflag="UO" then
            titlemsg = gm_tdvde
            'hicon = "[오버]<span class='s_up'>▲</span>"
            'aicon = "<span class='s_down'>▼</span>[언더]"
            hicon = "[오버] <img src='/images/sports/up.gif'>"
            aicon = "<img src='/images/sports/down.gif'> [언더]"
        elseif gm_hndflag="VS" then
            if gm_vschb="Y" then
                titlemsg = "VS"
            else
                if gm_tdvde>"0" then
                    titlemsg = formatnumber(gm_tdvde,2)
                    betting_yn = "Y"
                end if
            end if
        else
            if spo_tie<>"Y" then
                gm_tdvde="VS"
            else
                    betting_yn = "Y"
            end if
            '//
            titlemsg = gm_tdvde
        end if
        

%>

    <tr>
        <td>
	        
        	<div style="width:830" class="<%=class_nm%>">
                <div class="s_date" style="width:90px !important;float:left; padding-top:8px;"><%=game_dt%></div>
                <div class="s_home<%=stopflag%>" style="width:310px !important;float:left;">
	                <table id="homeTB_<%=gm_idx%>" width="310" border=0 cellspacing="0" cellpadding="0" onclick="clickChb('H','<%=gm_idx%>','<%=gm_hdvde%>','')" style="cursor:pointer;">
	                    <tr>
	                        <td width="270" class="s_home_name"><%=hteam_nm%><%=hicon%></td>
	                        <td width="40" id="homePO_<%=gm_idx%>" class="s_home_point"><%=gm_hdvde%></td>
	                    </tr>
	                </table>
                </div>

<% if betting_yn = "Y" then %>
                <div class="s_vs<%=stopflag%>" style="width:60px !important; margin-left:5px; float:left;">
	                <table id="tieTB_<%=gm_idx%>" width="60" border="0" cellspacing="0" cellpadding="0" onclick="clickChb('T','<%=gm_idx%>','<%=gm_tdvde%>','')" style="cursor:pointer;">
		                <tr>
	                        <td id="tiePO_<%=gm_idx%>" class="s_vs_point"><%=titlemsg%></td>
		                </tr>
	                </table>
                </div>
<% else %>
                <div class="s_vs_none" style="width:60px !important; margin-left:5px; float:left; padding-top:8px;"><%=titlemsg%></div>
<% end if %>
                <div class="s_away<%=stopflag%>" style="width:310px !important; margin-left:5px; float:left;">
	                <table id="awayTB_<%=gm_idx%>" width="300" border=0 cellspacing="0" cellpadding="0" onclick="clickChb('A','<%=gm_idx%>','<%=gm_advde%>','')" style="cursor:pointer;">
	                    <tr>
	                        <td width="40" id="awayPO_<%=gm_idx%>" class="s_away_point"><%=gm_advde%></td>
	                        <td width="270" class="s_away_name"><%=aicon%><%=ateam_nm%></td>
	                    </tr>
	                </table>
                </div>
                <div class="s_state_ing" style="width:40px !important; float:left; padding-top:8px;"><%=state_nm%></div>
			</div>
			
        </td>
    </tr>
    <tr>
        <td height="5"></td>
    </tr>

<script type="text/javascript">
/*
<%'    <%if cart_bttflag="H" then %>//$('#homeTB_<%=gm_idx%>').css("homeTeam list4");<%'end if%>%>
    <%if cart_bttflag="H" then %>$('#homeTB_<%=gm_idx%>').css("background-image", "url(/images/sports/home_ing_bg_on.gif)");<%end if%>
    <%if cart_bttflag="T" then %>$('#tieTB_<%=gm_idx%>').css("background-image", "url(/images/sports/tie_ing_bg_on.gif)");<%end if%>
    <%if cart_bttflag="A" then %>$('#awayTB_<%=gm_idx%>').css("background-image", "url(/images/sports/away_ing_bg_on.gif)");<%end if%>
*/
</script>


<%
    old_league_nm = league_nm
    rs.movenext
    loop
    rs.close
%>

<%
    '//
    old_league_nm = ""
    '//
    if gm_flag<>"13" then
        sql    = getListSql("50")
    else
        sql    = getListSql_sdi("50",sdiflag)
    end if

	Set Rs = Db.Execute(sql)
    '//
    i=1
    Do until rs.eof
        '//
        gm_idx     = rs("gm_idx")
        nation_img = rs("nation_img")
        league_nm  = rs("league_nm")
        gm_jcode	= rs("gm_jcode")
        game_dt    = rs("game_dt")
        hteam_nm   = rs("hteam_nm")
        ateam_nm   = rs("ateam_nm")
        gm_hdvde   = formatnumber(rs("gm_hdvde"),2)
        gm_tdvde   = rs("gm_tdvde")
        gm_advde   = formatnumber(rs("gm_advde"),2)
        state_nm   = rs("state_nm")
        gm_hndflag = rs("gm_hndflag")
        gm_vschb   = rs("gm_vschb")
        '//
        stopflag = "_stop"
        class_nm = "s_bg_stop"
        if nation_img<>"" then nation_img = "<img src='/fileupdown/icon/"& nation_img &"' width='20' height='15'>"
        if state_nm<>"" then
            stopflag = ""
'            class_nm = "s_bg_ing"
        else
            state_nm = "마감"
        end if
        
        strTclass = "s_tit_grey"
        '//
        if league_nm<>old_league_nm then
            if i>1 then response.write "<tr><td height=5></td></tr>"
            response.write "    <tr>"
            response.write "        <th class='s_league_bg " & strTclass & "'>"& nation_img &" "& league_nm &"</th>"
            response.write "    </tr>"
            response.write "    <tr>"
            response.write "        <td height='5'></td>"
            response.write "    </tr>"
        end if
        '//
        hicon = ""
        aicon = ""
        '//
        if gm_hndflag="CH" then
            titlemsg = "-" & gm_tdvde
        elseif gm_hndflag="CA" then
            titlemsg = "+" & gm_tdvde
        elseif gm_hndflag="UO" then
            titlemsg = gm_tdvde
            'hicon = "[오버]<span class='s_up'>▲</span>"
            'aicon = "<span class='s_down'>▼</span>[언더]"
            hicon = "[오버] <img src='/images/sports/up.gif'>"
            aicon = "<img src='/images/sports/down.gif'> [언더]"
        elseif gm_hndflag="VS" then
            if gm_vschb="Y" then
                titlemsg = "VS"
            else
                titlemsg = formatnumber(gm_tdvde,2)
            end if
        else
            if spo_tie<>"Y" then
                gm_tdvde="VS"
            else
                    betting_yn = "Y"
            end if
            '//
            titlemsg = gm_tdvde
        end if
'response.Write gm_vschb

%>

    <tr>
        <td>
        <table width="830" border="0" cellspacing="0" cellpadding="0" class="<%=class_nm%>">
            <tr>
                <td width="130" class="s_state_stop" ><%=game_dt%></td>
                <th width="300" class="s_home<%=stopflag%>">
                <table  width="300" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <th width="270" class="s_state_stop_l" align=left><%=hteam_nm%><%=hicon%></th>
                        <th width="30" class="s_home_point"><font color=#989898><%=gm_hdvde%></th>
                    </tr>
                </table>
                </th>
                <th width="60" class="s_vs<%=stopflag%>"><font color=#989898><%=titlemsg%></font></th>
                <th width="300" class="s_away<%=stopflag%>" >
                <table  width="300" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <th width="30" class="s_away_point"><font color=#989898><%=gm_advde%></th>
                        <th width="270" class="s_state_stop_r"><%=aicon%><%=ateam_nm%></th>
                    </tr>
                </table>
                </th>
                <th width="40" class="s_state_stop"><%=state_nm%></th>
            </tr>
        </table>
        </td>
    </tr>
    <tr>
        <td height="5"></td>
    </tr>

<%
    old_league_nm = league_nm
    rs.movenext
    loop
    rs.close
%>

</table>
