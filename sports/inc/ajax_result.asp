<%response.charset = "euc-kr"%>
<!--#include virtual="/db/db.asp" -->
<%
    '//
	SET RS = SERVER.CREATEOBJECT("ADODB.RECORDSET")
	SQL = "      SELECT TOP 10 "
	SQL = SQL & "       (SUBSTRING(A.GM_DATETIME,5,2) +'-'+ SUBSTRING(A.GM_DATETIME,7,2) +' '+ SUBSTRING(A.GM_DATETIME,9,2) +':'+ SUBSTRING(A.GM_DATETIME,11,2)) GMDATE , "
	SQL = SQL & " 	    B.SPO_NAME HNAME, A.RST_HTM_SCORE HNUM, A.RST_ATM_SCORE ANUM, C.SPO_NAME ANAME "
	SQL = SQL & "  FROM TB_SPO_GAME A, TB_SPO_KIND B, TB_SPO_KIND C "
	SQL = SQL & " WHERE A.GM_HTCODE = B.SPO_CODE "
	SQL = SQL & "   AND A.GM_ATCODE = C.SPO_CODE "
	SQL = SQL & "   AND A.GM_JCODE = 'vs'  "
	SQL = SQL & "   AND A.RST_HTM_SCORE<>'' "
	SQL = SQL & "   AND A.RST_HTM_SCORE IS NOT NULL "
	'SQL = SQL & "   AND A.RST_ATM_SCORE<>'' "
	'SQL = SQL & "   AND A.RST_ATM_SCORE IS NOT NULL "
	SQL = SQL & " ORDER BY A.GM_DATETIME DESC "
	RS.OPEN SQL, DB, 1
	'//
%>
		<table cellpadding="0" cellspacing="0" width="100%" border=0>
			<tr height="30">
				<td style="width:40px; color:#FFF; font:normal 13px 'arial'; text-align:center;">시간</td>
				<td style="width:110px; color:#FFF; font:normal 13px 'arial'; text-align:left;">홈</td>
				<td style="width:60px; color:#FFF; font:normal 13px 'arial'; text-align:center;">결과</td>
				<td style="width:130px; color:#FFF; font:normal 13px 'arial'; text-align:right;">원정</td>
			</tr>

<%
	Do Until rs.eof 
		gmdate = rs("gmdate")
		hname  = rs("hname")
		hnum   = rs("hnum")
		anum   = rs("anum")
		aname  = rs("aname")
		'//
		If hnum="" Or isnull(hnum) Then hnum=0
		If anum="" Or isnull(anum) Then anum=0
		'//
		hnum = CDbl(hnum)
		anum = CDbl(anum)
		If hnum>anum Then
			hcolor = "#FFF"
			acolor = "#666666"
		ElseIf hnum=anum Then
			acolor = "#FFF"
			hcolor = "#FFF"
		ElseIf hnum<anum Then
			acolor = "#FFF"
			hcolor = "#666666"
		End if
%>

			<tr height="28">
				<td style="color:#FFF; font:normal 13px 'arial'; text-align:center;"><%=gmdate%></td>
				<td style="color:<%=hcolor%>; font:normal 13px 'arial'; text-align:left;"><%=hname%></td>
				<td style="color:#FFFF66; font:normal 13px 'arial'; text-align:center;"><%=hnum%>:<%=anum%></td>
				<td style="color:<%=acolor%>; font:normal 13px 'arial'; text-align:right;"><%=aname%>&nbsp;</td>
			</tr>
			<tr><td height="4" colspan="4" style="border-top:#EBEBEB 1px solid;"></td></tr>

<%
	rs.movenext
	Loop
	rs.close
%>

		</table>    