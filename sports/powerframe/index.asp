<!DOCTYPE html>
<!--
top = 		{"round":"593486","ball":"01|24|12|18|06|06","powerball":"06","sum":"61","gameSecond":"216","endSecond":"156"}	
bottom =
			[{"round":"593486","sum":"61","ball":"01|24|12|18|06|06","powerball":"06"},
			{"round":"593485","sum":"90","ball":"19|21|13|28|09|01","powerball":"01"},
			{"round":"593484","sum":"86","ball":"13|27|22|09|15|04","powerball":"04"},
			{"round":"593483","sum":"77","ball":"22|24|17|10|04|02","powerball":"02"},
			{"round":"593482","sum":"87","ball":"28|07|22|04|26|05","powerball":"05"},
			{"round":"593481","sum":"89","ball":"26|27|01|10|25|00","powerball":"00"},
			{"round":"593480","sum":"70","ball":"21|23|22|03|01|07","powerball":"07"},
			{"round":"593479","sum":"72","ball":"14|10|19|17|12|03","powerball":"03"},
			{"round":"593478","sum":"55","ball":"05|07|23|18|02|06","powerball":"06"},
			{"round":"593477","sum":"63","ball":"09|06|23|04|21|08","powerball":"08"}]
top = 		{"round":"593486","ball":"01|24|12|18|06|06","powerball":"06","sum":"61","gameSecond":"89","endSecond":"29"}
-->
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<title>PowerBall</title>
<link rel="stylesheet" type="text/css" href="powerball.css">
<script language="javascript" type="text/javascript" src="jquery-1.8.3.min.js"></script>
<script language="javascript" type="text/javascript" src="jquery.comm.js"></script>
<script type="text/javascript">
<!--
function topLoadData()
{
	$(".top_loding_bar").find("span").html(" 데이터를 불러오는 중입니다.");
	$(".top_loding_bar").show().find("img").attr("src","loading.gif");

	$.ajax(
	{
		type:"GET",
		url:"/admin/sports/power/power_parseJson.asp",
		cache:false,
		success: function(str)
		{
			var json = JSON.parse(str);
			var strBall = json.ball.split('|');
			var nextRound = Number(json.round) +1; //다음회차
			
			$(".top_loding_bar").hide();

			$("#d_ballset1").attr("src","ball"+strBall[0]+".png");
			$("#d_ballset2").attr("src","ball"+strBall[1]+".png");
			$("#d_ballset3").attr("src","ball"+strBall[2]+".png");
			$("#d_ballset4").attr("src","ball"+strBall[3]+".png");
			$("#d_ballset5").attr("src","ball"+strBall[4]+".png");
			$("#d_ballset6").attr("src","power"+strBall[5]+".png");
			
			$("#gameSecond").val(json.gameSecond);
			$("#endSecond").val(json.endSecond);
			$("#nextRound").val(nextRound);
			//$("#gameSecond").val(15); //추첨 디버깅
			//$("#nextRound").val(json.round); // 추첨 디버깅
			$("#round").val(json.round);
			$("#nextRoundStr").html("제 " + nextRound + " 회");
			$("#roundStr").html("제 " + json.round + " 회 추첨번호");
			$(".topBox3").html(json.sum);

			autoLoadResult();
		},
		error: function(jqXHR, textStatus, errorThrown) {
			if (errorThrown == 'timeout') {
				$(".top_loding_bar").find("span").html(" 요청 시간을 초과하였습니다").addClass("error_msg");
				$(".top_loding_bar").show().find("img").attr("src","error.png");
			} else if (jqXHR.status == 404) {
				$(".top_loding_bar").find("span").html(" 요청하신 페이지를 찾을수 없습니다.").addClass("error_msg");
				$(".top_loding_bar").show().find("img").attr("src","error.png");
			} else if (jqXHR.status == 500) {
				
				alert(jqXHR.responseText)
				$(".top_loding_bar").find("span").html(" HTTP 500 내부서버오류 입니다.").addClass("error_msg");
				$(".top_loding_bar").show().find("img").attr("src","error.png");
			} else if (jqXHR.status === 0) {
				$(".top_loding_bar").find("span").html(" 인터넷 연결 상태를 확인해 주세요.").addClass("error_msg");
				$(".top_loding_bar").show().find("img").attr("src","error.png");
			} else {
				$(".top_loding_bar").find("span").html(" 일시적인장애로 데이터를 가져올 수 없습니다.").addClass("error_msg");
				$(".top_loding_bar").show().find("img").attr("src","error.png");
			}
		}
	});
}

function footerLoadData()
{
	$(".bottom_loding_bar").find("span").html(" 데이터를 불러오는 중입니다.");
	$(".bottom_loding_bar").show().find("img").attr("src","loading.gif");

	$.ajax(
	{
		type:"GET",
		url:"http://153.254.136.144:33355/game/powerball_json2/?mode=bottom",
		success: function(str)
		{
			var obj = JSON.parse(str);
			var roundTd = "";
			var sumTd ="";
			var powerTd = "";
			var sumColor = "";

			$(".bottom_loding_bar").hide();

			roundTd = "<td width='115'><b>회 차</b></td>";
			sumTd = "<td><b>합</b></td>";
			powerTd = "<td><b>파워볼</b></td>";		

			for(var i = obj.length-1; i >= 0; i--)
			{
				roundTd = roundTd + "<td width='64'>" + obj[i]["round"] + "</td>";

				//대중소 칼라볼
				if (obj[i]["sum"] >= 81){
					sumColor = "purple_ball";
				}else if(obj[i]["sum"] >= 65){
					sumColor = "orange_ball";
				}else{
					sumColor = "green_ball";
				}
				sumTd = sumTd + "<td><span class='"+sumColor+"'>" + obj[i]["sum"] + "</span></td>";

				// 홀짝 칼라볼
				if(obj[i]["powerball"] % 2 == 0){
					powerColor = "red_ball"
				}else{
					powerColor = "blue_ball"
				}
				powerTd = powerTd + "<td><span class='"+powerColor+"'>" + obj[i]["powerball"] + "</span></td>"
			}
		
			for (var x = 0; x < 10 - obj.length; x++ ){
				roundTd = roundTd + "<td width='64'>-</td>";
				sumTd = sumTd + "<td>-</td>";
				powerTd = powerTd + "<td>-</td>";
			}

			$(".txt2").html(roundTd);
			$(".txt3").html(sumTd);
			$(".txt4").html(powerTd);
		},
		error: function(jqXHR, textStatus, errorThrown) {
			if (errorThrown == 'timeout') {
				$(".bottom_loding_bar").find("span").html(" 요청 시간을 초과하였습니다").addClass("error_msg");
				$(".bottom_loding_bar").show().find("img").attr("src","error.png");
			} else if (jqXHR.status == 404) {
				$(".bottom_loding_bar").find("span").html(" 요청하신 페이지를 찾을수 없습니다.").addClass("error_msg");
				$(".bottom_loding_bar").show().find("img").attr("src","error.png");
			} else if (jqXHR.status == 500) {
				$(".bottom_loding_bar").find("span").html(" HTTP 500 내부서버오류 입니다.").addClass("error_msg");
				$(".bottom_loding_bar").show().find("img").attr("src","error.png");
			} else if (jqXHR.status === 0) {
				$(".bottom_loding_bar").find("span").html(" 인터넷 연결 상태를 확인해 주세요.").addClass("error_msg");
				$(".bottom_loding_bar").show().find("img").attr("src","error.png");
			} else {
				$(".bottom_loding_bar").find("span").html(" 일시적인장애로 데이터를 가져올 수 없습니다.").addClass("error_msg");
				$(".bottom_loding_bar").show().find("img").attr("src","error.png");
			}
		}
	});
}

function autoLoadResult()
{

	var t = setInterval(
		function()
		{		

			var gameSecond = Number($("#gameSecond").val()); //추첨시간
			var endSecond = Number($("#endSecond").val()); //베팅 마감시간

			if(gameSecond > 0)
			{	
				if(gameSecond < 60) {
					minute = "00";
					seconds = gameSecond % 60;
				}else {
					minute = parseInt((gameSecond % 3600) /60);
					seconds = gameSecond % 60;
				}

				minute = minute.toString();
				seconds = seconds.toString();

				if (minute <= 9) minute = "0" + minute; 
				if (seconds <= 9) seconds = "0" + seconds;

				if(endSecond > 0 ){
					$("#endtimer").html(endSecond +"초 후<br />" + $("#nextRound").val() + "회차 베팅 마감됩니다." ).show();
				}else {
					$("#endtimer").html($("#nextRound").val()+" 회차 베팅 마감되었습니다");
				}

				gameSecond--;
				endSecond--;
				$("#gameSecond").val(gameSecond);
				$("#endSecond").val(endSecond);

				$("#t1").attr("src",""+minute.charAt(0)+".png");
				$("#t2").attr("src",""+minute.charAt(1)+".png");
				$("#t3").attr("src",""+seconds.charAt(0)+".png");
				$("#t4").attr("src",""+seconds.charAt(1)+".png");
				
				// 추첨시간 카운트다운 10
				if(gameSecond <= 10)
				{
					$("#endtimer").hide();
					$("#count").show('fast');

					gameSecond = gameSecond + 1;
					gameSecond = gameSecond.toString();

					if (parseInt(gameSecond) <= 9) gameSecond = "0" + gameSecond;

					$("#count").attr("src","count"+gameSecond+".png");
				}

			} else {

				$("#t1").attr("src","0.png");
				$("#t2").attr("src","0.png");
				$("#t3").attr("src","0.png");
				$("#t4").attr("src","0.png");
				
				$("#count").hide();
				$("#transfer").attr("src","transfer.gif").show();

				$.ajax({
					type:"GET",
					url:"/admin/sports/power/power_parseJson.asp",
					success:function(str)
					{
						var json = JSON.parse(str)
						if( $("#nextRound").val() == json.round )							
						{
							clearInterval(t);

							$("#transfer").hide();
							$("#ready").css("background", "url(BallMove_01.gif)").show(); //일반볼 추첨 Move
							$("#moveBall").attr("src","BallMove_02.gif").show(); // 추첨 볼 Move

							var strBall = json.ball.split('|');
							$("#ballset1").attr("src","ball"+strBall[0]+".png");
							$("#ballset2").attr("src","ball"+strBall[1]+".png");
							$("#ballset3").attr("src","ball"+strBall[2]+".png");
							$("#ballset4").attr("src","ball"+strBall[3]+".png");
							$("#ballset5").attr("src","ball"+strBall[4]+".png");
							$("#ballset6").attr("src","power"+strBall[5]+".png");
							
							$("#ballpos1").fadeIn(2500).queue(function() {
								$("#ballpos2").fadeIn(1800).queue(function() {
									$("#ballpos3").fadeIn(1800).queue(function() {
										$("#ballpos4").fadeIn(1800).queue(function() {
											$("#ballpos5").fadeIn(1800).delay(3000).queue(function(){
												$("#ballpos1,#ballpos2,#ballpos3,#ballpos4,#ballpos5").hide();
												$("#ready").css("background", "url(ballmove_color.gif)").show(); //파워볼 추첨 Move
												
												$("#ballpos6").fadeIn(2500).delay(5000).queue(function() {
													$("#moveBall,#ballpos6").hide();
													$("#ready").css("background", "");
													$("#ballpos1,#ballpos2,#ballpos3,#ballpos4,#ballpos5,#ballpos6").clearQueue();
													topLoadData();
													footerLoadData();
												});
											});
										});
									});
								});
							});							
						} //End IF
					} //End function
				}); //End ajax
			} //End IF
		},1000
	);
}

$(document).ready(function(){
	topLoadData();
	footerLoadData();
});
-->
</script>

</head>
<body oncontextmenu="return false" onselectstart="return false" ondragstart="return false">
	<div id="wrap" style="background:url("warp_back.png");">
		<div id="header">
			<div class="top" style="background:url("top_back.png");">
				<div class="topBox1">
					<h1 id="nextRoundStr">제 591984 회</h1>
					<ul class="clock">
						<li><img src="0.png" id="t1"></li>
						<li><img src="4.png" id="t2"></li>
						<li><img src="colon.png" width="4" height="16"></li>
						<li><img src="2.png" id="t3"></li>
						<li><img src="4.png" id="t4"></li>
					</ul>
				</div>

				<div class="top_loding_bar" style="display: none;"><img src="loading.gif"><span> 데이터를 불러오는 중입니다.</span></div>
				<div class="bottom_loding_bar" style="display: none;"><img src="loading.gif"><span> 데이터를 불러오는 중입니다.</span></div>

				<div class="topBox2">
					<span id="roundStr">제 591983 회 추첨번호</span>
					<ul class="ball">
						<li><img id="d_ballset1" src="ball15.png"></li>
						<li><img id="d_ballset2" src="ball25.png"></li>
						<li><img id="d_ballset3" src="ball18.png"></li>
						<li><img id="d_ballset4" src="ball02.png"></li>
						<li><img id="d_ballset5" src="ball03.png"></li>
						<li><img id="d_ballset6" class="powerball" src="power01.png"></li>
					</ul>
				</div>

				<div class="topBox3">63</div>
				<div id="ballpos1"><img id="ballset1"></div>
				<div id="ballpos2"><img id="ballset2"></div>
				<div id="ballpos3"><img id="ballset3"></div>
				<div id="ballpos4"><img id="ballset4"></div>
				<div id="ballpos5"><img id="ballset5"></div>
				<div id="ballpos6"><img id="ballset6"></div>
			</div>
		</div>

		<div id="ready">
			 
			<img id="count">
			<img id="transfer">
			<img id="moveBall">
			<img id="moveBallBack">
		</div>

		<div id="footer" style="background:url('bottom_back.png');">      
			<table border="0" cellpadding="0" cellspacing="0">
				<tbody><tr>
					<td height="25"></td>
				</tr>
				<tr>
					<td colspan="11" class="txt1">
						최근 10 회차 추첨정보	
						<div class="topball_title">
							<span class="purple_ball ">대</span>
							<span class="orange_ball ">중</span>
							<span class="green_ball ">소</span>
							<span class="blue_ball ">홀</span>
							<span class="red_ball ">짝</span>
						</div>
					</td>
				</tr>
				<tr>
					<td height="12"></td>
				</tr>
				<tr class="txt2"><td width="115"><b>회 차</b></td><td width="64">591973</td><td width="64">591974</td><td width="64">591975</td><td width="64">591976</td><td width="64">591977</td><td width="64">591978</td><td width="64">591979</td><td width="64">591980</td><td width="64">591981</td><td width="64">591982</td></tr>
				<tr>
					<td height="12"></td>
				</tr>
				<tr class="txt3"><td><b>합</b></td><td><span class="green_ball">54</span></td><td><span class="orange_ball">73</span></td><td><span class="orange_ball">74</span></td><td><span class="purple_ball">98</span></td><td><span class="purple_ball">81</span></td><td><span class="purple_ball">102</span></td><td><span class="orange_ball">66</span></td><td><span class="orange_ball">71</span></td><td><span class="orange_ball">78</span></td><td><span class="purple_ball">82</span></td></tr>
				<tr>
					<td height="20"></td>
				</tr>
				<tr class="txt4"><td><b>파워볼</b></td><td><span class="blue_ball">05</span></td><td><span class="red_ball">08</span></td><td><span class="red_ball">06</span></td><td><span class="red_ball">02</span></td><td><span class="red_ball">06</span></td><td><span class="blue_ball">07</span></td><td><span class="blue_ball">09</span></td><td><span class="blue_ball">05</span></td><td><span class="blue_ball">07</span></td><td><span class="red_ball">08</span></td></tr>
			</tbody></table>

			<input type="hidden" id="gameSecond" value="263">
			<input type="hidden" id="endSecond" value="203">
			<input type="hidden" id="nextRound" value="591984">
			<input type="hidden" id="round" value="591983">
		</div>
	</div>

</body></html>