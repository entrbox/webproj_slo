<!--#include virtual="/include/sessionendF.asp" -->
<!--#include virtual="/db/db.asp" -->
<%
    FUNCTION getSameBttChb(val1,val2,val3)
        set frs = server.createobject("adodb.recordset")
        SQL = "       SELECT TOP 1 T1.GM_IDX, "
        SQL = SQL & "        ISNULL(SUM(T2.BTT_MONEY),0) BTT_MONEY_HAP,  "
        SQL = SQL & "        ISNULL(SUM(T2.BTT_DIVIDEND_MONEY),0) BTT_DIVIDEND_MONEY_HAP,         "
        SQL = SQL & "        ISNULL(COUNT(T2.GM_IDX),0) BTT_CNT_HAP, "
        SQL = SQL & "        CASE WHEN ISNULL(SUM(T2.BTT_DIVIDEND_MONEY),0)+"& VAL2 &" > 6000000 THEN '1' "
        SQL = SQL & "             WHEN ISNULL(SUM(T2.BTT_MONEY),0)+"& VAL3 &" > 3000000 THEN '2' "
        SQL = SQL & "             WHEN ISNULL(COUNT(T2.GM_IDX),0)+1 >5 THEN '3' "
        SQL = SQL & "             ELSE '9' "
        SQL = SQL & "        END BTT_CHB_GUBUN "
        SQL = SQL & " FROM TB_SPO_CART T1 "
        SQL = SQL & " LEFT OUTER JOIN ( "
        SQL = SQL & " 					SELECT A.USERID,B.GM_IDX,A.BTT_MONEY,A.BTT_DIVIDEND_MONEY,B.BTS_CHFLAG "
        SQL = SQL & " 					FROM TB_SPO_BETTING_MASTER A, TB_SPO_BETTING_INFO B "
        SQL = SQL & " 					WHERE A.BTT_IDX=B.BTT_IDX "
        SQL = SQL & " 					AND USERID='"& VAL1 &"' AND BTT_ACCFLAG='1' "
        SQL = SQL & " 				) T2 "
        SQL = SQL & " 				ON T1.GM_IDX=T2.GM_IDX  AND T1.GCT_BTFLAG=T2.BTS_CHFLAG "
        SQL = SQL & " WHERE T1.USERID = '"& VAL1 &"' "
        SQL = SQL & " GROUP BY T1.GM_IDX "
        SQL = SQL & " HAVING ISNULL(SUM(T2.BTT_DIVIDEND_MONEY),0)+"& VAL2 &" > 6000000  "
        SQL = SQL & "     OR ISNULL(SUM(T2.BTT_MONEY),0)+"& VAL3 &" > 3000000 "
        SQL = SQL & "     OR ISNULL(COUNT(T2.GM_IDX),0)+1 > 4 "
        frs.open sql, DB, 1, 1
        if not frs.eof then
            btt_money_hap          = frs("btt_money_hap")
            btt_dividend_money_hap = frs("btt_dividend_money_hap")
            btt_chb_gubun          = frs("btt_chb_gubun")
            if btt_chb_gubun="1" then Call getPopup("13","동일대상경기 축으로 최대 적중금상한 금액 6,000,000원을 초과 하였습니다.","")
            if btt_chb_gubun="2" then Call getPopup("13","동일대상경기 축으로 최대 베팅 금액 3,000,000원을 초과 하였습니다.","")
            if btt_chb_gubun="3" then Call getPopup("13","동일대상경기 축으로 4번이상 배팅이 불가합니다.","")
        end if
        frs.close
    END FUNCTION
    
    SUB chbDouble(val1,val2,val3)
        SET RS = SERVER.CREATEOBJECT("ADODB.RECORDSET")
        SQL = " SELECT ISNULL(SUM(A.CHCNT),0) CHCNT, ISNULL(SUM(A.SCHCNT),0) SCHCNT "
        SQL = SQL & "   FROM "
        SQL = SQL & " 		(  "
        SQL = SQL & " 		SELECT SUM(A.CHCNT) CHCNT, 0 SCHCNT "
        SQL = SQL & " 		  FROM "
        SQL = SQL & " 				( "
        SQL = SQL & " 				SELECT A.*, ISNULL(B.CHCNT,0) CHCNT "
        SQL = SQL & " 				  FROM vw_spo_cart A "
        SQL = SQL & " 					   LEFT OUTER JOIN ( "
        SQL = SQL & " 										SELECT COUNT(*) CHCNT, "
        SQL = SQL & " 											   A.GM_HTCODE, A.GM_ATCODE, B.USERID, A.GM_HNDFLAG "
        SQL = SQL & " 										  FROM vw_spo_betting_info A, tb_spo_betting_master B, tb_spo_KIND C "
        SQL = SQL & " 										 WHERE A.BTT_IDX=B.BTT_IDX "
        SQL = SQL & " 										   AND A.GM_JCODE=C.SPO_CODE AND B.BTT_ACCFLAG='1' "
        SQL = SQL & " 										   AND C.SPO_FLAG='J' "
        SQL = SQL & " 										   AND C.SPO_NAME IN ('야구', '배구', '스타') "
        SQL = SQL & " 										 GROUP BY A.GM_HTCODE, A.GM_ATCODE, B.USERID, A.GM_HNDFLAG "
        SQL = SQL & " 									   ) B "
        SQL = SQL & " 									ON A.USERID=B.USERID AND A.GM_HTCODE=B.GM_HTCODE AND A.GM_ATCODE=B.GM_ATCODE "
        SQL = SQL & " 				 WHERE A.SESSIONID            = '"& val1 &"' "
        SQL = SQL & " 				   AND A.USERID               = '"& val2 &"' "
        SQL = SQL & " 				   AND A.GM_FLAG              = '"& val3 &"' "
        SQL = SQL & "                  AND SUBSTRING(A.WDATE,1,8) = '"& MID(ConSet_boardNowDate,1,8) &"' "
        SQL = SQL & " 				) A "
        SQL = SQL & " 		UNION ALL "
        SQL = SQL & " 		SELECT 0, SUM(A.SCHCNT) SCHCNT "
        SQL = SQL & " 		  FROM "
        SQL = SQL & " 				( "
        SQL = SQL & " 				SELECT A.*, ISNULL(B.SCHCNT,0) SCHCNT "
        SQL = SQL & " 				  FROM vw_spo_cart A "
        SQL = SQL & " 					   LEFT OUTER JOIN ( "
        SQL = SQL & " 										SELECT COUNT(*) SCHCNT, "
        SQL = SQL & " 											   A.GM_HTCODE, A.GM_ATCODE, B.USERID, A.GM_HNDFLAG, A.BTS_CHFLAG "
        SQL = SQL & " 										  FROM vw_spo_betting_info A, tb_spo_betting_master B, tb_spo_KIND C "
        SQL = SQL & " 										 WHERE A.BTT_IDX=B.BTT_IDX "
        SQL = SQL & " 										   AND A.GM_JCODE=C.SPO_CODE AND B.BTT_ACCFLAG='1' "
        SQL = SQL & " 										   AND C.SPO_FLAG='J' "
        SQL = SQL & " 										   AND C.SPO_NAME IN ('야구', '배구', '스타')	"
        SQL = SQL & " 										 GROUP BY A.GM_HTCODE, A.GM_ATCODE, B.USERID, A.GM_HNDFLAG, A.BTS_CHFLAG "
        SQL = SQL & " 									   ) B "
        SQL = SQL & " 									ON A.USERID=B.USERID AND A.GM_HTCODE=B.GM_HTCODE AND A.GM_ATCODE=B.GM_ATCODE AND A.GCT_BTFLAG=B.BTS_CHFLAG "
        SQL = SQL & " 				 WHERE A.SESSIONID            = '"& val1 &"' "
        SQL = SQL & " 				   AND A.USERID               = '"& val2 &"' "
        SQL = SQL & " 				   AND A.GM_FLAG              = '"& val3 &"' "
        SQL = SQL & "                  AND SUBSTRING(A.WDATE,1,8) = '"& MID(ConSet_boardNowDate,1,8) &"' "
        SQL = SQL & " 				) A "
        SQL = SQL & " 		) A "
        rs.open sql, DB, 1, 1
        if not rs.eof then
            chcnt  = rs(0)
            schcnt = rs(1)
        else
            chcnt  = 0
            schcnt = 0
        end if
        rs.close

        if chcnt=2 and schcnt=1 then Call getPopup("13","한경기 축으로 다른경기 (승,패) 상관없이 당첨 조건입니다.\n단폴더 배팅 규정위반 다시 배팅하시기 바랍니다.","")
    END SUB    
%>

<%
    '//
    paramval  = replace(rq("paramval"),",","") '// 겜구분^^베팅건수^^누적배당^^배당금액
    btt_money = replace(rq("btt_money"),",","")
    '//
    tot_gct_ch_dividend     = round(rq("tot_gct_ch_dividend")-0.005,2)
    tot_gct_ch_dividend_old = rq("tot_gct_ch_dividend_old")
    cpuseyn = rq("cpuseyn")
    cpusenm = rq("cpusenm")
    '//
    sessionid = session.sessionid
    gm_userid = session("userid")
    wdate     = getDateTime(now())
    '//
    paramvalArr        = split(paramval,"^^")
    gm_flag            = trim(paramvalArr(0))
    btt_cnt            = trim(paramvalArr(1))
    btt_tot_dividend   = trim(paramvalarr(2))
    btt_dividend_money = trim(paramvalarr(3))
    '//
    if cdbl(btt_money)<0 then Call getPopup("13","베팅금액이 잘못되었습니다.","")
    if cdbl(ConSet_usermoney) < cdbl(btt_money) then Call getPopup("13","보유금액이 부족합니다.","")
    if cdbl(ConSet_usermoney) <= 0 then Call getPopup("13","보유금액이 부족합니다.","")
    if btt_money =0 or btt_money="" then Call getPopup("13","베팅금액이 잘못되었습니다.","")
    '//

    SET RS = SERVER.CREATEOBJECT("ADODB.RECORDSET")
	SQL = "       SELECT COUNT(GCT_IDX) OTHERCNT "
	SQL = SQL & "   FROM ( "
	SQL = SQL & "   		SELECT A.GCT_IDX,A.GM_IDX,A.GCT_CH_DIVIDEND CH_GMDVDE, "
	SQL = SQL & "   			   CASE WHEN A.GCT_BTFLAG='H' THEN B.GM_HDVDE "
	SQL = SQL & "   					WHEN A.GCT_BTFLAG='T' THEN B.GM_TDVDE "
	SQL = SQL & "   					WHEN A.GCT_BTFLAG='A' THEN B.GM_ADVDE "
	SQL = SQL & "   			   END OR_GMDVDE "
	SQL = SQL & "   		FROM TB_SPO_CART A "
	SQL = SQL & "   		INNER JOIN TB_SPO_GAME B ON A.GM_IDX=B.GM_IDX "
	SQL = SQL & "   		WHERE A.USERID = '"& gm_userid &"' "
	SQL = SQL & "   	 ) TT "
	SQL = SQL & "  WHERE TT.CH_GMDVDE<>TT.OR_GMDVDE "
    rs.open sql, DB, 1, 1
	err_othercnt = rs(0)
    rs.close
    if err_othercnt <> 0 then 
		'//
        SQL = "DELETE TB_SPO_CART WHERE USERID = '"& GM_USERID &"' "
        DB.EXECUTE SQL
		'//
		Call getPopup("35","배당율 변경으로 다시 베팅해 주시기 바랍니다.","/sports/sports_01.asp?gm_flag=11")
		Response.end
	End if
	'//
    GM_STATE_NOT_CNT = 0
    SET RS = SERVER.CREATEOBJECT("ADODB.RECORDSET")
    SQL =       " SELECT isnull(COUNT(B.GM_STATE),0) GM_STATE_NOT_CNT "
    SQL = SQL & "   FROM TB_SPO_CART A, TB_SPO_GAME B "
    SQL = SQL & "  WHERE A.GM_IDX=B.GM_IDX AND A.SESSIONID = '"& SESSIONID &"' AND A.USERID = '"& GM_USERID &"' AND B.GM_STATE<>'20' "
    rs.open sql, DB, 1, 1
    if not rs.eof then
        gm_state_not_cnt = rs(0)
    end if
    rs.close
    if gm_state_not_cnt>0 then
        '//
        SQL =       " DELETE TB_SPO_CART WHERE GCT_IDX IN (SELECT GCT_IDX "
        SQL = SQL & "   FROM TB_SPO_CART A, TB_SPO_GAME B "
        SQL = SQL & "  WHERE A.GM_IDX=B.GM_IDX AND A.SESSIONID = '"& SESSIONID &"' AND A.USERID = '"& GM_USERID &"' AND B.GM_STATE<>'20')  "
        DB.EXECUTE SQL
        '//
        Call getPopup("32","마감된 경기가 포함되어 있습니다.","")
        '//
    end if
    '//
    alone_cnt = 0
    SET RS = SERVER.CREATEOBJECT("ADODB.RECORDSET")
    SQL =       " SELECT COUNT(GCT_IDX) "
    SQL = SQL & "   FROM TB_SPO_CART "
    SQL = SQL & "  WHERE SESSIONID = '"& SESSIONID &"' AND USERID = '"& GM_USERID &"' "
    rs.open sql, DB, 1, 1
    if not rs.eof then
        alone_cnt = rs(0)
    end if
    rs.close
    '//
    if alone_cnt=1 and cdbl(btt_money)>500000 then call getPopup("32","단폴더 베팅은 최대 500,000원까지만 가능합니다.","")
    '//
    if cpuseyn="Y" and (cpusenm = "cp_addper_3" or cpusenm = "cp_addper_5" or cpusenm = "cp_addper_10") then
        '//
        btt_tot_dividend_cp   = tot_gct_ch_dividend_old
        btt_dividend_money_cp = fix(tot_gct_ch_dividend_old * btt_money)
        '//
        If cpusenm = "cp_addper_3" Then
            btt_tot_dividend = round(tot_gct_ch_dividend_old*1.03,2)
        elseIf cpusenm = "cp_addper_5" Then
            btt_tot_dividend = round(tot_gct_ch_dividend_old*1.05,2)
        elseIf cpusenm = "cp_addper_10" Then
            btt_tot_dividend = round(tot_gct_ch_dividend_old*1.10,2)
        End if
        btt_dividend_money = fix(btt_tot_dividend * btt_money)  '쿠폰사용
    else
        btt_tot_dividend_cp   = 0
        btt_dividend_money_cp = 0
        btt_tot_dividend      = tot_gct_ch_dividend_old
        btt_dividend_money    = fix(btt_tot_dividend * btt_money)  '쿠폰사용 안함
    end If
    '//
'response.Write btt_tot_dividend &" - "& btt_tot_dividend_cp &"<BR>"
'response.Write btt_dividend_money &" - "& btt_dividend_money_cp
'response.end
    '//
    max_same_money  = conlv_btt_max_money   '//최대베팅액
    max_sames_money = conlv_btt_max_money
    max_btt_money   = conlv_max_money       '//상한가
    max_btts_money  = 1500000
    slq_gm_flag     = "'10','11'"
    ss_btt_money    = 0
    ss_btt_dmoney   = 0
    ss_btt_cnt      = 0
    '//
    if cpuseyn="Y" and cpusenm = "cp_upmax" then
        max_btt_money  = max_btt_money + (max_btt_money/2)
        max_btts_money = max_btt_money
    end if
    '//
    if cpuseyn="Y" and (cpusenm = "cp_notSOmin_40" or cpusenm = "cp_notBAmin_40" or cpusenm = "cp_notBSmin_40") then
        if cpusenm = "cp_notSOmin_40" then
            gmjcode = getGmJcode("축구")
        elseif cpusenm = "cp_notBAmin_40" then
            gmjcode = getGmJcode("야구")
        elseif cpusenm = "cp_notBSmin_40" then
            gmjcode = getGmJcode("농구")
        end if
        '//
        if gmjcode="" then Call getPopup("13","동일종목이 아니면 쿠폰을 사용할수 없습니다.","")
        '//
        imsicnt = 1
        SET RS = SERVER.CREATEOBJECT("ADODB.RECORDSET")
        SQL =       " SELECT isnull(COUNT(A.GM_IDX),0) "
        SQL = SQL & "   FROM TB_SPO_CART A, TB_SPO_GAME B "
        SQL = SQL & "  WHERE A.GM_IDX=B.GM_IDX AND B.GM_JCODE <> '"& gmjcode &"' "
        rs.open sql, DB, 1, 1
        if not rs.eof then
            imsicnt = rs(0)
        end if
        '//
        if imsicnt<>0 then Call getPopup("13","동일종목이 아니면 쿠폰을 사용할수 없습니다.","")
        '//
    end if
    '//
    if gm_flag="12" or gm_flag="13" then
        slq_gm_flag   = "'12','13'"
        max_btt_money  = max_btts_money
        max_same_money = max_sames_money
    end if
    '//
    call getSameBttChb(gm_userid,btt_dividend_money,btt_money)
    '//
    if btt_cnt=2 then   '//2건베팅시 축베팅 검사
        call chbDouble(SESSIONID, GM_USERID, GM_FLAG)
    end if
    '//
    On Error Resume Next
	DB.BeginTrans
        '//
        if cpuseyn="Y" and cpusenm <> "" then
            SET RS = SERVER.CREATEOBJECT("ADODB.RECORDSET")
            SQL = "SELECT top 1 CP_IDX FROM TB_SPO_CP_USE WHERE UFLAG = 'Y' AND CPNM = '"& CPUSENM &"' "
            rs.Open sql, db, 1
            CP_IDX = RS(0)
            if not rs.eof then
                SQL = "UPDATE TB_SPO_CP_USE SET UFLAG = 'N', UWDATE = '"& WDATE &"' WHERE CP_IDX = "& RS(0) &" "
                DB.EXECUTE SQL
            else
                Call getPopup("13","사용할 쿠폰이 없습니다.","")
            end if
            rs.close
        end if
        if cp_idx="" or isnull(cp_idx) then cp_idx = 0
        '//
        SQL = "INSERT INTO TB_SPO_BETTING_MASTER ( GM_FLAG,USERID,BTT_CNT,BTT_MONEY,BTT_TOT_DIVIDEND,BTT_DIVIDEND_MONEY,WDATE, BTT_TOT_DIVIDEND_CP,BTT_DIVIDEND_MONEY_CP,CP_IDX) VALUES ( "
        SQL = SQL & " '"& GM_FLAG &"','"& GM_USERID &"',"& BTT_CNT &","& BTT_MONEY &","& BTT_TOT_DIVIDEND &","& BTT_DIVIDEND_MONEY &",'"& WDATE &"', '"& BTT_TOT_DIVIDEND_CP &"','"& BTT_DIVIDEND_MONEY_CP &"', "& CP_IDX &" ) "
        DB.EXECUTE SQL
        '//
        btt_idx     = getIDENT_CURRENT("TB_SPO_BETTING_MASTER")
        bts_chmoney = CDBL(BTT_MONEY/BTT_CNT)
        '//
        SET RS = SERVER.CREATEOBJECT("ADODB.RECORDSET")
        SQL =       " SELECT A.GM_IDX, A.GM_FLAG, B.GM_DATETIME, A.GCT_BTFLAG BTS_CHFLAG, A.GCT_CH_DIVIDEND BTS_DIVIDEND, B.GM_HDVDE, B.GM_TDVDE, B.GM_ADVDE, B.GM_HNDFLAG "
        SQL = SQL & "   FROM TB_SPO_CART A "
        SQL = SQL & "  INNER JOIN TB_SPO_GAME B ON A.GM_IDX=B.GM_IDX "
        SQL = SQL & "  WHERE A.SESSIONID = '"& SESSIONID &"' "
        SQL = SQL & "    AND A.USERID    = '"& GM_USERID &"' "
        'SQL = SQL & "    AND A.GM_FLAG   = '"& GM_FLAG &"' "
        SQL = SQL & "  ORDER BY A.WDATE ASC "
        rs.open sql, DB, 1, 1
        '//
        orcnt = 0
        Do UNTIL RS.EOF
            '//
            gm_idx       = rs(0)
            gm_flag      = rs(1)
            gm_datetime  = rs(2)
            bts_chflag   = rs(3)
            bts_dividend = rs(4)
            bts_hdvde    = rs(5)
            bts_tdvde    = rs(6)
            bts_advde    = rs(7)
			gm_hndflag   = rs(8)
            '//
            if bts_hdvde="" then bts_hdvde=0
            if bts_tdvde="" then bts_tdvde=0
            if bts_advde="" then bts_advde=0
            '//BTS_HDVDE, BTS_TDVDE, BTS_ADVDE
            SQL = "INSERT INTO TB_SPO_BETTING_INFO ( BTT_IDX, GM_IDX, GM_FLAG, GM_DATETIME, BTS_CHFLAG, BTS_DIVIDEND, BTS_CHMONEY, WDATE, BTS_HDVDE, BTS_TDVDE, BTS_ADVDE,GM_HNDFLAG ) VALUES ( "
            SQL = SQL & " "& BTT_IDX &", "& GM_IDX &", '"& GM_FLAG &"', '"& GM_DATETIME &"', '"& BTS_CHFLAG &"', "& BTS_DIVIDEND &", "& BTS_CHMONEY &", '"& WDATE &"', "
            SQL = SQL & " "& BTS_HDVDE &", "& BTS_TDVDE &", "& BTS_ADVDE &", '"& GM_HNDFLAG &"' ) "
            DB.EXECUTE SQL
            '//
            orcnt = orcnt+1
        RS.MOVENEXT
        Loop
        rs.close
        '//
        SQL = "DELETE TB_SPO_CART WHERE SESSIONID = '"& SESSIONID &"' AND USERID = '"& GM_USERID &"' "
        DB.EXECUTE SQL
        '//
        gmflag_nm = getGmflagRe(gm_flag)
        Call subMemUmoneyLog_spo(gm_userid,btt_money,"2","[" & gmflag_nm & "] - 게임구매 !!",btt_idx)		'아이디 / 사용포인트 / 가감구분(1+,2-) / 사용내역
        '//
	If Err.Number<>0 Then
		DB.RollBackTrans

		response.write "에러넘버:" & Err.Number & "<br>"
		response.write "에러설명:" & Err.Description & "<br>"
		response.write "에러소스:" & Err.Source & "<br>"
		response.write "에러파일3:" & Err.NativeError & "<br>"
		response.write "에러파일1:" & Err.HelpFile & "<br>"
		response.write "에러파일2:" & Err.HelpContext & "<br>"

		Call getPopup("13","예기치 않은 오류 관리자 문의!!","")
	Else
        if CDBL(orcnt)=CDBL(btt_cnt) then
            if CDBL(orcnt)=0 then
           		DB.RollBackTrans
        		Call getPopup("13","구매수량이 맞지 않습니다.","")
            else
                '//
                master_cnt = 0
                info_cnt   = 0
                '//
                SET RS = SERVER.CREATEOBJECT("ADODB.RECORDSET")
                SQL =       " SELECT isnull(COUNT(BTT_IDX),0), "
                SQL = SQL & "        isnull((SELECT COUNT(BTT_IDX) FROM TB_SPO_BETTING_INFO WHERE BTT_IDX = "& BTT_IDX &" ),0) "
                SQL = SQL & "   FROM TB_SPO_BETTING_MASTER  "
                SQL = SQL & "  WHERE BTT_IDX = "& BTT_IDX &" "
                rs.open sql, DB, 1, 1
                if not rs.eof then
                    master_cnt = rs(0)
                    info_cnt   = rs(1)
                end if
                rs.close
                '//
                if master_cnt=1 and info_cnt>0 then
                    DB.CommitTrans
                    Call getPopup("35","구매 성공했습니다.\n구매내역에서 확인하세요.","/mypage/mypage_sports.asp")
                else
                    DB.RollBackTrans
                    Call getPopup("13","구매수량 오류!!","")
                end if

            end if
        else
    		DB.RollBackTrans
'response.Write orcnt &" - "& btt_cnt
'response.end
    		Call getPopup("13","구매수량 오류!!","")
        end if
	End If
%>